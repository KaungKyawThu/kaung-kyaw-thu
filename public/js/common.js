$(document).on('click','.edit-data, .publish-content, .hide-content',function(){
    let action = $(this).attr('action');
    let index = $(this).parent().parent().closest('tr').find('.data-index').html();
    $(this).attr('href',action+"?index="+index);
});

$(document).on('click','.reply-btn',function(){
   let comment_id = $(this).attr('id');
   let index = $(this).parent().closest('tr').find('.data-index').html();
   $('#comment-reply-modal').find('form#comment-reply-form').attr('action','/comment/'+comment_id+'/reply');
   $('#comment-reply-modal').find('form#comment-reply-form input[name=current_index]').val(index);

   $('#comment-reply-modal').find('form#comment-reply-form textarea[name=reply_msg]').val(null);
   $('#comment-reply-modal').find('form#comment-reply-form textarea[name=reply_msg]').removeClass('is-invalid');
   $('#comment-reply-modal').find('form#comment-reply-form .invalid-feedback strong').html(null);

   $('#comment-reply-modal').modal('show');
});

$(document).on('click','.reply-comment',function(){
    var that = $(this);
    $.ajax({
        url :  $('#comment-reply-modal').find('form#comment-reply-form').attr('action'),
        method : "POST",
        data : {
            'reply_msg' : $('#comment-reply-modal').find('form#comment-reply-form textarea[name=reply_msg]').val(),
            'current_index' : $('#comment-reply-modal').find('form#comment-reply-form input[name=current_index]').val(),
        },
        headers : {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        dataType : 'json',
        success : function(data){
            console.log(data);
            window.location.href = '/content/'+data.content_id+'/comment?index='+data.index;
        },

        error: function(e){
            removeProcessing(that);
            console.log(e.responseJSON);
            if(e.responseJSON.reply_msg!=undefined){
                $('form#comment-reply-form').find('textarea[name=reply_msg]').addClass('is-invalid');
                $('form#comment-reply-form').find('textarea[name=reply_msg]').siblings().find('strong').html(e.responseJSON.reply_msg)
            }
        }
    });
});

$(document).on('click','.delete-data, .publish-data, .deactive-data, .active-data, .block-data, .unblock-data',function(){
    let action = $(this).attr('action');
    let index = $(this).parent().parent().parent().closest('tr').find('.data-index').html();

    let msg = '';
    let method = '';
    if($(this).hasClass('publish-data')){
       msg = "Are you sure you want to publish?";
       method = 'PUT';
    }else if($(this).hasClass('delete-data')){
         msg = "Are you sure you want to delete?";
         method = "DELETE";
    }else if($(this).hasClass('deactive-data')){
         msg = "Are you sure you want to deactivate ?";
         method = 'PUT';
    }else if($(this).hasClass('active-data')){
         msg = "Are you sure you want to activate ?";
         method = 'PUT';
    }else if($(this).hasClass('block-data')){
        msg = "Are you sure you want to block ?";
        method = 'PUT';
    }else if($(this).hasClass('unblock-data')){
        msg = "Are you sure you want to unblock ?";
        method = 'PUT';
    }

    $('#confirm-modal .confirm-message').html(msg);
    $('#confirm-modal .modal-footer #confirm-form').find('input[name=_method]').val(method);

    $('#confirm-modal .modal-footer #confirm-form').find('input[name=current_index]').val(index);
    $('#confirm-modal .modal-footer #confirm-form').attr('action',action);
    $('#confirm-modal').modal('show');
});

$(document).on('keyup','input,textarea',function(){
   $(this).removeClass('is-invalid');
   $(this).siblings().find('strong').html(null);
});

$(document).on('keyup','input[name="quiz_option[]"]',function(){
    $(this).parent().siblings().find('strong').html(null);
});

$(document).on('change','select',function(){
    $(this).removeClass('is-invalid');
    $(this).siblings().find('strong').html(null);
    $(this).parent().siblings().find('strong').html(null);
});

$(document).on('click','.save-btn',function(){
    $(this).addClass('disabled');
    $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> processing...')
});

$(document).on('click','.confirm-delete-button',function(){
    $(this).parent().submit();
});


$(document).on("change","input[type=file]",function(){
    readURL(this);
});

$(document).on("click",".remove-photo",function(){
    $(this).parent().parent().parent().find('input[type=file]').val(null);
    $(this).parent().parent().find("#upload-preview").attr('src','');
    $(this).parent().parent().css('display','none');
    $(this).parent().parent().parent().find('#virtual-img').val(null);
    $(this).parent().parent().parent().find('input[type=file]').css('display','block');
    $(this).parent().parent().parent().find('img.img_none').removeClass('img_none');
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        console.log(input.files[0]);
        if((input.files[0].size / 1024) > 2000){
           $(input).val(null);
           alert('The uploaded file is exceed 2MB. ');
           return false;
        }else{
            reader.onload = function (e) {
                $(input).parent().parent().find('#upload-preview').attr('src', e.target.result);
                $(input).parent().parent().find('.preview-row').css('display','');
                $(input).css('display','none');
                $(input).siblings().find('strong').html(null);
                if($(input).parent().parent().find('input[name="preview[]"')!=undefined){
                    $(input).parent().parent().find('input[name="preview[]"').val(e.target.result)
                }
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
}

function removeProcessing(that){
    that.removeClass('disabled');
    if(that.hasClass('reply-comment')) that.html('Reply');
    else that.html('Save');
}


function clearErrorMsg(){
    $(document).find('.invalid-feedback strong').html(null);
    $(document).find('form.data-form input').removeClass('is-invalid');
    $(document).find('form.data-form select').removeClass('is-invalid');
    $(document).find('form.data-form textarea').removeClass('is-invalid');

}

