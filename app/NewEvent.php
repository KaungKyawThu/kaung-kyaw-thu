<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewEvent extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'type','title','description','created_by','updated_by','deleted_by','schedule_date','hours','status'
    ];
    protected $guarded = [];
}
