<?php

namespace App;

use App\Course;
use App\CourseBook;
use App\CourseVideo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class CourseChapter extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'course_id','order','chapter_name','chapter_name_mm','status','created_by','updated_by','deleted_by','published_by','published_at','deactivated_at','deactivated_at'
    ];
    protected $guarded = [];

    public static function saveVideo($data,$chapter_id)
    {
        $result = true;

        for ($i=0; $i < count($data['video_title']) ; $i++) { 
            if($data['virtual_img'][$i]==null && isset($data['video_thumbnail'][$i])){
                $photo = Storage::disk('s3')->put('images', $data['video_thumbnail'][$i]);
                $video_thumbnail = Storage::disk('s3')->url($photo);
            }else{
                $video_thumbnail = $data['virtual_img'][$i];
            }

            $ans = CourseVideo::create([
                                 'course_chapter_id' => $chapter_id,
                                 'video_thumbnail' =>  $video_thumbnail,
                                 'course_video_link' => $data['course_video_link'][$i],
                                 'video_title' => $data['video_title'][$i],
                                 'video_duration' => $data['video_duration'][$i],
                  ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
        
    }

    public static function saveTopic($data,$chapter_id)
    {
        $result = true;

        for ($i=0; $i < count($data['content_id']) ; $i++) { 

            $ans = CourseBook::create([
                                 'course_chapter_id' => $chapter_id,
                                 'content_id' =>  $data['content_id'][$i],
                  ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
        
    }

    public function videos()
    {
        return $this->hasMany(CourseVideo::Class,'course_chapter_id');
    }

    public function topics()
    {
        return $this->hasMany(CourseBook::Class,'course_chapter_id');
    }

    public function course()
    {
        return $this->hasOne(Course::Class,'id','course_id');
    }
}
