<?php

namespace App;

use App\ContentQuiz;
use App\Quizchoiceitem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quiz extends Model
{
    use SoftDeletes;
    protected $fillable = [
                            'quiz_question','quiz_type','quiz_content_type','status','created_by','description','course_id',
                            'updated_by','deleted_by','deactivated_by','published_at','deactivated_at','published_by'
                        ];

    public function options()
    {
        return $this->hasMany(Quizchoiceitem::Class, 'quiz_id');
    }

    public function contents()
    {
        return $this->hasMany(ContentQuiz::Class,'quiz_id');
    }

    public static function saveOption($quiz_option,$corrects,$option_id,$quiz_id){
        $ans = true;
        for ($i=0; $i < count($quiz_option); $i++) {
            if($option_id[$i]==null)
                $option = Quizchoiceitem::create([
                                         'quiz_id' => $quiz_id,
                                         'quiz_option' => $quiz_option[$i],
                                         'is_correct' => $corrects[$i],
                                    ]);
            else $option = Quizchoiceitem::where('id',$option_id[$i])
                                           ->update([
                                                'quiz_option' => $quiz_option[$i],
                                                'is_correct' => $corrects[$i],
                                           ]);

            if(!$option){
                $ans = false;
                break;
            }
        }

        return $ans;
    }

    public static function saveContentQuiz($data,$quiz_id)
    {

        $result = true;
        for ($i=0; $i < count($data['content_id']) ; $i++) {
            $ans = ContentQuiz::create([
                                 'quiz_id' => $quiz_id,
                                 'content_id' => $data['content_id'][$i]
                  ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;

    }
}
