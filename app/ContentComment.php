<?php

namespace App;

use App\CommentReply;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentComment extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function replies()
    {
      return $this->hasMany(CommentReply::Class,'content_comment_id');
    }

    public function user()
    {
      return $this->hasOne(User::Class, 'id', 'user_id');
    }

}
