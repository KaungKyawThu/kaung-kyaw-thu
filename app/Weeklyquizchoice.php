<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Weeklyquizchoice extends Model
{
    use SoftDeletes;
    protected $fillable = ['weekly_quiz_id','quiz_option','is_correct'];
}
