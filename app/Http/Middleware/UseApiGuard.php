<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UseApiGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        auth()->setDefaultDriver('api');

        if (Auth::check()) {

            if(!$request->route()->parameter('user_id')){
                if (auth()->user()->status != config('constant.user_status.active')){
                    return response()->json(['success' => 'false', 'result' => 'မသင့်လျော်သောလုပ်ဆောင်ချက်ကြောင့် သင့်အကောင့်ကို ဆိုင်းငံ့ထားပါသည်။'], 401);
                }
                if ( auth()->user()->role_id != config('constant.user_role.user') )
                {
                    abort('401', 'Unauthorized');
                }
            }else {
                if(auth()->user()->status != config('constant.user_status.active')){
                    return response()->json(['success' => 'false', 'result' => 'မသင့်လျော်သောလုပ်ဆောင်ချက်ကြောင့် သင့်အကောင့်ကို ဆိုင်းငံ့ထားပါသည်။'], 401);
                }
                if (
                    ($request->route()->parameter('user_id') !=  auth()->user()->id) ||
                    (auth()->user()->role_id != config('constant.user_role.user'))
                ){
                    abort('401', 'Unauthorized');
                }
            }

            
        }

        return $next($request);
    }
}
