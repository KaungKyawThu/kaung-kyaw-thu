<?php

namespace App\Http\Middleware;

use Closure;

class XSS_testeditor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userInput = $request->all();
        array_walk_recursive($userInput, function (&$userInput) {
            $userInput = strip_tags($userInput,'<p></p>
                            <strong></strong>
                            <em></em>
                            <s></s>
                            <ul></ul>
                            <ol></ol>
                            <li></li>
                            <a></a>
                            <img>
                            <h1></h1>
                            <h2></h2>
                            <h3></h3>
                            <h4></h4>
                            <h5></h5>
                            <h6></h6>
                            <big></big>
                            <small></small>
                            <div></div>
                            <pre></pre>
                            <table></table>
                            <hr/>
                            <ins></ins>
                            <del></del>
                            <q></q>
                            <span></span>
                            <var></var>
                            <tt></tt>
                            <code></code>
                            <kbd></kbd>
                            <samp></samp>
                            <cite></cite>');
                        });

        $request->merge($userInput);
        return $next($request);
    }
}
