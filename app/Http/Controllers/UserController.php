<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Chat;
use App\User;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-view|user-delete|user-block', ['only' => ['index']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-view', ['only' => ['show']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->middleware('permission:user-block', ['only' => ['block']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
           $status = $request->status;
           $region = $request->region_id;
           $township = $request->township_id;
        //    dd($status,$region,$township);
           return Excel::download(new UsersExport($status,$region,$township), 'users.xlsx');


    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = new User();

            if($request->role_id!=null)
               $data = $data->where('role_id',$request->role_id);

            if ($request->user_status != null) {
                $data = $data->where('users.status', $request->user_status);
            }
            if($request->township !=null){
                $data = $data->where('users.township_id',$request->township);
            }
            if($request->region !=null){
                $data = $data->where('users.region_id',$request->region);
            }

            $data = $data->leftJoin('roles','roles.id','users.role_id')
                         ->leftJoin('regions','regions.id','users.region_id')
                         ->leftJoin('townships','townships.id','users.township_id')
                          ->latest()
                          ->get(['users.*','roles.name as role_name','regions.name as region_name','townships.name as township_name']);

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('user-edit') || auth()->user()->can('user-block') || auth()->user()->can('user-delete')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('user-edit') && $row->role_id !=4 )
                              $btn .= '<a class="dropdown-item text-blue edit-user edit-data" href="#" action="user/'.$row->id.'/edit"> Edit</a>';

                            if(auth()->user()->can('user-block') && $row->status==1 && $row->role_id==4)
                              $btn .= '<a class="dropdown-item text-blue block-data" href="#" action="user/'.$row->id.'/block"> Block</a>';

                            if(auth()->user()->can('user-block')&& $row->status==0  && $row->role_id==4)
                              $btn .= '<a class="dropdown-item text-blue unblock-data" href="#" action="user/'.$row->id.'/block"> Unblock</a>';

                            if(auth()->user()->can('user-view') && $row->role_id==4)
                              $btn .= '<a class="dropdown-item text-blue view-user view-data" href="user/'.$row->id.'"> View Detail</a>';

                            if(auth()->user()->can('user-delete')  && $row->role_id!=4)
                              $btn .= '<a class="dropdown-item text-blue edit-user delete-data" href="#" action="/user/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('status', function($row){
                        return "<span class='status_".config("web_constant.user_status.$row->status")."'>".config("web_constant.user_status.$row->status")."</span>";
                    })
                    ->addColumn('created_date', function($row){
                        return $row->created_at==null ? $row->created_at : date('d/m/Y',strtotime($row->created_at));
                    })
                    ->addColumn('gender', function($row){
                        return config("web_constant.gender.$row->gender_id");
                    })
                    ->rawColumns(['action','status','created_date'])
                    ->make(true);
        }

        return view('user.index',[
                   'content_header' => 'User Lists',
                   'create_permission' => 'user-create',
                   'route' => 'user',
                   'keyword' => 'user'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        return view('user.create',[
                    'user' => $user,
                    'content_header' => 'Create New User',
                    'route' => 'user',
                    'is_profile' => false
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string',Rule::unique('users')->whereNull('deleted_at'),'max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'role_id' => ['required'],
            'password' => $request->role_id == 5 ? 'nullable' : ['required', 'string', 'min:8', 'confirmed'],
            'phone_no' => $request->role_id == 5 ? 'nullable' : ['required', Rule::unique('users')->whereNull('deleted_at'), 'regex:/^[\-\d-]{8,12}$/'],
            'email' => $request->role_id == 5 ? 'nullable' : ['required', Rule::unique('users')->whereNull('deleted_at'),'regex:/[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}/'],
        ],
        [
           'role_id.required' => 'The user role field is required.',
        ]);

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();
             $data['status'] = 1;
             $data['nick_name'] = $request->name;
             $data['avator_img_id'] = '-1';

             if($request->password!=null)
                $data['password'] = Hash::make($data['password']);

             $user = User::create($data);

             $user->syncRoles([$request->role_id]);

             if(!$user){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'User was created successfully!']);
        }else{
            session(['error' => 'User can not create!']);
        }

        return redirect('user?index='.$request->current_index.'&type='.$request->role_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = User::where('users.id', $user->id)
                     ->leftJoin('regions','regions.id','users.region_id')
                     ->leftJoin('townships','townships.id','users.township_id')
                     ->select('users.*','regions.name as region_name','townships.name as tsp_name')
                     ->with([
                        'certificates',
                        'answer_quizzes' => function($q) {
                            $q->leftJoin('quizzes','quizzes.id','answered_quizzes.quiz_id')
                              ->select('answered_quizzes.*','quizzes.quiz_question','quizzes.quiz_content_type');
                        },
                        'security_answers' => function($q){
                            $q->leftJoin('security_questions','security_questions.id','user_security_answers.security_question_id')
                              ->leftJoin('security_choices','security_choices.id','user_security_answers.security_choice_id')
                              ->select('user_security_answers.*','security_questions.question','security_choices.security_ans');
                        }
                    ])
                    ->first();

        return view("user.show",[
            'user' => $user,
            'content_header' => 'User Detail',
            'route' => 'user',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,User $user)
    {

        $user->current_index = $request->index;
        return view('user.update',[
                    'user' => $user,
                    'content_header' => 'Update User',
                    'route' => 'user',
                    'is_profile' => false
                ]);
    }

    public function profile()
    {
        $user = auth()->user();
        return view('user.update',[
                    'user' => $user,
                    'content_header' => 'User Profile',
                    'route' => '/',
                    'is_profile' => true
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if($request->is_profile && auth()->id()!=$user->id)  abort('403','User does not have the right permissions.');

        $request->validate([
            'name' => ['required', 'string', Rule::unique('users')->whereNull('deleted_at')->ignore($user->id),'max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'role_id' => ['required'],
            'password' => $request->role_id == 5 ? 'nullable' : ['required', 'string', 'min:8', 'confirmed'],
            'phone_no' => ($request->role_id == 5) ? 'nullable' : ['required', Rule::unique('users')->whereNull('deleted_at')->ignore($user->id), 'regex:/^[\-\d-]{8,12}$/'],
            'email' => ($request->role_id == 5) ? 'nullable' : ['required', Rule::unique('users')->whereNull('deleted_at')->ignore($user->id), 'regex:/[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}/'],
        ],
        [
           'role_id.required' => 'The user role field is required.',
        ]);

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['nick_name'] = $request->name;
             if($request->password!=null){
                $data['password'] = Hash::make($data['password']);
             }else $data['password'] = $user->password;

             $ans = $user->update($data);
             $user->syncRoles([$request->role_id]);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($request->is_profile) $m = "Profile";
        else $m = "User";
        if($result){
            session(['success' => "$m was updated successfully!"]);
        }else{
            session(['error' => "$m can not update!"]);
        }

        if($request->is_profile) return redirect('profile');
        else return redirect('user?index='.$request->current_index.'&type='.$request->role_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,User $user)
    {
        $id = $user->role_id;
        $result = true;
        DB::beginTransaction();
        try {

             $user->deleted_by = Auth::id();
             $user->save();
             $user->delete();
             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "User was deleted successfully!"]);
        }else{
            session(['error' => "User can not delete!"]);
        }

        return redirect('user?index='.$request->current_index.'&type='.$id);
    }

    public function block(Request $request,$id)
    {
        $user = User::find($id);
        $result = true;
        DB::beginTransaction();
        try {
                if($user->status==0){
                    $msg = "unblocked";
                    $user->status=1;
                    $user->blocked_by = Auth::id();
                    $user->blocked_at = date('Y-m-d H:i:s');
                    $user->save();
                }elseif($user->status==1){
                    $msg = "blocked";
                    $user->status=0;
                    $user->blocked_by = Auth::id();
                    $user->blocked_at = date('Y-m-d H:i:s');
                    $user->save();
                }

                DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "User was $msg successfully!"]);
        }else{
            session(['error' => "User was not $msg!"]);
        }

        return redirect('/user?index='.$request->current_index.'&type=4');
    }

    public function read_chat($user_id)
    {
        $ans = Chat::where('user_id',$user_id)->update(['is_read' => 1]);

        return json_encode(['status' => $ans]);
    }
}
