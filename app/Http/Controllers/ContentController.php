<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use DataTables;
use App\Content;
use App\Keyword;
use App\ContentFaq;
use App\ContentQuiz;
use App\CommentReply;
use App\ContentDetail;
use App\ExploreKeyword;
use App\ContentCategory;
use App\ContentComment;
use App\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:content-list|content-create|content-edit|content-delete|content-publish|content-active|content-view|content-reply', ['only' => ['index']]);
        $this->middleware('permission:content-create', ['only' => ['create','store']]);
        $this->middleware('permission:content-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:content-view', ['only' => ['show']]);
        $this->middleware('permission:content-publish', ['only' => ['publish']]);
        $this->middleware('permission:content-active', ['only' => ['active']]);
        $this->middleware('permission:content-reply', ['only' => ['reply','comment','toReply']]);
        $this->middleware('permission:comment-delete', ['only' => ['deleteComment']]);
        $this->middleware('permission:content-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new Content();

            if($request->content_type!=null)
                $data = $data->where('content_type',$request->content_type);

            if($request->category_id!=null){
                $category_ids = ContentCategory::where('category_id',$request->category_id)->pluck('content_id')->toArray();
                $data = $data->whereIn('contents.id',$category_ids);
            }
            if($request->content_status!=null)
            $data = $data->where('contents.status',$request->content_status);



            $data = $data->with([
                            'categories' => function($q){
                                $q->leftJoin('categories','categories.id','content_categories.category_id')
                                  ->select('categories.name_en','content_categories.*');
                            }
                        ])
                        ->leftJoin('users as createdUsers','createdUsers.id','contents.created_by')
                        ->leftJoin('users as publishedUsers','publishedUsers.id','contents.published_by')
                        ->leftJoin('users as authors','authors.id','contents.author_id');
            $data = $data->latest()
                         ->get([
                                'contents.*',
                                'createdUsers.name as created_user_name',
                                'publishedUsers.name as published_user_name',
                                'authors.name as author_name'
                          ]);

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('content-edit') || auth()->user()->can('content-delete') || auth()->user()->can('content-publish') || auth()->user()->can('content-active')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('content-edit'))
                              $btn .= '<a class="dropdown-item text-blue edit-content edit-data" href="#" action="content/'.$row->id.'/edit"> Edit</a>';

                            if(auth()->user()->can('content-publish') && $row->status == "1")
                               $btn .= '<a class="dropdown-item text-blue edit-content publish-data" href="#" action="/content/'.$row->id.'/publish">Publish</a>';

                            if(auth()->user()->can('content-active') && $row->status == "3")
                                $btn .= '<a class="dropdown-item text-blue edit-content active-data" href="#" action="/content/'.$row->id.'/active">Activate</a>';

                            if(auth()->user()->can('content-active') && $row->status == "2")
                               $btn .= '<a class="dropdown-item text-blue edit-content deactive-data" href="#" action="/content/'.$row->id.'/active">Deactivate</a>';

                            if(auth()->user()->can('content-view'))
                               $btn .= '<a class="dropdown-item text-blue edit-content view-data" href="/content/'.$row->id.'">View Detail</a>';

                            if(auth()->user()->can('content-reply') && $row->content_type==2)
                               $btn .= '<a class="dropdown-item text-blue edit-content view-data" href="/content/'.$row->id.'/comment">Comment & Reply</a>';

                            if(auth()->user()->can('content-delete')&& $row->status == "1")
                               $btn .= '<a class="dropdown-item text-blue edit-content delete-data" href="#" action="/content/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('category', function($row){
                        return implode(', ',$row->categories->pluck('name_en')->toArray());
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->addColumn('status', function($row){
                        return "<span class='status_".config("web_constant.status.$row->status")."'>".config("web_constant.status.$row->status")."</span>";
                    })
                    ->rawColumns(['action','category','published_at','status'])
                    ->make(true);
        }

        return view('content.index',[
                   'content_header' => 'Content Lists',
                   'create_permission' => 'content-create',
                   'route' => 'content',
                   'keyword' => 'content'
               ]);
    }

    public function toReply(Request $request)
    {
        if ($request->ajax()) {

            $data = new Content();

            $data = $data->where(['contents.content_type' => 2,'is_new_comment' => 1]);

            if($request->category_id!=null){
                $category_ids = ContentCategory::where('category_id',$request->category_id)->pluck('content_id')->toArray();
                $data = $data->whereIn('contents.id',$category_ids);
            }

            $data = $data->with([
                            'categories' => function($q){
                                $q->leftJoin('categories','categories.id','content_categories.category_id')
                                  ->select('categories.name_en','content_categories.*');
                            }
                        ])
                        ->leftJoin('users as createdUsers','createdUsers.id','contents.created_by')
                        ->leftJoin('users as publishedUsers','publishedUsers.id','contents.published_by')
                        ->leftJoin('users as authors','authors.id','contents.author_id');
            $data = $data->get([
                                'contents.*',
                                'createdUsers.name as created_user_name',
                                'publishedUsers.name as published_user_name',
                                'authors.name as author_name'
                          ]);

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        return "<a href='content/".$row->id."/comment' class='btn btn-sm btn-info'>View</a>";
                    })
                    ->addColumn('category', function($row){
                        return implode(', ',$row->categories->pluck('name_en')->toArray());
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    // ->addColumn('status', function($row){
                    //     return "<span class='status_".config("web_constant.status.$row->status")."'>".config("web_constant.status.$row->status")."</span>";
                    // })
                    ->rawColumns(['action','category','published_at'])
                    ->make(true);
        }

        return view('content.to-reply',[
                   'content_header' => 'Comment & Reply',
                   'create_permission' => 'content-create',
                   'route' => 'to-reply',
                   'keyword' => 'content'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $content = new Content();
        return view('content.create',[
                    'content' => $content,
                    'content_header' => 'Create New Topic/Article',
                    'route' => 'content',
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has('category_id')){
            $request->merge([
                'category_id' => null,
            ]);
        }

        // if(!$request->has('keyword_id')){
        //     $request->merge([
        //         'keyword_id' => null,
        //     ]);
        // }

        if(!$request->has('detail_photo')){
            $photo = [];
            for ($i=0; $i < count($request->detail_description) ; $i++) {
                array_push($photo,null);
            }
            $request->merge([
                'detail_photo' => $photo
            ]);
        }
        $detail_desc = explode('@#$%^&*',$request->detail_desc);
        unset($detail_desc[count($detail_desc) - 1]);

        $request->merge([
            'detail_description' => $detail_desc,
        ]);

        $validator = Validator::make($request->all(),[
            'category_id' => 'required|array',
            // 'keyword_id' => 'required|array',
            'content_cover_photo' => 'required|mimes:jpg,jpeg,png',
            'content_title' =>  'required|max:255',
            'content_type' =>  'required',
            'author_id' =>  ($request->content_type==2) ?  'required' : 'nullable',
            // 'video_id' =>  'required',
            'read_time' =>  'numeric|nullable',
            'detail_photo.*' => 'nullable|mimes:jpg,jpeg,png',
            // 'detail_description.*' => 'required'
        ],[
            'detail_photo.*.required' => 'The photo field is required.' ,
            'detail_description.*.required' => 'The description field is required.'
        ]);

        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();
             $data['status'] = 1;
             $data['read_time'] = $request->read_time ?? 0;
             $data['author_id'] = $request->author_id=="" ? null : $request->author_id;
             $data['video_id'] = $request->video_id=="" ? null : $request->video_id;

             if(isset($request->content_cover_photo)){
                $photo = Storage::disk('s3')->put('images', $request->content_cover_photo);
                $data['content_cover_photo'] = Storage::disk('s3')->url($photo);
             }

             $content = Content::create($data);

             if(!$content){
                 $result = false;
                 DB::rollback();
             }

             if($result){
                if(!Content::saveDetail($request->all(),$content->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             if($result){
                if(!Content::saveCategory($request->all(),$content->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             if($result && $request->has('keyword_id')){
                if(!Keyword::saveExploreKeyword($request->keyword_id,$content->id,$request->content_type)){
                    $result = false;
                    DB::rollback();
                }
             }

            //  if($result && $request->content_type==1){
            //     if(!Content::saveFAQ($request->all(),$content->id)){
            //         $result = false;
            //         DB::rollback();
            //     }
            //  }

            //  if($result && $request->content_type==1){
            //     if(!Content::saveQuiz($request->all(),$content->id)){
            //         $result = false;
            //         DB::rollback();
            //     }
            //  }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($request->content_type==1) $t = 'Topic';
        else $t = 'Article';

        if($result){
            session(['success' => "$t was created successfully!"]);
        }else{
            session(['error' => "$t can not create!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'content_type' => $request->content_type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content)
    {
        $quiz = Content::where('contents.id',$content->id)
                      ->leftJoin('users as createdUsers','createdUsers.id','contents.created_by')
                      ->leftJoin('users as updatedUsers','updatedUsers.id','contents.updated_by')
                      ->leftJoin('users as publishedUsers','publishedUsers.id','contents.published_by')
                      ->leftJoin('users as deactivatedUsers','deactivatedUsers.id','contents.deactivated_by')
                      ->leftJoin('users as authors','authors.id','contents.author_id')
                      ->with([
                            'categories' => function($q){
                                $q->leftJoin('categories','categories.id','content_categories.category_id')
                                ->select('categories.name_en','content_categories.*');
                            },
                            'keywords' => function($q) use ($content){
                                $q->leftJoin('keywords','keywords.id','explore_keywords.keyword_id')
                                  ->where('explore_type',$content->content_type)
                                  ->select('keywords.keyword_name','explore_keywords.*');
                            },
                            'faqs' => function($q){
                                $q->leftJoin('faqs','faqs.id','content_faqs.faq_id')
                                ->select('faqs.faq_question','content_faqs.*');
                            },
                            'quizzes' => function($q){
                                $q->leftJoin('quizzes','quizzes.id','content_quizzes.quiz_id')
                                ->select('quizzes.quiz_question','content_quizzes.*');
                            },
                      ])
                      ->first([
                           'createdUsers.name as created_user_name',
                           'updatedUsers.name as updated_user_name',
                           'publishedUsers.name as published_user_name',
                           'deactivatedUsers.name as deactivated_user_name',
                           'authors.name as author_name',
                           'contents.*'
                      ]);

        return view('content.view',[
                    'content' => $quiz,
                    'content_header' => config("web_constant.content_types.$quiz->content_type").' Detail',
                    'route' => 'content',
               ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content,Request $request)
    {
        $content = Content::where('contents.id',$content->id)
                            ->first();
                        //    ->with([
                        //       'quizzes' => function($q){
                        //         $q->leftJoin('quizzes','quizzes.id','content_quizzes.quiz_id')
                        //           ->select('content_quizzes.*','quizzes.quiz_question');
                        //       },
                        //       'faqs' => function($q){
                        //         $q->leftJoin('faqs','faqs.id','content_faqs.faq_id')
                        //           ->select('content_faqs.*','faqs.faq_question');
                        //       }
                        //    ])

        $content->current_index = $request->index;
        return view('content.update',[
                    'content' => $content,
                    'content_header' => "Update ".config("web_constant.content_types.$content->content_type"),
                    'route' => 'content',
                    'is_update' => true
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Content $content)
    {
        if(!$request->has('content_cover_photo') && $request->virtual_img==null){
            $request->merge([
                'content_cover_photo' => null
            ]);
        }else{
            $request->merge([
                'content_cover_photo' => $request->virtual_img ?? $request->content_cover_photo
            ]);
        }

         if(!$request->has('category_id')){
            $request->merge([
                'category_id' => null,
            ]);
        }

        // if(!$request->has('keyword_id')){
        //     $request->merge([
        //         'keyword_id' => null,
        //     ]);
        // }

        $detail_desc = explode('@#$%^&*',$request->detail_desc);
        unset($detail_desc[count($detail_desc) - 1]);

        $request->merge([
            'detail_description' => $detail_desc,
        ]);

        $validator = Validator::make($request->all(),[
                        'category_id' => 'required|array',
                        // 'keyword_id' => 'required|array',
                        'content_cover_photo' => $request->virtual_img==null ? 'required|mimes:jpg,jpeg,png' : 'required',
                        'content_title' =>  'required|max:255',
                        'content_type' =>  'required',
                        'author_id' =>  ($request->content_type==2) ?  'required' : 'nullable',
                        // 'video_id' =>  'required',
                        'read_time' =>  'numeric|nullable',
                        'detail_photo.*' => 'nullable|mimes:jpg,jpeg,png',
                        // 'detail_description.*' => 'required'
                    ],[
                        'detail_photo.*.required' => 'The photo field is required.' ,
                        'detail_description.*.required' => 'The description field is required.'
                    ]);

        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['updated_by'] = Auth::id();
             $data['read_time'] = $request->read_time ?? 0;
             $data['author_id'] = $request->author_id=="" ? null : $request->author_id;
             $data['video_id'] = $request->video_id=="" ? null : $request->video_id;

             if($request->virtual_img==null){
                $photo = Storage::disk('s3')->put('images', $request->content_cover_photo);
                $data['content_cover_photo'] = Storage::disk('s3')->url($photo);
             }
             $ans = $content->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             if($result){
                ContentDetail::where('content_id',$content->id)->forceDelete();
                if(!Content::saveDetail($request->all(),$content->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             if($result){
                ContentCategory::where('content_id',$content->id)->forceDelete();
                if(!Content::saveCategory($request->all(),$content->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             if($result && $request->has('keyword_id')){
                ExploreKeyword::where(['explore_id' => $content->id, 'explore_type' => $content->content_type])->forceDelete();
                if(!Keyword::saveExploreKeyword($request->keyword_id,$content->id,$request->content_type)){
                    $result = false;
                    DB::rollback();
                }
             }

            //  ContentFaq::where('content_id',$content->id)->forceDelete();
            //  if($result && $request->content_type==1){
            //     if(!Content::saveFAQ($request->all(),$content->id)){
            //         $result = false;
            //         DB::rollback();
            //     }
            //  }

            //  ContentQuiz::where('content_id',$content->id)->forceDelete();
            //  if($result && $request->content_type==1){
            //     if(!Content::saveQuiz($request->all(),$content->id)){
            //         $result = false;
            //         DB::rollback();
            //     }
            //  }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($request->content_type==1) $t = 'Topic';
        else $t = 'Article';

        if($result){
            session(['success' => "$t was updated successfully!"]);
        }else{
            session(['error' => "$t can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'content_type' => $request->content_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content,Request $request)
    {
        if($content->content_type==1) $t = 'Topic';
        else $t = 'Article';

        $result = true;
        DB::beginTransaction();
        try {

            $ans = Content::where('id',$content->id)
                        ->update([
                            'deleted_by' => Auth::id(),
                        ]);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             if(!$content->delete()){
                $result = false;
                DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($ans){
            session(['success' => "$t was deleted successfully!"]);
        }else{
            session(['error' => "$t can not delete!"]);
        }

        return redirect('content?index='.$request->current_index."&content_type=".$content->content_type);
    }

    public function publish(Request $request,$id)
    {
        $content = Content::find($id);
        $ans = Content::where('id',$id)
                    ->update([
                        'status' => 2,
                        'published_by' => Auth::id(),
                        'published_at' => date('Y-m-d H:i:s')
                    ]);

        if($content->content_type==1) $t = 'Topic';
        else $t = 'Article';

        if($ans){
            session(['success' => "$t was published successfully!"]);
        }else{
            session(['error' => "$t can not publish!"]);
        }

        return redirect('content?index='.$request->current_index."&content_type=".$content->content_type);
    }

    public function active(Request $request,$id)
    {
        $content = Content::find($id);
        $status = null;

        if($content->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactivate";
        }elseif($content->status==3){
            $status = 2;
            $msg1 = "activated";
            $msg2 = "activate";
        }

        $ans = Content::where('id',$id)
                    ->update([
                        'status' => $status ,
                        'deactivated_by' => Auth::id(),
                        'deactivated_at' => date('Y-m-d H:i:s')
                    ]);

        if($content->content_type==1) $t = 'Topic';
        else $t = 'Article';

        if($ans){
            session(['success' => "$t was $msg1 successfully!"]);
        }else{
            session(['error' => "$t can not $msg2!"]);
        }

        return redirect('content?index='.$request->current_index."&content_type=".$content->content_type);
    }

    public function comment($content_id)
    {
        $content = Content::find($content_id);
        $comments = ContentComment::where('content_id',$content_id)
                                    ->leftJoin('users','users.id','content_comments.user_id')
                                    ->with([
                                        'replies' => function($q){
                                            $q->leftJoin('users','users.id','comment_replies.user_id')
                                               ->select('comment_replies.*','users.name as user_name');
                                        }
                                    ])
                                    ->latest()
                                    ->get([
                                        'users.name as user_name',
                                        'content_comments.*'
                                    ]);

        return view('content.comment',[
                        'comments' => $comments,
                        'content' => $content,
                        'content_header' => 'Article Comments',
                        'route' => 'content',
                    ]);
    }

    public function reply(Request $request,$comment_id)
    {
        $validator = Validator::make($request->all(),[
            'reply_msg' => 'required',
        ]);

        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            $result = CommentReply::create([
                'content_comment_id' => $comment_id,
                'reply_msg' => $request->reply_msg,
                'reply_type' => 1,
                'user_id' => Auth::id()
            ]);

            $cmt = ContentComment::find($comment_id);
            $content_id = $cmt->content_id;

            ContentComment::find($comment_id)->update(['need_reply' =>0]);
            $is_need_reply = ContentComment::where(['content_id' => $content_id,'need_reply' => 1])->count();
            if($is_need_reply==0){
                Content::where('id',$content_id)->update(['is_new_comment' => 0]);
            }

            $fcm_token = User::where('id',$cmt->user_id)->pluck('fcm_token')->toArray();
            PushNotification::pushAdriod($fcm_token, 'Comment Reply', $request->reply_msg, 4);

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "The comment was replied successfully!"]);
        }else{
            session(['error' => "The comment was not replied!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'content_id' => $content_id ]);
    }

    public function deleteComment(Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

            if($request->has('comment_id')){
                ContentComment::whereIn('id',$request->comment_id)->update(['deleted_by' => Auth::id()]);
                ContentComment::whereIn('id',$request->comment_id)->delete();

            }

            if($request->has('reply_id')){
                CommentReply::whereIn('id',$request->reply_id)->update(['deleted_by' => Auth::id()]);
                CommentReply::whereIn('id',$request->reply_id)->delete();;
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "The comment was deleted successfully!"]);
        }else{
            session(['error' => "The comment was not deleted!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'content_id' => $request->content_id ]);
    }

    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('upload')->storeAs('public/uploads', $filenametostore);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/uploads/'.$filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }
}
