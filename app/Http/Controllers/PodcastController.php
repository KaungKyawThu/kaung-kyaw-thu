<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use Illuminate\Validation\Rule;
use App\Podcast;
use App\PodcastSerie;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PodcastController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:podcast-list|podcast-create|podcast-edit|podcast-delete|podcast-publish|podcast-active', ['only' => ['index']]);
        $this->middleware('permission:podcast-create', ['only' => ['create','store']]);
        $this->middleware('permission:podcast-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:podcast-delete', ['only' => ['destroy']]);
        $this->middleware('permission:podcast-publish', ['only' => ['publish']]);
        $this->middleware('permission:podcast-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $data = new Podcast();

            if($request->id!=null){
            $data = $data->where('id',$request->id);}

            if($request->podcast_status != null){
                $data = $data->where('podcasts.status',$request->podcast_status);
             }

            if($request->podcast_series_id != null){
                $data = $data->where('podcasts.podcast_series_id',$request->podcast_series_id);
             }
             $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'podcasts.created_by')
                            ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'podcasts.published_by')
                            ->leftJoin('podcast_series', 'podcasts.podcast_series_id', '=', 'podcast_series.id')
                            // ->select('podcast_series.series_name')
                            // ->latest('podcasts.created_at')
                            ->get( ['podcasts.*',
                            'podcast_series.series_name',
                            'createdUsers.name as created_user_name',
                            'publishedUsers.name as published_user_name']);


            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('podcast-edit') || auth()->user()->can('podcast-delete') || auth()->user()->can('podcast-publish') || auth()->user()->can('podcast-active')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('podcast-edit'))
                              $btn .= '<a class="dropdown-item text-blue edit-podcast edit-data" href="#" action="podcast/'.$row->id.'/edit">Edit</a>';

                              if(auth()->user()->can('podcast-publish') && $row->status == "1")
                            $btn .= '<a class="dropdown-item text-blue edit-podcast publish-data" href="#" action="/podcast/'.$row->id.'/publish">Publish</a>';

                            if(auth()->user()->can('podcast-active') && $row->status == "3")
                            $btn .= '<a class="dropdown-item text-blue edit-podcast active-data" href="#" action="/podcast/'.$row->id.'/publish">Activate</a>';

                            if(auth()->user()->can('podcast-active') && $row->status == "2")
                            $btn .= '<a class="dropdown-item text-blue edit-podcast deactive-data" href="#" action="/podcast/'.$row->id.'/active">Deactivate</a>';

                            if(auth()->user()->can('podcast-delete')&& $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue edit-podcast delete-data" href="#" action="/podcast/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('status', function ($row) {
                        return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->addColumn('thumbnailz', function ($row) {
                        return "<a href='".$row->podcast_link."' target='_blank'><img class='td-img' src='" .$row->thumbnail. "'> </img></a>";
                    })
                    ->rawColumns(['thumbnailz','action','status','published_at'])
                    ->make(true);
        }

        return view('podcast.index',[
                   'content_header' => 'Podcast Lists',
                   'create_permission' => 'podcast-create',
                   'route' => 'podcast',
                   'keyword' => 'podcast'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $podcast = new Podcast();
        $series_podcast= PodcastSerie::where('status', 2)->get(['series_name','id']);

        return view('podcast.create',[
                    'podcast' => $podcast,
                    'series_podcast' => $series_podcast,
                    'content_header' => 'Create New Podcast',
                    'route' => 'podcast',
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'podcast_title' => ['required'],
            'thumbnail' => 'required|mimes:jpg,jpeg,png',
            'podcast_link' => ['required', function($atr,$value,$fail){
                if(strpos($value,'anchor.fm')===false){ 
                    $fail("The podcast link is invalid format.");
                }
            }],
            'podcast_share_link' => ['required', function($atr,$value,$fail){
                if(strpos($value,'anchor.fm')===false){ 
                    $fail("The podcast share link is invalid format.");
                }
            }],
            'podcast_series' => ['required'],
            'podcast_duration' => 'required|regex:/^[\d]{1,2}\:[\d]{1,2}$/',
        ],
        [
            'podcast_duration.date_format' => 'The podcast duration is invalid format.',
        ]);
        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;


        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['created_by'] = Auth::id();
            $data['podcast_series_id'] = $request->podcast_series;
            $data['status'] = 1;

            if(isset($request->thumbnail)){
                $photo = Storage::disk('s3')->put('images', $request->thumbnail);
                $data['thumbnail'] = Storage::disk('s3')->url($photo);
             }

            $video = Podcast::create($data);
            if (!$video) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'Podcast was created successfully!']);
        } else {
            session(['error' => 'Podcast can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }


    public function publish(Request $request,$id)
    {
        $status = Podcast::find($id);
        $status = $status->status;

        $result = true;
        DB::beginTransaction();
        try {

            $data['published_by'] = Auth::id();
            $data['published_at'] = date("Y-m-d H:i:s");
            $data['status'] = 2;
            $ans = Podcast::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Podcast was published successfully!"]);
        }else{
            session(['error' => "Podcast can not publish!"]);
        }

        return redirect('podcast?index='.$request->current_index);

    }

    public function active(Request $request,$id)
    {
        $podcast = Podcast::find($id);
        $status = null;

        if($podcast->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactive";
        }elseif($podcast->status==3){

            $status = 2;
            $msg1 = "activated";
            $msg2 = "active";
        }

        $result = true;
        DB::beginTransaction();
        try {

             $data['deactivated_by'] = Auth::id();
             $data['deactivated_at'] = date("Y-m-d H:i:s");
             $data['status'] = $status;
             $ans = Podcast::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Podcast was $msg1 successfully!"]);
        }else{
            session(['error' => "Podcast can not $msg2!"]);
        }

        return redirect('podcast?index='.$request->current_index);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Podcast $podcast,Request $request)
    {
        $podcast = Podcast::where('podcasts.id',$podcast->id)->first();
        $podcast->current_index = $request->index;

        $series_podcast= PodcastSerie::where('status', 2)->get(['series_name','id']);

        return view('podcast.update',[
                    'podcast' => $podcast,
                    'series_podcast' => $series_podcast,
                    'content_header' => 'Update Podcast',
                    'route' => 'podcast',
                    'is_update' => true
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Podcast $podcast,Request $request)
    {
        if(!$request->has('thumbnail') && $request->virtual_img == null){
            $request->merge([
                'thumbnail' => null
            ]);
        }else{
            $request->merge([
                'thumbnail' => $request->virtual_img ?? $request->thumbnail
            ]);
        }

        $validator = Validator::make($request->all(),[
            'podcast_title' => ['required'],
            'thumbnail' => $request->virtual_img==null ? 'required|mimes:jpg,jpeg,png' : 'required',
            'podcast_link' => ['required', function($atr,$value,$fail){
                if(strpos($value,'anchor.fm')===false){ 
                    $fail("The podcast link is invalid format.");
                }
            }],
            'podcast_share_link' => ['required', function($atr,$value,$fail){
                if(strpos($value,'anchor.fm')===false){ 
                    $fail("The podcast share link is invalid format.");
                }
            }],
            'podcast_series' => ['required'],
            'podcast_duration' => 'required|regex:/^[\d]{1,2}\:[\d]{1,2}$/',
        ],
        [
            'podcast_duration.date_format' => 'The podcast duration is invalid format.',
        ]);
        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['updated_by'] = Auth::id();
             $data['podcast_series_id'] = $request->podcast_series;

             if($request->virtual_img==null){
                $photo = Storage::disk('s3')->put('images', $request->thumbnail);
                $data['thumbnail'] = Storage::disk('s3')->url($photo);
             }
             $ans = $podcast->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Podcast was updated successfully!"]);
        }else{
            session(['error' => "Podcast can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Podcast $podcast,Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

             $podcast['deleted_by'] = Auth::id();
             $ans = Podcast::where('id',$podcast->id)
                        ->update([
                            'deleted_by' => Auth::id(),
                        ]);
             $podcast->delete();
             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Podcast was deleted successfully!"]);
        }else{
            session(['error' => "Podcast can not delete!"]);
        }

        return redirect('podcast?index='.$request->current_index);
    }
}
