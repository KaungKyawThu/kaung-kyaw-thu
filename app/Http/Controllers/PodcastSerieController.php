<?php

namespace App\Http\Controllers;

use App\PodcastSerie;
use Auth;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PodcastSerieController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:podcast-serie-list|podcast-serie-create|podcast-serie-edit|podcast-serie-delete|podcast-serie-publish|podcast-serie-active', ['only' => ['index']]);
        $this->middleware('permission:podcast-serie-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:podcast-serie-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:podcast-serie-delete', ['only' => ['destroy']]);
        $this->middleware('permission:podcast-serie-publish', ['only' => ['publish']]);
        $this->middleware('permission:podcast-serie-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new PodcastSerie();
            // $data = $data->withTrashed();
            if ($request->podcast_series_status != null) {
                $data = $data->where('podcast_series.status', $request->podcast_series_status);
            }
            $data = $data->leftJoin('users as createdUsers', 'createdUsers.id', 'podcast_series.created_by')
                ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'podcast_series.published_by')
                ->latest()
                ->get([
                'podcast_series.*',
                'createdUsers.name as created_user_name',
                'publishedUsers.name as published_user_name',
            ]);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if (auth()->user()->can('podcast-serie-edit') || auth()->user()->can('podcast-serie-delete') || auth()->user()->can('podcast-serie-publish') || auth()->user()->can('podcast-serie-active')) {
                        $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                        if (auth()->user()->can('podcast-serie-edit')) {
                            $btn .= '<a class="dropdown-item text-blue edit-podcast-serie edit-data" href="#" action="podcast-serie/' . $row->id . '/edit">Edit</a>';
                        }

                        if (auth()->user()->can('podcast-serie-publish') && $row->status == "1") {
                            $btn .= '<a class="dropdown-item text-blue edit-podcast-serie publish-data" href="#" action="/podcast-serie/' . $row->id . '/publish">Publish</a>';
                        }

                        if (auth()->user()->can('podcast-serie-active') && $row->status == "3") {
                            $btn .= '<a class="dropdown-item text-blue edit-podcast-serie active-data" href="#" action="/podcast-serie/' . $row->id . '/publish">Activate</a>';
                        }

                        if (auth()->user()->can('podcast-serie-active') && $row->status == "2") {
                            $btn .= '<a class="dropdown-item text-blue edit-podcast-serie deactive-data" href="#" action="/podcast-serie/' . $row->id . '/active">Deactivate</a>';
                        }

                        if (auth()->user()->can('podcast-serie-delete')&& $row->status == "1") {
                            $btn .= '<a class="dropdown-item text-blue edit-podcast-serie delete-data" href="#" action="/podcast-serie/' . $row->id . '">Delete</a>';
                        }

                        $btn .= '</div>';
                    }
                    return $btn;
                })
                ->addColumn('status', function ($row) {
                    return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                })
                ->addColumn('published_at', function($row){
                    return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                })
                ->addColumn('thumbnailz', function ($row) {
                    return "<img class='td-img' src='" . $row->thumbnail . "'> </img>";
                })
                ->rawColumns(['thumbnailz', 'action', 'status','published_at'])
                ->make(true);
        }

        return view('podcast-serie.index', [
            'content_header' => 'Podcast Series Lists',
            'create_permission' => 'podcast-serie-create',
            'route' => 'podcast-serie',
            'keyword' => 'series',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $podcast_serie = new PodcastSerie();
        return view('podcast-serie.create', [
            'podcast' => $podcast_serie,
            'content_header' => 'Create New Podcast Series',
            'route' => 'podcast-serie',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'series_name' => ['required'],
            'thumbnail' => 'required|mimes:jpg,jpeg,png',
        ]
        );
        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $result = true;

        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['created_by'] = Auth::id();
            $data['podcast_series_id'] = $request->podcast_series;
            $data['status'] = 1;

            if (isset($request->thumbnail)) {
                $photo = Storage::disk('s3')->put('images', $request->thumbnail);
                $data['thumbnail'] = Storage::disk('s3')->url($photo);
            }

            $video = PodcastSerie::create($data);
            if (!$video) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'Podcast Series was created successfully!']);
        } else {
            session(['error' => 'Podcast Series can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    public function publish(Request $request, $id)
    {
        $status = PodcastSerie::find($id);
        $status = $status->status;

        $result = true;
        DB::beginTransaction();
        try {

            $data['published_by'] = Auth::id();
            $data['published_at'] = date("Y-m-d H:i:s");
            $data['status'] = 2;
            $ans = PodcastSerie::where('id', $id)->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Podcast Series was published successfully!"]);
        } else {
            session(['error' => "Podcast Series can not publish!"]);
        }

        return redirect('podcast-serie?index=' . $request->current_index);

    }

    public function active(Request $request, $id)
    {
        $podcast = PodcastSerie::find($id);
        $status = null;

        if ($podcast->status == 2) {
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactive";
        } elseif ($podcast->status == 3) {

            $status = 2;
            $msg1 = "activated";
            $msg2 = "active";
        }

        $result = true;
        DB::beginTransaction();
        try {

            $data['deactivated_by'] = Auth::id();
            $data['deactivated_at'] = date("Y-m-d H:i:s");
            $data['status'] = $status;
            $ans = PodcastSerie::where('id', $id)->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Podcast Series was $msg1 successfully!"]);
        } else {
            session(['error' => "Podcast Series can not $msg2!"]);
        }

        return redirect('podcast-serie?index=' . $request->current_index);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PodcastSerie $podcast_serie, Request $request)
    {
        $podcast_serie = PodcastSerie::where('id', $podcast_serie->id)->first();
        $podcast_serie->current_index = $request->index;

        return view('podcast-serie.update', [
            'podcast' => $podcast_serie,
            'content_header' => 'Update Podcast Series',
            'route' => 'podcast-serie',
            'is_update' => true,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PodcastSerie $podcast_serie, Request $request)
    {
        if (!$request->has('thumbnail') && $request->virtual_img == null) {
            $request->merge([
                'thumbnail' => null,
            ]);
        } else {
            $request->merge([
                'thumbnail' => $request->virtual_img ?? $request->thumbnail,
            ]);
        }

        $validator = Validator::make($request->all(), [
            'series_name' => ['required'],
            'thumbnail' => $request->virtual_img == null ? 'required|mimes:jpg,jpeg,png' : 'required',
        ]
        );
        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['updated_by'] = Auth::id();

            if ($request->virtual_img == null) {
                $photo = Storage::disk('s3')->put('images', $request->thumbnail);
                $data['thumbnail'] = Storage::disk('s3')->url($photo);
            }
            $ans = $podcast_serie->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Podcast Series was updated successfully!"]);
        } else {
            session(['error' => "Podcast Series can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PodcastSerie $podcast_serie, Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

            $podcast_serie['deleted_by'] = Auth::id();
            $ans = PodcastSerie::where('id', $podcast_serie->id)
                ->update([
                    'deleted_by' => Auth::id(),
                ]);
            $podcast_serie->delete();
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Podcast Serie was deleted successfully!"]);
        } else {
            session(['error' => "Podcast Serie can not delete!"]);
        }

        return redirect('podcast-serie?index=' . $request->current_index);
    }
}
