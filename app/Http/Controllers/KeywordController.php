<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use App\Keyword;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class KeywordController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:keyword-list|keyword-create|keyword-edit|keyword-delete|keyword-publish|keyword-active', ['only' => ['index']]);
        $this->middleware('permission:keyword-create', ['only' => ['create','store']]);
        $this->middleware('permission:keyword-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:keyword-delete', ['only' => ['destroy']]);
        $this->middleware('permission:keyword-active', ['only' => ['publish']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
        $data = new Keyword();
        // $data = $data->withTrashed();

        if($request->id!=null)
        $data = $data->where('id',$request->id);


        $data = $data->leftJoin('users as createdUsers', 'createdUsers.id', 'keywords.created_by')
        ->latest()->get(['keywords.*','createdUsers.name as created_user_name']);

        return Datatables::of($data)
                ->addColumn('created_at', function($row){
                    return $row->created_at==null ? $row->created_at : date('d/m/Y',strtotime($row->created_at));
                })
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '';
                    if(auth()->user()->can('keyword-edit') || auth()->user()->can('keyword-delete') || auth()->user()->can('keyword-publish') || auth()->user()->can('keyword-active')){
                        $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                </a>
                                <div class="dropdown-menu action-list" aria-labelledby="actions">';
                        if(auth()->user()->can('keyword-edit'))
                          $btn .= '<a class="dropdown-item text-blue edit-keyword edit-data" href="#" action="keyword/'.$row->id.'/edit"> Edit</a>';

                        //   if(auth()->user()->can('keyword-publish') && $row->status == "1")
                        // $btn .= '<a class="dropdown-item text-blue edit-keyword publish-data" href="#" action="/keyword/'.$row->id.'/publish">Publish</a>';

                        // if(auth()->user()->can('keyword-active') && $row->status == "3")
                        // $btn .= '<a class="dropdown-item text-blue edit-keyword publish-data" href="#" action="/keyword/'.$row->id.'/publish">Activate</a>';

                        // if(auth()->user()->can('keyword-active') && $row->status == "2")
                        // $btn .= '<a class="dropdown-item text-blue edit-keyword deactive-data" href="#" action="/keyword/'.$row->id.'/deactivate">Deactivate</a>';

                        if(auth()->user()->can('keyword-delete'))
                          $btn .= '<a class="dropdown-item text-blue edit-keyword delete-data" href="#" action="/keyword/'.$row->id.'">Delete</a>';

                        $btn .=  '</div>';
                    }
                    return $btn;
                })
                ->addColumn('is_most_search', function ($row) {
                    if($row->is_most_search == 1){
                       return "<span class='')> Yes </span>";
                    }
                    return "<span class='')> No </span>";
               })
                ->rawColumns(['action','is_most_search'])
                ->make(true);
    }

    return view('keyword.index',[
               'content_header' => 'Keyword Lists',
               'create_permission' => 'keyword-create',
               'route' => 'keyword',
               'keyword' => 'keyword'
           ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $keyword = new Keyword();
        return view('keyword.create',[
                    'keyword' => $keyword,
                    'content_header' => 'Create New Keyword',
                    'route' => 'keyword',
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'is_most_search' => $request->is_most_search ?? 0
        ]);

        $request->validate([
            'keyword_name' => ['required', 'string', 'unique:keywords','max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],

        ]);

        if ($request->is_most_search == null){
            $request["is_most_search"] = 0;
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();
             $keyword = Keyword::create($data);
             if(!$keyword){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'Keyword was created successfully!']);
        }else{
            session(['error' => 'Keyword can not create!']);
        }

        return redirect('/keyword');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function show(Keyword $keyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function edit(Keyword $keyword,Request $request)
    {

        $keyword->current_index = $request->index;

        return view('keyword.update',[
                    'keyword' => $keyword,
                    'content_header' => 'Update keyword',
                    'route' => 'keyword',
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keyword $keyword)
    {

        $request->merge([
            'is_most_search' => $request->is_most_search ?? 0
        ]);

        $request->validate([
            'keyword_name' => ['required', 'string', Rule::unique('keywords')->ignore($keyword->id),'max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
        ]);


        if ($request->is_most_search == null){

            $request['is_most_search'] = 0;

        }



        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
            //  $data['updated_by'] = Auth::id();
             $ans = $keyword->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Keyword was updated successfully!"]);
        }else{
            session(['error' => "Keyword can not update!"]);
        }

        return redirect('keyword?index='.$request->current_index);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyword $keyword,Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

            //  $keyword->deleted_by = Auth::id();
             $keyword->delete();
             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Keyword was deleted successfully!"]);
        }else{
            session(['error' => "Keyword can not delete!"]);
        }

        return redirect('keyword?index='.$request->current_index);
    }
}
