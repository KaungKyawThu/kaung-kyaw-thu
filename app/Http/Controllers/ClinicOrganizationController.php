<?php

namespace App\Http\Controllers;

use App\ClinicOrganization;
use Auth;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClinicOrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:clinic-list|clinic-create|clinic-edit|clinic-delete|clinic-publish|clinic-active', ['only' => ['index']]);
        $this->middleware('permission:clinic-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:clinic-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:clinic-delete', ['only' => ['destroy']]);
        $this->middleware('permission:clinic-publish', ['only' => ['publish']]);
        $this->middleware('permission:clinic-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new ClinicOrganization();
            // $data = $data->withTrashed();

            if ($request->id != null) {
                $data = $data->where('id', $request->id);
            }

            if ($request->organization_status != null) {
                $data = $data->where('clinic_organizations.status', $request->organization_status);
            }
            $data = $data->leftJoin('users as createdUsers', 'createdUsers.id', 'clinic_organizations.created_by')
                         ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'clinic_organizations.published_by');

            $data = $data->latest()->get([
                'clinic_organizations.*',
                'createdUsers.name as created_user_name',
                'publishedUsers.name as published_user_name',

            ]);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if (auth()->user()->can('organization-edit') || auth()->user()->can('organization-delete') || auth()->user()->can('organization-publish') || auth()->user()->can('organization-active')) {
                        $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                        if (auth()->user()->can('organization-edit')) {
                            $btn .= '<a class="dropdown-item text-blue edit-organization edit-data" href="#" action="organization/' . $row->id . '/edit"> Edit</a>';
                        }

                        if (auth()->user()->can('organization-publish') && $row->status == "1") {
                            $btn .= '<a class="dropdown-item text-blue edit-organization publish-data" href="#" action="/organization/' . $row->id . '/publish">Publish</a>';
                        }

                        if (auth()->user()->can('organization-active') && $row->status == "3") {
                            $btn .= '<a class="dropdown-item text-blue edit-organization active-data" href="#" action="/organization/' . $row->id . '/publish">Activate</a>';
                        }

                        if (auth()->user()->can('organization-active') && $row->status == "2") {
                            $btn .= '<a class="dropdown-item text-blue edit-organization deactive-data" href="#" action="/organization/' . $row->id . '/active">Deactivate</a>';
                        }

                        if (auth()->user()->can('organization-delete')&& $row->status == "1") {
                            $btn .= '<a class="dropdown-item text-blue edit-organization delete-data" href="#" action="/organization/' . $row->id . '">Delete</a>';
                        }

                        $btn .= '</div>';
                    }
                    return $btn;
                })
                ->addColumn('status', function ($row) {
                    return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                })
                ->addColumn('published_at', function($row){
                    return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                })
                ->rawColumns(['action', 'status','published_at'])
                ->make(true);
        }

        return view('organization.index', [
            'content_header' => 'Clinic Organization Lists',
            'create_permission' => 'organization-create',
            'route' => 'organization',
            'keyword' => 'organization',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organization = new ClinicOrganization();
        return view('organization.create', [
            'organization' => $organization,
            'content_header' => 'Create New Clinic Organization',
            'route' => 'organization',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', Rule::unique('clinic_organizations')->whereNull('deleted_at'),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'organization_mm_name' => ['required', Rule::unique('clinic_organizations')->whereNull('deleted_at'),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],

        ]);

        $request['status'] = 1;

        $result = true;
        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['created_by'] = Auth::id();

            $organization = ClinicOrganization::create($data);

            if (!$organization) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'Clinic Organization was created successfully!']);
        } else {
            session(['error' => 'Clinic Organization can not create!']);
        }

        return redirect('/organization');
    }

    public function publish(Request $request, $id)
    {
        $status = ClinicOrganization::find($id);
        $status = $status->status;

        $result = true;
        DB::beginTransaction();
        try {

            $data['published_by'] = Auth::id();
            $data['published_at'] = date("Y-m-d H:i:s");
            $data['status'] = 2;
            $ans = ClinicOrganization::where('id', $id)->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Clinic Organization was published successfully!"]);
        } else {
            session(['error' => "Clinic Organization can not publish!"]);
        }

        return redirect('organization?index=' . $request->current_index);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClinicOrganization  $clinicOrganization
     * @return \Illuminate\Http\Response
     */
    public function show(ClinicOrganization $clinicOrganization)
    {

    }

    public function active(Request $request, $id)
    {
        $organization = ClinicOrganization::find($id);
        $status = null;

        if ($organization->status == 2) {
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactive";
        } elseif ($organization->status == 3) {

            $status = 2;
            $msg1 = "activated";
            $msg2 = "active";
        }

        $result = true;
        DB::beginTransaction();
        try {

            $data['deactivated_by'] = Auth::id();
            $data['deactivated_at'] = date("Y-m-d H:i:s");
            $data['status'] = $status;
            $ans = ClinicOrganization::where('id', $id)->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Clinic Organization was $msg1 successfully!"]);
        } else {
            session(['error' => "Clinic Organization can not $msg2!"]);
        }

        return redirect('organization?index=' . $request->current_index);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClinicOrganization  $clinicOrganization
     * @return \Illuminate\Http\Response
     */
    public function edit(ClinicOrganization $organization, Request $request)
    {
        $organization->current_index = $request->index;

        return view('organization.update', [
            'organization' => $organization,
            'content_header' => 'Update Organization',
            'route' => 'organization',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClinicOrganization  $clinicOrganization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClinicOrganization $organization)
    {
        $request->validate([
            'name' => ['required', Rule::unique('clinic_organizations')->whereNull('deleted_at')->ignore($organization->id),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'organization_mm_name' => ['required',Rule::unique('clinic_organizations')->whereNull('deleted_at')->ignore($organization->id),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
        ]);

        $result = true;
        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['updated_by'] = Auth::id();
            $ans = $organization->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Clinic Organization was updated successfully!"]);
        } else {
            session(['error' => "Clinic Organization can not update!"]);
        }

        return redirect('organization?index=' . $request->current_index);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClinicOrganization  $clinicOrganization
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClinicOrganization $organization, Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

            $organization['deleted_by'] = Auth::id();
            $ans = ClinicOrganization::where('id', $organization->id)
                ->update([
                    'deleted_by' => Auth::id(),
                ]);
            $organization->delete();
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Clinic Organization was deleted successfully!"]);
        } else {
            session(['error' => "Clinic Organization can not delete!"]);
        }

        return redirect('organization?index=' . $request->current_index);
    }
}
