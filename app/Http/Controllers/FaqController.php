<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Faq;
use DataTables;
use App\Keyword;
use App\ContentFaq;
use App\FaqCategory;
use App\ExploreKeyword;
use Illuminate\Http\Request;
use App\Http\Controllers\FaqController;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:faq-list|faq-create|faq-edit|faq-delete', ['only' => ['index']]);
        $this->middleware('permission:faq-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:faq-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:content-view', ['only' => ['show']]);
        $this->middleware('permission:faq-delete', ['only' => ['destroy']]);
        $this->middleware('permission:faq-publish', ['only' => ['publish']]);
        $this->middleware('permission:faq-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            if ($request->ajax()) {
                $data = new Faq();


                if ($request->category_id != null) {
                    $category_ids = FaqCategory::where('category_id', $request->category_id)->pluck('faq_id')->toArray();
                    $data = $data->whereIn('faqs.id', $category_ids);
                }

                if ($request->faq_status != null) {
                    $data = $data->where('faqs.status', $request->faq_status);
                }

                $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'faqs.created_by')
                              ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'faqs.published_by');

                              $data = $data->with([
                                'category' => function ($q) {
                                    $q->leftJoin('categories', 'categories.id', 'faq_categories.category_id')
                                        ->select('categories.name_en', 'faq_categories.*');
                                },
                            ]);
                $data = $data->latest()
                            ->get([
                                'faqs.*',
                                'createdUsers.name as created_user_name',
                                'publishedUsers.name as published_user_name',

                            ]);

                //     ->leftJoin('users as createdUsers', 'createdUsers.id', 'faqs.created_by')
                //     ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'faqs.published_by');

                // $data = $data->get([
                //     'contents.*',
                //     'createdUsers.name as created_user_name',
                //     'publishedUsers.name as published_user_name',

                // ]);



                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $btn = '';
                        if (auth()->user()->can('faq-edit') || auth()->user()->can('faq-delete')) {
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if (auth()->user()->can('faq-edit')) {
                                $btn .= '<a class="dropdown-item text-blue edit-faq edit-data" href="#" action="faq/' . $row->id . '/edit"> Edit</a>';
                             }

                            if (auth()->user()->can('faq-publish') && $row->status == 1) {
                                $btn .= '<a class="dropdown-item text-blue publish-data" href="#" action="faq/' . $row->id . '/publish"> Publish</a>';
                            }

                            if (auth()->user()->can('faq-active') && $row->status == 2) {
                                $btn .= '<a class="dropdown-item text-blue deactive-data" href="#" action="faq/' . $row->id . '/active">Deactive</a>';
                            }

                            if (auth()->user()->can('faq-active') && $row->status == 3) {
                                $btn .= '<a class="dropdown-item text-blue active-data" href="#" action="faq/' . $row->id . '/active">Activate</a>';
                            }

                            if (auth()->user()->can('faq-view')) {
                                $btn .= '<a class="dropdown-item text-blue  view-data" href="/faq/' . $row->id . '">View Detail</a>';
                            }

                            if (auth()->user()->can('faq-delete')&& $row->status == "1") {
                                $btn .= '<a class="dropdown-item text-blue edit-faq delete-data" href="#" action="/faq/' . $row->id . '">Delete</a>';
                            }

                            $btn .= '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('status', function ($row) {
                        return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->addColumn('category', function ($row) {

                        return implode(', ', $row->category->pluck('name_en')->toArray());
                    })
                    ->rawColumns(['action', 'status', 'category','published_at'])
                    ->make(true);

            }
            //code...
        } catch (\Throwable $th) {
            dd($th);
        }
        return view('FAQ.index', [
            'content_header' => 'FAQ Lists',
            'create_permission' => 'faq-create',
            'route' => 'faq',
            'keyword' => 'FAQ',
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $faq = new Faq();
        return view('FAQ.create', [
            'faq' => $faq,
            'content_header' => 'Create New FAQ',
            'route' => 'faq',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $len = isset($request->topic_id) ? count($request->topic_id) : 0;


        $validator = Validator::make($request->all(), [
            'faq_question.*' => ['required', 'string', 'max:255'],

            'faq_answer.*' => ['required'],

            'category_id' => ['required'],

        ], [
            'faq_question.*.required' => 'The FAQ Question field is required.',
            'faq_question.*.string' => 'The FAQ Question field must be string.',
            'faq_question.*.max' => 'The FAQ Question field must not exceed 255',
            'faq_answer.*.required' => 'The FAQ Answer field is required.',
            'faq_answer.*.string' => 'The FAQ Answer field  must be string.',
            'faq_answer.*.max' => 'The FAQ Answer field must not exceed 255.',
            'category_id.required' => 'The Category field is required',
        ]
        );

        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $result = true;

        DB::beginTransaction();
        try {

            for ($i = 0; $i < count($request->faq_question); $i++) {

                $data['faq_answer'] = $request->faq_answer[$i];
                $data['faq_question'] = $request->faq_question[$i];
                $data['created_by'] = Auth::id();
                $data['status'] = 1;
                $faq = Faq::create($data);

                if (!$faq) {
                    $result = false;
                    DB::rollback();
                    break;
                }
                if($request->has('keyword_id')){
                if (!Keyword::saveExploreKeyword($request->keyword_id, $faq->id, 5)) {

                    $result = false;
                    DB::rollback();
                }
            }
                for ($k = 0; $k < count($request->category_id); $k++) {
                    $category['category_id'] = $request->category_id[$k];
                    $category['faq_id'] = $faq->id;
                    $categor = FaqCategory::create($category);
                    if (!$categor) {
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
                if($request->has('topic_id')){
                for ($k = 0; $k < count($request->topic_id); $k++) {
                    $topic['content_id'] = $request->topic_id[$k];
                    $topic['faq_id'] = $faq->id;
                    $topi = ContentFaq::create($topic);
                    if (!$topi) {
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
            }
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'FAQ was created successfully!']);
        } else {
            session(['error' => 'FAQ can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //

        $faq = Faq::where('faqs.id', $faq->id)
            ->leftJoin('users as createdUsers', 'createdUsers.id', 'faqs.created_by')
            ->leftJoin('users as updatedUsers', 'updatedUsers.id', 'faqs.updated_by')
            ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'faqs.published_by')
            ->leftJoin('users as deactivatedUsers', 'deactivatedUsers.id', 'faqs.deactivated_by')

            ->with([
                'category' => function ($q) {
                    $q->leftJoin('categories', 'categories.id', 'faq_categories.category_id')
                        ->select('categories.name_en', 'faq_categories.*');
                },
                'keywords' => function ($q) use ($faq) {
                    $q->leftJoin('keywords', 'keywords.id', 'explore_keywords.keyword_id')
                        ->where('explore_type', 5)
                        ->select('keywords.keyword_name', 'explore_keywords.*');
                },
                'topics' => function($q){
                    $q->leftJoin('contents','contents.id','content_faqs.content_id')
                      ->select('content_faqs.*','contents.content_title');
                }


            ])
            ->first([
                'createdUsers.name as created_user_name',
                'updatedUsers.name as updated_user_name',
                'publishedUsers.name as published_user_name',
                'deactivatedUsers.name as deactivated_user_name',
                'faqs.*'

            ]);

        return view('FAQ.view', [
            'faq' => $faq,
            'content_header' => 'FAQ Detail',
            'route' => 'faq',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Faq $faq)
    {
        //

        $faq=Faq::where('id',$faq->id)
                  ->with([
                    'topics' => function($q){
                        $q->leftJoin('contents','contents.id','content_faqs.content_id')
                          ->select('content_faqs.*','contents.content_title');
                      }
                  ])
                  ->first();
        $faq->current_index = $request->index;
        return view('FAQ.update', [
            'faq' => $faq,
            'content_header' => 'Update FAQ',
            'route' => 'faq',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {

        $validator = Validator::make($request->all(), [
            'faq_question.*' => ['required', 'string', 'max:255'],

            'faq_answer.*' => ['required', 'string', 'max:255'],

            'category_id' => ['required'],

        ], [
            'faq_question.*.required' => 'The FAQ Question field is required.',
            'faq_question.*.string' => 'The FAQ Question field must be string.',
            'faq_question.*.max' => 'The FAQ Question field must not exceed 255',
            'faq_answer.*.required' => 'The FAQ Answer field is required.',
            'faq_answer.*.string' => 'The FAQ Answer field  must be string.',
            'faq_answer.*.max' => 'The FAQ Answer field must not exceed 255.',
            'category_id.required' => 'The Category field is required',
        ]
        );

        $result = true;
        DB::beginTransaction();

        if ($result) {
            FaqCategory::where(['faq_id' => $faq->id])->forceDelete();
            ContentFaq::where(['faq_id' => $faq->id])->forceDelete();
            ExploreKeyword::where(['explore_id' => $faq->id, 'explore_type' => 5])->forceDelete();
        }
        try {

            for ($i = 0; $i < count($request->faq_question); $i++) {

                $data['faq_answer'] = $request->faq_answer[$i];
                $data['faq_question'] = $request->faq_question[$i];
                $data['updated_by'] = Auth::id();
                $ans = $faq->update($data);

                if (!$ans) {
                    $result = false;
                    DB::rollback();
                    break;
                }
                if($request->has('keyword_id')){
                if (!Keyword::saveExploreKeyword($request->keyword_id, $faq->id, 5)) {
                    $result = false;
                    DB::rollback();
                 }
                }
                for ($k = 0; $k < count($request->category_id); $k++) {
                    $category['category_id'] = $request->category_id[$k];
                    $category['faq_id'] = $faq->id;
                    $updateCategory = FaqCategory::create($category);
                    if (!$updateCategory) {
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
                for ($k = 0; $k < count($request->topic_id); $k++) {
                    $topic['content_id'] = $request->topic_id[$k];
                    $topic['faq_id'] = $faq->id;
                    $updateTopic = ContentFaq::create($topic);
                    if (!$updateTopic) {
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Video was updated successfully!"]);
        } else {
            session(['error' => "Video can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Faq $faq)
    {

        $result = true;
        DB::beginTransaction();
        try {
            FaqCategory::where(['faq_id' => $faq->id])->delete();
            ExploreKeyword::where(['explore_id' => $faq->id, 'explore_type' => 5])->delete();
            ContentFaq::where(['faq_id' => $faq->id])->delete();
            $faq->deleted_by = Auth::id();
            $faq->save();
            $faq->delete();
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "FAQ was deleted successfully!"]);
        } else {
            session(['error' => "FAQ can not delete!"]);
        }

        return redirect('faq?index=' . $request->current_index);

    }
    public function publish(Request $request, $id)
    {

        $result = Faq::where('id', $id)->update(['status' => 2, 'published_by' => Auth::id(), 'published_at' => date('Y-m-d H:i:s')]);

        if ($result) {
            session(['success' => "FAQ was published successfully!"]);
        } else {
            session(['error' => "FAQ was not published!"]);
        }

        return redirect('/faq?index=' . $request->current_index);
    }

    public function active(Request $request, $id)
    {

        $faq = Faq::where('id', $id)->first();
        $result = true;
        DB::beginTransaction();
        try {
            if ($faq->status == 3) {
                $msg = "activated";

                $faq->status = 2;
                $faq->deactivated_by = Auth::id();
                $faq->deactivated_at = date('Y-m-d H:i:s');
                $faq->save();

            } elseif ($faq->status == 2) {
                $msg = "deactivated";
                $faq->status = 3;
                $faq->deactivated_by = Auth::id();
                $faq->deactivated_at = date('Y-m-d H:i:s');
                $faq->save();

            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "FAQ was $msg successfully!"]);
        } else {
            session(['error' => "FAQ was not $msg!"]);
        }

        return redirect('/faq?index=' . $request->current_index);
    }
}
