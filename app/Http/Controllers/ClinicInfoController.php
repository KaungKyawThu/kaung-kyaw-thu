<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use Illuminate\Validation\Rule;
use App\ClinicInfo;
use App\Township;
use App\ClinicOrganization;
use App\ClinicService;
use Illuminate\Http\Request;

class ClinicInfoController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:clinic-info-list|clinic-info-create|clinic-info-view|clinic-info-edit|clinic-info-delete|clinic-info-publish|clinic-info-active', ['only' => ['index']]);
        $this->middleware('permission:clinic-info-create', ['only' => ['create','store']]);
        $this->middleware('permission:clinic-info-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:clinic-info-view', ['only' => ['show']]);
        $this->middleware('permission:clinic-info-delete', ['only' => ['destroy']]);
        $this->middleware('permission:clinic-info-publish', ['only' => ['publish']]);
        $this->middleware('permission:clinic-info-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request);
        try{
            if ($request->ajax()) {

                $data = new ClinicInfo();
                // $data = $data->withTrashed();


                if($request->clinic_service_id != null){
                    $data = $data->where('clinic_infos.clinic_service_id',$request->clinic_service_id);
                 }

                 if($request->clinic_organization_id != null){
                    $data = $data->where('clinic_organizations.id',$request->clinic_organization_id);

                 }
                 if($request->region_id != null){
                    $data = $data->where('regions.id',$request->region_id);

                 }
                 if($request->township_id != null){
                    $data = $data->where('townships.id',$request->township_id);
                 }


                // if($request->quiz_content_type!=null)
                //    $data = $data->where('quiz_content_type',$request->quiz_content_type);


                if($request->id!=null)
                $data = $data->where('id',$request->id);

                if ($request->clinic_status != null) {
                    $data = $data->where('clinic_infos.status', $request->clinic_status);
                }
                $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'clinic_infos.created_by');
                // $data = $data ->get(['clinic_infos.*']);

                $data = $data
                ->leftJoin('clinic_services', 'clinic_infos.clinic_service_id', '=', 'clinic_services.id')
                ->leftJoin('clinic_organizations', 'clinic_infos.clinic_organization_id', '=', 'clinic_organizations.id')
                ->leftJoin('regions', 'clinic_infos.region_id', '=', 'regions.id')
                ->leftJoin('townships', 'clinic_infos.township_id', '=', 'townships.id')
                ->select('clinic_infos.*','clinic_services.service_name','townships.name as tsp_name','clinic_organizations.name','createdUsers.name as created_user_name',)
                ->latest()
                ->get();

                // ,'clinic_organizations.name','township_name.name as tsp_name'
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '';
                            if(auth()->user()->can('clinic-info-edit') || auth()->user()->can('clinic-info-delete') || auth()->user()->can('clinic-info-publish') || auth()->user()->can('clinic-info-active')){
                                $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        </a>
                                        <div class="dropdown-menu action-list" aria-labelledby="actions">';
                                if(auth()->user()->can('clinic-info-edit'))
                                  $btn .= '<a class="dropdown-item text-blue edit-clinic-info edit-data" href="#" action="clinic-info/'.$row->id.'/edit"> Edit</a>';

                                  if(auth()->user()->can('clinic-info-publish') && $row->status == "1")
                                $btn .= '<a class="dropdown-item text-blue edit-clinic-info publish-data" href="#" action="/clinic-info/'.$row->id.'/publish">Publish</a>';

                                if(auth()->user()->can('clinic-info-active') && $row->status == "3")
                                $btn .= '<a class="dropdown-item text-blue edit-clinic-info active-data" href="#" action="/clinic-info/'.$row->id.'/publish">Activate</a>';

                                if(auth()->user()->can('clinic-info-active') && $row->status == "2")
                                $btn .= '<a class="dropdown-item text-blue edit-clinic-info deactive-data" href="#" action="/clinic-info/'.$row->id.'/active">Deactivate</a>';

                                if(auth()->user()->can('clinic-info-view'))
                                  $btn .= '<a class="dropdown-item text-blue view-data" href="/clinic-info/'.$row->id.'">View Detail</a>';
                                if(auth()->user()->can('clinic-info-delete') && $row->status == "1")
                                  $btn .= '<a class="dropdown-item text-blue edit-clinic-info delete-data" href="#" action="/clinic-info/'.$row->id.'">Delete</a>';

                                $btn .=  '</div>';
                            }
                            return $btn;
                        })
                        ->addColumn('status', function ($row) {
                            return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                        })
                        ->rawColumns(['action','status'])
                        ->make(true);
        }}
        catch (Throwable $e) {
            report($e);

            return false;
        }




        return view('clinic-info.index',[
                   'content_header' => 'Clinic Lists',
                   'create_permission' => 'clinic-info-create',
                   'route' => 'clinic-info',
                   'keyword' => 'Clinic'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clinic_info = new ClinicInfo();
        $c_service = Clinicservice::where('status', 2)->get(['service_name','id','service_mm_name']);
        $c_organization = ClinicOrganization::where('status', 2)->get(['name','id','organization_mm_name']);
        $township = Township::get(['name','id','mm_unicode']);

        return view('clinic-info.create',[
                    'clinic_info' => $clinic_info,
                    'clinic_service' => $c_service,
                    'clinic_organization' => $c_organization,
                    'township' => $township,
                    'content_header' => 'Create New Clinic Information',
                    'route' => 'clinic-info',
                    'is_update' => false,
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has('service_name')){
            $request->merge([
                'service_name'  => null
            ]);
        }

        if(!$request->has('township_id')){
            $request->merge([
                'township_id'  => null
            ]);
        }

        $request->validate([
            'clinic_name' => ['required','max:255'],
            'clinic_mm_name' => ['required','max:255'],
            'service_name' => ['required','array'],
            'clinic_organization_id' => ['required'],
            'clinic_phone_no' =>'nullable|regex:/^[\-\d-]{8,12}$/',
            'clinic_mm_phone_no' =>'nullable',
            'township_id' => ['required', 'array'],
            'clinic_address' => ['nullable'],
            'clinic_mm_address' => ['nullable'],
            'clinic_description_en' => ['required'],
            'clinic_description_mm' => ['required'],
            'facebook_page_id' => 'nullable|digits:15'
        ]);

        $result = true;

        DB::beginTransaction();
        try {
            $data = $request->except('township_id','service_name');
            $data['created_by'] = Auth::id();
            $data['status'] = 1;

            for ($i=0; $i < count($request->township_id) ; $i++) {
                for ($j=0; $j < count($request->service_name); $j++) {
                   $data['township_id'] = $request->township_id[$i];
                   $data['clinic_service_id'] = $request->service_name[$j];

                   $video = ClinicInfo::create($data);
                   if (!$video) {
                       $result = false;
                       DB::rollback();
                       break;
                   }

                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            session(['error' => $e->getMessage()]);
        }

        if ($result) {
            session(['success' => 'Clinic Information was created successfully!']);
        } else {
            session(['error' => 'Clinic Information can not create!']);
        }

        return redirect('/clinic-info');
    }


    public function publish(Request $request,$id)
    {
        $status = ClinicInfo::find($id);
        $status = $status->status;

        $result = true;
        DB::beginTransaction();
        try {

         $data['published_by'] = Auth::id();
         $data['published_at'] = date("Y-m-d H:i:s");
             $data['status'] = 2;
             $ans = ClinicInfo::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Clinic Information was published successfully!"]);
        }else{
            session(['error' => "Clinic Information can not publish!"]);
        }

        return redirect('clinic-info?index='.$request->current_index);

    }

    public function active(Request $request,$id)
    {
        $clinic_info = ClinicInfo::find($id);
        $status = null;

        if($clinic_info->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactive";
        }elseif($clinic_info->status==3){

            $status = 2;
            $msg1 = "activated";
            $msg2 = "active";
        }

        $result = true;
        DB::beginTransaction();
        try {

             $data['deactivated_by'] = Auth::id();
             $data['deactivated_at'] = date("Y-m-d H:i:s");
             $data['status'] = $status;
             $data['region_id'] = $request->region_id=="" ? null : $request->region_id;
             $ans = ClinicInfo::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Clinic Information was $msg1 successfully!"]);
        }else{
            session(['error' => "Clinic Information can not $msg2!"]);
        }

        return redirect('clinic-info?index='.$request->current_index);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClinicInfo  $clinicInfo
     * @return \Illuminate\Http\Response
     */
    public function show(ClinicInfo $clinic_info)
    {
        $clinic_info = ClinicInfo::where('clinic_infos.id',$clinic_info->id)
                      ->leftJoin('users as createdUsers','createdUsers.id','clinic_infos.created_by')
                      ->leftJoin('users as updatedUsers','updatedUsers.id','clinic_infos.updated_by')
                      ->leftJoin('users as publishedUsers','publishedUsers.id','clinic_infos.published_by')
                      ->leftJoin('users as deactivatedUsers','deactivatedUsers.id','clinic_infos.deactivated_by')
                      ->leftJoin('clinic_services', 'clinic_services.id', '=', 'clinic_infos.clinic_service_id')
                      ->leftJoin('clinic_organizations', 'clinic_organizations.id', '=', 'clinic_infos.clinic_organization_id')
                      ->leftJoin('townships', 'townships.id', '=', 'clinic_infos.township_id')
                      ->first([
                           'createdUsers.name as created_user_name',
                           'updatedUsers.name as updated_user_name',
                           'publishedUsers.name as published_user_name',
                           'deactivatedUsers.name as deactivated_user_name',
                           'clinic_infos.*','townships.name','mm_unicode','clinic_services.service_name','clinic_services.service_mm_name','clinic_organizations.name as organization_name','organization_mm_name',
                      ]);
// dd($clinic_info);
        return view('clinic-info.view',[
                    'clinic_info' => $clinic_info,
                    'content_header' => 'Clinic Information Detail',
                    'route' => 'clinic-info',
               ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClinicInfo  $clinicInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(ClinicInfo $clinic_info,Request $request)
    {

        $clinic_info->current_index = $request->index;
        $c_service = Clinicservice::where('status', 2)->get(['service_name','id','service_mm_name']);
        $c_organization = ClinicOrganization::where('status', 2)->get(['name','id','organization_mm_name']);
        $township = $clinic_info->region_id==null ? Township::get(['name','id','mm_unicode']) :  Township::where('region_id',$clinic_info->region_id)->get(['name','id','mm_unicode']);

        return view('clinic-info.update',[
                    'clinic_info' => $clinic_info,
                    'clinic_service' => $c_service,
                    'clinic_organization' => $c_organization,
                    'township' => $township,
                    'content_header' => 'Update Clinic Information',
                    'route' => 'clinic-info',
                    'is_update' => true,
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClinicInfo  $clinicInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClinicInfo $clinic_info)
    {

        $request->validate([
            'clinic_name' => ['required','max:255'],
            'clinic_mm_name' => ['required','max:255'],
            'service_name' => ['required'],
            'clinic_organization_id' => ['required'],
            'clinic_phone_no' =>'nullable|regex:/^[\-\d-]{8,12}$/',
            'clinic_mm_phone_no' =>'nullable',
            'township_id' => ['required'],
            'clinic_address' => ['nullable'],
            'clinic_mm_address' => ['nullable'],
            'clinic_description_en' => ['required'],
            'clinic_description_mm' => ['required'],
            'facebook_page_id' => 'nullable|digits:15'
        ]);


        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['updated_by'] = Auth::id();
             $data['clinic_service_id'] = $request->service_name;
             $data['region_id'] = $request->region_id=="" ? null : $request->region_id;

             $ans = $clinic_info->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Clinic Information was updated successfully!"]);
        }else{
            session(['error' => "Clinic Information can not update!"]);
        }

        return redirect('clinic-info?index='.$request->current_index);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClinicInfo  $clinicInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClinicInfo $clinic_info,Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

             $clinic_info['deleted_by'] = Auth::id();
             $ans = ClinicInfo::where('id',$clinic_info->id)
                        ->update([
                            'deleted_by' => Auth::id(),
                        ]);
             $clinic_info->delete();
             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Clinic Information was deleted successfully!"]);
        }else{
            session(['error' => "Clinic Information can not delete!"]);
        }

        return redirect('clinic-info?index='.$request->current_index);
    }

    public function getTownships($region_id)
    {
        $data = new Township();

        if($region_id!=0)
           $data = $data->where('region_id',$region_id);

        $data = $data->get()
                      ->toArray();

        return json_encode(['townships' => $data]);
    }
}
