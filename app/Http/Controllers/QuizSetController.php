<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use App\QuizSet;
use App\Quiz;
use App\QuizSetDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

class QuizSetController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:quiz-set-list|quiz-set-create|quiz-set-edit|quiz-set-delete|quiz-set-view|quiz-set-publish|quiz-set-active', ['only' => ['index']]);
        $this->middleware('permission:quiz-set-create', ['only' => ['create','store']]);
        $this->middleware('permission:quiz-set-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:quiz-set-view', ['only' => ['show']]);
        $this->middleware('permission:quiz-set-delete', ['only' => ['destroy']]);
        $this->middleware('permission:quiz-set-publish', ['only' => ['publish']]);
        $this->middleware('permission:quiz-set-active', ['only' => ['active']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new QuizSet();

            if($request->level!=null){
                $data = $data->where('courses.course_level',$request->level);
            }

            if ($request->quiz_set_status!= null) {
                $data = $data->where('quiz_sets.status', $request->quiz_set_status);
            }

            $data = $data->leftJoin('courses','courses.id','quiz_sets.course_id')
                         ->leftJoin('users as createdUsers','createdUsers.id','quiz_sets.created_by')
                         ->leftJoin('users as publishedUsers','publishedUsers.id','quiz_sets.published_by');

            $data = $data->latest('quiz_sets.created_at')
                         ->with([
                            'details' => function($q){
                                $q->leftJoin('quizzes','quizzes.id','quiz_set_details.quiz_id')
                                  ->select('quiz_set_details.*','quizzes.quiz_type');
                            }
                         ])
                         ->get([
                            'quiz_sets.*',
                            'courses.course_level',
                            'createdUsers.name as created_user_name',
                            'publishedUsers.name as published_user_name',
                        ]);

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('quiz-set-edit') || auth()->user()->can('quiz-set-delete') || auth()->user()->can('quiz-set-view') || auth()->user()->can('quiz-set-publish') || auth()->user()->can('quiz-set-active')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('quiz-set-edit'))
                              $btn .= '<a class="dropdown-item text-blue edit-data" href="#" action="quiz-set/'.$row->id.'/edit"> Edit</a>';

                            if(auth()->user()->can('quiz-set-publish') && $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue publish-data" href="#" action="/quiz-set/'.$row->id.'/publish">Publish</a>';

                            if(auth()->user()->can('quiz-set-active') && $row->status == "3")
                              $btn .= '<a class="dropdown-item text-blue active-data" href="#" action="/quiz-set/'.$row->id.'/active">Activate</a>';

                            if(auth()->user()->can('quiz-set-active') && $row->status == "2")
                              $btn .= '<a class="dropdown-item text-blue deactive-data" href="#" action="/quiz-set/'.$row->id.'/active">Deactivate</a>';

                            if(auth()->user()->can('quiz-set-view'))
                              $btn .= '<a class="dropdown-item text-blue view-data" href="/quiz-set/'.$row->id.'">View Detail</a>';

                            if(auth()->user()->can('quiz-set-delete')&& $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue delete-data" href="#" action="/quiz-set/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('quiz_types', function($row){
                        $collections = $row->details->pluck('quiz_type')->toArray();
                        $collections = collect($collections)->unique()->toArray();

                        $types = array_map(function($item) {
                           return config("web_constant.quiz_types.$item");
                        },$collections);

                        return implode(', ',$types);
                    })
                    ->addColumn('status', function($row){
                        return "<span class='status_".config("web_constant.status.$row->status")."'>".config("web_constant.status.$row->status")."</span>";
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->rawColumns(['action','quiz_types','status','published_at'])
                    ->make(true);
        }

        return view('quiz-set.index',[
                   'content_header' => 'Quiz Sets',
                   'create_permission' => 'quiz-set-create',
                   'route' => 'quiz-set',
                   'keyword' => 'quiz set'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quiz = new QuizSet();
        return view('quiz-set.create', [
            'quiz' => $quiz,
            'content_header' => 'Create New Quiz Set',
            'route' => 'quiz-set',
        ]);
    }

    public function getQuiz($course,$quiz_type)
    {
        $data = Quiz::where('status',2)->where('course_id',$course)->where('quiz_type',$quiz_type)->get();
        return json_encode($data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
                        'set_name' => 'required|max:255',
                        'course_id' => 'required'
                    ]);

        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();
             $data['status'] = 1;

             $set = QuizSet::create($data);

             if(!$set){
                 $result = false;
                 DB::rollback();
             }

             if($result){
                if(!QuizSet::saveDetail($request->all(),$set->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Quiz set was created successfully!"]);
        }else{
            session(['error' => "Quiz set can not create!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'level' => $set->course->course_level]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuizSet  $quizSet
     * @return \Illuminate\Http\Response
     */
    public function show(QuizSet $quizSet)
    {
        $quiz = QuizSet::where('quiz_sets.id',$quizSet->id)
                            ->leftJoin('users as createdUsers','createdUsers.id','quiz_sets.created_by')
                            ->leftJoin('users as updatedUsers','updatedUsers.id','quiz_sets.updated_by')
                            ->leftJoin('users as publishedUsers','publishedUsers.id','quiz_sets.published_by')
                            ->leftJoin('users as deactivatedUsers','deactivatedUsers.id','quiz_sets.deactivated_by')
                            ->with([
                                'details' => function($q){
                                    $q->leftJoin('quizzes','quizzes.id','quiz_set_details.quiz_id')
                                       ->select('quiz_set_details.*','quizzes.quiz_question','quizzes.quiz_type');
                                },
                            ])
                            ->first([
                                'createdUsers.name as created_user_name',
                                'updatedUsers.name as updated_user_name',
                                'publishedUsers.name as published_user_name',
                                'deactivatedUsers.name as deactivated_user_name',
                                'quiz_sets.*'
                            ]);

        return view('quiz-set.show',[
                    'quiz' => $quiz,
                    'content_header' => 'Quiz Set Detail',
                    'route' => 'quiz-set',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuizSet  $quizSet
     * @return \Illuminate\Http\Response
     */
    public function edit(QuizSet $quizSet,Request $request)
    {
        $quiz = QuizSet::where('quiz_sets.id',$quizSet->id)
                            ->with([
                                'details' => function($q){
                                    $q->leftJoin('quizzes','quizzes.id','quiz_set_details.quiz_id')
                                       ->select('quiz_set_details.*','quizzes.quiz_question','quizzes.quiz_type');
                                },
                            ])
                            ->first();

        $quiz->current_index = $request->index;
        return view('quiz-set.update',[
                    'quiz' => $quiz,
                    'content_header' => 'Update Quiz Set',
                    'route' => 'quiz-set',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuizSet  $quizSet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuizSet $quizSet)
    {
        $validator = Validator::make($request->all(),[
            'set_name' => 'required|max:255',
            'course_id' => 'required'
        ]);

        if($validator->fails()){
           return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['updated_by'] = Auth::id();

            $set = $quizSet->update($data);

            if(!$set){
                $result = false;
                DB::rollback();
            }

            if($result){
                QuizSetDetail::where('quiz_set_id',$quizSet->id)->forceDelete();
                if(!QuizSet::saveDetail($request->all(),$quizSet->id)){
                    $result = false;
                    DB::rollback();
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
        session(['success' => "Quiz set was updated successfully!"]);
        }else{
        session(['error' => "Quiz set can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'level' => $quizSet->course->course_level]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuizSet  $quizSet
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuizSet $quizSet,Request $request)
    {
        $level = $quizSet->course->course_level;
        $result = true;
        DB::beginTransaction();
        try {
            $ans = QuizSet::where('id',$quizSet->id)
                                ->update([
                                    'deleted_by' => Auth::id(),
                                ]);

            QuizSetDetail::where('quiz_set_id',$quizSet->id)->delete();

            if(!$quizSet->delete()){
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($ans){
            session(['success' => "Quiz set was deleted successfully!"]);
        }else{
            session(['error' => "Quiz set can not delete!"]);
        }

        return redirect('quiz-set?index='.$request->current_index.'&level='.$level);
    }

    public function publish(Request $request,$id)
    {
        $set = QuizSet::find($id);
        $ans = QuizSet::where('id',$id)
                            ->update([
                                'status' => 2,
                                'published_by' => Auth::id(),
                                'published_at' => date('Y-m-d H:i:s')
                            ]);

        if($ans){
            session(['success' => "Quiz set was published successfully!"]);
        }else{
            session(['error' => "Quiz set can not publish!"]);
        }

        return redirect('quiz-set?index='.$request->current_index.'&level='.$set->course->course_level);
    }

    public function active(Request $request,$id)
    {
        $set = QuizSet::find($id);
        $status = null;

        if($set->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactivate";
        }elseif($set->status==3){
            $status = 2;
            $msg1 = "activated";
            $msg2 = "activate";
        }

        $ans = QuizSet::where('id',$id)
                            ->update([
                                'status' => $status ,
                                'deactivated_by' => Auth::id(),
                                'deactivated_at' => date('Y-m-d H:i:s')
                            ]);

        if($ans){
            session(['success' => "Quiz set was $msg1 successfully!"]);
        }else{
            session(['error' => "Quiz set can not $msg2!"]);
        }

        return redirect('quiz-set?index='.$request->current_index.'&level='.$set->course->course_level);
    }

}
