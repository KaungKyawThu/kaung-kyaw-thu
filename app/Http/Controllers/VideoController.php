<?php

namespace App\Http\Controllers;

use App\ExploreKeyword;
use App\Http\Controllers\VideoController;
use App\Keyword;
use App\Video;
use Auth;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:video-list|video-create|video-edit|video-delete|video-publish|video-active', ['only' => ['index']]);
        $this->middleware('permission:video-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:video-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:video-delete', ['only' => ['destroy']]);
        $this->middleware('permission:video-publish', ['only' => ['publish']]);
        $this->middleware('permission:video-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $data = new Video();
            $data = $data->orderBy('order', 'ASC');

            if ($request->video_status != null) {
                $data = $data->where('videos.status', $request->video_status);
            }

            $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'videos.created_by')
                          ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'videos.published_by');


            $data = $data->get([
                'videos.*',
                'createdUsers.name as created_user_name',
                'publishedUsers.name as published_user_name',

            ]);



            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if (auth()->user()->can('video-edit') || auth()->user()->can('video-delete')) {
                        $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';

                        if (auth()->user()->can('video-edit')) {
                            $btn .= '<a class="dropdown-item text-blue edit-video edit-data" href="#" action="video/' . $row->id . '/edit"> Edit</a>';
                        }

                        if (auth()->user()->can('video-publish') && $row->status == 1) {
                            $btn .= '<a class="dropdown-item text-blue publish-data" href="#" action="video/' . $row->id . '/publish"> Publish</a>';
                        }

                        if (auth()->user()->can('video-active') && $row->status == 2) {
                            $btn .= '<a class="dropdown-item text-blue deactive-data" href="#" action="video/' . $row->id . '/active">Deactive</a>';
                        }

                        if (auth()->user()->can('video-active') && $row->status == 3) {
                            $btn .= '<a class="dropdown-item text-blue active-data" href="#" action="video/' . $row->id . '/active">Activate</a>';
                        }

                        if (auth()->user()->can('video-delete')&& $row->status == "1") {
                            $btn .= '<a class="dropdown-item text-blue edit-video delete-data" href="#" action="/video/' . $row->id . '">Delete</a>';
                        }

                        $btn .= '</div>';
                    }
                    return $btn;
                })
                ->addColumn('status', function ($row) {
                    return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                })
                ->addColumn('thumbnail', function ($row) {
                    return '<img src=' . $row->thumbnail . ' width="40" height="40"/>';
                })
                ->addColumn('published_at', function($row){
                    return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                })
                ->addColumn('video_title', function ($row) {

                    return '<a  href=' . $row->video_link . ' target="_blank" rel="noopener noreferrer" />' . $row->video_title . '</a>';
                })

                ->rawColumns(['thumbnail', 'action', 'status', 'video_title','published_at'])
                ->make(true);

        }

        return view('video.index', [
            'content_header' => 'Video Lists',
            'create_permission' => 'video-create',
            'route' => 'video',
            'keyword' => 'video',
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $video = new Video();
        return view('video.create', [
            'video' => $video,
            'content_header' => 'Create New Video',
            'route' => 'video',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if (!$request->has('keyword_id')) {
            $request->merge([
                'keyword_id' => null,
            ]);
        }

        $validator = Validator::make($request->all(), [
                        'video_title' => ['required', 'string', 'max:255'],
                        'video_link' => ['required', function($atr,$value,$fail){
                            if(strpos($value,'youtube.com')===false && strpos($value,'youtu.be')===false){ 
                                $fail("The video link is invalid format.");
                            }
                        }],
                        'video_duration' => 'required|regex:/^[\d]{1,2}\:[\d]{1,2}$/',
                        'thumbnail' => 'required|mimes:jpeg,png,jpg',
                        'order' => 'required',
                ]);

        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $result = true;

        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['created_by'] = Auth::id();
            $data['status'] = 1;
            if ($request->has('thumbnail')) {
                $photo = Storage::disk('s3')->put('images', $request->thumbnail);
                $data['thumbnail'] = Storage::disk('s3')->url($photo);
            }

            if( Video::where('order',$request->order)->count() > 0){
                Video::where('order','>=',$request->order)->increment('order',1);
            }

            $video = Video::create($data);
            if (!$video) {
                $result = false;
                DB::rollback();
            }

            if($request->keyword_id!=null){
                if (!Keyword::saveExploreKeyword($request->keyword_id, $video->id, 3)) {
                    $result = false;
                    DB::rollback();
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'video was created successfully!']);
        } else {
            session(['error' => 'video can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Video $video)
    {
        //
        $video->current_index = $request->index;
        return view('video.update', [
            'video' => $video,
            'content_header' => 'Update Video',
            'route' => 'video',
            'is_update' => true,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
        if (!$request->has('thumbanil') && $request->virtual_img == null) {
            $request->merge([
                'thumbnail' => null,
            ]);
        } else {
            $request->merge([
                'thumbnail' => $request->virtual_img ?? $request->thumbnail,
            ]);
        }
        $validator = Validator::make($request->all(), [
                                    'video_title' => ['required', 'string', 'max:255'],
                                    'video_link' => ['required', function($atr,$value,$fail){
                                        if(strpos($value,'youtube.com')===false && strpos($value,'youtu.be')===false){ 
                                            $fail("The video link is invalid format.");
                                        }
                                    }],
                                    'video_duration' => 'required|regex:/^[\d]{1,2}\:[\d]{1,2}$/',
                                    'thumbnail' => $request->virtual_img==null ? 'required|mimes:jpg,jpeg,png' : 'required',
                                    // 'keyword_id' => 'required|array',
                                    'order' => ['required', Rule::unique('videos')->ignore($video->id)],
                                  ],
                                  [
                                    'video_duration.date_format' => 'The video duration is invalid format.',
                                  ]
                    );

        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            $data = $request->all();
            if ($request->virtual_img == null) {
                $photo = Storage::disk('s3')->put('images', $request->thumbnail);
                $data['thumbnail'] = Storage::disk('s3')->url($photo);
            }
            $ans = $video->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            if ($result && $request->keyword_id!=null) {
                ExploreKeyword::where(['explore_id' => $video->id, 'explore_type' => 3])->forceDelete();
                if (!Keyword::saveExploreKeyword($request->keyword_id, $video->id, 3)) {
                    $result = false;
                    DB::rollback();
                }
            }

            if( Video::where('order',$request->order)->where('id', '!=' , $video->id)->count() > 0){
                Video::where('id', '!=' , $video->id)->where('order','>=',$request->order)->increment('order',1);
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Video was updated successfully!"]);
        } else {
            session(['error' => "Video can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Video $video)
    {
        //
        $result = true;
        DB::beginTransaction();
        try {
            ExploreKeyword::where(['explore_id' => $video->id, 'explore_type' => 3])->delete();
            $video->deleted_by = Auth::id();

            $video->delete();
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Video was deleted successfully!"]);
        } else {
            session(['error' => "Video can not delete!"]);
        }

        return redirect('video?index=' . $request->current_index);
    }
    public function publish(Request $request, $id)
    {

        $result = Video::where('id', $id)->update(['status' => 2, 'published_by' => Auth::id(), 'published_at' => date('Y-m-d H:i:s')]);

        if ($result) {
            session(['success' => "Video was published successfully!"]);
        } else {
            session(['error' => "Video was not published!"]);
        }

        return redirect('/video?index=' . $request->current_index);
    }

    public function active(Request $request, $id)
    {

        $video = Video::where('id', $id)->first();
        $result = true;
        DB::beginTransaction();
        try {
            if ($video->status == 3) {
                $msg = "activated";

                $video->status = 2;
                $video->deactived_by = Auth::id();
                $video->deactived_at = date('Y-m-d H:i:s');
                $video->save();

            } elseif ($video->status == 2) {
                $msg = "deactivated";
                $video->status = 3;
                $video->deactived_by = Auth::id();
                $video->deactived_at = date('Y-m-d H:i:s');
                $video->save();

            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "Article was $msg successfully!"]);
        } else {
            session(['error' => "Article was not $msg!"]);
        }

        return redirect('/video?index=' . $request->current_index);
    }

}
