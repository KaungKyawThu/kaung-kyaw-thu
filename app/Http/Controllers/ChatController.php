<?php

namespace App\Http\Controllers;

use DB;
use App\Chat;
use App\User;
use DataTables;
use App\PushNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {       

            $data = new Chat();
            $data = $data->latest()->get();

            $data = collect($data)->unique('user_id')->all();
            return view('chat.index',[
                   'data' => $data,
                   'content_header' => 'Chats',
                   'route' => 'chat',
                   'keyword' => 'chat'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $result = true;
        try {
         
            if($request->message_type==2){  //if photo message
                $photo = Storage::disk('s3')->put('images', $request->message);
                $message = Storage::disk('s3')->url($photo);
            }else{
                $message = $request->message;
            }

            $msg = Chat::create([
                        'user_id' => $request->user_id,
                        'chat_type' => 2,
                        'message_type' => $request->message_type,
                        'message' => $message,
                        'is_read' => 1
                    ]);

            $fcm_token = User::where('id',$request->user_id)->pluck('fcm_token')->toArray();
            PushNotification::pushAdriod($fcm_token, 'Message Reply', $message, 3);

            if(!$msg){
                $result = false;
                DB::rollback();
            }

            if(!User::where('id',$request->user_id)->update(['is_new_msg' => 1])){
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
           dd($th);
        }
        
       $chats  = \Crypt::encryptString(User::where('id',$request->user_id)->with('chats')->first());
       return json_encode(['success' => $result, 'chats' => $chats, 'new_message' => $msg]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show(Chat $chat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit(Chat $chat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chat $chat,Request $request)
    {
        $chat_id = $chat->id;
        DB::beginTransaction();
        $result = true;
        try {
            $result = $chat->delete();
            DB::commit();
        } catch (\Throwable $th) {
           dd($th);
        }
        
       $chats = User::where('id',$request->user_id)->with('chats')->first();

       $last_msg = collect($chats->chats)->last();

       $chats  = \Crypt::encryptString($chats);

       return json_encode(['success' => $result, 'chats' => $chats, 'last_message' => $last_msg, 'chat_id'  => $chat_id]);
    }

    public function getNewCount()
    {
        $notifications = Chat::where('is_read',0)->count();
    
        return [
            'label'       => $notifications,
            'icon_color'   => 'default', 
            'label_color'  => 'danger',
            'dropdown'    =>  false,
        ];
    }
}
