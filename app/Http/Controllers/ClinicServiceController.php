<?php

namespace App\Http\Controllers;


use DB;
use Auth;
use DataTables;
use Illuminate\Validation\Rule;
use App\ClinicService;
use Illuminate\Http\Request;

class ClinicServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:clinic-list|clinic-create|clinic-edit|clinic-delete|clinic-publish|clinic-active', ['only' => ['index']]);
        $this->middleware('permission:clinic-create', ['only' => ['create','store']]);
        $this->middleware('permission:clinic-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:clinic-delete', ['only' => ['destroy']]);
        $this->middleware('permission:clinic-active', ['only' => ['publish']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new ClinicService();
            // $data = $data->withTrashed();


            if ($request->service_status != null) {
                $data = $data->where('clinic_services.status', $request->service_status);
            }
            $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'clinic_services.created_by')
            ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'clinic_services.published_by');


$data = $data->latest()->get([
  'clinic_services.*',
  'createdUsers.name as created_user_name',
  'publishedUsers.name as published_user_name',

]);




            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('clinic-edit') || auth()->user()->can('clinic-delete') || auth()->user()->can('clinic-publish') || auth()->user()->can('clinic-active')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('clinic-edit'))
                              $btn .= '<a class="dropdown-item text-blue edit-clinic edit-data" href="#" action="clinic/'.$row->id.'/edit"> Edit</a>';

                              if(auth()->user()->can('clinic-publish') && $row->status == "1")
                            $btn .= '<a class="dropdown-item text-blue edit-clinic publish-data" href="#" action="/clinic/'.$row->id.'/publish">Publish</a>';

                            if(auth()->user()->can('clinic-active') && $row->status == "3")
                            $btn .= '<a class="dropdown-item text-blue edit-clinic active-data" href="#" action="/clinic/'.$row->id.'/publish">Activate</a>';

                            if(auth()->user()->can('clinic-active') && $row->status == "2")
                            $btn .= '<a class="dropdown-item text-blue edit-clinic deactive-data" href="#" action="/clinic/'.$row->id.'/active">Deactivate</a>';

                            if(auth()->user()->can('clinic-delete')&& $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue edit-clinic delete-data" href="#" action="/clinic/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('status', function ($row) {
                        return "<span class='status_".config("web_constant.status.$row->status")."'>".config("web_constant.status.$row->status")."</span>";
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->rawColumns(['action','status','published_at'])
                    ->make(true);
        }

        return view('service.index',[
                   'content_header' => 'Service Lists',
                   'create_permission' => 'clinic-create',
                   'route' => 'clinic',
                   'keyword' => 'service'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clinic = new ClinicService();
        return view('service.create',[
                    'clinic' => $clinic,
                    'content_header' => 'Create New Service',
                    'route' => 'clinic',
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'service_name' => ['required',Rule::unique('clinic_services')->whereNull('deleted_at'),'max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'service_mm_name' => ['required', Rule::unique('clinic_services')->whereNull('deleted_at'),'max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'service_description' => ['required'],
            'service_mm_description' => ['required']

        ]);

        $request['status'] = 1;

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();

             $clinic = ClinicService::create($data);

             if(!$clinic){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'Service was created successfully!']);
        }else{
            session(['error' => 'Service can not create!']);
        }

        return redirect('/clinic');
    }

    public function publish(Request $request,$id)
    {
        $status = ClinicService::find($id);
        $status = $status->status;

        $result = true;
        DB::beginTransaction();
        try {

         $data['published_by'] = Auth::id();
         $data['published_at'] = date("Y-m-d H:i:s");
             $data['status'] = 2;
             $ans = ClinicService::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Service was published successfully!"]);
        }else{
            session(['error' => "Service can not publish!"]);
        }

        return redirect('clinic?index='.$request->current_index);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClinicService  $clinicService
     * @return \Illuminate\Http\Response
     */
    public function show(ClinicService $clinicService)
    {
        //
    }

    public function active(Request $request,$id)
    {
        $clinic = ClinicService::find($id);
        $status = null;

        if($clinic->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactive";
        }elseif($clinic->status==3){

            $status = 2;
            $msg1 = "activated";
            $msg2 = "active";
        }

        $result = true;
        DB::beginTransaction();
        try {

             $data['deactivated_by'] = Auth::id();
             $data['deactivated_at'] = date("Y-m-d H:i:s");
             $data['status'] = $status;
             $ans = ClinicService::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Service was $msg1 successfully!"]);
        }else{
            session(['error' => "Service can not $msg2!"]);
        }

        return redirect('clinic?index='.$request->current_index);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClinicService  $clinicService
     * @return \Illuminate\Http\Response
     */
    public function edit(ClinicService $clinic,Request $request)
    {
        $clinic->current_index = $request->index;

        return view('service.update',[
                    'clinic' => $clinic,
                    'content_header' => 'Update clinic',
                    'route' => 'clinic',
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClinicService  $clinicService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClinicService $clinic)
    {
        $request->validate([
            'service_name' => ['required', Rule::unique('clinic_services')->whereNull('deleted_at')->ignore($clinic->id),'max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'service_mm_name' => ['required', Rule::unique('clinic_services')->whereNull('deleted_at')->ignore($clinic->id),'max:50','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'service_description' => ['required'],
            'service_mm_description' => ['required']
        ]);

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['updated_by'] = Auth::id();
             $ans = $clinic->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Service was updated successfully!"]);
        }else{
            session(['error' => "Service can not update!"]);
        }

        return redirect('clinic?index='.$request->current_index);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClinicService  $clinicService
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClinicService $clinic,Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

             $clinic['deleted_by'] = Auth::id();
             $ans = ClinicService::where('id',$clinic->id)
                        ->update([
                            'deleted_by' => Auth::id(),
                        ]);
             $clinic->delete();
             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Service was deleted successfully!"]);
        }else{
            session(['error' => "Service can not delete!"]);
        }

        return redirect('clinic?index='.$request->current_index);
    }
}
