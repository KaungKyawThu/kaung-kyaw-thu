<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Faq;
use App\Content;
use App\ClinicService;
use App\ClinicInfo;
use App\Aboutus;

class OfflineController extends Controller
{
    public function getOfflineFaq()
    {
        $faqs = Faq::where('status', config('constant.status.Active'))->latest('id')->get(['id', 'faq_question', 'faq_answer']);

        return $faqs;
    }

    public function getOfflineTopic()
    {
        $topics = Content::with(
            'details:id,content_id,detail_photo,detail_description',
        )
        ->where('status', config('constant.status.Active'))
        ->latest('id')
        ->get(['id', 'content_title', 'content_cover_photo']);

        return $topics;
    }

    public function getOfflineServiceType()
    {
        $types = ClinicService::where('status', config('constant.status.Active'))->latest('id')->get(['id', 'service_mm_name as service_name']);

        return $types;
    }

    public function getOfflineServiceTownships()
    {
        $townships = ClinicInfo::leftJoin('townships', 'townships.id', 'clinic_infos.township_id')
        ->distinct('clinic_infos.township_id')
        ->orderBy('townships.id', 'ASC')
        ->select('townships.id', 'townships.name', 'townships.mm_unicode')
        ->get();

        return $townships;
    }

    public function getOfflineServices()
    {
        $hotline = Aboutus::first();
        $services = ClinicService::where('status', config('constant.status.Active'))->latest('id')->get(['id', 'service_mm_name as service_name', 'service_mm_description as service_description']);

        $data = [];
        foreach ($services as $key => $service) {
           $data[$key]['service'] = $service;
           $data[$key]['hotline'] = $hotline->phone_no;
           $data[$key]['clinics'] = ClinicInfo::leftJoin('townships', 'townships.id', 'clinic_infos.township_id')
           ->leftJoin('clinic_organizations as corg','corg.id','clinic_infos.clinic_organization_id')
           ->where('clinic_service_id', $service->id)
           ->latest('clinic_infos.id')
           ->select('clinic_infos.id','clinic_infos.clinic_mm_name as clinic_name','clinic_infos.clinic_mm_address as clinic_address','clinic_infos.clinic_phone_no','clinic_infos.clinic_mm_phone_no','corg.organization_mm_name as org_name','clinic_description_mm as clinic_description','townships.mm_unicode as township_name')
           ->get();
        }

        return $data;
    }
}
