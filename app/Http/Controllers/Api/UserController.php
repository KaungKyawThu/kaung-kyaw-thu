<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Http\Controllers\Controller;
use App\User;
use App\UserCertificate;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getProfile($user_id)
    {
        $user = User::findOrFail($user_id);

        $users_arr = User::where('role_id', config('constant.user_role.user'))->OrderBy('user_point', 'desc')->get()->toArray();

        $rank = $this->searchUserRank($users_arr, $user->id);

        return response()->json([
            'name' => $user->name,
            'phone_no' => $user->phone_no,
            'avator_img_id' => $user->avator_img_id,
            'point' => $user->user_point,
            'rank' => $rank,
            'has_account' => ($user->phone_no == null && $user->password ==null)? false : true 
        ]);
    }

    public function updateNickName($user_id, Request $request){
        $user = User::findOrFail($user_id);

        $user->nick_name = $request->nick_name;
        $user->name = $user->nick_name . '_' . $user->user_no;
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Nick Name Updated'
        ]);

    }

    public function updateAvator($user_id, Request $request){
        $user = User::findOrFail($user_id);

        $user->avator_img_id = $request->avator_img_id;
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Avator Updated'
        ]);

    }

    public function getUserCerificate($user_id){
        $user = User::findOrFail($user_id);

        $users_arr = User::where('role_id', config('constant.user_role.user'))->OrderBy('user_point', 'desc')->get()->toArray();

        $rank = $this->searchUserRank($users_arr, $user->id);

        $certificate = UserCertificate::leftJoin('courses', 'courses.id', '=', 'user_certificates.course_level_id')
            ->where('user_certificates.user_id', $user->id)
            ->where('user_certificates.is_generated', true)
            ->select([
                'user_certificates.id',
                'courses.course_name_en as name',
                'user_certificates.certificate_link'
            ])
            ->get();

        return response()->json([
            'name' => $user->name,
            'avator_img_id' => $user->avator_img_id,
            'point' => $user->user_point,
            'rank' => $rank,
            'house_link' => ($user->course_level_id == 0)? "": Course::find($user->course_level_id)->house_link ,
            'certificates' => $certificate
        ]);
    }

    protected function searchUserRank($users_arr, $user_id)
    { //linear search
        for ($i = 0; $i < count($users_arr); $i++) {
            if ($users_arr[$i]['id'] == $user_id)
                return $i + 1;
        }
    }
}
