<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Category;
use App\Video;
use App\Podcast;
use App\Content;
use App\PodcastSerie;
use App\ContentCategory;
use App\Quiz;
use App\Keyword;
use App\ExploreKeyword;
use App\Faq;
use App\Bookmark;
use App\ContentComment;
use App\CommentReply;
use App\Policy;
use DB;
use Validator;

class ExploreController extends Controller
{
    public function getCategories(Request $request)
    {
        $categories = Category::where('is_popular',1)->where('status', config('constant.status.Active'))->latest()->select(['id', 'name_en','name_mm'])->get();
        return response()->json([
            'topic_categories' => $categories,
        ]);
    }

    public function getLatestVideosAndPodcasts()
    {
        $userId = Auth()->user()->id;
        $latest_videos = Video::where('status', config('constant.status.Active'))
                        ->latest()
                        ->take(5)
                        ->select(['id','video_title', 'thumbnail', 'video_duration', 'video_link'])
                        ->withCount(['bookmarks as is_bookmark' => function($q) use($userId){
                            $q->where('user_id', $userId)
                            ->where('explore_type', config('constant.explore_type.video'));
                        }])
                        ->get();
        
        $latest_podcasts = Podcast::join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
                        ->where('podcasts.status', config('constant.status.Active'))
                        ->latest('podcasts.id')
                        ->take(5)
                        ->select(['podcasts.id','podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcast_share_link','ps.series_name as podcast_series'])
                        ->withCount(['bookmarks as is_bookmark' => function($q) use($userId){
                            $q->where('user_id', $userId)
                            ->where('explore_type', config('constant.explore_type.podcast'));
                        }])
                        ->get();

        return response()->json([
            'latest_video' => $latest_videos,
            'latest_podcast' => $latest_podcasts
        ]);
    }

    public function getPopularTopics(Request $request)
    {
        $popular_topics = new Content();
        if($request->cat_id == null){
            $latest_category = Category::where('status', config('constant.status.Active'))
                                ->where('is_popular', 1)
                                ->latest()
                                ->first('id');
            $latest_category = $latest_category->id;
        }else {
            $latest_category = $request->cat_id;
        }
        $userId = Auth()->user()->id;
        $popular_topics = $popular_topics::with([
            'categories' => function($q){
                $q->leftJoin('categories','categories.id','content_categories.category_id')
                ->select('categories.name_en','categories.name_mm','content_categories.id', 'content_categories.content_id', 'content_categories.category_id as id');
            },
            'details:id,content_id,detail_photo,detail_description',
        ])
        ->leftJoin('content_categories as cat','contents.id', 'cat.content_id')
        ->leftJoin('categories as c','c.id','cat.category_id')
        ->where('cat.category_id', $latest_category)
        ->where('content_type', config('constant.explore_type.topic'))
        ->where('is_trending', 1)
        ->latest('contents.id')
        ->select(['contents.id', 'contents.content_title', 'contents.content_cover_photo', 'contents.read_time', 'c.name_en as content_category'])
        ->withCount(['bookmarks as is_bookmark' => function($q) use($userId){
            $q->where('user_id', $userId)
            ->where('explore_type', config('constant.explore_type.topic'));
        }])
        ->take(5)
        ->get();
        return response()->json([
            'popular_topics' => $popular_topics,
        ]);
    }

    public function getVideos(Request $request)
    {
        $videos = Video::where('status', config('constant.status.Active'))
                        ->select(['id','video_title', 'thumbnail', 'video_duration', 'video_link'])
                        ->withCount(['bookmarks as is_bookmark' => function($q) {
                            $q->where('user_id', Auth()->user()->id)
                            ->where('explore_type', config('constant.explore_type.video'));
                        }])
                        ->orderBy('order', 'DESC')
                        ->paginate(config('constant.page_size'));
        $result['videos'] = $videos->items();
        $result['pagination'] = [
            "per_page" => $videos->perPage(),
            "current_page" => $videos->currentPage(),
            "last_page" => $videos->lastPage()
        ];
        return $result;
    }

    public function getPodcasts(Request $request)
    {
        $series = PodcastSerie::select(['id', 'series_name', 'thumbnail'])
        ->where('podcast_series.status', config('constant.status.Active'))
        ->withCount('podcast')
        ->get();
        
        $podcasts = Podcast::join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
        ->where('podcasts.status', config('constant.status.Active'))
        ->latest('podcasts.id')
        ->select(['podcasts.id','podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcast_share_link','ps.series_name as podcast_series'])
        ->withCount(['bookmarks as is_bookmark' => function($q) {
            $q->where('user_id', Auth()->user()->id)
            ->where('explore_type', config('constant.explore_type.podcast'));
        }])
        ->orderBy('podcasts.id', 'DESC')
        ->paginate(config('constant.page_size'));

        $result['podcast_series'] = $series;
        $result['podcasts'] = $podcasts->items();
        $result['pagination'] = [
            "per_page" => $podcasts->perPage(),
            "current_page" => $podcasts->currentPage(),
            "last_page" => $podcasts->lastPage()
        ];

        return $result;
    }

    public function getPodcastSeriesDetail($series_id)
    {
        $series = PodcastSerie::select('series_name','thumbnail')->withCount('podcast')->where('id', $series_id)->first();
        $podcasts = Podcast::join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
        ->where('podcasts.podcast_series_id', $series_id)
        ->where('podcasts.status', config('constant.status.Active'))
        ->latest('podcasts.id')
        ->select(['podcasts.id','podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcast_share_link','ps.series_name as podcast_series'])
        ->withCount(['bookmarks as is_bookmark' => function($q) {
            $q->where('user_id', Auth()->user()->id)
            ->where('explore_type', config('constant.explore_type.podcast'));
        }])
        ->orderBy('podcasts.id', 'DESC')
        ->paginate(config('constant.page_size'));

        return response()->json([
            'series_name' => $series->series_name,
            'thumbnail' => $series->thumbnail,
            'podcast_count' => $series->podcast_count,
            'podcasts' => $podcasts->items(),
            'pagination' => [
                "per_page" => $podcasts->perPage(),
                "current_page" => $podcasts->currentPage(),
                "last_page" => $podcasts->lastPage()
            ]
        ]);
    }

    public function getTrendingContents(Request $request)
    {
        $content_type = $request->type;

        if($request->category_id == null){
            $trending_contents = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                      ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                      ->get();
                }
            ])
            ->where('contents.status', config('constant.status.Active'))
            ->where('is_trending', 1);
        }else {
            $trending_contents = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                      ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id');
                }
            ])
                                    ->where('contents.status', config('constant.status.Active'))
                                    ->where('cc.category_id', $request->category_id)
                                    ->where('is_trending', 1);
        }
        
        
        
        if($content_type == config('constant.explore_type.topic')){
            $trending_contents = $trending_contents->where('content_type', config('constant.explore_type.topic'))
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.topic'));
            }])
            ->take(10)
            ->get();
        }
        elseif($content_type == config('constant.explore_type.article')) {
            $trending_contents = $trending_contents->leftJoin('users', 'users.id', 'author_id')
            ->where('content_type', config('constant.explore_type.article'))
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.article'));
            }])
            ->take(10)
            ->get();
        }else {
            return response()->json([
                'message' => 'Record not found.'
            ], 404);
        }


        return response()->json([
            'categories' => $this->allCategories(),
            'trending_contents' => $trending_contents
        ]);

    }

    public function allCategories()
    {
        return Category::where('status', config('constant.status.Active'))->select(['id', 'name_en','name_mm', 'is_popular'])->get();
    }

    public function getAllContents(Request $request)
    {
        $content_type = $request->type;
        $contents = Content::with([
                        'categories' => function($q){
                            $q->leftJoin('categories','categories.id','content_categories.category_id')
                            ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                            ->get();
                        }
                    ])
                    ->where('contents.status', config('constant.status.Active'));

        if($content_type == config('constant.explore_type.topic')){
            $contents = $contents->where('content_type', config('constant.explore_type.topic'))
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.topic'));
            }])
            ->orderBy('contents.content_title')
            ->paginate(config('constant.page_size'));
        }
        elseif($content_type == config('constant.explore_type.article')) {
            $contents = $contents->leftJoin('users', 'users.id', 'author_id')
            ->where('content_type', config('constant.explore_type.article'))
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.article'));
            }])
            ->orderBy('contents.content_title')
            ->paginate(config('constant.page_size'));
        }else {
            return response()->json([
                'message' => 'Record not found.'
            ], 404);
        }

        $result['contents'] = $contents->items();
        $result['pagination'] = [
            "per_page" => $contents->perPage(),
            "current_page" => $contents->currentPage(),
            "last_page" => $contents->lastPage()
        ];

        return $result;

    }

    public function getContentsByCategory(Request $request,$category_id)
    {
        $content_type = $request->type;
        $category_ids = ContentCategory::where('category_id',$request->category_id)->pluck('content_id')->toArray();
        $contents = Content::with([
                        'categories' => function($q){
                            $q->leftJoin('categories','categories.id','content_categories.category_id')
                            ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                            ->get();
                        }
                    ])
                    ->where('contents.status', config('constant.status.Active'))
                    ->whereIn('contents.id',$category_ids);

        if($content_type == config('constant.explore_type.topic')){
            $contents = $contents->where('content_type', config('constant.explore_type.topic'))
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.topic'));
            }])
            ->orderBy('contents.content_title')
            ->paginate(config('constant.page_size'));
        }
        elseif($content_type == config('constant.explore_type.article')) {
            $contents = $contents->leftJoin('users', 'users.id', 'author_id')
            ->where('content_type', config('constant.explore_type.article'))
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.article'));
            }])
            ->orderBy('contents.content_title')
            ->paginate(config('constant.page_size'));
        }else {
            return response()->json([
                'message' => 'Record not found.'
            ], 404);
        }

        $result['contents'] = $contents->items();
        $result['pagination'] = [
            "per_page" => $contents->perPage(),
            "current_page" => $contents->currentPage(),
            "last_page" => $contents->lastPage()
        ];

        return $result;
    }

    public function getArticleDetail($id)
    {
        $content = Content::findOrFail($id);
        $result = Content::where('contents.id',$content->id)
                ->leftJoin('users as authors','authors.id','contents.author_id')
                ->with([
                    'categories' => function($q){
                        $q->leftJoin('categories','categories.id','content_categories.category_id')
                        ->select('categories.name_en','categories.name_mm','content_categories.id', 'content_categories.content_id', 'content_categories.category_id');
                    },
                    'details:id,content_id,detail_photo,detail_description',
                ])
                ->select([
                    'contents.id',
                    'contents.content_title',
                    'contents.content_cover_photo',
                    'contents.is_new_comment',
                    'contents.read_time',
                    'authors.name as author_name',
                ])
                ->withCount(['bookmarks as is_bookmark' => function($q) {
                    $q->where('user_id', Auth()->user()->id)
                    ->where('explore_type', config('constant.explore_type.article'));
                }])
                ->first();

    return $result;
        
    }

    public function getTopicDetail($id)
    {
        $content = Content::findOrFail($id);

        if($content->video_id <> null)
        $related_video = Video::where('status', config('constant.status.Active'))
        ->select(['id', 'video_title', 'thumbnail', 'video_duration', 'video_link'])
        ->withCount(['bookmarks as video_bookmark' => function($q) {
            $q->where('user_id', Auth()->user()->id)
            ->where('explore_type', config('constant.explore_type.video'));
        }])
        ->where('id', $content->related_video->id)
        ->first();
        else $related_video = null;
        
        $result = Content::where('contents.id',$content->id)
                ->with([
                    'categories' => function($q){
                        $q->leftJoin('categories','categories.id','content_categories.category_id')
                        ->select('categories.name_en','categories.name_mm','content_categories.id', 'content_categories.content_id', 'content_categories.category_id as id');
                    },
                    'details:id,content_id,detail_photo,detail_description',
                ])
                ->select([
                    'contents.id',
                    'contents.content_title',
                    'contents.content_cover_photo',
                    'contents.is_new_comment',
                    'contents.read_time'
                ])
                ->withCount(['bookmarks as is_bookmark' => function($q) {
                    $q->where('user_id', Auth()->user()->id)
                    ->where('explore_type', config('constant.explore_type.topic'));
                }])
                ->first();
    // $quiz = Quiz::with('options:id,quiz_id,quiz_option,is_correct')->leftJoin('content_quizzes as cq','quizzes.id','cq.quiz_id')->where('cq.content_id',$id)->get(['quizzes.id','quizzes.quiz_question','quizzes.quiz_type','quizzes.description']);

    $faqs = Faq::with([
        'category' => function($q){
            $q->leftJoin('categories','categories.id','faq_categories.category_id')
            ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
            ->get();
        }
    ])
    ->leftJoin('content_faqs as cf', 'faqs.id', 'cf.faq_id')
    ->where('faqs.status', config('constant.status.Active'))
    ->where('cf.content_id', $id)
    ->take(3)
    ->get(['faqs.id','faqs.faq_question','faqs.faq_answer']);
    
    $result['related_video'] = $related_video;
    $result['faqs'] = $faqs;

    return $result;
        
    }

    public function getFaqsByTopic($topic_id)
    {
        $faqs = Faq::with([
            'category' => function($q){
                $q->leftJoin('categories','categories.id','faq_categories.category_id')
                ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
                ->get();
            }
        ])
        ->leftJoin('content_faqs as cf', 'faqs.id', 'cf.faq_id')
        ->where('cf.content_id', $topic_id)
        ->select(['faqs.id','faqs.faq_question','faqs.faq_answer'])
        ->orderBy('id', 'DESC')
        ->paginate(config('constant.page_size'));

        $result['faqs'] = $faqs->items();
        $result['pagination'] = [
            "per_page" => $faqs->perPage(),
            "current_page" => $faqs->currentPage(),
            "last_page" => $faqs->lastPage()
        ];

        return $result;
    }

    public function getKeywords(Request $request)
    {
        $keywords = Keyword::where('is_most_search', 1)->get(['id','keyword_name']);

        return response()->json([
            'keywords' => $keywords
        ]);
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;

        $keywords = ExploreKeyword::leftJoin('keywords', 'keywords.id', 'explore_keywords.keyword_id')
        ->where('keywords.keyword_name', 'LIKE', '%'.$keyword.'%');
        
        if(isset($request->explore_type)){
            $explore_type_id = $request->explore_type;

            if($explore_type_id == config('constant.explore_type.faq')){

                $keywords_id = $keywords->where('explore_type', config('constant.explore_type.faq'))
                ->pluck('explore_keywords.explore_id');

                $data = Faq::with([
                    'category' => function($q){
                        $q->leftJoin('categories','categories.id','faq_categories.category_id')
                        ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
                        ->get();
                    }
                ])
                ->where('faqs.status', config('constant.status.active'))
                ->where('faqs.faq_question', 'LIKE','%'.$keyword.'%')
                ->orWhere('faqs.faq_answer', 'LIKE','%'.$keyword.'%')
                ->orWhereIn('faqs.id', $keywords_id)
                ->latest('faqs.id')
                ->select(['id','faq_question','faq_answer'])
                ->paginate(config('constant.page_size'));
            }
    
            if($explore_type_id == config('constant.explore_type.topic')){
    
                $ids = Content::where('contents.status', config('constant.status.Active'))
                ->where('contents.content_type', config('constant.explore_type.topic'))->pluck('id');

                $data = Content::with([
                    'categories' => function($q){
                        $q->leftJoin('categories','categories.id','content_categories.category_id')
                        ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                        ->get();
                    }
                ])
                ->whereIn('contents.id', $ids)
                ->where('contents.content_title', 'LIKE','%'.$keyword.'%')
                ->orWhereHas('details', function($query) use ($keyword){
                    $query->where('detail_description','LIKE','%'.$keyword.'%');
                })
                ->where('contents.content_type', config('constant.explore_type.topic'))
                ->where('contents.status', config('constant.status.Active'))
                ->latest('contents.id')
                ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
                ->withCount(['bookmarks as is_bookmark' => function($q) {
                    $q->where('user_id', Auth()->user()->id)
                    ->where('explore_type', config('constant.explore_type.topic'));
                }])
                ->paginate(config('constant.page_size'));
            }
    
            if($explore_type_id == config('constant.explore_type.article')){
                
                $ids = Content::where('contents.status', config('constant.status.Active'))
                ->where('contents.content_type', config('constant.explore_type.article'))->pluck('id');
                // dd($ids);
                $data = Content::with([
                    'categories' => function($q){
                        $q->leftJoin('categories','categories.id','content_categories.category_id')
                        ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                        ->get();
                    }
                ])
                ->whereIn('contents.id', $ids)
                ->where('contents.content_title', 'LIKE','%'.$keyword.'%')
                ->orWhereHas('details', function($query) use ($keyword){
                    $query->where('detail_description','LIKE','%'.$keyword.'%');
                })
                ->leftJoin('users', 'users.id', 'contents.author_id')
                ->where('contents.status', config('constant.status.Active'))
                ->where('contents.content_type', config('constant.explore_type.article'))
                
                ->latest('contents.id')
                ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
                ->withCount(['bookmarks as is_bookmark' => function($q) {
                    $q->where('user_id', Auth()->user()->id)
                    ->where('explore_type', config('constant.explore_type.article'));
                }])
                ->paginate(config('constant.page_size'));
            }
    
            if($explore_type_id == config('constant.explore_type.video')){

                $keywords_id = $keywords->where('explore_type', config('constant.explore_type.video'))
                ->pluck('explore_keywords.explore_id');
    
                $data = Video::select(['id','video_title','video_link','video_duration','thumbnail'])
                            ->withCount(['bookmarks as is_bookmark' => function($q) {
                                $q->where('user_id', Auth()->user()->id)
                                ->where('explore_type', config('constant.explore_type.video'));
                            }])
                            ->where('video_title', 'LIKE','%'.$keyword.'%')
                            ->orWhereIn('id', $keywords_id)
                            ->latest('videos.id')
                            ->where('videos.status', config('constant.status.Active'))
                            ->paginate(config('constant.page_size'));
            }
    
            if($explore_type_id == config('constant.explore_type.podcast')){

                $data = Podcast::join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
                        ->select(['podcasts.id', 'podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcast_share_link', 'ps.series_name as podcast_series'])
                        ->withCount(['bookmarks as is_bookmark' => function($q) {
                            $q->where('user_id', Auth()->user()->id)
                            ->where('explore_type', config('constant.explore_type.video'));
                        }])
                        ->where('podcast_title', 'LIKE','%'.$keyword.'%')
                        ->latest('podcasts.id')
                        ->where('podcasts.status', config('constant.status.Active'))
                        ->paginate(config('constant.page_size'));
            }
    
            $result['data'] = $data->items();
            $result['pagination'] = [
                "per_page" => $data->perPage(),
                "current_page" => $data->currentPage(),
                "last_page" => $data->lastPage()
            ];
    
            return $result;
        }else {
            $topic_ids = Content::where('contents.status', config('constant.status.Active'))
            ->where('contents.content_type', config('constant.explore_type.topic'))->pluck('id');
            
            $article_ids = Content::where('contents.status', config('constant.status.Active'))
            ->where('contents.content_type', config('constant.explore_type.article'))->pluck('id');

            $faq_keywords_id = $keywords->where('explore_type', config('constant.explore_type.faq'))
            ->pluck('explore_keywords.explore_id');

            $video_keywords_id = $keywords->where('explore_type', config('constant.explore_type.video'))
            ->pluck('explore_keywords.explore_id');

            $topics_data = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                    ->get();
                }
            ])
            ->whereIn('contents.id', $topic_ids)
            ->where('contents.content_title', 'LIKE','%'.$keyword.'%')
            ->orWhereHas('details', function($query) use ($keyword){
                $query->where('detail_description','LIKE','%'.$keyword.'%');
            })
            ->where('contents.status', config('constant.status.Active'))
            ->where('contents.content_type', config('constant.explore_type.topic'))
            ->latest('contents.id')
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.topic'));
            }])
            ->get();

            $articles_data = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                    ->get();
                }
            ])
            ->whereIn('contents.id', $article_ids)
            ->where('contents.content_title', 'LIKE','%'.$keyword.'%')
            ->orWhereHas('details', function($query) use ($keyword){
                $query->where('detail_description','LIKE','%'.$keyword.'%');
            })
            ->leftJoin('users', 'users.id', 'contents.author_id')
            ->where('contents.status', config('constant.status.Active'))
            ->where('contents.content_type', config('constant.explore_type.article'))
            ->latest('contents.id')
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.article'));
            }])
            ->get();
            
            $videos = Video::select(['id','video_title','video_link','video_duration','thumbnail'])
                    ->withCount(['bookmarks as is_bookmark' => function($q) {
                        $q->where('user_id', Auth()->user()->id)
                        ->where('explore_type', config('constant.explore_type.video'));
                    }])
                    ->where('status', config('constant.status.Active'))
                    ->where('video_title', 'LIKE','%'.$keyword.'%')
                    ->orWhereIn('id', $video_keywords_id)
                    ->latest('id')
                    ->get();

            $podcasts = Podcast::select(['podcasts.id','podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcasts.podcast_share_link','ps.series_name as podcast_series'])
                ->withCount(['bookmarks as is_bookmark' => function($q) {
                    $q->where('user_id', Auth()->user()->id)
                    ->where('explore_type', config('constant.explore_type.podcast'));
                }])
                ->join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
                ->where('podcasts.status', config('constant.status.Active'))
                ->where('podcast_title', 'LIKE','%'.$keyword.'%')
                ->latest('podcasts.id')
                ->get();

            $faqs = Faq::with([
                'category' => function($q){
                    $q->leftJoin('categories','categories.id','faq_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
                    ->get();
                }
            ])
            ->where('faqs.status', config('constant.status.active'))
            ->where('faqs.faq_question', 'LIKE','%'.$keyword.'%')
            ->orWhere('faqs.faq_answer', 'LIKE','%'.$keyword.'%')
            ->orWhereIn('id', $faq_keywords_id)
            ->latest('faqs.id')
            ->get(['id','faq_question','faq_answer']);


            return response()->json([
            'topics' => $topics_data,
            'articles' => $articles_data,
            'videos' => $videos,
            'podcasts' => $podcasts,
            'faqs' => $faqs
            ]);
        }
        
        
    }

    public function searchByKeywords($keyword_id)
    {
        $faq_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.faq'))->pluck('explore_id')->toArray();
        
        $topic_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.topic'))->pluck('explore_id')->toArray();

        $article_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.article'))->pluck('explore_id')->toArray();

        $videos_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.video'))->pluck('explore_id')->toArray();

        $podcasts_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.podcast'))->pluck('explore_id')->toArray();

        $topics = Content::with([
            'categories' => function($q){
                $q->leftJoin('categories','categories.id','content_categories.category_id')
                ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                ->get();
            }
        ])
        ->whereIn('contents.id', $topic_ids)
        ->where('contents.status', config('constant.status.Active'))
        ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
        ->withCount(['bookmarks as is_bookmark' => function($q) {
            $q->where('user_id', Auth()->user()->id)
            ->where('explore_type', config('constant.explore_type.topic'));
        }])
        ->latest('contents.id')
        ->take(5)
        ->get();

        $articles = Content::with([
            'categories' => function($q){
                $q->leftJoin('categories','categories.id','content_categories.category_id')
                ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                ->get();
            }
        ])
        ->leftJoin('users', 'users.id', 'author_id')
        ->whereIn('contents.id', $article_ids)
        ->where('contents.status', config('constant.status.Active'))
        ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
        ->withCount(['bookmarks as is_bookmark' => function($q) {
            $q->where('user_id', Auth()->user()->id)
            ->where('explore_type', config('constant.explore_type.article'));
        }])
        ->latest('contents.id')
        ->take(5)
        ->get();

        $videos = Video::select(['id','video_title','video_link','video_duration','thumbnail'])
                        ->withCount(['bookmarks as is_bookmark' => function($q) {
                            $q->where('user_id', Auth()->user()->id)
                            ->where('explore_type', config('constant.explore_type.video'));
                        }])
                        ->whereIn('videos.id', $videos_ids)
                        ->where('videos.status', config('constant.status.Active'))
                        ->latest('videos.id')
                        ->take(5)
                        ->get();

        $podcasts = Podcast::join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
                    ->select(['podcasts.id', 'podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcast_share_link', 'ps.series_name as podcast_series'])
                    ->withCount(['bookmarks as is_bookmark' => function($q) {
                        $q->where('user_id', Auth()->user()->id)
                        ->where('explore_type', config('constant.explore_type.video'));
                    }])
                    ->whereIn('podcasts.id', $podcasts_ids)
                    ->where('podcasts.status', config('constant.status.Active'))
                    ->latest('podcasts.id')
                    ->take(5)
                    ->get();

        $faqs = Faq::with([
            'category' => function($q){
                $q->leftJoin('categories','categories.id','faq_categories.category_id')
                ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
                ->get();
            }
        ])
        ->where('faqs.status', config('constant.status.Active'))
        ->whereIn('faqs.id', $faq_ids)
        ->latest('faqs.id')
        ->take(5)
        ->get(['id','faq_question','faq_answer']);
        

        return response()->json([
            'topics' => $topics,
            'articles' => $articles,
            'videos' => $videos,
            'podcasts' => $podcasts,
            'faqs' => $faqs
        ]);
    }

    public function searchByExploreType($keyword_id, $explore_type_id)
    {
        if($explore_type_id == config('constant.explore_type.faq')){
            $faq_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.faq'))->pluck('explore_id')->toArray();

            $data = Faq::with([
                'category' => function($q){
                    $q->leftJoin('categories','categories.id','faq_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
                    ->get();
                }
            ])
            ->where('faqs.status', config('constant.status.Active'))
            ->whereIn('faqs.id', $faq_ids)
            ->latest('faqs.id')
            ->select(['id','faq_question','faq_answer'])
            ->paginate(config('constant.page_size'));
        }

        if($explore_type_id == config('constant.explore_type.topic')){
            $topic_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.topic'))->pluck('explore_id')->toArray();

            $data = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                    ->get();
                }
            ])
            ->whereIn('contents.id', $topic_ids)
            ->where('contents.status', config('constant.status.Active'))
            ->latest('contents.id')
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.topic'));
            }])
            ->paginate(config('constant.page_size'));
        }

        if($explore_type_id == config('constant.explore_type.article')){
            $article_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.article'))->pluck('explore_id')->toArray();

            $data = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                    ->get();
                }
            ])
            ->leftJoin('users', 'users.id', 'author_id')
            ->whereIn('contents.id', $article_ids)
            ->where('contents.status', config('constant.status.Active'))
            ->latest('contents.id')
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
            ->withCount(['bookmarks as is_bookmark' => function($q) {
                $q->where('user_id', Auth()->user()->id)
                ->where('explore_type', config('constant.explore_type.article'));
            }])
            ->paginate(config('constant.page_size'));
        }

        if($explore_type_id == config('constant.explore_type.video')){
            $videos_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.video'))->pluck('explore_id')->toArray();

            $data = Video::select(['id','video_title','video_link','video_duration','thumbnail'])
                        ->withCount(['bookmarks as is_bookmark' => function($q) {
                            $q->where('user_id', Auth()->user()->id)
                            ->where('explore_type', config('constant.explore_type.video'));
                        }])
                        ->whereIn('videos.id', $videos_ids)
                        ->latest('videos.id')
                        ->where('videos.status', config('constant.status.Active'))
                        ->paginate(config('constant.page_size'));
        }

        if($explore_type_id == config('constant.explore_type.podcast')){
            $podcasts_ids = ExploreKeyword::where('keyword_id', $keyword_id)->where('explore_type', config('constant.explore_type.podcast'))->pluck('explore_id')->toArray();

            $data = Podcast::join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
                    ->select(['podcasts.id', 'podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcast_share_link', 'ps.series_name as podcast_series'])
                    ->withCount(['bookmarks as is_bookmark' => function($q) {
                        $q->where('user_id', Auth()->user()->id)
                        ->where('explore_type', config('constant.explore_type.video'));
                    }])
                    ->whereIn('podcasts.id', $podcasts_ids)
                    ->latest('podcasts.id')
                    ->where('podcasts.status', config('constant.status.Active'))
                    ->paginate(config('constant.page_size'));
        }

        $result['data'] = $data->items();
        $result['pagination'] = [
            "per_page" => $data->perPage(),
            "current_page" => $data->currentPage(),
            "last_page" => $data->lastPage()
        ];

        return $result;
    }

    public function bookmark($user_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'explore_id' => 'required',
                'explore_type' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $has_bookmark = Bookmark::where('user_id',$user_id)
        ->where('explore_id',$request->explore_id)
        ->where('explore_type',$request->explore_type)
        ->first();

        if($has_bookmark){
            $result = true;
            DB::beginTransaction();
            try {
                $bookmark = Bookmark::where('user_id',$user_id)
                    ->where('explore_id',$request->explore_id)
                    ->where('explore_type',$request->explore_type)
                    ->delete();
                if(!$bookmark){
                    DB::rollback();
                    $result = false;
                }
                DB::commit();
            } catch (\Throwable $th) {
                dd($th);
            }

            if (!$result) {
                return response()->json([
                    'success' => false,
                    'message' => 'Failed'
                ], 400);
            } else return response()->json([
                'success' => true,
                'is_bookmark' => 0,
                'message' => 'Completed'
            ]);
        }else {
            $success = true;
            DB::beginTransaction();

            try {
                $bookmark = new Bookmark();
                $bookmark->user_id = $user_id;
                $bookmark->explore_id = $request->explore_id;
                $bookmark->explore_type = $request->explore_type;

                if (!$bookmark->save()) {
                    DB::rollback();
                    $success = false;
                }

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();
                $success = false;
            }

            if (!$success) {
                return response()->json([
                    'success' => false,
                    'message' => 'Failed'
                ], 400);
            } else return response()->json([
                'success' => true,
                'is_bookmark' => 1,
                'message' => 'Completed'
            ]);
        }
        
    }

    public function commentArticle($user_id, $article_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'comment' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $success = true;
        DB::beginTransaction();

        try {
            $content_comment = new ContentComment();
            $content_comment->user_id = $user_id;
            $content_comment->content_id = $article_id;
            $content_comment->comment = $request->comment;
            $content_comment->need_reply = 1;

            if (!$content_comment->save()) {
                DB::rollback();
                $success = false;
            }

            $content = Content::findOrFail($article_id)->update(['is_new_comment' => 1]);
            if (!$content) {
                DB::rollback();
                $success = false;
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $success = false;
        }

        if (!$success) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed'
        ]);
    }

    public function replyCommentArticle($user_id, $article_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'content_comment_id' => 'required',
                'comment' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $success = true;
        DB::beginTransaction();

        try {
            $comment_reply = new CommentReply();
            $comment_reply->user_id = $user_id;
            $comment_reply->content_comment_id = $request->content_comment_id;
            $comment_reply->reply_msg = $request->comment;
            $comment_reply->reply_type = config('constant.reply_type.user');

            if (!$comment_reply->save()) {
                DB::rollback();
                $success = false;
            }

            $content_comment = ContentComment::findOrFail($request->content_comment_id)->update(['need_reply' => 1]);
            if (!$content_comment) {
                DB::rollback();
                $success = false;
            }
            $content_id = ContentComment::findOrFail($request->content_comment_id);
            $content = Content::findOrFail($content_id->content_id)->update(['is_new_comment' => 1]);
            if (!$content) {
                DB::rollback();
                $success = false;
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $success = false;
        }

        if (!$success) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed'
        ]);
    }

    public function getComments($user_id, $article_id)
    {
        $content_comments = ContentComment::where('content_id', $article_id);

        if($content_comments->count() > 0){
            $comments = $content_comments->leftJoin('users as u', 'u.id', 'content_comments.user_id')
            ->select('content_comments.id','content_comments.comment','content_comments.created_at as comment_date','u.name as name','u.avator_img_id', 'u.id as user_id',
            DB::raw(('(CASE 
            WHEN user_id = '.Auth()->user()->id.' THEN 1 ELSE 0 END
            ) AS is_reply')))
            ->withCount('replies')
            ->latest('content_comments.id')
            ->paginate(config('constant.page_size'));
            $result['comments'] = $comments->items();
            $result['pagination'] = [
                "per_page" => $comments->perPage(),
                "current_page" => $comments->currentPage(),
                "last_page" => $comments->lastPage()
            ];
        }else {
            $result['comments'] = [];
            $result['pagination'] = [
                "per_page" => 0,
                "current_page" => 0,
                "last_page" => 0
            ];
        }

        return $result;
    }

    public function getRepliesByComment($user_id, $article_id, $comment_id)
    {
        $comment = ContentComment::find($comment_id);
        
        $replies = CommentReply::leftJoin('users as u', 'u.id', 'comment_replies.user_id')
        ->where('comment_replies.content_comment_id', $comment_id)
        ->orderBy('comment_replies.id', 'DESC')
        ->select(['reply_type', 'reply_msg','comment_replies.created_at as comment_date', 'u.name', 'u.id as user_id','u.avator_img_id'])
        ->get();

        $result['replies'] = $replies;

        return $result;
    }

    public function getBookmarkLists(Request $request)  
    {
        $sorting = ($request->sortBy == null ) ? 'DESC' : $request->sortBy;
        $explore_type = $request->explore_type_id;

        $explore_ids = Bookmark::where('explore_type', $explore_type)
            ->where('user_id', auth()->user()->id)
            ->pluck('explore_id')
            ->toArray();

        if($explore_type == config('constant.explore_type.article')){

            $data = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                    ->get();
                }
            ])
            ->leftJoin('users', 'users.id', 'author_id')
            ->where('contents.status', config('constant.status.Active'))
            ->whereIn('contents.id', $explore_ids)
            ->orderBy('contents.id', $sorting)
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time', 'users.name'])
            ->paginate(config('constant.page_size'));
        }

        if($explore_type == config('constant.explore_type.topic')){

            $data = Content::with([
                'categories' => function($q){
                    $q->leftJoin('categories','categories.id','content_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','content_categories.id','content_categories.content_id','content_categories.category_id')
                    ->get();
                }
            ])
            ->where('contents.status', config('constant.status.Active'))
            ->whereIn('id', $explore_ids)
            ->orderBy('contents.id', $sorting)
            ->select(['contents.id','contents.content_cover_photo', 'contents.content_title', 'contents.read_time'])
            ->paginate(config('constant.page_size'));
        }

        if($explore_type == config('constant.explore_type.video')){

            $data = Video::where('status', config('constant.status.Active'))
            ->whereIn('id', $explore_ids)
            ->orderBy('id', $sorting)
            ->select(['id','video_title', 'thumbnail', 'video_duration', 'video_link'])
            ->paginate(config('constant.page_size'));
        }

        if($explore_type == config('constant.explore_type.podcast')){

            $data = Podcast::join('podcast_series as ps', 'ps.id', 'podcasts.podcast_series_id')
            ->where('podcasts.status', config('constant.status.Active'))
            ->whereIn('podcasts.id', $explore_ids)
            ->orderBy('podcasts.id', $sorting)
            ->select(['podcasts.id','podcasts.podcast_title', 'podcasts.thumbnail', 'podcasts.podcast_duration', 'podcasts.podcast_link','podcast_share_link','ps.series_name as podcast_series'])
            ->paginate(config('constant.page_size'));
        }

        $result['data'] = $data->items();
        $result['pagination'] = [
            "per_page" => $data->perPage(),
            "current_page" => $data->currentPage(),
            "last_page" => $data->lastPage()
        ];
        return $result;
    }

    public function getPolicy()
    {
        $data = Policy::select(['id', 'title', 'description'])->first();
        return $data;
    }
}
