<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use App\Content;
use App\ContentQuiz;
use App\AnsweredQuiz;
use App\Quiz;
use App\Weeklyquiz;
use App\User;

class QuizController extends Controller
{
    public function getQuizByTopic($user_id, $topic_id)
    {
        $topic = Content::findOrFail($topic_id);

        $content_quizzes = ContentQuiz::where('content_id', $topic_id)->pluck('quiz_id');

        $answered_quizzes = AnsweredQuiz::where('user_id', $user_id)
        ->whereIn('quiz_id', $content_quizzes)
        ->where('explore_type', 1)
        ->pluck('quiz_id');
        
        $user_quizzes = ContentQuiz::whereNotIn('quiz_id', $answered_quizzes)
        ->where('content_id', $topic_id)
        ->pluck('quiz_id');
        
        $quiz = Quiz::with('options:id,quiz_id,quiz_option,is_correct')->whereIn('quizzes.id',$user_quizzes)->get(['quizzes.id','quizzes.quiz_question','quizzes.quiz_type','quizzes.description']);

        $result['reward_point'] = config('constant.reward.topic');
        $result['quizzes'] = $quiz;

        return $result;
    }

    public function checkPoint(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required',
                'chance_type' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $user_point = auth()->user()->user_point;

        if($request->chance_type == 'topic'){
            $has_chance = ($user_point>= config('constant.chance.topic')) ? 1 : 0;
        }
        if($request->chance_type == 'game'){
            $has_chance = ($user_point>= config('constant.chance.topic')) ? 1 : 0;
        }
        if($request->chance_type == 'basic_course'){
            $has_chance = ($user_point>= config('constant.chance.topic')) ? 1 : 0;
        }
        if($request->chance_type == 'intermediate_course'){
            $has_chance = ($user_point>= config('constant.chance.topic')) ? 1 : 0;
        }
        if($request->chance_type == 'advanced_course'){
            $has_chance = ($user_point>= config('constant.chance.topic')) ? 1 : 0;
        }

        return $has_chance;
    }

    public function usePoint(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required',
                'chance_type' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $result = true;
        DB::beginTransaction();

        try {
            if($request->chance_type == 'topic'|| $request->chance_type == 'game' || $request->chance_type == 'basic_course'){
                $points = auth()->user()->user_point - 100;

                $user = User::find(auth()->user()->id)->update(['user_point' => $points]);
            }
            if($request->chance_type == 'intermediate_course'){
                $points = auth()->user()->user_point - config('constant.chance.intermediate_course');

                $user = User::find(auth()->user()->id)->update(['user_point' => $points]);
            }
            if($request->chance_type == 'advanced_course'){
                $points = auth()->user()->user_point - config('constant.chance.advanced_course');

                $user = User::find(auth()->user()->id)->update(['user_point' => $points]);
            }
            if(!$user){
                $result = false;
                DB::rollback();
            }
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed'
        ]);
    }

    public function updateQuizAnswer($user_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'quiz_id' => 'required',
                'is_correct_answer' => 'required',
                'explore_type' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        
        $result = true;
        if($request->is_correct_answer == 1){
            DB::beginTransaction();
            try {
                $data = $request->all();
                $data['user_id'] = $user_id;
                $answered_quiz = AnsweredQuiz::create($data);

                if(!$answered_quiz){
                    $result = false;
                    DB::rollback();
                }

                $reward_point = auth()->user()->user_point + config('constant.reward.topic');
                $user = User::find($user_id)->update(['user_point' => $reward_point]);
                if(!$user){
                    $result = false;
                    DB::rollback();
                }

                DB::commit();
            } catch (\Throwable $th) {
                dd($th);
            }
        }

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Successfully updated'
        ], 201);
    }

    public function game($user_id, Request $request)
    {
        $game_type = $request->game_type;

        $answered_quizzes = AnsweredQuiz::where('user_id', $user_id)
            ->where('explore_type', config('constant.explore_type.game'))
            ->pluck('quiz_id');

        $quizzes = $this->gameQuiz($answered_quizzes, $game_type);

        if(count($quizzes) == 0 || count($quizzes) < 10){
            $result = true;
            DB::beginTransaction();
            try {
                $delete_answered_quiz = AnsweredQuiz::where('user_id', $user_id)
                                    ->whereIn('quiz_id', $answered_quizzes)
                                    ->delete();

                if(!$delete_answered_quiz){
                    $result = false;
                    DB::rollback();
                }

                DB::commit();

            } catch (\Throwable $th) {
                dd($th);
            }

            $ans = AnsweredQuiz::where('user_id', $user_id)
                ->where('explore_type', config('constant.explore_type.game'))
                ->pluck('quiz_id');

                $quizzes = $this->gameQuiz($ans, $game_type);
        }

        return response()->json([
            'user_point' => User::findOrFail($user_id)->user_point,
            'quizzes' => $quizzes
        ]);
    }

    protected function gameQuiz($ans, $type){
        $game_quiz = Quiz::with('options:id,quiz_id,quiz_option,is_correct')
        ->where('status', config('constant.status.Active'))
        ->where('quiz_content_type', config('constant.quiz_content_type.Game'))
        ->whereNotIn('id', $ans)
        ->inRandomOrder();

        if($type == '1' || $type == '3'){
            $quizzes = $game_quiz->take(10)
            ->get(['quizzes.id','quizzes.quiz_question','quizzes.quiz_type','quizzes.description']);
            
        }
        if($type == '2'){
            $quizzes = $game_quiz->take(9)
            ->get(['quizzes.id','quizzes.quiz_question','quizzes.quiz_type','quizzes.description']);
        }
        

        return $quizzes;
    }

    public function getWeeklyQuiz(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'date' => 'required'
            ]
        );

        $quizz = Weeklyquiz::with('options:id,quiz_option,is_correct,weekly_quiz_id')->where('schedule_date', $request->date)->first(['id', 'weekly_fact', 'quiz_question', 'answer_description']);

        return $quizz;
    }

    public function updateGameResult($user_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'game_type' => 'required',
                'point' => 'required',
                'is_win' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            if(!AnsweredQuiz::saveQuiz($request->quizzes,$user_id)){
                $result = false;
                DB::rollback();
            }

            if($request->is_win == true){
                if($request->game_type == 3)
                    $points = auth()->user()->user_point + $request->point;
                

                if($request->game_type == 1)
                    $points = auth()->user()->user_point + config('constant.reward.quiz_squad');

                if($request->game_type == 2)
                    $points = auth()->user()->user_point + config('constant.reward.tic_tac_toe');
                

                $user = User::find(auth()->user()->id)->update(['user_point' => $points]);

                if(!$user){
                    $result = false;
                    DB::rollback();
                }
            }

            DB::commit();

        } catch (\Throwable $th) {
            dd($th);
        }

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed'
        ]);
    }

    public function getLeaderBoardList()
    {
        $users = User::where('role_id', config('constant.user_role.user'))
        ->orderBy('user_point', 'DESC')
        ->orderBy('created_at', 'ASC')
        ->take(20)
        ->select(['id', 'name', 'avator_img_id', 'user_point'])
        ->get();

        return $users;
    }
}
