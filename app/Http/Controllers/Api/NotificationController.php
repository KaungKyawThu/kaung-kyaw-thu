<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\NewEvent;
use App\User;
use Illuminate\Http\Request;
use DB;

class NotificationController extends Controller
{
    public function getNotification($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->unread_noti_count = 0;
        $user->save();

        $notification = NewEvent::where('status',2)
            ->select([
                'id',
                'title',
                DB::raw("CONCAT (SUBSTRING(description,1,50), '...') as cover_description")
            ])
            ->orderBy('id', 'DESC')
            ->paginate(config('constant.page_size'));

        $result['notification'] = $notification->items();
        $result['pagination'] = [
            "per_page" => $notification->perPage(),
            "current_page" => $notification->currentPage(),
            "last_page" => $notification->lastPage()
        ];
        return $result;
    }

    public function getNotiUnreadCount($user_id){
        $user = User::findOrFail($user_id);

        return response()->json([
            "unread_noti_count" => $user->unread_noti_count
        ]);
    }
    
}
