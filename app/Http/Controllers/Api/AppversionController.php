<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Appversion;

class AppversionController extends Controller
{
    public function getCurrentVersion()
    {
        return Appversion::orderBy('id','DESC')->first(['id','version_no','link', 'ios_link','is_force_update','description']);
    }
}
