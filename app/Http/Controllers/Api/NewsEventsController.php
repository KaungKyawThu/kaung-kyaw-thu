<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\NewEvent;
use Illuminate\Http\Request;
use DB;

class NewsEventsController extends Controller
{
    public function getNewsEventsList()
    {
        $news_event = NewEvent::where('status',2)
            ->select([
                'id',
                'type',
                'title',
                DB::raw("CONCAT (SUBSTRING(description,1,50), '...') as cover_description"),
                'schedule_date as published_at'
            ])
            ->orderBy('id', 'DESC')
            ->paginate(config('constant.page_size'));

        $result['news_event'] = $news_event->items();
        $result['pagination'] = [
            "per_page" => $news_event->perPage(),
            "current_page" => $news_event->currentPage(),
            "last_page" => $news_event->lastPage()
        ];
        return $result;
    }

    public function getNewsEventDetail($id)
    {
        return NewEvent::select('id', 'type', 'title', 'description', 'schedule_date as published_at')->find($id);
    }
}
