<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\CourseBook;
use App\CourseVideo;
use App\Http\Controllers\Controller;
use App\User;
use App;
use App\Quiz;
use App\QuizSet;
use App\UserCertificate;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class CourseController extends Controller
{
    public function getCourseList($user_id, Request $request)
    {
        $user = User::findOrFail($user_id);

        $current_course = $user->course_level_id + 1;

        $course_name_column = ($request->language == 'mm') ? 'course_name_mm' : 'course_name_en';

        $courses = Course::where('status', config("constant.status.Active"))
            ->select([
                'id',
                $course_name_column.' as course_name',
                'preview_text',
                DB::raw(('(CASE 
                        WHEN id = ' . $current_course . ' THEN ' . config("constant.user_course_status.open") . '
                        WHEN id > ' . $current_course . ' THEN ' . config("constant.user_course_status.locked") . '
                        ELSE ' . config("constant.user_course_status.completed") . '
                        END) AS status')),
                DB::raw(('(CASE 
                        WHEN id < ' . $current_course . ' THEN true
                        ELSE false
                        END) AS is_completed')),
                DB::raw(('IFNULL((SELECT is_generated from user_certificates where user_id = ' . $user->id . ' and course_level_id = courses.id), 0) AS is_generated')),
                DB::raw(('IFNULL((SELECT certificate_link from user_certificates where user_id = ' . $user->id . ' and course_level_id = courses.id), "") AS certificate_link')),
            ])
            ->orderBy('order', 'ASC')
            ->get();

        return response()->json([
            'house_link' => ($user->course_level_id == 0) ? "" : Course::find($user->course_level_id)->house_link,
            'courses' => $courses
        ]);
    }

    public function getCourseDetail($user_id, $course_id, Request $request)
    {
        $user = User::findOrFail($user_id);

        if ($course_id > $user->course_level_id + 1)
            abort('403', 'Course has been locked');

        $category_column = ($request->language == 'mm') ? 'name_mm' : 'name_en';
        $course = Course::findOrFail($course_id);
        $current_course = $user->course_level_id + 1;
        $chapters = [];

        foreach ($course->chapters as $chapter) {
            if ($chapter->status == config('constant.status.Active')) {
                $course_chapter = [];
                $course_videos = CourseVideo::where('course_chapter_id', $chapter->id)
                    ->select([
                        'id',
                        'course_video_link',
                        'video_title',
                        'video_duration',
                        'video_thumbnail'
                    ])
                    ->get();
    
                $course_books = CourseBook::leftJoin('contents', 'contents.id', '=', 'course_books.content_id')
                    ->leftJoin('content_categories', 'contents.id', 'content_categories.content_id')
                    ->leftJoin('categories', 'categories.id', '=', 'content_categories.category_id')
                    ->where('course_books.course_chapter_id', $chapter->id)
                    ->select([
                        'contents.id',
                        'contents.content_title',
                        'contents.content_cover_photo',
                        'contents.read_time',
                        DB::raw('GROUP_CONCAT(' . $category_column . ') as categories'),
                    ])
                    ->groupBy('contents.id')
                    ->get();
    
                $course_chapter = [
                    'name' => ($request->language == 'mm')? $chapter->chapter_name_mm : $chapter->chapter_name,
                    'videos' => $course_videos,
                    'books' => $course_books
                ];
    
                array_push($chapters, $course_chapter); 
            }
        }

        return response()->json([
            'id' => $course->id,
            'course_name' => ($request->language == 'mm') ? $course->course_name_mm : $course->course_name_en,
            'description' => ($request->language == 'mm') ? $course->course_description_mm : $course->course_description,
            'chapters_count' => $course->chapters->where('status', config('constant.status.Active'))->count(),
            'is_completed' => ($course->id < $current_course) ? 1 : 0,
            'chapters' => $chapters
        ]);
    }

    public function getCourseQuiz($user_id, $course_id)
    {
        $user = User::findOrFail($user_id);
        $current_course = $user->course_level_id + 1;

        if ($course_id > $current_course || $course_id < $current_course)
            abort('403', 'You are not allowed to start test');

        $course = Course::findOrFail($course_id);

        $rand_quiz_set = QuizSet::where('course_id', $course->id)->inRandomOrder()->get()->first();

        $quizzes = Quiz::leftJoin('quiz_set_details', 'quiz_set_details.quiz_id', '=', 'quizzes.id')
            ->where('quiz_set_details.quiz_set_id', $rand_quiz_set->id)
            ->where('quizzes.status', config("constant.status.Active"))
            ->with('options:id,quiz_id,quiz_option,is_correct')
            ->select([
                'quizzes.id',
                'quizzes.quiz_question',
                'quizzes.quiz_type',
                'quizzes.description'
            ])
            ->get();

        return $quizzes;
    }

    public function completeCourse($user_id, $course_id)
    {
        if ( UserCertificate::where('user_id', $user_id)->where('course_level_id', $course_id)->count() > 0)
            abort('403', 'Certificate has already existed.');

        $success = true;
        DB::beginTransaction();

        try {
            $user = User::findOrFail($user_id);
            $user->course_level_id = $course_id;

            if (!$user->save()) {
                DB::rollback();
                $success = false;
            }

            $user_certificate = UserCertificate::create([
                'user_id' => $user->id,
                'certificate_link' => "",
                'course_level_id' => $course_id,
                'is_generated' => false
            ]);
            if (!$user_certificate) {
                DB::rollback();
                $success = false;
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
        }

        if (!$success) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed'
        ]);
    }

    public function getCertificateUserInfo($user_id)
    {
        $user = User::findOrFail($user_id);

        return response()->json([
            'certificate_name' => $user->certificate_name,
            'certificate_phone_no' => $user->certificate_phone_no
        ]);
    }

    public function generateCertificate($user_id, $course_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'certificate_name' => 'required',
                'certificate_phone_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $success = true;
        DB::beginTransaction();

        try {
            $user = User::findOrFail($user_id);
            $user->certificate_name = $request->certificate_name;
            $user->certificate_phone_no = $request->certificate_phone_no;

            if (!$user->save()) {
                DB::rollback();
                $success = false;
            }

            $cert_link = $this->uploadCertificate($user, $request->certificate_name, $course_id);

            $user_cert = UserCertificate::where('user_id', $user->id)->where('course_level_id', $course_id)->get()->first();

            $user_cert->certificate_link = $cert_link;
            $user_cert->is_generated = true;

            if (!$user_cert->save()) {
                DB::rollback();
                $success = false;
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
        }

        if (!$success) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed',
            'certificate_link' => $cert_link
        ]);
    }

    public function uploadCertificate($user, $name, $level)
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf = $pdf->loadView('certificate', compact('level', 'name'));

        $pdf_name = config("web_constant.courses.$level") . "_" . $user->user_no;
        $pdf_content = $pdf->download()->getOriginalContent();

        $path = 'certificates/' . $pdf_name . '.pdf';

        Storage::disk('s3')->put($path, $pdf_content);

        return config("constant.s3_cloudfront") . '/' . $path;
    }

    public function getCourseHouseLink($user, $course_id)
    {
        $house_link = Course::findOrFail($course_id);

        return $house_link->house_link;
    }
}
