<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Faq;
use App\Category;
use App\ClinicService;
use App\ClinicInfo;
use App\Township;
use App\Aboutus;
use App\Chat;
use App\User;
use App\Region;
use DB;
use Validator;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    public function allCategories()
    {
        return Category::where('status', config('constant.status.Active'))->select(['id', 'name_en','name_mm', 'is_popular'])->get();
    }

    public function getFaqs(Request $request)
    {
        if ($request->category_id == null) {
            $faqs = Faq::with([
                'category' => function($q){
                    $q->leftJoin('categories','categories.id','faq_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
                    ->get();
                }
            ])
            ->where('faqs.status', config('constant.status.Active'))
            ->select(['id','faq_question','faq_answer'])
            ->latest('id')
            ->take(10)
            ->get();
            return response()->json([
                'categories' => $this->allCategories(),
                'faqs' => $faqs
            ]);
        }else {
            $cateogry_id = $request->category_id;
            $category = Category::find($cateogry_id, ['id','name_en', 'name_mm']);
            $faqs = Faq::with([
                'category' => function($q){
                    $q->leftJoin('categories','categories.id','faq_categories.category_id')
                    ->select('categories.name_en','categories.name_mm','faq_categories.category_id as id','faq_categories.faq_id')
                    ->get();
                }
            ])
            ->leftJoin('faq_categories as fc', 'faqs.id', 'fc.faq_id')
            ->where('fc.category_id', $cateogry_id)
            ->where('faqs.status', config('constant.status.Active'))
            ->select(['faqs.id','faqs.faq_question','faqs.faq_answer'])
            ->latest('faqs.id')
            ->paginate(config('constant.page_size'));

            $result['category'] = $category;
            $result['faqs'] = $faqs->items();
            $result['pagination'] = [
                "per_page" => $faqs->perPage(),
                "current_page" => $faqs->currentPage(),
                "last_page" => $faqs->lastPage()
            ];

            return $result;
        }
    }

    public function getServiceTypes(Request $request)
    {
        if($request->lang == "mm")
        $services = ClinicService::all(['id', 'service_mm_name as service_name']);

        if($request->lang == "en")
        $services = ClinicService::all(['id', 'service_name']);

        return $services;

    }

    public function getServices(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'lang' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        if($request->township_id == null){
            $township_id = auth()->user()->township_id;
        }else {
            $township_id = $request->township_id;
        }

        if($request->service_id == null){
            $service_id = ClinicService::first()->id;
        }else {
            $service_id = $request->service_id;
        }

        $township = Township::find($township_id);
        $hotline = Aboutus::first();
        if($request->lang == 'mm'){
            $township_name = $township->mm_unicode;
            $service_info = ClinicService::where('id', $service_id)
            ->where('status', config('constant.status.Active'))
            ->select(['id', 'service_mm_name as service_name', 'service_mm_description as service_description'])
            ->first();
    
            $clinics = ClinicInfo::leftJoin('clinic_organizations as org', 'org.id', 'clinic_infos.clinic_organization_id')
                ->where('clinic_infos.clinic_service_id', $service_id)
                ->where('clinic_infos.township_id', $township_id)
                ->where('clinic_infos.status', config('constant.status.Active'))
                ->select(['clinic_infos.id','clinic_infos.clinic_mm_name as clinic_name', 'clinic_infos.clinic_mm_address as clinic_address', 'clinic_infos.clinic_mm_phone_no', 'clinic_infos.clinic_phone_no', 'clinic_infos.clinic_website', 'org.organization_mm_name as org_name', 'clinic_infos.clinic_description_mm as clinic_description', 'clinic_infos.clinic_fb_link', 'clinic_infos.facebook_page_id'])
                ->paginate(config('constant.page_size'));
        }

        if($request->lang == 'en'){
            $township_name = $township->name;
            $service_info = ClinicService::where('id', $service_id)
            ->where('status', config('constant.status.Active'))
            ->select(['id', 'service_name', 'service_description'])
            ->first();
    
            $clinics = ClinicInfo::leftJoin('clinic_organizations as org', 'org.id', 'clinic_infos.clinic_organization_id')
            ->where('clinic_infos.clinic_service_id', $service_id)
            ->where('clinic_infos.township_id', $township_id)
            ->where('clinic_infos.status', config('constant.status.Active'))
            ->select(['clinic_infos.id','clinic_infos.clinic_name', 'clinic_infos.clinic_address', 'clinic_infos.clinic_phone_no', 'clinic_infos.clinic_mm_phone_no', 'clinic_infos.clinic_website', 'org.name as org_name','clinic_infos.clinic_description_en as clinic_description', 'clinic_infos.clinic_fb_link', 'clinic_infos.clinic_fb_link', 'clinic_infos.facebook_page_id'])
            ->paginate(config('constant.page_size'));
        }

        $result['service'] = $service_info;
        $result['township'] = $township_name;
        $result['male_phone_no'] = $hotline->male_phone_no;
        $result['male_mm_phone_no'] = $hotline->male_mm_phone_no;
        $result['female_phone_no'] = $hotline->female_phone_no;
        $result['female_mm_phone_no'] = $hotline->female_mm_phone_no;
        $result['clinics'] = $clinics->items();
        $result['pagination'] = [
            "per_page" => $clinics->perPage(),
            "current_page" => $clinics->currentPage(),
            "last_page" => $clinics->lastPage()
        ];

        return $result;
        
    }

    public function contactUs()
    {
        return Aboutus::first();
    }

    public function getServiceTownships()
    {
        $clinic_townships = ClinicInfo::leftJoin('townships', 'townships.id', 'clinic_infos.township_id')
        ->distinct('clinic_infos.township_id')
        ->orderBy('townships.id', 'ASC')
        ->select('townships.id', 'townships.name', 'townships.mm_unicode')
        ->get();

        return $clinic_townships;
    }

    public function sendMessage($user_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'message' => 'required',
                'message_type' => 'required',
                'chat_type' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            $data = $request->all();

            if($request->message_type==2){  //if photo message
                $file = $request->file('message');
                $photo = Storage::disk('s3')->put('images', $file);
                $message = Storage::disk('s3')->url($photo);
            }else{
                $message = $request->message;
            }

            $data['user_id'] = $user_id;
            $data['message'] = $message;
            $ans = Chat::create($data);
            if(!$ans){
                $result = false;
                DB::rollback();
            }

            DB::commit();
            
        } catch (\Throwable $th) {
            dd($th);
        }

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed'
        ], 201);
    }

    public function getAllMessages($user_id)
    {
        $msg = Chat::where('user_id', $user_id)
        ->orderBy('id', 'DESC')
        ->select(['id','user_id','message','message_type','chat_type','created_at as date'])
        ->paginate(30);

        $ans = true;
        DB::beginTransaction();
        try {
            $user = User::find($user_id)->update(['is_new_msg' => 0]);
            if(!$user){
                $ans = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if (!$ans) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        }else {
            $result['messages'] = $msg->items();
            $result['pagination'] = [
                "per_page" => $msg->perPage(),
                "current_page" => $msg->currentPage(),
                "last_page" => $msg->lastPage()
            ];
    
            return $result;
        }
    }

    public function getChatNoti($user_id){
        $noti = User::findOrFail($user_id);

        return response()->json([
            'is_new_message' => $noti->is_new_msg == 1 ? true : false
        ]);
    }

    public function getRegions()
    {
        $regions = Region::select(['id', 'name', 'mm_unicode'])->get();

        return $regions;
    }

    public function townshipsByRegions(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'region_id' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $townships = Township::where('region_id', $request->region_id)
        ->select('id', 'name', 'mm_unicode', 'region_id')
        ->get();

        return $townships;
    }
}
