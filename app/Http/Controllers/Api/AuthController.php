<?php

namespace App\Http\Controllers\Api;

use App\Fact;
use App\Http\Controllers\Controller;
use App\SecurityQuestion;
use App\User;
use App\UserSecurityAnswer;
use Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'gender_id' => 'required',
                'region_id' => 'required',
                'township_id' => 'required',
                'birth_year' => 'required',
                'nick_name' => 'required',
                'avator_img_id' => 'required',
                'fcm_token' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $user = $request->all();
        $user['role_id'] = Role::where('name', 'User')->pluck('id')->first();
        $user['status'] = config('constant.user_status.active');
        $user['avator_img_id'] = $request->avator_img_id;
        $user['user_point'] = 0;
        $user['unread_noti_count'] = 0;
        $user['is_new_msg'] = 0;
        $user['course_level_id'] = 0;

        $user = User::create($user);
        $user->user_no = $user->id + 111;
        $user->name = $user->nick_name . '_' . $user->user_no;

        $user->save();

        $jwtToken = auth()->login($user);

        return $this->responseForHome($user, $jwtToken);
    }

    public function binding_account($user_id, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'phone_no' => 'required|unique:users|regex:/^([0-9\s\-\+\(\)]*)$/|min:9',
                'password' => 'required',
                'security_questions' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $success = true;
        DB::beginTransaction();

        try {
            $user = User::findOrFail($user_id);
            $user->phone_no = $request->get('phone_no');
            $user->password = Hash::make($request->get('password'));

            if (!$user->save()) {
                DB::rollback();
                $success = false;
            }

            $user_sesecurity_questions = $request->security_questions;

            for ($i = 0; $i < count($user_sesecurity_questions); $i++) {

                $ans = UserSecurityAnswer::create([
                    'user_id' => $user->id,
                    'security_question_id' => $user_sesecurity_questions[$i]['security_question_id'],
                    'security_choice_id' => $user_sesecurity_questions[$i]['security_choice_id'],
                ]);

                if (!$ans) {
                    DB::rollback();
                    $success = false;
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
        }

        if (!$success) {
            return response()->json([
                'success' => false,
                'message' => 'Failed'
            ], 400);
        } else return response()->json([
            'success' => true,
            'message' => 'Completed'
        ]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'phone_no' => 'sometimes|regex:/^([0-9\s\-\+\(\)]*)$/|min:9',
                'password' => 'sometimes|min:6',
                'fcm_token' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        if (!auth()->attempt(request(['phone_no', 'password']))) {
            return response()->json(['error' => 'Invalid Credentials'], 401);
        }

        $user = User::find(auth()->user()->id);

        if (($user->status != config('constant.user_status.active')) || ($user->role_id != config('constant.user_role.user')))
            abort('401', 'Unauthorized');

        $jwtToken = auth()->login($user);

        $user->fcm_token = $request->get('fcm_token');
        $user->save();

        return $this->responseForHome($user, $jwtToken);
    }

    public function getSecurityQuestions()
    {
        return SecurityQuestion::with('choices:id,security_question_id,security_ans')->select(['id', 'question'])->get();
    }

    public function getFacts()
    {
        return Fact::select(['id', 'title', 'description'])->get();
    }

    public function checkPhoneNoExist(Request $request)
    {
        $valid = true;
        if (User::where('phone_no', $request->phone_no)->count() > 0) {
            if( $request->expected )
                $valid = true;
            else
                $valid = false;    
        }
        else{
            if( $request->expected )
                $valid = false;
            else
                $valid = true; 
        }

        return response()->json([
            'is_valid' => $valid
        ]);
    }

    public function forgetPassword(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'phone_no' => 'required',
                'new_password' => 'required',
                'security_questions' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        if (User::where('phone_no', $request->phone_no)->count() != 1) {
            abort('422', 'Phone no does not exist');
        }

        $user = User::where('phone_no', $request->phone_no)->get()->first();
        $user_sesecurity_questions = $request->security_questions;

        for ($i = 0; $i < count($user_sesecurity_questions); $i++) {

            $count = UserSecurityAnswer::where('user_id', $user->id)
                ->where('security_question_id', $user_sesecurity_questions[$i]['security_question_id'])
                ->where('security_choice_id', $user_sesecurity_questions[$i]['security_choice_id'])
                ->count();

            if($count == 0)
                abort('403', 'Wrong Answers');   
        }

        $user->password = Hash::make($request->get('new_password'));
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Password Updated'
        ]);
        
    }

    protected function responseForHome($user, $token)
    {
        return response()->json([
            'user_id' => auth()->user()->id,
            'avator_img_id' => $user->avator_img_id,
            'nick_name' => $user->name,
            'token' => $token,
        ]);
    }

    public function deleteUserAccount(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);

        if($user->phone_no != null && $user->password !=null)
            if (!auth()->attempt(request(['phone_no', 'password']))) {
                return response()->json(['error' => 'Invalid Credentials'], 401);
            }

        $query = 'DELETE answered_quizzes, bookmarks, user_certificates, user_security_answers, users FROM users 
            LEFT JOIN answered_quizzes ON answered_quizzes.user_id = users.id 
            LEFT JOIN bookmarks ON bookmarks.user_id = users.id 
            LEFT JOIN user_certificates ON user_certificates.user_id = users.id 
            LEFT JOIN user_security_answers ON user_security_answers.user_id = users.id 
            where users.id = ?';
        
        DB::delete($query, array($user->id));

        return response()->json([
            'success' => true,
            'message' => 'Completed'
        ]);
    }
}
