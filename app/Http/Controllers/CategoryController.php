<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use App\Category;
use App\ContentCategory;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:category-list|category-create|category-edit|category-delete|category-publish|category-active', ['only' => ['index']]);
        $this->middleware('permission:category-create', ['only' => ['create','store']]);
        $this->middleware('permission:category-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:category-delete', ['only' => ['destroy']]);
        $this->middleware('permission:category-publish', ['only' => ['publish']]);
        $this->middleware('permission:category-active', ['only' => ['active']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new Category();
            // $data = $data->withTrashed();

            if($request->id!=null)
            $data = $data->where('id',$request->id);

            if($request->category_status!=null)
            $data = $data->where('categories.status',$request->category_status);

            $data = $data->latest()->get(['categories.*']);


            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('category-edit') || auth()->user()->can('category-delete') || auth()->user()->can('category-publish') || auth()->user()->can('category-active')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('category-edit'))
                              $btn .= '<a class="dropdown-item text-blue edit-category edit-data" href="#" action="category/'.$row->id.'/edit"> Edit</a>';

                              if(auth()->user()->can('category-publish') && $row->status == "1")
                            $btn .= '<a class="dropdown-item text-blue edit-category publish-data" href="#" action="/category/'.$row->id.'/publish">Publish</a>';

                            if(auth()->user()->can('category-active') && $row->status == "3")
                            $btn .= '<a class="dropdown-item text-blue edit-category active-data" href="#" action="/category/'.$row->id.'/active">Activate</a>';

                            if(auth()->user()->can('category-active') && $row->status == "2")
                            $btn .= '<a class="dropdown-item text-blue edit-category deactive-data" href="#" action="/category/'.$row->id.'/active">Deactivate</a>';

                            if(auth()->user()->can('category-delete') && $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue edit-category delete-data" href="#" action="/category/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('status', function ($row) {
                        return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                    })
                    ->addColumn('is_popular', function ($row) {
                         if($row->is_popular == 1){
                            return "<span class='')> Yes </span>";
                         }
                         return "<span class='')> No </span>";
                    })
                    ->rawColumns(['action','status','is_popular'])
                    ->make(true);
        }

        return view('category.index',[
                   'content_header' => 'Category Lists',
                   'create_permission' => 'category-create',
                   'route' => 'category',
                   'keyword' => 'category'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
        return view('category.create',[
                    'category' => $category,
                    'content_header' => 'Create New Category',
                    'route' => 'category',
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'is_popular' => $request->is_popular ?? 0
        ]);
        $request->validate([
            'name_en' => ['required', Rule::unique('categories')->whereNull('deleted_at'),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'name_mm' => ['required',Rule::unique('categories')->whereNull('deleted_at'),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/']
        ]);
        $request['status'] = 1;

        if ($request->is_popular == null){
            $request["is_popular"] = 0;
        };

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();

             $category = Category::create($data);

             if(!$category){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'Category was created successfully!']);
        }else{
            session(['error' => 'Category can not create!']);
        }

        return redirect('/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    public function publish(Request $request,$id)
    {
        $status = Category::find($id);
        $status = $status->status;

        $result = true;
        DB::beginTransaction();
        try {

         $data['published_by'] = Auth::id();
         $data['published_at'] = date("Y-m-d H:i:s");
             $data['status'] = 2;
             $ans = Category::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Category was published successfully!"]);
        }else{
            session(['error' => "Category can not publish!"]);
        }

        return redirect('category?index='.$request->current_index);

    }

    public function active(Request $request,$id)
    {
        $category = Category::find($id);
        $status = null;

        if($category->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactive";
        }elseif($category->status==3){

            $status = 2;
            $msg1 = "activated";
            $msg2 = "active";
        }

        $result = true;
        DB::beginTransaction();
        try {

             $data['deactivated_by'] = Auth::id();
             $data['deactivated_at'] = date("Y-m-d H:i:s");
             $data['status'] = $status;
             $ans = Category::where('id', $id)->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Category was $msg1 successfully!"]);
        }else{
            session(['error' => "Category can not $msg2!"]);
        }

        return redirect('category?index='.$request->current_index);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Category $category)
    {
        $category->current_index = $request->index;

        return view('category.update',[
                    'category' => $category,
                    'content_header' => 'Update Category',
                    'route' => 'category',
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->merge([
            'is_popular' => $request->is_popular ?? 0
        ]);

        $request->validate([
            'name_en' => ['required', 'string',Rule::unique('categories')->whereNull('deleted_at')->ignore($category->id),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
            'name_mm' => ['required', 'string', Rule::unique('categories')->whereNull('deleted_at')->ignore($category->id),'max:150','regex:/^[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;\(\)\-\.\,]{1,100}[^\~\!\@\#\$\$\%\^\&\*\+\=\\\`\<\>\?\_\:\;]*$/'],
        ]);


        if ($request->is_popular == null){
            $request['is_popular'] = 0;
        }
        else{
            $request['is_popular'] = 1;
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['updated_by'] = Auth::id();
             $ans = $category->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Category was updated successfully!"]);
        }else{
            session(['error' => "Category can not update!"]);
        }

        return redirect('category?index='.$request->current_index);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Category $category)
    {
        $result = true;
        DB::beginTransaction();
        try {
             $ans = Category::where('id',$category->id)
                        ->update([
                            'deleted_by' => Auth::id(),
                        ]);
             $category->delete();
             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Category was deleted successfully!"]);
        }else{
            session(['error' => "Category can not delete!"]);
        }

        return redirect('category?index='.$request->current_index);
    }

    public function topics(Request $request)
    {
       $category_ids = explode(',',$request->category_id);
       $topics = ContentCategory::whereIn('category_id',$category_ids)
                                  ->where(['content_type' => 1, 'status' => 2])
                                  ->leftJoin('contents','contents.id','content_categories.content_id')
                                  ->select('contents.id','contents.content_title')
                                  ->get();

       return response()->json(['topics' => $topics]);
    }
}
