<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use DataTables;
use App\NewEvent;
use Illuminate\Http\Request;
use App\Jobs\NewsEventNotification;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\NewEventController;
use App\Jobs\PushNotification;
class NewEventController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:new-event-list|new-event-create|new-event-edit|new-event-delete|new-event-publish|new-event-active', ['only' => ['index']]);
        $this->middleware('permission:new-event-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:new-event-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:new-event-delete', ['only' => ['destroy']]);
        $this->middleware('permission:new-event-publish', ['only' => ['publish']]);
        $this->middleware('permission:new-event-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {


            $data = new NewEvent();
            if ($request->new_event_type!= null) {

                $data = $data->where('type',$request->new_event_type);
            }
            $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'new_events.created_by');



            $data = $data->latest()->get(['new_events.*','createdUsers.name as created_user_name',]);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if (auth()->user()->can('new-event-edit') || auth()->user()->can('new-event-delete')) {
                        $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';

                        if (auth()->user()->can('new-event-edit')) {
                            $btn .= '<a class="dropdown-item text-blue edit-new-event edit-data" href="#" action="new-event/' . $row->id . '/edit"> Edit</a>';
                        }

                        if (auth()->user()->can('new-event-delete') && $row->status == 1) {
                            $btn .= '<a class="dropdown-item text-blue edit-new-event delete-data" href="#" action="/new-event/' . $row->id . '">Delete</a>';
                        }

                        $btn .= '</div>';
                    }
                    return $btn;
                })
                ->addColumn('type', function ($row) {
                    return "<span class='type_" . config("web_constant.news_event_type.$row->type") . "'>" . config("web_constant.news_event_type.$row->type") . "</span>";
                })
                ->addColumn('schedule', function($row){
                    return $row->schedule_date==null ? $row->schedule_date : date('d/m/Y',strtotime($row->schedule_date));
                })
                ->addColumn('status', function($row){
                    return "<span class='status_".config("web_constant.notification_status.$row->status")."'>".config("web_constant.notification_status.$row->status")."</span>";
                })
                ->addColumn('created_at', function($row){
                    return $row->created_at==null ? $row->created_at : date('d/m/Y',strtotime($row->created_at));
                })
                ->rawColumns(['action','type','created_at','schedule','status'])
                ->make(true);

        }

        return view('news-events.index', [
            'content_header' => 'Notification Lists',
            'create_permission' => 'new-event-create',
            'route' => 'new-event',
            'keyword' => 'notification',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $newEvent = new NewEvent();
        return view('news-events.create', [
            'newEvent' => $newEvent,
            'content_header' => 'Create New Notification',
            'route' => 'new-event',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required'],
            'type' => 'required',
            'hours' => 'required',
            'schedule_date' => 'required'
        ]);

        $request['status'] = 1;

        if($validator->fails()){
           return response($validator->messages(), 422);
        }


        $result = true;

        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['created_by'] = Auth::id();

            $newEvent = NewEvent::create($data);

            // NewsEventNotification::dispatch($newEvent)->delay(now()->addMinutes(2));

            User::where('role_id',4)
                 ->increment('unread_noti_count', 1);

            if (!$newEvent) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'News and Events was created successfully!']);
        } else {
            session(['error' => 'News and Events can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewEvent  $newEvent
     * @return \Illuminate\Http\Response
     */
    public function show(NewEvent $newEvent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewEvent  $newEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,NewEvent $newEvent)
    {
        $newEvent->current_index = $request->index;
        return view('news-events.update', [
            'newEvent' => $newEvent,
            'content_header' => 'Update Notification',
            'route' => 'new-event',
            'is_update'=> true,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewEvent  $newEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewEvent $newEvent)
    {
        $validator = Validator::make($request->all(),[
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required'],
            'type' => 'required',
            'hours' => 'required',
            'schedule_date' => 'required'
        ]
        );

        $result = true;
        DB::beginTransaction();
        try {
            $data = $request->all();
            $ans = $newEvent->update($data);

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "News And Events was updated successfully!"]);
        } else {
            session(['error' => "News And Events can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewEvent  $newEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewEvent $newEvent,Request $request)
    {
        //
        $result = true;
        DB::beginTransaction();
        try {

            $newEvent->deleted_by = Auth::id();

            $newEvent->delete();
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => "News And Events was deleted successfully!"]);
        } else {
            session(['error' => "News And Events can not delete!"]);
        }

        return redirect('new-event?index=' . $request->current_index);
    }


}
