<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Quizchoiceitem;
use Auth;
use App\Content;
use DataTables;
use DB;
use App\ContentQuiz;
use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:quiz-list|quiz-create|quiz-view|quiz-edit|quiz-publish|quiz-active|quiz-delete', ['only' => ['index']]);
        $this->middleware('permission:quiz-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:quiz-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:quiz-view', ['only' => ['show']]);
        $this->middleware('permission:quiz-delete', ['only' => ['destroy']]);
        $this->middleware('permission:quiz-publish', ['only' => ['publish']]);
        $this->middleware('permission:quiz-active', ['only' => ['active']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = new Quiz();

            if ($request->quiz_content_type != null) {
                $data = $data->where('quiz_content_type', $request->quiz_content_type);
            }

            if ($request->quiz_type != null) {
                $data = $data->where('quiz_type', $request->quiz_type);
            }

            if ($request->quiz_status != null) {
                $data = $data->where('quizzes.status', $request->quiz_status);
            }
            $data = $data->leftJoin('users as createdUsers', 'createdUsers.id', 'quizzes.created_by')
                ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'quizzes.published_by');

            $data = $data->latest()
                ->get([
                    'quizzes.*',
                    'createdUsers.name as created_user_name',
                    'publishedUsers.name as published_user_name',

                ]);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if (auth()->user()->can('quiz-edit') || auth()->user()->can('quiz-view') || auth()->user()->can('quiz-active') || auth()->user()->can('quiz-publish') || auth()->user()->can('quiz-delete')) {
                        $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                        if (auth()->user()->can('quiz-edit')) {
                            $btn .= '<a class="dropdown-item text-blue edit-data" href="#" action="quiz/' . $row->id . '/edit"> Edit</a>';
                        }

                        if (auth()->user()->can('quiz-publish') && $row->status == 1) {
                            $btn .= '<a class="dropdown-item text-blue publish-data" href="#" action="quiz/' . $row->id . '/publish"> Publish</a>';
                        }

                        if (auth()->user()->can('quiz-active') && $row->status == 2) {
                            $btn .= '<a class="dropdown-item text-blue deactive-data" href="#" action="quiz/' . $row->id . '/active">Deactivate</a>';
                        }

                        if (auth()->user()->can('quiz-active') && $row->status == 3) {
                            $btn .= '<a class="dropdown-item text-blue active-data" href="#" action="quiz/' . $row->id . '/active">Activate</a>';
                        }

                        if (auth()->user()->can('quiz-view')) {
                            $btn .= '<a class="dropdown-item text-blue view-data" href="quiz/' . $row->id . '"> View Details</a>';
                        }

                        if (auth()->user()->can('quiz-delete')&& $row->status == "1") {
                            $btn .= '<a class="dropdown-item text-blue delete-data" href="#" action="/quiz/' . $row->id . '">Delete</a>';
                        }

                        $btn .= '</div>';
                    }
                    return $btn;
                })
                ->addColumn('status', function ($row) {
                    return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                })
                ->addColumn('quiz_type', function ($row) {
                    return config("web_constant.quiz_types.$row->quiz_type");
                })
                ->addColumn('published_at', function ($row) {
                    return $row->published_at == null ? $row->published_at : date('d/m/Y', strtotime($row->published_at));
                })
                ->rawColumns(['action', 'status', 'quiz_type', 'published_at'])
                ->make(true);
        }

        return view('quiz.index', [
            'content_header' => 'Quiz Lists',
            'create_permission' => 'quiz-create',
            'route' => 'quiz',
            'keyword' => 'quiz',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quiz = new Quiz();
        $course = new Course();
        $content = new Content();

        return view('quiz.create', [
            'quiz' => $quiz,
            'course' => $course,
            'content' => $content,
            'content_header' => 'Create New Quiz',
            'route' => 'quiz',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'quiz_option' => array_map(function($arr){ return  explode(',', $arr);},explode(',&&&,', $request->quiz_option)) ,
            'is_correct_answer' =>  array_map(function($arr){ return  explode(',', $arr);},explode(',&&&,', $request->is_correct_answer)) ,
            'option_id' =>  array_map(function($arr){ return  explode(',', $arr);},explode(',&&&,', $request->option_id)) ,
            'quiz_type' => explode(',', $request->quiz_type) ,
        ]);

        $validator = Validator::make($request->all(),[
                                    'quiz_question.*' => $request->quiz_content_type==3 ?  'required|max:236' : 'required',
                                    'description.*' => 'nullable',
                                    // 'select_topic_id'=> 'required',
                                    'quiz_content_type' => 'required',
                                    'quiz_option.*.*' => $request->quiz_content_type==3 ?  'required|max:42' : 'required',
                                    'content_id' => ($request->quiz_content_type == 1) ?  'required' : 'nullable',
                                    'course_id' => ($request->quiz_content_type == 2) ?  'required' : 'nullable',
                                ],[
                                    'quiz_question.*.required' => 'TThe quiz question field is required..' ,
                                    'quiz_content_type.required' => 'The category field is required.' ,
                                    'quiz_option.*.*.required' => 'The quiz option field is required.',
                                    'content_id.required' => 'The topic is required.',
                                    'course_id.required' => 'The course name is required.',
                                    'quiz_question.*.max' => 'The quiz question may not be greater than 236 characters.',
                                    'quiz_option.*.*.max' => 'The quiz option may not be greater than 42 characters.'
                                ]);

        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            for ($i=0; $i < count($request->quiz_type) ; $i++) {
                $data = [];
                $data['created_by'] = Auth::id();
                $data['status'] = 1;
                $data['quiz_question'] = $request->quiz_question[$i] ?? ' ';
                $data['quiz_type'] = $request->quiz_type[$i];
                $data['quiz_content_type'] = $request->quiz_content_type;
                $data['description'] = $request->description[$i];
                 
                if($request->quiz_content_type==2)
                  $data['course_id'] = $request->course_id;

                $quiz = Quiz::create($data);

                if (!$quiz) {
                    $result = false;
                    DB::rollback();
                    break;
                }

                if ($result) {
                    if (!Quiz::saveOption($request->quiz_option[$i],$request->is_correct_answer[$i],$request->option_id[$i], $quiz->id)) {
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
        
                if($result && $request->quiz_content_type==1){
                    if(!Quiz::saveContentQuiz($request->all(),$quiz->id)){
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'Quiz was created successfully!']);
        } else {
            session(['error' => 'Quiz can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'type' => $request->quiz_content_type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        $quiz = Quiz::where('quizzes.id', $quiz->id)
            ->leftJoin('users as createdUsers', 'createdUsers.id', 'quizzes.created_by')
            ->leftJoin('users as updatedUsers', 'updatedUsers.id', 'quizzes.updated_by')
            ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'quizzes.published_by')
            ->leftJoin('users as deactivatedUsers', 'deactivatedUsers.id', 'quizzes.deactivated_by')
            ->with([
                'contents' => function($q){
                    $q->leftJoin('contents','contents.id','content_quizzes.content_id')
                    ->select('content_quizzes.*','contents.content_title');
                }
            ])
            ->first([
                'createdUsers.name as created_user_name',
                'updatedUsers.name as updated_user_name',
                'publishedUsers.name as published_user_name',
                'deactivatedUsers.name as deactivated_user_name',
                'quizzes.*',
            ]);

        return view('quiz.view', [
            'quiz' => $quiz,
            'content_header' => 'Quiz Detail',
            'route' => 'quiz',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz, Request $request)
    {
        $quiz = Quiz::where('quizzes.id',$quiz->id)
                           ->with([
                              'contents' => function($q){
                                $q->leftJoin('contents','contents.id','content_quizzes.content_id')
                                  ->select('content_quizzes.*','contents.content_title');
                              }
                           ])
                           ->first();

        $quiz->current_index = $request->index;

        $content = new Content();
        $course = Course::where('id',$quiz->course_id)->first();

        return view('quiz.update', [
            'quiz' => $quiz,
            'course' => $course,
            'content' => $content,
            'content_header' => 'Update Quiz',
            'route' => 'quiz',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        $request->merge([
            'quiz_option' => array_map(function($arr){ return  explode(',', $arr);},explode(',&&&,', $request->quiz_option)) ,
            'is_correct_answer' =>  array_map(function($arr){ return  explode(',', $arr);},explode(',&&&,', $request->is_correct_answer)) ,
            'option_id' =>  array_map(function($arr){ return  explode(',', $arr);},explode(',&&&,', $request->option_id)) ,
            'quiz_type' => explode(',', $request->quiz_type) ,
        ]);

        $validator = Validator::make($request->all(),[
                                    'quiz_question.*' => $request->quiz_content_type==3 ?  'required|max:236' : 'required',
                                    'description.*' => 'nullable',
                                    // 'select_topic_id'=> 'required',
                                    'quiz_content_type' => 'required',
                                    'quiz_option.*.*' => $request->quiz_content_type==3 ?  'required|max:42' : 'required',
                                    'content_id' => ($request->quiz_content_type == 1) ?  'required' : 'nullable',
                                    'course_id' => ($request->quiz_content_type == 2) ?  'required' : 'nullable',
                                ],[
                                    'quiz_question.*.required' => 'TThe quiz question field is required..' ,
                                    'quiz_content_type.required' => 'The category field is required.' ,
                                    'quiz_option.*.*.required' => 'The quiz option field is required.',
                                    'content_id.required' => 'The topic is required.',
                                    'course_id.required' => 'The course name is required.',
                                    'quiz_question.*.max' => 'The quiz question may not be greater than 236 characters.',
                                    'quiz_option.*.*.max' => 'The quiz option may not be greater than 42 characters.'
                                ]);

        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            for ($i=0; $i < count($request->quiz_type) ; $i++) {
                $data = [];
                $data['updated_by'] = Auth::id();
                $data['quiz_question'] = $request->quiz_question[$i] ?? ' ';
                $data['quiz_type'] = $request->quiz_type[$i];
                $data['quiz_content_type'] = $request->quiz_content_type;
                $data['description'] = $request->description[$i];

                if($request->quiz_content_type==2)
                   $data['course_id'] = $request->course_id[$i];

                $ans = $quiz->update($data);

                if (!$ans) {
                    $result = false;
                    DB::rollback();
                    break;
                }

                if ($result) {
                    $old_options = array_filter($request->option_id[$i]);
                    Quizchoiceitem::where('quiz_id', $quiz->id)->whereNotIn('id', $old_options)->forceDelete();
                    if (!Quiz::saveOption($request->quiz_option[$i],$request->is_correct_answer[$i],$request->option_id[$i], $quiz->id)) {
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
        
                if($result && $request->quiz_content_type==1){
                    ContentQuiz::where('quiz_id', $quiz->id)->forceDelete();
                    if(!Quiz::saveContentQuiz($request->all(),$quiz->id)){
                        $result = false;
                        DB::rollback();
                        break;
                    }
                }
            }
            
            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($result) {
            session(['success' => 'Quiz was updated successfully!']);
        } else {
            session(['error' => 'Quiz can not update!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'type' => $request->quiz_content_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quiz, Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

            $ans = Quiz::where('id', $quiz->id)

                ->update([
                    'deleted_by' => Auth::id(),
                ]);
            ContentQuiz::where('quiz_id', $quiz->id)->delete();

            if (!$ans) {
                $result = false;
                DB::rollback();
            }

            if (!$quiz->delete()) {
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($ans) {
            session(['success' => "Quiz was deleted successfully!"]);
        } else {
            session(['error' => "Quiz can not delete!"]);
        }

        return redirect('quiz?index=' . $request->current_index . "&type=" . $quiz->quiz_content_type);
    }

    public function publish(Request $request, $id)
    {
        $quiz = Quiz::find($id);
        $ans = Quiz::where('id', $id)
            ->update([
                'status' => 2,
                'published_by' => Auth::id(),
                'published_at' => date('Y-m-d H:i:s'),
            ]);

        if ($ans) {
            session(['success' => 'Quiz was published successfully!']);
        } else {
            session(['error' => 'Quiz can not publish!']);
        }

        return redirect('quiz?index=' . $request->current_index . "&type=" . $quiz->quiz_content_type);
    }

    public function active(Request $request, $id)
    {
        $quiz = Quiz::find($id);
        $status = null;

        if ($quiz->status == 2) {
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactive";
        } elseif ($quiz->status == 3) {
            $status = 2;
            $msg1 = "activated";
            $msg2 = "active";
        }

        $ans = Quiz::where('id', $id)
            ->update([
                'status' => $status,
                'deactivated_by' => Auth::id(),
                'deactivated_at' => date('Y-m-d H:i:s'),
            ]);

        if ($ans) {
            session(['success' => "Quiz was $msg1 successfully!"]);
        } else {
            session(['error' => "Quiz can not $msg2!"]);
        }

        return redirect('quiz?index=' . $request->current_index . "&type=" . $quiz->quiz_content_type);
    }
}
