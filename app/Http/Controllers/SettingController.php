<?php

namespace App\Http\Controllers;

use DB;
use App\Policy;
use App\Aboutus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:setting-list', ['only' => ['index']]);
        $this->middleware('permission:setting-create', ['only' => ['create','store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $about = Aboutus::first() ?? new Aboutus();

            $policies = Policy::get();
            return view('setting',[
                'about' => $about,
                'policies' => $policies,
                'content_header' => 'General Setting',
                'route' => '/',
            ]);
    }

    public function store(Request $request)
    {
        $detail_desc = explode('*****',$request->detail_desc);
        unset($detail_desc[count($detail_desc) - 1]);

        $request->merge([
            'description' => $detail_desc,
        ]);
        $validator = Validator::make($request->all(),[
                                    'male_phone_no' => 'required|regex:/^[\+0-9 ,]+$/',
                                    'male_mm_phone_no' => 'required',
                                    'female_phone_no' => 'required|regex:/^[\+0-9 ,]+$/',
                                    'female_mm_phone_no' => 'required',
                                    'facebook_page_id' => 'required',
                                    'facebook_link' => ['required', function($atr,$value,$fail){
                                        if(strpos($value,'facebook.com')===false){ 
                                            $fail("The facebook link is invalid format.");
                                        }
                                    }],
                                    'messenger_link' => ['required', function($atr,$value,$fail){
                                        if(strpos($value,'facebook.com')===false){ 
                                            $fail("The messenger link is invalid format.");
                                        }
                                    }],
                                    'title.*' => 'required|max:255',
                                    'description.*' => 'required',
                                ],[
                                    'description.*.required' => 'The description field is required.' ,
                                    'title.*.required' => 'The title field is required.' ,
                                    'title.*.max' => 'The title.0 may not be greater than 255 characters.'
                                ]);

        if($validator->fails()){
           return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
            $ans = Aboutus::updateOrCreate(
                ['id' => $request->about_id],
                [
                    'male_phone_no' => $request->male_phone_no,
                    'male_mm_phone_no'=> $request->male_mm_phone_no,
                    'female_phone_no' => $request->female_phone_no,
                    'female_mm_phone_no' => $request->female_mm_phone_no,
                    'facebook_page_id' => $request->facebook_page_id,
                    'facebook_link' => $request->facebook_link,
                    'messenger_link' => $request->messenger_link,
                ]
            );

            if(!$ans){
                $result = false;
                DB::rollback();
            }

           DB::table('policies')->truncate();

           for ($i=0; $i < count($request->title) ; $i++) {
                $ans = Policy::create([
                            'title' => $request->title[$i],
                            'description' => $request->description[$i],
                      ]);

                if(!$ans){
                    $result = false;
                    DB::rollback();
                    break;
                }
           }
            DB::commit();
        } catch (\Throwable $th) {
           dd($th);
        }

        if($result){
            session(['success' => 'Setting was updated successfully!']);
        }else{
            session(['error' => 'Setting can not updated!']);
        }

        return json_encode(['success' => $result]);

    }
}
