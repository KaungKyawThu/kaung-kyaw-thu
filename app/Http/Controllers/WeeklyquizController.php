<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use App\Weeklyquiz;
use App\Weeklyquizchoice;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class WeeklyquizController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:weekly-quiz-list|weekly-quiz-create|weekly-quiz-edit|weekly-quiz-view|weekly-quiz-delete', ['only' => ['index']]);
        $this->middleware('permission:weekly-quiz-create', ['only' => ['create','store']]);
        $this->middleware('permission:weekly-quiz-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:weekly-quiz-view', ['only' => ['show']]);
        $this->middleware('permission:weekly-quiz-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new Weeklyquiz();

            if($request->from_date!=null)
              $data = $data->whereDate('schedule_date','>=',$request->from_date);

            if($request->to_date!=null){
              $data = $data->whereDate('schedule_date','<=',$request->to_date);}

              $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'weeklyquizzes.created_by');




            $data = $data->latest()
                         ->get([
                            'weeklyquizzes.*',
                            'createdUsers.name as created_user_name',

                        ]);

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('weekly-quiz-edit') || auth()->user()->can('weekly-quiz-view') || auth()->user()->can('weekly-quiz-delete')  ){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('weekly-quiz-edit') && $row->status == 1)
                              $btn .= '<a class="dropdown-item text-blue edit-weekly-quiz edit-data" href="#" action="weeklyquiz/'.$row->id.'/edit"> Edit</a>';

                            if(auth()->user()->can('weekly-quiz-view'))
                               $btn .= '<a class="dropdown-item text-blue edit-weekly-quiz view-data" href="/weeklyquiz/'.$row->id.'">View Detail</a>';

                            if(auth()->user()->can('weekly-quiz-delete') && $row->status == 1)
                               $btn .= '<a class="dropdown-item text-blue edit-weekly-quiz delete-data" href="#" action="/weeklyquiz/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('status', function($row){
                        return "<span class='status_".config("web_constant.notification_status.$row->status")."'>".config("web_constant.notification_status.$row->status")."</span>";
                    })
                    ->addColumn('date', function($row){
                        return date('d/m/Y',strtotime($row->schedule_date));
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->rawColumns(['action','date','published_at','status'])
                    ->make(true);
        }

        return view('weekly-quiz.index',[
                   'content_header' => 'Weekly Quiz Lists',
                   'create_permission' => 'content-create',
                   'route' => 'weeklyquiz',
                   'keyword' => 'weekly quiz'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quiz = new Weeklyquiz();
        return view('weekly-quiz.create',[
                    'quiz' => $quiz,
                    'content_header' => 'Create New Weekly Quiz',
                    'route' => 'weeklyquiz',
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'quiz_option' => explode(',',$request->quiz_option),
            'is_correct_answer' => explode(',',$request->is_correct_answer),
            'option_id' => explode(',',$request->option_id)
        ]);

        $validator = Validator::make($request->all(),[
                                    'quiz_question' => 'required',
                                    'weekly_fact' => 'required',
                                    'answer_description' => 'required',
                                    'schedule_date' => 'required|unique:weeklyquizzes',
                                    'quiz_option.*' => 'required',
                                ],[
                                    'quiz_option.*.required' => 'The quiz option field is required.'
                                ]);
        if($validator->fails()){
           return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();
             $data['status'] = 1;

             $quiz = Weeklyquiz::create($data);
             if(!$quiz){
                 $result = false;
                 DB::rollback();
             }

             if($result){
                if(!Weeklyquiz::saveOption($request->all(),$quiz->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'Weekly quiz was created successfully!']);
        }else{
            session(['error' => 'Weekly quiz can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Weeklyquiz  $weeklyquiz
     * @return \Illuminate\Http\Response
     */
    public function show(Weeklyquiz $weeklyquiz)
    {
        $weeklyquiz = Weeklyquiz::where('weeklyquizzes.id',$weeklyquiz->id)
                      ->leftJoin('users as createdUsers','createdUsers.id','weeklyquizzes.created_by')
                      ->leftJoin('users as updatedUsers','updatedUsers.id','weeklyquizzes.updated_by')
                      ->first([
                           'createdUsers.name as created_user_name',
                           'updatedUsers.name as updated_user_name',
                           'weeklyquizzes.*'
                      ]);

        return view('weekly-quiz.view',[
                    'quiz' => $weeklyquiz,
                    'content_header' => 'Weekly Quiz Detail',
                    'route' => 'weeklyquiz',
               ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Weeklyquiz  $weeklyquiz
     * @return \Illuminate\Http\Response
     */
    public function edit(Weeklyquiz $weeklyquiz,Request $request)
    {
        $weeklyquiz->current_index = $request->index;
        return view('weekly-quiz.update',[
                    'quiz' => $weeklyquiz,
                    'content_header' => 'Update Weekly Quiz',
                    'route' => 'weeklyquiz',
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Weeklyquiz  $weeklyquiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Weeklyquiz $weeklyquiz)
    {
        $request->merge([
            'quiz_option' => explode(',',$request->quiz_option),
            'is_correct_answer' => explode(',',$request->is_correct_answer),
            'option_id' => explode(',',$request->option_id)
        ]);

        $validator = Validator::make($request->all(),[
                                    'quiz_question' => 'required',
                                    'weekly_fact' => 'required',
                                    'answer_description' => 'required',
                                    'schedule_date' => ['required', Rule::unique('weeklyquizzes')->ignore($weeklyquiz->id),],
                                    'quiz_option.*' => 'required',
                                ],[
                                    'quiz_option.*.required' => 'The quiz option field is required.'
                                ]);
        if($validator->fails()){
           return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['updated_by'] = Auth::id();

             $ans = $weeklyquiz->update($data);
             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             if($result){
                $old_options = array_filter($request->option_id);
                Weeklyquizchoice::where('weekly_quiz_id',$weeklyquiz->id)->whereNotIn('id',$old_options)->forceDelete();
                if(!Weeklyquiz::saveOption($request->all(),$weeklyquiz->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'Weekly quiz was created successfully!']);
        }else{
            session(['error' => 'Weekly quiz can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Weeklyquiz  $weeklyquiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Weeklyquiz $weeklyquiz,Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

            $ans = Weeklyquiz::where('id',$weeklyquiz->id)
                            ->update([
                                'deleted_by' => Auth::id(),
                            ]);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             if(!$weeklyquiz->delete()){
                $result = false;
                DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($ans){
            session(['success' => "Weekly quiz was deleted successfully!"]);
        }else{
            session(['error' => "Weekly quiz can not delete!"]);
        }

        return redirect('weeklyquiz?index='.$request->current_index);
    }
}
