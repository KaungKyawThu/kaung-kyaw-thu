<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use App\CourseBook;
use App\CourseVideo;
use App\CourseChapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CourseChapterController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:course-chapter-list|course-chapter-create|course-chapter-edit|course-chapter-delete|course-chapter-view|course-chapter-publish|course-chapter-active', ['only' => ['index']]);
        $this->middleware('permission:course-chapter-create', ['only' => ['create','store']]);
        $this->middleware('permission:course-chapter-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:course-chapter-view', ['only' => ['show']]);
        $this->middleware('permission:course-chapter-delete', ['only' => ['destroy']]);
        $this->middleware('permission:course-chapter-publish', ['only' => ['publish']]);
        $this->middleware('permission:course-chapter-active', ['only' => ['active']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new CourseChapter();

            if($request->level!=null){
                $data = $data->where('courses.course_level',$request->level);
            }
            if($request->course_chapter_status!=null){
                $data = $data->where('course_chapters.status',$request->course_chapter_status);
            }

            $data = $data->leftJoin('courses','courses.id','course_chapters.course_id')
                         ->leftJoin('users as createdUsers','createdUsers.id','course_chapters.created_by')
                         ->leftJoin('users as publishedUsers','publishedUsers.id','course_chapters.published_by');

            $data = $data->latest('course_chapters.created_at')
                         ->get([
                            'course_chapters.*',
                            'courses.course_level',
                            'createdUsers.name as created_user_name',
                            'publishedUsers.name as published_user_name',
                        ]);

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('course-chapter-edit') || auth()->user()->can('course-chapter-delete') || auth()->user()->can('course-chapter-view') || auth()->user()->can('course-chapter-publish') || auth()->user()->can('course-chapter-active')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('course-chapter-edit'))
                              $btn .= '<a class="dropdown-item text-blue edit-course edit-data" href="#" action="/course-chapter/'.$row->id.'/edit"> Edit</a>';

                            if(auth()->user()->can('course-chapter-publish') && $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue edit-course publish-data" href="#" action="/course-chapter/'.$row->id.'/publish">Publish</a>';

                            if(auth()->user()->can('course-chapter-active') && $row->status == "3")
                              $btn .= '<a class="dropdown-item text-blue edit-course active-data" href="#" action="/course-chapter/'.$row->id.'/active">Activate</a>';

                            if(auth()->user()->can('course-chapter-active') && $row->status == "2")
                              $btn .= '<a class="dropdown-item text-blue edit-course deactive-data" href="#" action="/course-chapter/'.$row->id.'/active">Deactivate</a>';

                            if(auth()->user()->can('course-chapter-view'))
                              $btn .= '<a class="dropdown-item text-blue edit-course view-data" href="/course-chapter/'.$row->id.'">View Detail</a>';

                            if(auth()->user()->can('course-chapter-delete')&& $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue edit-course delete-data" href="#" action="/course-chapter/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->addColumn('status', function($row){
                        return "<span class='status_".config("web_constant.status.$row->status")."'>".config("web_constant.status.$row->status")."</span>";
                    })
                    ->rawColumns(['action','published_at','status'])
                    ->make(true);
        }

        return view('course-chapter.index',[
                   'content_header' => 'Course Chapters',
                   'create_permission' => 'course-chapter-create',
                   'route' => 'course-chapter',
                   'keyword' => 'chapter'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = new CourseChapter();
        return view('course-chapter.create', [
            'course' => $course,
            'content_header' => 'Create New Chapter',
            'route' => 'course-chapter',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'course_id' => 'required',
            'chapter_name' =>  'required|max:255',
            'chapter_name_mm' =>  'required|max:255',
            'order' =>  'required|integer',
            'video_thumbnail.*' => 'required|mimes:jpg,jpeg,png',
            'video_title.*' => 'required|max:255',
            'course_video_link.*' => ['required', function($atr,$value,$fail){
                if(strpos($value,'youtube.com')===false && strpos($value,'youtu.be')===false){ 
                    $fail("The video link is invalid format.");
                }
            }],
            'video_duration.*' =>  'required|regex:/^[\d]{1,2}\:[\d]{1,2}$/',
        ],[
            'video_thumbnail.*.required' => 'The video thumbnail field is required.' ,
            'video_thumbnail.*.mimes' => 'The video thumbnail must be a file of type: jpg, jpeg, png.' ,

            'video_title.*.required' => 'The video title field is required.',
            'video_title.*.max' => 'The video title field may not be greater than 255 characters.',

            'course_video_link.*.required' => 'The video link field is required.',
            'course_video_link.*.url' => 'The video link field is invalid format.',
            'course_video_link.*.max' => 'The video link field may not be greater than 255 characters.',

            'video_duration.*.required' => 'The video duration field is required.',
            'video_duration.*.max' => 'The video duration field may not be greater than 8 characters.',
            'video_duration.*.date_format'=>'The video duration field must be 0:00 format',

        ]);

        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();
             $data['status'] = 1;

             if(CourseChapter::where('order',$request->order)
                            ->where('course_id',$request->course_id)
                            ->count() > 0){

                CourseChapter::where('order','>=',$request->order)
                            ->where('course_id',$request->course_id)
                            ->increment('order',1);
             }

                            
             $chapter = CourseChapter::create($data);

             if(!$chapter){
                 $result = false;
                 DB::rollback();
             }

             if($result){
                if(!CourseChapter::saveVideo($request->all(),$chapter->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             if($result && $request->content_id!=null){
                if(!CourseChapter::saveTopic($request->all(),$chapter->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Chapter was created successfully!"]);
        }else{
            session(['error' => "Chapter can not create!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'level' => $chapter->course->course_level]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseChapter  $courseChapter
     * @return \Illuminate\Http\Response
     */
    public function show(CourseChapter $courseChapter)
    {
        $chapter = CourseChapter::where('course_chapters.id',$courseChapter->id)
                                ->leftJoin('users as createdUsers','createdUsers.id','course_chapters.created_by')
                                ->leftJoin('users as updatedUsers','updatedUsers.id','course_chapters.updated_by')
                                ->leftJoin('users as publishedUsers','publishedUsers.id','course_chapters.published_by')
                                ->leftJoin('users as deactivatedUsers','deactivatedUsers.id','course_chapters.deactivated_by')
                                ->with([
                                        'topics' => function($q){
                                            $q->leftJoin('contents','contents.id','course_books.content_id')
                                            ->select('contents.content_title','course_books.*');
                                        },
                                ])
                                ->first([
                                    'createdUsers.name as created_user_name',
                                    'updatedUsers.name as updated_user_name',
                                    'publishedUsers.name as published_user_name',
                                    'deactivatedUsers.name as deactivated_user_name',
                                    'course_chapters.*'
                                ]);

        return view('course-chapter.show',[
                    'course' => $chapter,
                    'content_header' => 'Chapter Detail',
                    'route' => 'course-chapter',
               ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseChapter  $courseChapter
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseChapter $courseChapter,Request $request)
    {
        $course = CourseChapter::where('course_chapters.id',$courseChapter->id)
                            ->with([
                                'topics' => function($q){
                                    $q->leftJoin('contents','contents.id','course_books.content_id')
                                    ->select('course_books.*','contents.content_title');
                                },
                            ])
                            ->first();

        $course->current_index = $request->index;
        return view('course-chapter.update',[
                    'course' => $course,
                    'content_header' => 'Update Chapter',
                    'route' => 'course-chapter',
                    'is_update' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseChapter  $courseChapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseChapter $courseChapter)
    {
        $validator = Validator::make($request->all(),[
            'course_id' => 'required',
            'chapter_name' =>  'required|max:255',
            'chapter_name_mm' =>  'required|max:255',
            'video_thumbnail.*' => 'required|mimes:jpg,jpeg,png',
            'video_title.*' => 'required|max:255',
            'course_video_link.*' => ['required', function($atr,$value,$fail){
                if(strpos($value,'youtube.com')===false && strpos($value,'youtu.be')===false){ 
                    $fail("The video link is invalid format.");
                }
            }],
            'video_duration.*' =>  'required|regex:/^[\d]{1,2}\:[\d]{1,2}$/',
            'order' =>  'required|integer',
        ],[
            'video_thumbnail.*.required' => 'The video thumbnail field is required.' ,
            'video_thumbnail.*.mimes' => 'The video thumbnail must be a file of type: jpg, jpeg, png.' ,

            'video_title.*.required' => 'The video title field is required.',
            'video_title.*.max' => 'The video title field may not be greater than 255 characters.',

            'course_video_link.*.required' => 'The video link field is required.',
            'course_video_link.*.url' => 'The video link field is invalid format.',
            'course_video_link.*.max' => 'The video link field may not be greater than 255 characters.',

            'video_duration.*.required' => 'The video duration field is required.',
            'video_duration.*.max' => 'The video duration field may not be greater than 8 characters.',

        ]);

        if($validator->fails()){
            return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             
            if(CourseChapter::where('order',$request->order)
                            ->where('course_id',$request->course_id)
                            ->where('id','!=',$courseChapter->id)
                            ->count() > 0){

                CourseChapter::where('order','>=',$request->order)
                                ->where('id','!=',$courseChapter->id)
                                ->where('course_id',$request->course_id)
                                ->increment('order',1);
             }
           
                            
             $data = $request->all();
             $data['updated_by'] = Auth::id();

             $ans = $courseChapter->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             if($result){
                CourseVideo::where('course_chapter_id',$courseChapter->id)->forceDelete();
                if(!CourseChapter::saveVideo($request->all(),$courseChapter->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             CourseBook::where('course_chapter_id',$courseChapter->id)->forceDelete();
             if($result && $request->content_id!=null){
                if(!CourseChapter::saveTopic($request->all(),$courseChapter->id)){
                    $result = false;
                    DB::rollback();
                }
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => "Chapter was updated successfully!"]);
        }else{
            session(['error' => "Chapter can not update!"]);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index, 'level' => $courseChapter->course->course_level]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseChapter  $courseChapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseChapter $courseChapter,Request $request)
    {
        $level = $courseChapter->course->course_level;
        $result = true;
        DB::beginTransaction();
        try {
            $ans = CourseChapter::where('id',$courseChapter->id)
                                ->update([
                                    'deleted_by' => Auth::id(),
                                ]);

            CourseVideo::where('course_chapter_id',$courseChapter->id)->delete();
            CourseBook::where('course_chapter_id',$courseChapter->id)->delete();

            if(!$courseChapter->delete()){
                $result = false;
                DB::rollback();
            }

            DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($ans){
            session(['success' => "Chapter was deleted successfully!"]);
        }else{
            session(['error' => "Chapter can not delete!"]);
        }

        return redirect('course-chapter?index='.$request->current_index.'&level='.$level);
    }

    public function publish(Request $request,$id)
    {
        $chapter = CourseChapter::find($id);
        $ans = CourseChapter::where('id',$id)
                            ->update([
                                'status' => 2,
                                'published_by' => Auth::id(),
                                'published_at' => date('Y-m-d H:i:s')
                            ]);

        if($ans){
            session(['success' => "Chapter was published successfully!"]);
        }else{
            session(['error' => "Chapter can not publish!"]);
        }

        return redirect('course-chapter?index='.$request->current_index.'&level='.$chapter->course->course_level);
    }

    public function active(Request $request,$id)
    {
        $chapter = CourseChapter::find($id);
        $status = null;

        if($chapter->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactivate";
        }elseif($chapter->status==3){
            $status = 2;
            $msg1 = "activated";
            $msg2 = "activate";
        }

        $ans = CourseChapter::where('id',$id)
                            ->update([
                                'status' => $status ,
                                'deactivated_by' => Auth::id(),
                                'deactivated_at' => date('Y-m-d H:i:s')
                            ]);

        if($ans){
            session(['success' => "Chapter was $msg1 successfully!"]);
        }else{
            session(['error' => "Chapter can not $msg2!"]);
        }

        return redirect('course-chapter?index='.$request->current_index.'&level='.$chapter->course->course_level);
    }

}
