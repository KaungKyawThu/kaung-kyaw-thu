<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Auth;
use DataTables;
use App\Course;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:course-list|course-create|course-edit|course-delete|course-view|course-publish|course-active', ['only' => ['index']]);
        $this->middleware('permission:course-create', ['only' => ['create','store']]);
        $this->middleware('permission:course-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:course-view', ['only' => ['show']]);
        $this->middleware('permission:course-delete', ['only' => ['destroy']]);
        $this->middleware('permission:course-publish', ['only' => ['publish']]);
        $this->middleware('permission:course-active', ['only' => ['active']]);
        $this->middleware('permission:course-overview', ['only' => ['overview']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = new Course();

            if ($request->course_status != null) {
                $data = $data->where('courses.status', $request->course_status);
            }

            $data = $data ->leftJoin('users as createdUsers', 'createdUsers.id', 'courses.created_by')
                          ->leftJoin('users as publishedUsers', 'publishedUsers.id', 'courses.published_by');





            $data = $data->orderBy('order','ASC')
                         ->get([
                            'courses.*',
                            'createdUsers.name as created_user_name',
                            'publishedUsers.name as published_user_name',

                         ]);

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '';
                        if(auth()->user()->can('course-edit') || auth()->user()->can('course-delete') || auth()->user()->can('course-view') || auth()->user()->can('course-publish') || auth()->user()->can('course-active')){
                            $btn = ' <a id="actions" class="nav-link dropdown-toggle black-text actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    </a>
                                    <div class="dropdown-menu action-list" aria-labelledby="actions">';
                            if(auth()->user()->can('course-edit'))
                              $btn .= '<a class="dropdown-item text-blue edit-course edit-data" href="#" action="course/'.$row->id.'/edit"> Edit</a>';

                            if(auth()->user()->can('course-publish') && $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue edit-course publish-data" href="#" action="/course/'.$row->id.'/publish">Publish</a>';

                            if(auth()->user()->can('course-active') && $row->status == "3")
                              $btn .= '<a class="dropdown-item text-blue edit-course active-data" href="#" action="/course/'.$row->id.'/active">Activate</a>';

                            if(auth()->user()->can('course-active') && $row->status == "2")
                              $btn .= '<a class="dropdown-item text-blue edit-course deactive-data" href="#" action="/course/'.$row->id.'/active">Deactivate</a>';

                            if(auth()->user()->can('course-view'))
                              $btn .= '<a class="dropdown-item text-blue edit-course view-data" href="/course/'.$row->id.'">View Detail</a>';

                            if(auth()->user()->can('course-view'))
                              $btn .= '<a class="dropdown-item text-blue edit-course view-data" href="/certificate?level='.$row->course_level.'" target="_blank">View Certificate</a>';

                            if(auth()->user()->can('course-delete')&& $row->status == "1")
                              $btn .= '<a class="dropdown-item text-blue edit-course delete-data" href="#" action="/course/'.$row->id.'">Delete</a>';

                            $btn .=  '</div>';
                        }
                        return $btn;
                    })
                    ->addColumn('course_level', function ($row) {
                        return config("web_constant.courses.$row->course_level");
                    })
                    ->addColumn('course_name', function ($row) {
                        return $row->course_name_mm." ( ".$row->course_name_en." )";
                    })
                    ->addColumn('house', function ($row) {
                        return "<img src='$row->house_link' width='150'>";
                    })
                    ->addColumn('published_at', function($row){
                        return $row->published_at==null ? $row->published_at : date('d/m/Y',strtotime($row->published_at));
                    })
                    ->addColumn('status', function ($row) {
                        return "<span class='status_" . config("web_constant.status.$row->status") . "'>" . config("web_constant.status.$row->status") . "</span>";
                    })
                    ->rawColumns(['action','status','course_level','house','course_name','published_at'])
                    ->make(true);
        }

        return view('course.index',[
                   'content_header' => 'Course Lists',
                   'create_permission' => 'course-create',
                   'route' => 'course',
                   'keyword' => 'course'
               ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = new Course();
        return view('course.create', [
            'course' => $course,
            'content_header' => 'Create New Course',
            'route' => 'course',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'course_level' => ['required', 'unique:courses'],
            'course_description' => ['required'],
            'course_description_mm' => ['required'],
            'preview_text' => ['required'],
            'house_link' => ['required'],
            'course_name_en' => ['required','max:255'],
            'course_name_mm' => ['required','max:255'],
            'order' => ['required','integer', 'between:1,4','unique:courses'],
        ]);

        if($validator->fails()){
           return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['created_by'] = Auth::id();
             $data['status'] = 1;

             if($request->has('house_link')){
                $photo = Storage::disk('s3')->put('images', $request->house_link);
                $data['house_link'] = Storage::disk('s3')->url($photo);
             }

             $course = Course::create($data);

             if(!$course){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'Course was created successfully!']);
        }else{
            session(['error' => 'Course can not create!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        $course = Course::where('courses.id',$course->id)
                        ->leftJoin('users as createdUsers','createdUsers.id','courses.created_by')
                        ->leftJoin('users as updatedUsers','updatedUsers.id','courses.updated_by')
                        ->leftJoin('users as publishedUsers','publishedUsers.id','courses.published_by')
                        ->leftJoin('users as deactivatedUsers','deactivatedUsers.id','courses.deactivated_by')
                        ->first([
                            'createdUsers.name as created_user_name',
                            'updatedUsers.name as updated_user_name',
                            'publishedUsers.name as published_user_name',
                            'deactivatedUsers.name as deactivated_user_name',
                            'courses.*'
                        ]);

        return view('course.show',[
                    'course' => $course,
                    'content_header' => 'Course Detail',
                    'route' => 'course',
               ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course,Request $request)
    {
        $course->current_index = $request->index;
        return view('course.update',[
                    'course' => $course,
                    'content_header' => 'Update Course',
                    'route' => 'course',
                    'is_update' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        // if(!$request->has('house_link') && $request->virtual_img==null){
        //     $request->merge([
        //         'house_link' => null
        //     ]);
        // }else{
        //     $request->merge([
        //         'house_link' => $request->virtual_img ?? $request->house_link
        //     ]);
        // }

        $validator = Validator::make($request->all(),[
            // 'course_level' => ['required', Rule::unique('courses')->ignore($course->id)],
            'course_description' => ['required'],
            'course_description_mm' => ['required'],
            'preview_text' => ['required'],
            // 'house_link' => ['required'],
            'course_name_en' => ['required','max:255'],
            'course_name_mm' => ['required','max:255'],
            'order' => ['required','integer', 'between:1,4',Rule::unique('courses')->ignore($course->id)],
        ]);

        if($validator->fails()){
           return response($validator->messages(), 422);
        }

        $result = true;
        DB::beginTransaction();
        try {
             $data = $request->all();
             $data['updated_by'] = Auth::id();

            //  if($request->virtual_img==null){
            //     $photo = Storage::disk('s3')->put('images', $request->house_link);
            //     $data['house_link'] = Storage::disk('s3')->url($photo);
            //  }

             $ans = $course->update($data);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($result){
            session(['success' => 'Course was updated successfully!']);
        }else{
            session(['error' => 'Course can not update!']);
        }

        return json_encode(['success' => $result, 'index' => $request->current_index]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course,Request $request)
    {
        $result = true;
        DB::beginTransaction();
        try {

            $ans = Course::where('id',$course->id)
                        ->update([
                            'deleted_by' => Auth::id(),
                        ]);

             if(!$ans){
                 $result = false;
                 DB::rollback();
             }

             if(!$course->delete()){
                $result = false;
                DB::rollback();
             }

             DB::commit();
        } catch (\Throwable $th) {
            dd($th);
        }

        if($ans){
            session(['success' => "Course was deleted successfully!"]);
        }else{
            session(['error' => "Course can not delete!"]);
        }

        return redirect('course?index='.$request->current_index);
    }

    public function publish(Request $request,$id)
    {
        $content = Course::find($id);
        $ans = Course::where('id',$id)
                    ->update([
                        'status' => 2,
                        'published_by' => Auth::id(),
                        'published_at' => date('Y-m-d H:i:s')
                    ]);

        if($ans){
            session(['success' => "Course was published successfully!"]);
        }else{
            session(['error' => "Course can not publish!"]);
        }

        return redirect('course?index='.$request->current_index);
    }

    public function active(Request $request,$id)
    {
        $content = Course::find($id);
        $status = null;

        if($content->status==2){
            $status = 3;
            $msg1 = "deactivated";
            $msg2 = "deactivate";
        }elseif($content->status==3){
            $status = 2;
            $msg1 = "activated";
            $msg2 = "activate";
        }

        $ans = Course::where('id',$id)
                    ->update([
                        'status' => $status ,
                        'deactivated_by' => Auth::id(),
                        'deactivated_at' => date('Y-m-d H:i:s')
                    ]);

        if($ans){
            session(['success' => "Course was $msg1 successfully!"]);
        }else{
            session(['error' => "Course can not $msg2!"]);
        }

        return redirect('course?index='.$request->current_index);
    }

    public function overview()
    {
        return view('course.overview',[
            'content_header' => 'Course Overview'
        ]);
    }

    public function certificate(Request $request)
    {
        $pdf_name = config("web_constant.courses.$request->level")."_".$request->name;
        $name = $request->name;
        $level = $request->level;
        $pdf = PDF::loadView('certificate',compact('level','name'));
        return $pdf->stream($pdf_name.'.pdf');
    }

}
