<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnsweredQuiz extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','quiz_id','explore_type'
    ];
    protected $guarded = [];

    public static function saveQuiz($data, $user_id){
        $result = true;

        for ($i=0; $i < count($data) ; $i++) { 
        
            $ans = AnsweredQuiz::create([
                'user_id' => $user_id,
                'quiz_id' => $data[$i]['id'],
                'explore_type' => config('constant.explore_type.game')
            ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
    }
}
