<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ClinicInfo extends Model
{
    use SoftDeletes;
    protected $fillable = [
      'clinic_name','clinic_mm_name','region_id','facebook_page_id','clinic_description_mm','clinic_description_en','clinic_address','clinic_mm_address','clinic_phone_no','clinic_mm_phone_no','clinic_website','clinic_organization_id','clinic_fb_link','township_id','clinic_service_id','status','created_by','updated_by','published_by','deactivated_by','deleted_by','published_at','deactivated_at'
    ];
}
