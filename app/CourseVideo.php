<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseVideo extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'course_chapter_id','course_video_link','video_title','video_duration','video_thumbnail'
    ];
    protected $guarded = [];
}
