<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuizSetDetail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'quiz_set_id','quiz_id'
    ];
    protected $guarded = [];
}
