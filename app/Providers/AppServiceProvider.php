<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        if(env('APP_ENV') !== 'local')
        {
            URL::forceScheme('https');
        }
        
        View::share('title','ASRH');
        View::share('upload_photo_description','The uploaded file size must be less than 2MB.');

        $index = $request->index ?? 1;
        $index = (intval(floor((intval($index) - 1)/10))*10 ) ;

        View::share('current_index',$index);
    }
}
