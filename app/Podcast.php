<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Podcast extends Model
{
    use SoftDeletes;
    protected $fillable = ['podcast_share_link','thumbnail','podcast_title','podcast_link','podcast_duration','podcast_series_id', 'status','created_by','updated_by','published_by','deactivated_by','deleted_by','published_at','deactivated_at'];

    public function series()
    {
        return $this->hasOne('App\PodcastSerie', 'id', 'podcast_series_id');
    }

    public function bookmarks()
    {
        return $this->hasOne(Bookmark::Class, 'explore_id');
    }
}
