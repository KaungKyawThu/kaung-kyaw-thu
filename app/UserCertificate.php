<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCertificate extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','certificate_link','course_level_id','is_generated'
    ];
    protected $guarded = [];
}
