<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PodcastSerie extends Model
{
    use SoftDeletes;
    protected $fillable = ['series_name','thumbnail','status','created_by','updated_by','published_by','deactivated_by','deleted_by','published_at','deactivated_at'];

    public function podcast()
    {
        return $this->belongsTo('App\Podcast', 'id', 'podcast_series_id');
    }
}
