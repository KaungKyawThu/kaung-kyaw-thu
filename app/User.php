<?php

namespace App;

use App\Chat;
use App\Region;
use App\AnsweredQuiz;
use App\UserCertificate;
use App\UserSecurityAnswer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id','phone_no','gender_id','blocked_by','blocked_at','region_id','township_id',
        'birth_year','nick_name','user_no','avator_img_id','certificate_name','certificate_phone_no',
        'user_point','unread_noti_count','fcm_token','is_new_msg','course_level_id', 'status','deleted_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function adminlte_image()
    {
        return config('web_constant.user_photo');
    }

    public function adminlte_desc()
    {
        return 'That\'s a nice guy';
    }

    public function adminlte_profile_url()
    {
        return 'profile';
    }

    public function chats()
    {
        return $this->hasMany(Chat::Class,'user_id');
    }

    public function certificates()
    {
        return $this->hasMany(UserCertificate::Class,'user_id');
    }

    public function security_answers()
    {
        return $this->hasMany(UserSecurityAnswer::Class,'user_id');
    }

    public function answer_quizzes()
    {
        return $this->hasMany(AnsweredQuiz::Class,'user_id');
    }
    public function regions()
    {
        return $this->hasOne(Region::class,'id','region_id');
    }
}
