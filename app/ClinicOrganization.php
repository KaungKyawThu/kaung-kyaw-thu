<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClinicOrganization extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','organization_mm_name', 'status','created_by','updated_by','published_by','deactivated_by','deleted_by','published_at','deactivated_at'
    ];
}
