<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityChoice extends Model
{
    protected $hidden = array('security_question_id');
    protected $fillable = ['security_question_id', 'security_ans'];
}
