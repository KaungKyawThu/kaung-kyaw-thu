<?php

namespace App;

use Auth;
use App\Content;
use App\ContentFaq;
use App\FaqCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['faq_question','faq_answer','status','created_by','updated_by','deleted_by','deactived_by','published_at','published_by','deactived_at'];
    public function category()
    {
        return $this->hasMany(FaqCategory::Class,'faq_id');
    }
    public function keywords()
    {
        return $this->hasMany(ExploreKeyword::Class, 'explore_id');
    }
    public function topics(){
        return $this->hasMany(ContentFaq::Class,'faq_id');
    }

}
