<?php

namespace App;

use App\ContentFaq;
use App\ContentQuiz;
use App\ContentDetail;
use App\ContentCategory;
use App\ExploreKeyword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'content_title','content_cover_photo','video_id','is_trending','is_new_comment','author_id','read_time',
        'content_type','status','created_by','updated_by','published_by','deactivated_by','deleted_by','published_at','deactivated_at'
    ];
    protected $guarded = [];

    public function bookmarks()
    {
        return $this->hasOne(Bookmark::Class, 'explore_id');
    }

    public function related_video()
    {
        return $this->hasOne(Video::Class, 'id', 'video_id');
    }

    public function details()
    {
        return $this->hasMany(ContentDetail::Class,'content_id');
    }

    public function categories()
    {
        return $this->hasMany(ContentCategory::Class,'content_id');
    }

    public function quizzes()
    {
        return $this->hasMany(ContentQuiz::Class,'content_id');
    }

    public function faqs()
    {
        return $this->hasMany(ContentFaq::Class,'content_id');
    }

    public function keywords()
    {
        return $this->hasMany(ExploreKeyword::Class,'explore_id');
    }

    public static function saveDetail($data,$content_id)
    {
        $result = true;

        for ($i=0; $i < count($data['detail_description']) ; $i++) { 
        
            if($data['detail_virtual_img'][$i]==null && isset($data['detail_photo'][$i])){
                $photo = Storage::disk('s3')->put('images', $data['detail_photo'][$i]);
                $detail_photo = Storage::disk('s3')->url($photo);
            }else{
                $detail_photo = $data['detail_virtual_img'][$i];
            }

            if($data['detail_description'][$i]!=null){
                $ans = ContentDetail::create([
                                    'content_id' => $content_id,
                                    'detail_photo' =>  $detail_photo,
                                    'detail_description' => $data['detail_description'][$i]
                    ]);

                if(!$ans){
                    $result = false;
                    break;
                }
            }
        }

        return $result;
        
    }

    public static function saveCategory($data,$content_id)
    {
        $result = true;

        for ($i=0; $i < count($data['category_id']) ; $i++) { 
            $ans = ContentCategory::create([
                                 'content_id' => $content_id,
                                 'category_id' => $data['category_id'][$i]
                  ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
        
    }

    public static function saveFAQ($data,$content_id)
    {
        $result = true;

        for ($i=0; $i < count($data['faq_id']) ; $i++) { 
            $ans = ContentFaq::create([
                                 'content_id' => $content_id,
                                 'faq_id' => $data['faq_id'][$i]
                  ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
        
    }

    public static function saveQuiz($data,$content_id)
    {
        $result = true;

        for ($i=0; $i < count($data['quiz_id']) ; $i++) { 
            $ans = ContentQuiz::create([
                                 'content_id' => $content_id,
                                 'quiz_id' => $data['quiz_id'][$i]
                  ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
        
    }
}
