<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClinicService extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'service_name','status','service_description','service_mm_description','service_mm_name','created_by','updated_by','published_by','deactivated_by','deleted_by','published_at','deactivated_at'
    ];
}
