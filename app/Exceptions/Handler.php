<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    // public function render($request, Throwable $exception)
    // {
    //     return parent::render($request, $exception);
    // }

    public function render($request, Throwable $exception)
    {
        if ($request->wantsJson()) {   //add Accept: application/json in request
            return $this->handleApiException($request, $exception);
        } else {
            return parent::render($request, $exception);
        }
    }

    private function handleApiException($request, Throwable $exception)
    {
        $exception = $this->prepareException($exception);

        if ($exception instanceof HttpResponseException) {
            $exception = $exception->getResponse();
        }

        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            $exception = $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            $exception = $this->convertValidationExceptionToResponse($exception, $request);
        }

        return $this->customApiResponse($exception);
    }

    private function customApiResponse($exception)
    {

        if (method_exists($exception, 'getStatusCode')) {
            $statusCode = $exception->getStatusCode();
        } else {
            $statusCode = 500;
        }

        $response = [];

        switch ($statusCode) {
            case 400:
                $response['message'] = method_exists($exception,'getMessage')?(empty($exception->getMessage())?'Bad Request': $exception->getMessage()):'Bad Request';
                break;
            case 401:
                $response['message'] = method_exists($exception,'getMessage')?(empty($exception->getMessage())?'Unauthorized': $exception->getMessage()):'Unauthorized';
                break;    
            case 403:
                $response['message'] = method_exists($exception,'getMessage')?(empty($exception->getMessage())?'Forbidden': $exception->getMessage()):'Forbidden';
                break;
            case 404:
                $response['message'] = method_exists($exception,'getMessage')?(empty($exception->getMessage())?'Not Found': $exception->getMessage()):'Not Found';
                break;
            case 405:
                $response['message'] = method_exists($exception,'getMessage')?(empty($exception->getMessage())?'Method Not Allowed': $exception->getMessage()):'Method Not Allowed';
                break;
            case 422:
                $response['message'] = method_exists($exception,'getMessage')?(empty($exception->getMessage())?'Unprocessable Entity': $exception->getMessage()):'Unprocessable Entity';
                break;
            default:
                $response['message'] = ($statusCode == 500) ? 'Internal Server Error' : $exception->getMessage();
                break;
        }

        if (config('app.debug') && $statusCode == 500) {
            $response['trace'] = $exception->getTrace();
            $response['code'] = $exception->getCode();
        }

        $response['status'] = $statusCode;

        return response()->json($response, $statusCode);
    }
}
