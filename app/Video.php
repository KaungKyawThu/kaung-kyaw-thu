<?php

namespace App;

use App\ExploreKeyword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'video_title','video_link','order','video_duration','thumbnail','status','created_by','updated_by','deleted_by','deactived_by','published_at','published_by','deactived_at'
    ];

    public function keywords()
    {
        return $this->hasMany(ExploreKeyword::Class, 'explore_id');
    }

    public function bookmarks()
    {
        return $this->hasOne(Bookmark::Class, 'explore_id');
    }

}
