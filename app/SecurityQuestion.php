<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    protected $fillable = ['id', 'question'];

    public function choices()
    {
        return $this->hasMany('App\SecurityChoice');
    }
}
