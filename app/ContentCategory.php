<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentCategory extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function content_category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }
}
