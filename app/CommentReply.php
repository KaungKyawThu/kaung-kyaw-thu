<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentReply extends Model
{
    use SoftDeletes;
    protected $fillable = ['content_comment_id','reply_type','reply_msg','user_id','deleted_by'];
}
