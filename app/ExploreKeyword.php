<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExploreKeyword extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'explore_id','explore_type','keyword_id'
    ];
    protected $guarded = [];

}
