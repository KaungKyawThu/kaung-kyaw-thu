<?php

namespace App;

use App\Quiz;
use App\QuizSetDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuizSet extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'course_id','set_name','created_by','updated_by','status','published_by','published_at','deactivated_by','deactivated_at','deleted_by'
    ];
    protected $guarded = [];

    public function details()
    {
        return $this->hasMany(QuizSetDetail::Class,'quiz_set_id');
    }

    public function course()
    {
        return $this->hasOne(Course::Class,'id','course_id');
    }

    public static function saveDetail($data,$set_id)
    {
        $result = true;

        for ($i=0; $i < count($data['quiz_id']) ; $i++) { 

            $ans = QuizSetDetail::create([
                                 'quiz_set_id' => $set_id,
                                 'quiz_id' =>  $data['quiz_id'][$i],
                  ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
        
    }

   
}
