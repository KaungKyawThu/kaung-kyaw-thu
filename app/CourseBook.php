<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseBook extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'course_chapter_id','content_id'
    ];
    protected $guarded = [];
}
