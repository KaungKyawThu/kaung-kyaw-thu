<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSecurityAnswer extends Model
{
    protected $fillable = ['user_id', 'security_question_id', 'security_choice_id'];
}
