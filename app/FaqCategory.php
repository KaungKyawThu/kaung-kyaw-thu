<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class FaqCategory extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['category_id','faq_id'];
}
