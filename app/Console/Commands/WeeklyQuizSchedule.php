<?php

namespace App\Console\Commands;

use App\User;
use App\NewEvent;
use App\Weeklyquiz;
use App\PushNotification;
use Illuminate\Console\Command;

class WeeklyQuizSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:noti';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Schedule Noti';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //for weekly quiz noti
        $schedule = Weeklyquiz::whereDate('schedule_date',date('Y-m-d'))->where('hours',date('H'))->get();
        $result = true;
        if($schedule->count() > 0){

            $users = User::where('role_id',4)->pluck('fcm_token')->toArray();
            $title = 'HtarWaRa Weekly Quiz';
            $body = 'Please check out the weekly quizzes and get extra points and knowledge.';
            $ans = PushNotification::pushAdriod($users, $title, $body, 2);

            Weeklyquiz::whereDate('schedule_date',date('Y-m-d'))->where('hours',date('H'))->update(['status' => 2]);
               
        }

        //end of weekly noti

        //for news and events noti
        $news = NewEvent::whereDate('schedule_date',date('Y-m-d'))->where('hours',date('H'))->get();
        if($news->count() > 0){
           foreach ($news as $key => $value) {
                $users = User::where('role_id',4)->pluck('fcm_token')->toArray();
                $result = PushNotification::pushAdriod($users, $value->title, $value->description, 1);
                $value->update(['status' => 2]);
           }
        }
       //end of new and events noti
       
        \Log::info("Push Weekly Quizzes!.");

        return true;
    }
}
