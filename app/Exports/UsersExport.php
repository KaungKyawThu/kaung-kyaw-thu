<?php

namespace App\Exports;

use App\User;
use App\Region;
use App\Township;
use DataTables;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class UsersExport implements FromView , WithEvents
//  FromCollection, WithHeadings
{
    protected $status;
    protected $region;
    protected $township;
    public function __construct(?String $status,$region,$township)
    {
        // dd($status,$region,$township);
        $this->status = $status;
        $this->region = $region;
        $this->township = $township;
    }

    public function view(): View
    {
        $status = $this->status;
        $region =  $this->region;
        $township =  $this->township;

        $header = "";
        $data  =  new User();

        if( $region != null){
            $data = $data->where('users.region_id',$region);
            $region_name = Region::where('id',$region)->first();
            $header .= " Region: ".$region_name->name.",";

        }
        if( $township != null){
            $data = $data->where('users.township_id',$township);
            $tsp_name = Township::where('id',$township)->first();
            $header .= " Township: ".$tsp_name->name.",";

        }

        if( $status != null){
            $data = $data->where('users.status',$status);
            $header .= " Status: ".config("web_constant.user_status.$status");
        }

         $data = $data->where('role_id',4)
                ->leftJoin('regions','users.region_id','=','regions.id')
                ->leftJoin('townships','users.township_id','=','townships.id')
                ->select('users.name','email','phone_no','regions.name as region_name','townships.name as tsp_name',\DB::raw('(CASE
                WHEN gender_id = "1" THEN "Man"
                WHEN gender_id = "2" THEN "Woman"
                WHEN gender_id = "3" THEN "Transgender Man"
                WHEN gender_id = "4" THEN "Transgender Woman"
                WHEN gender_id = "5" THEN "Non Binary"
                ELSE ""
                END) AS gender'),'birth_year')
                ->selectRaw('DATE_FORMAT(users.created_at,"%d/%m/%Y")')
                ->get();

        return view('exports.userExport', [
            'datas' => $data,
            'header' => $header,
        ]);
    }

    public function registerEvents(): array

    {

        return [

            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A3:I3')
                ->getFont()
                ->setBold(true);

                $event->sheet->getDelegate()->getStyle('A1')
                ->getFont()
                ->setBold(true);

                $event->sheet->getDelegate()->getStyle('A2')
                ->getFont()
                ->setBold(true);

                // $event->sheet->getDelegate()->setWidth('A1',100)->getStyle('A1')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getRowDimension('3')->setRowHeight(20);
                $event->sheet->getStyle('G')->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('A')->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('H')->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('A2')->getAlignment()->setHorizontal('center');
                // $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(15);

            },

        ];

    }

}
