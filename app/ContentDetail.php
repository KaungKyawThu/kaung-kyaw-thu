<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentDetail extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $fillable = ['content_id','detail_photo','detail_description'];

}
