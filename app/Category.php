<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name_en', 'name_mm', 'status','is_popular','created_by','updated_by','published_by','deactivated_by','deleted_by','published_at','deactivated_at'
    ];
}
