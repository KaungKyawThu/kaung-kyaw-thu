<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'course_name_en','course_name_mm','course_level','preview_text','course_description','course_description_mm','house_link','order','created_by','status','updated_by','published_by','published_at','deleted_by','deactivated_by','deactivated_at'
    ];
    protected $guarded = [];

    public function chapters()
    {
        return $this->hasMany('App\CourseChapter')->orderBy('order', 'asc');
    }
}
