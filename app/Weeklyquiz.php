<?php

namespace App;

use App\Weeklyquizchoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Weeklyquiz extends Model
{
    use SoftDeletes;
    protected $fillable = ['schedule_date','hours','status','weekly_fact','quiz_question','quiz_type','answer_description','created_by','updated_by','deleted_by'];

    public function options()
    {
        return $this->hasMany(Weeklyquizchoice::Class, 'weekly_quiz_id');
    }

    public static function saveOption($data,$quiz_id){
        $ans = true;
        for ($i=0; $i < count($data['quiz_option']); $i++) { 
            if($data['option_id'][$i]==null)
                $option = Weeklyquizchoice::create([
                                         'weekly_quiz_id' => $quiz_id,
                                         'quiz_option' => $data['quiz_option'][$i],
                                         'is_correct' => $data['is_correct_answer'][$i],
                                    ]);
            else $option = Weeklyquizchoice::where('id',$data['option_id'][$i])
                                           ->update([
                                                'quiz_option' => $data['quiz_option'][$i],
                                                'is_correct' => $data['is_correct_answer'][$i],
                                           ]);

            if(!$option){
                $ans = false;
                break;
            }
        }

        return $ans;
    }
}
