<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aboutus extends Model
{
    protected $fillable = ['facebook_link','messenger_link','male_phone_no','male_mm_phone_no','female_phone_no','female_mm_phone_no','facebook_page_id'];
}
