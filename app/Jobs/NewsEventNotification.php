<?php

namespace App\Jobs;

use App\User;
use App\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NewsEventNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $title,$body;
    public function __construct($event)
    {
        $this->title = $event->title;
        $this->body = $event->body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = true;
        $users = User::where('role_id',4)->pluck('fcm_token')->toArray();
        $result = PushNotification::pushAdriod($users, $this->title, $this->body, 1);

        \Log::info("New Announcement!, $result.");

        return $result;
    }
}
