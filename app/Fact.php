<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Fact extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'description'];
}
