<?php

namespace App;

use App\ExploreKeyword;
use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $fillable = ['keyword_name','is_most_search','created_by'];

    public static function saveExploreKeyword($keywords,$content_id,$content_type)
    {
        $result = true;
        foreach ($keywords as $k) {
           $ans = ExploreKeyword::create([
                        'keyword_id' => $k,
                        'explore_id' => $content_id,
                        'explore_type' => $content_type
                    ]);

            if(!$ans){
                $result = false;
                break;
            }
        }

        return $result;
    }
}
