<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','message','message_type','chat_type','is_read'
    ];
    protected $guarded = [];

    public function user()
    {               
        return $this->hasOne(User::Class,'id','user_id');
    }
}
