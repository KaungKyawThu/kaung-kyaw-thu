<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quizchoiceitem extends Model
{
    use SoftDeletes;
    protected $hidden = array('quiz_id');
    protected $fillable = [
                             'quiz_id','quiz_option','is_correct'
                          ];
}
