<?php

return [
    'firebase_server_key' =>'AAAAyb8oM7U:APA91bHR2iplg-LZpkfnuQeA3ACCOnBXGXZ5ZDEpcOtNjNWl59DG3gmwJbtVqwCCYXbX0vUtOFyX4OTNAhRfaHZ-eEjKrxxhTz-nP5OYon4_5Br6abJFmmzEgCDHW7S7yKmiHwF81Sxg',
    'firebase_url' => 'https://fcm.googleapis.com/fcm/send',

    'status' => [
        1 => 'Pending',
        2 => 'Active',
        3 => 'Deactivated'
    ],

    'user_status' => [
        0 => 'Inactive',
        1 => 'Active'
    ],

    'quiz_types' => [
        1 => 'True/False',
        2 => 'Single Choice',
        3 => 'Multiple Choice',
        4 => 'Fill Type'
    ],

    'quiz_content_types' => [
        1 => 'Topic',
        2 => 'Course',
        3 => 'Game'
    ],

    'keyword_content_types' => [
        1 => 'Topic',
        2 => 'Article',
        3 => 'Video',
        4 => 'Podcast',
        5 => 'FAQ'
    ],

    'content_types' => [
        1 => 'Topic',
        2 => 'Article'
    ],

    'user_roles' => [
        1 => 'Admin',
        2 => 'Editor',
        3 => 'Reporter',
        4 => 'User',
        5 => 'Author'
    ],

    'reply_type' => [
        1 => 'Admin',
        2 => 'User'
    ],

    'courses' => [
        1 => 'Basic',
        2 => 'Intermediate' ,
        3 => 'Advanced',
        4 => 'Trainer'
    ],

    'message_type' => [
        1 => 'Text',
        2 => 'Photo',
    ],

    'chat_type' => [
        1 => 'Request',
        2 => 'Reply'
    ],

    'news_event_type' => [
        1=>'News' ,
        2=>'Event',
    ],

    'gender' => [
        1 => 'Man' ,
        2 => 'Woman',
        3 => 'Transgender Man',
        4 => 'Transgender Woman',
        5 => 'Non Binary'
    ],

    'notification_type' => [
        1 => 'information',
        2 => 'weekly_qiz',
        3 => 'chat',
        4 => 'comment_reply'
    ],

    'notification_status' => [
        1 => 'Pending',
        2 => 'Pushed'
    ],

    'user_photo' => '/images/user.jpg',

];

?>
