<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Defined Variables
    |--------------------------------------------------------------------------
    |
    | This is a set of variables that are made specific to this application
    | that are better placed here rather than in .env file.
    | Use config('your_key') to get the values.
    |
    */

    'page_size' => 10,
    's3_cloudfront' => env('AWS_URL'),

    'notification_type' => [
        'information' => 1,
        'weekly_qiz' => 2,
        'chat' => 3,
        'comment_reply' => 4
    ],

    'user_status' => [
        'inactive' => 0,
        'active' => 1,
    ],

    'user_role' => [
        'admin' => 1,
        'editor' => 2,
        'reporter' => 3,
        'user' => 4,
        'author' => 5
    ],

    'course' => [
        'basic' => 1,
        'intermediate' => 2,
        'advanced' => 3,
        'trainer' => 4,
    ],

    'gender' => [
        'man' => 1,
        'woman' => 2,
        'transgender_man' => 3,
        'transgender_woman' => 4,
        'non_binary' => 5,
    ],

    'status' => [
        'Pending' => 1,
        'Active' => 2, 
        'Deactive' => 3,
    ],

    'reply_type' => [
        'admin' => 1,
        'user' => 2 
    ],

    'explore_type' => [
        'topic' => 1,
        'article' => 2,
        'video' => 3,
        'podcast' => 4,
        'faq' => 5,
        'weekly_quiz' => 6,
        'game' => 7
    ],
    
    'news_event_type' => [
        'news' => 1,
        'event' => 2, 
    ],

    'user_course_status' => [
        'open' => 1, 
        'locked' => 2,
        'completed' => 3,
    ],

    'message_type' => [
        'text' => 1,
        'photo' => 2 
    ],

    'chat_type' => [
        'request' => 1,
        'reply' => 2 
    ],

    'chance' => [
        'topic' => 100,
        'game' => 100,
        'basic_course' => 100,
        'intermediate_course' => 200,
        'advanced_course' => 300,
        'trainer_course' => 0,
    ],

    'reward' => [
        'quiz_squad' => 300,
        'tic_tac_toe' => 300,
        'topic' => 25,
    ],

    'quiz_types' => [
        'Ture/False' => 1,
        'Single Choice' => 2,
        'Multiple Choice' => 3,
        'Fill In Blank' => 4
    ],

    'quiz_content_type' => [
        'Topic' => 1,
        'Course' => 2,
        'Game' => 3
    ],

    'game_type' => [
        'quiz_squad' => 1,
        'tic_tac_toe' => 2,
        'millionarie' => 3
    ],

];