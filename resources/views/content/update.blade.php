@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('content/'.$content->id) }}" id="content-form" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        @include('content.form')
    </form>
</div>
@stop
