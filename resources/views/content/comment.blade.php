@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<style>
    th[aria-label='User Name']{
        width: 250px!important;
    }
    th[aria-label='']{
        width: 70px!important;
    }

    .check-to-delete{
        display: none;
    }

    #DataTables_Table_0_wrapper{
        width: 100%!important;
    }
</style>

<div class="card-body">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">{{$content->content_title}}</h3>
        </div>
    </div><br>
    <h5>Comments & Replies</h5>
    <div class="row">
        <table class="table data-table table-bordered">
           <thead>
                <tr>
                    <th class="check-to-delete">-</th>
                    <th style="display: none;">No.</th>
                    <th>Message</th>
                    <th>User Name</th>
                    <th class="action-col"><span></span></th>
                </tr>
           </thead>
           <tbody>
               @php
                   $index = 0;
               @endphp
               @foreach ($comments as $c)
                    <tr>
                         <td class="check-to-delete"> <input type="checkbox" class="cmts" name="comment_id[]" value="{{$c->id}}"> </td>
                         <td class="data-index" style="display: none;">{{++$index}}</td>
                         <td><i class="fa fa-comment" ></i> <b>{{$c->comment}}</b></td>
                         <td>{{$c->user_name}}</td>
                         <td>
                            {{-- @if($c->need_reply) --}}
                                <span class="btn btn-sm btn-primary reply-btn" id="{{$c->id}}"><i class="fa fa-reply"></i> Reply</span>
                            {{-- @endif --}}
                         </td>
                    </tr>
                    @foreach ($c->replies as $r)
                        <tr>
                            <td class="check-to-delete"> <input type="checkbox" class="rps cmts-{{$c->id}}" cmt="{{$c->id}}" name="reply_id[]" value="{{$r->id}}"> </td>
                            <td class="data-index" style="display: none;">{{++$index}}</td>
                            <td class="reply-msg"><i class="fa fa-reply" aria-hidden="true"></i> {{$r->reply_msg}}</td>
                            <td>{{$r->user_name}}</td>
                            <td></td>
                        </tr>
                    @endforeach
               @endforeach

           </tbody>
        </table>
    </div>
</div><br>

@if(auth()->user()->can('comment-delete'))
    <div class="row">
        <div class="col-md-3" style="margin-top: -6.5%;margin-left: 1%;">
            <span class="btn btn-sm btn-danger delete-comment"><i class="fa fa-times" style="color: white!important;"></i> Delete</span><br>
            <span class="invalid-feedback" role="alert">
                <strong class="check-err"></strong>
            </span>
        </div>
    </div><br>
@endif
@include('back-btn')

<script>
    $(document).ready(function(){
        let table = $('.data-table').DataTable({
                        "aaSorting": [],
                        displayStart : "{{$current_index}}",
                        //stateSave: true,
                        pageLength : 30,
                        columnDefs: [
                            { targets: '_all', sortable: false }
                        ]
                    });

        $('.delete-comment').click(function(){
            let check = $('table.data-table > tbody ').find('input[type=checkbox]:checked').length;
            if(check==0){
                $('.check-to-delete').css('display','block');
                $('.check-err').html('Please, check at least one comment/reply to delete.');
            }else{
                $('#confirm-modal').find('.save-btn').removeClass('confirm-delete-button');
                $('#confirm-modal').find('.save-btn').addClass('delete-comment-reply');
                $('#confirm-modal').find('.save-btn').removeClass('disabled');
                $('#confirm-modal').find('.save-btn').html('Sure');
                $('#confirm-modal').modal('show');
            }
        });

        $(document).on('click','.delete-comment-reply',function(){
             let comment_id = [];
             let reply_id = [];

             $('table.data-table > tbody ').find('input[name="comment_id[]"]:checked').each(function(i,e){
                  comment_id.push($(e).val());
             });

             $('table.data-table > tbody ').find('input[name="reply_id[]"]:checked').each(function(i,e){
                reply_id.push($(e).val());
             });

             $.ajax({
                url : '/delete-comment',
                method : 'DELETE',
                data : {
                    'comment_id' : comment_id,
                    'reply_id' : reply_id,
                    'current_index' :   1,
                    'content_id' : "{{$content->id}}"
                },
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                },
                dataType : 'json',
                success : function(data){
                    window.location.href = '/content/'+data.content_id+'/comment?index='+data.index;
                },

                error: function(e){
                   console.log(e);
                }
             })
        });

        $('input[type=checkbox]').click(function(){
            if($(this).is(':checked')){
                $('.check-err').html(null);

                if($(this).hasClass('cmts')){
                   $(document).find(".cmts-"+$(this).val()).prop('checked',true);
                }
            }else{
                if($(this).hasClass('cmts')){
                   $(document).find(".cmts-"+$(this).val()).prop('checked',false);
                }else{
                   $(document).find(".cmts[value='"+$(this).attr('cmt')+"']").prop('checked',false);
                }
            }
        })
    })
</script>
@stop
