@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

@php
    $active_tab = isset($_GET['content_type']) ? $_GET['content_type'] : 1;
@endphp
<div class="row">
    @include('search')
    <div class="col-md-3">
        <select class="form-control select2 type-filter-select2" name="content_type" style="width: 100%;">
            <option value=""></option>
            @foreach (\App\Category::get() as $k=>$t)
                <option value="{{$t->id}}">{{$t->name_en}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3"></div>
    {{-- @include('new-btn') --}}
  </div><br>

  <div class="row">
    <div class="col-md-12">
        <table class="table table-bordered data-table type-2-table" style="width : 100%!important;">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Article Name</th>
                    <th>Category</th>
                    <th>Author Name</th>
                    <th>Published By</th>
                    <th>Published At</th>
                    {{-- <th class="status-col">Status</th> --}}
                    @if(auth()->user()->can('content-edit') || auth()->user()->can('content-view') || auth()->user()->can('content-publish') || auth()->user()->can('content-reply') || auth()->user()->can('content-active') || auth()->user()->can('content-delete'))
                      <th class="action-col"><span></span></th>
                    @else
                      <th class="hide-col" ><span></span></th>
                    @endif
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

   </div>
</div>
<script>
     $(document).ready(function () {
        var table = $('.type-2-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    ajax: "{{ url('to-reply') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                    },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'content_title', name: 'content_title'},
                        {data: 'category', name: 'category'},
                        {data: 'author_name', name: 'author_name'},
                        {data: 'published_user_name', name: 'published_user_name'},
                        {data: 'published_at', name: 'published_at'},
                        // {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });

          $('.list-search').on( 'keyup', function () {
              table.search( this.value ).draw();
          } );

          $('.list-search').on( 'search', function () {
             table.search( this.value ).draw();
          } );

          $('.type-filter-select2').select2({ placeholder : "Filter By Category", allowClear : true});

          $(document).on('change','.type-filter-select2',function(){
              var category_id = $('.type-filter-select2').val();
              table.ajax.url("{{ url('to-reply?category_id=')}}"+category_id).load();
          });
     });
   </script>
@stop
