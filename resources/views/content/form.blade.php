<input type="hidden" name="current_index" value="{{$content->current_index ?? 1}}">
<input type="hidden" name="quiz_id" value="{{$content->id}}">

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Content Type') }} <span class="text-red">*</span></label>
            <select name="content_type" class="form-control">
                <option value="">Select Type</option>
                @foreach (config("web_constant.content_types") as $i =>$k )
                    <option value="{{$i}}" @if($i==$content->content_type) selected @endif>{{$k}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Content Title') }} <span class="text-red">*</span></label>
            <input id="content_title" type="text" class="form-control" name="content_title" value="{{ $content->content_title }}" placeholder="Content Tilte">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Category') }} <span class="text-red">*</span></label>
            @php

                $categories = $content->categories->pluck('category_id')->toArray();

            @endphp
            <select name="category_id[]" class="form-control" multiple>
                <option value="">Select Category</option>
                @foreach (\App\Category::where('status',2)->get() as $c)
                    <option value="{{$c->id}}" @if(in_array($c->id,$categories)) selected @endif>{{$c->name_en}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-4 author">
        <div class="form-group">
            <label>{{ __('Author') }} <span class="text-red">*</span></label>
            <select name="author_id" class="form-control">
                <option value="">Select Author</option>
                @foreach (\App\User::where('role_id',5)->get() as $u )
                    <option value="{{$u->id}}" @if($u->id==$content->author_id) selected @endif>{{$u->name}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-4" style="display: block!important;" >
        <label for="" style="display: block;">{{__('Thumbnail')}} <span class="text-red">* </span><span class="photo_upload_description">( {{$upload_photo_description}} )</span></label>

        <div class="row preview-row" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null ) style="" @else style="display:none" @endif>
            <div class="col-md-10">
                <img id="upload-preview" src="{{$content->content_cover_photo}}" alt="upload photo" style="width:100%"/>
            </div>
            <div class="col-md-1">
                <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
            </div>
        </div>
        <div class="row">
            <input type="file" id="single-photo" class="@error('photo') is-invalid @enderror photo_file form-input form-control" name="content_cover_photo" placeholder="{{__('Upload Photo')}}" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null) style="display:none" @else style="" @endif>
            <input type="hidden" class=" @error('content_cover_photo') is-invalid @enderror" >
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>

            <input type="hidden" name="virtual_img" id="virtual-img" value="{{$content->content_cover_photo}}">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            @php
                $keywords = $content->keywords->where('explore_type',$content->content_type)->pluck('keyword_id')->toArray();
            @endphp
            <label>{{ __('Keyword') }}</label>
            <select name="keyword_id[]" class="form-control" multiple>
                <option value="">Select Keyword</option>
                @foreach (\App\Keyword::get() as $v )
                    <option value="{{$v->id}}" @if(in_array($v->id,$keywords)) selected @endif>{{$v->keyword_name}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Related Video') }} </label>
            <select name="video_id" class="form-control">
                <option value="">Select Video</option>
                @foreach (\App\Video::get() as $v )
                    <option value="{{$v->id}}" @if($v->id==$content->video_id) selected @endif>{{$v->video_title}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Reading Time') }}</label><span class="text-red"> (Minutes)</span>
            <input id="read_time" type="number" class="form-control" name="read_time" value="{{ $content->read_time }}" placeholder="Read Time">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-4" style="display : flex;margin-top: -5px">
        <div class="form-group">
        <label for="" class="ml-1 mt-1">Is Trending ?</label>
        @if ($content->is_trending)
           <x-adminlte-input-switch id="lte-switch" name="is_trending" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="1" checked/>
        @else
           <x-adminlte-input-switch id="lte-switch" name="is_trending" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="0"/>
        @endif
        </div>

    </div>


</div><br>

<div class="row">
    <div class="col-md-12">
        <h5>Content Details</h5>
        <table class="table data-table table-bordered content-detail-table">
            <thead>
                <tr>
                    <th style="width: 50px;">No.</th>
                    <th>Content Detail</th>
                    <th style="width: 50px!important;" class="action-col"><span></span></th>
                </tr>
            </thead>
            <tbody>
                @if($content->details->count() == 0)
                    <tr class="update-content-detail">
                        <td><span class="tb-index">1</span></td>
                        <td>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="">Upload Photo <span class="photo_upload_description">( {{$upload_photo_description}} )</span></label>
                                    <div class="row preview-row" style="display:none">
                                        <div class="col-md-2">
                                            <img id="upload-preview" src="#" alt="upload photo" style="width:100%"/>
                                        </div>
                                        <div class="col-md-1">
                                            <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="file" id="single-photo" class=" photo_file form-input form-control" name="detail_photo[]" placeholder="{{__('Upload Photo')}}">
                                        <input type="hidden" class="" >
                                        <span class="invalid-feedback" role="alert">
                                            <strong></strong>
                                        </span>

                                        <input type="hidden" name="detail_virtual_img[]" id="virtual-img" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="" class="mt-1">Description <span class="text-red">*</span></label>
                                    <textarea name="detail_description[]" class="form-control detail_description" id="detail_description_0" cols="30" rows="5"></textarea>
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <span class="btn remove-detail" style="display: none;"><i class="fa fa-times"></i></span>
                        </td>
                    </tr>
                @else
                    @foreach ($content->details as $k => $detail)
                        <tr class="update-content-detail">
                            <td><span class="tb-index">{{$k+1}}</span></td>
                            <td>
                               <div class="row">
                                   <div class="col-md-12">
                                        <label for="">Upload Photo <span class="photo_upload_description">( {{$upload_photo_description}} )</span></label>
                                        <div class="row preview-row" @if(isset($is_update) && $detail->detail_photo!=null) style="" @else style="display:none" @endif>
                                            <div class="col-md-2">
                                                <img id="upload-preview" src="{{$detail->detail_photo}}" alt="upload photo" style="width:100%"/>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="file" id="single-photo" class=" photo_file form-input form-control" name="detail_photo[]" placeholder="{{__('Upload Photo')}}" @if(isset($is_update) && $detail->detail_photo!=null) style="display:none" @else style="" @endif>
                                            <input type="hidden" class="" >
                                            <span class="invalid-feedback" role="alert">
                                                <strong></strong>
                                            </span>

                                            <input type="hidden" name="detail_virtual_img[]" id="virtual-img" value="{{$detail->detail_photo}}">
                                        </div><br>
                                   </div>
                               </div>

                               <div class="row">
                                  <div class="col-md-12">
                                        <label for="">Description <span class="text-red">*</span></label>
                                        <textarea name="detail_description[]" class="form-control detail_description" id="detail_description_{{$k}}" cols="30" rows="5">{{$detail->detail_description}}</textarea>
                                        <span class="invalid-feedback" role="alert">
                                            <strong></strong>
                                        </span>
                                  </div>
                               </div>
                            </td>
                            <td>
                                <span class="btn remove-detail" @if($loop->first) style="display: none;" @endif><i class="fa fa-times"></i></span>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" class="text-right"><span class="btn btn-info btn-sm add-detail"><i class="fa fa-plus"></i></span></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div><br>
{{--
<div class="faq-quiz">
<div class="row">
    <div class="col-md-6 ">
        <div class="form-group">
            <label>{{ __('FAQ') }} </label><br>
            <select name="select_faq_id" class="form-control">
                <option value="">Select FAQ</option>
                @foreach (\App\Faq::where('status',2)->get() as $c)
                    <option value="{{$c->id}}" data="{{Crypt::encryptString($c)}}">{{$c->faq_question}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
    <div class="col-md-6 ">
        <div class="form-group">
            <label>{{ __('Quiz') }} </label>
            <select name="select_quiz_id" class="form-control"><br>
                <option value="">Select Quiz</option>
                @foreach (\App\Quiz::where('status',2)->where('quiz_content_type',1)->get() as $c)
                    <option value="{{$c->id}}" data="{{Crypt::encryptString($c)}}">{{$c->quiz_question}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

</div><br>

<div class="row " >

   <div class="col-md-6 ">
        <h5>FAQ Lists</h5>
        <table class="table data-table table-bordered faq-table">
            <thead>
                <th style="width: 50px;">No.</th>
                <th>FAQ Question</th>
                <th style="width: 50px;" class="action-col"><span></span></th>
            </thead>
            <tbody>
                @if ($content->faqs->count() > 0)
                    @foreach ($content->faqs as $k => $faq)
                    <tr key="{{$faq->faq_id}}">
                        <td><span class="faq-index">{{$k+1}}</span></td>
                        <td>{{$faq->faq_question}}</td>
                        <td>
                            <span class="btn remove-faq">
                                <i class="fa fa-times"></i>
                            </span>
                            <input type="hidden" name="faq_id[]" value="{{$faq->faq_id}}">
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div> --}}

    {{-- <div class="col-md-6 ">
        <h5>Quiz Lists</h5>
        <table class="table table-bordered data-table quiz-table">
            <thead>
                <th style="width: 50px;">No.</th>
                <th>Quiz Question</th>
                <th style="width: 50px;" class="action-col"><span></span></th>
            </thead>
            <tbody>
               @if ($content->quizzes->count() > 0)
                   @foreach ($content->quizzes as $k => $quiz)
                      <tr key="{{$quiz->quiz_id}}">
                         <td><span class="quiz-index">{{$k+1}}</span></td>
                         <td>{{$quiz->quiz_question}}</td>
                         <td>
                             <span class="btn remove-quiz">
                                 <i class="fa fa-times"></i>
                             </span>
                             <input type="hidden" name="quiz_id[]" value="{{$quiz->quiz_id}}">
                         </td>
                      </tr>
                   @endforeach
               @endif
            </tbody>
        </table>
    </div>
</div><br> --}}
{{-- <div class="row">
    <div class="col-md-6">
        <strong class="text-red quiz-err"></strong>
    </div>
    <div class="col-md-6">
        <strong class="text-red faq-err"></strong>
    </div>
</div><br>
</div> --}}

<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div><br>

<script>
$(document).ready(function(){
    $('.update-content-detail').each(function(i,e){
        CKEDITOR.replace('detail_description_'+i, {
            filebrowserUploadUrl: "{{ route('content.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form',
            image_previewText: ' '
        });
    });

    checkContentType();
    checkIsTrending();
    $('select[name="category_id[]"]').select2({
        placeholder : 'Select Category',
        allowClear : true
    });

    $('select[name="keyword_id[]"]').select2({
        placeholder : 'Select Keywords',
        allowClear : true
    });

    $('select[name="select_faq_id"]').select2({
        placeholder : 'Select FAQ',
        allowClear : true
    });

    $('select[name="select_quiz_id"]').select2({
        placeholder : 'Select Quiz',
        allowClear : true
    });

    $('select[name=content_type]').select2({
        placeholder : 'Select Content',
        allowClear : true
    });

    $('select[name=video_id]').select2({
        placeholder : 'Select Video',
        allowClear : true
    });

    $('select[name=author_id]').select2({
        placeholder : 'Select Author',
        allowClear : true
    });



    $('.save-btn').click(function(){
        var that = $(this);

        var formData = new FormData(document.getElementById("content-form"));
        var quiz_id = $('form#content-form').find('input[name=quiz_id]').val();
        if(quiz_id.length > 0){
            var route = '/content/'+quiz_id;
            formData.append('_method', 'PUT');
        }else if(quiz_id.length == 0){
            var route = '/content';
        }

        // if($('select[name=content_type] option:selected').val()==1){
        //     let valid = true;
        //     if($('table.faq-table > tbody > tr').length==0){
        //         $('.faq-err').html('The faq must be at least one.');
        //         valid = false;
        //     }

        //     if($('table.quiz-table > tbody > tr').length==0){
        //         $('.quiz-err').html('The quiz must be at least one.');
        //         valid = false;
        //     }

        //     if(!valid) return false;

        // }

        let detail_desc = "";
        $.each( CKEDITOR.instances, function(i,e) {
            detail_desc += e.getData()+"@#$%^&*";
        });


        formData.append('detail_desc', detail_desc);

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){
                window.location.href = '/content?index='+data.index+'&type='+data.content_type;
            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.category_id!=undefined){
                    $('form#content-form').find('select[name="category_id[]"]').addClass('is-invalid');
                    $('form#content-form').find('select[name="category_id[]"]').siblings().find('strong').html(e.responseJSON.category_id)
                }

                if(e.responseJSON.content_cover_photo!=undefined){
                    $('form#content-form').find('input[name=content_cover_photo]').addClass('is-invalid');
                    $('form#content-form').find('input[name=content_cover_photo]').siblings().find('strong').html(e.responseJSON.content_cover_photo)
                }

                if(e.responseJSON.content_title!=undefined){
                    $('form#content-form').find('input[name=content_title]').addClass('is-invalid');
                    $('form#content-form').find('input[name=content_title]').siblings().find('strong').html(e.responseJSON.content_title)
                }

                if(e.responseJSON.content_type!=undefined){
                    $('form#content-form').find('select[name=content_type]').addClass('is-invalid');
                    $('form#content-form').find('select[name=content_type]').siblings().find('strong').html(e.responseJSON.content_type)
                }

                if(e.responseJSON.author_id!=undefined){
                    $('form#content-form').find('select[name=author_id]').addClass('is-invalid');
                    $('form#content-form').find('select[name=author_id]').siblings().find('strong').html(e.responseJSON.author_id)
                }

                if(e.responseJSON.video_id!=undefined){
                    $('form#content-form').find('select[name=video_id]').addClass('is-invalid');
                    $('form#content-form').find('select[name=video_id]').siblings().find('strong').html(e.responseJSON.video_id)
                }

                if(e.responseJSON.keyword_id!=undefined){
                    $('form#content-form').find('select[name="keyword_id[]"]').addClass('is-invalid');
                    $('form#content-form').find('select[name="keyword_id[]"]').siblings().find('strong').html(e.responseJSON.keyword_id)
                }

                if(e.responseJSON.read_time!=undefined){
                    $('form#content-form').find('input[name=read_time]').addClass('is-invalid');
                    $('form#content-form').find('input[name=read_time]').siblings().find('strong').html(e.responseJSON.read_time)
                }

                for(let i=0; i < $('table.content-detail-table > tbody > tr').length; i++ ){
                    if(e.responseJSON['detail_photo.'+i]!=undefined){
                        $('table.content-detail-table > tbody > tr:eq('+i+') ').find('input[name="detail_photo[]"]').addClass('is-invalid');
                        $('table.content-detail-table > tbody > tr:eq('+i+') ').find('input[name="detail_photo[]"]').siblings().find('strong').html(e.responseJSON['detail_photo.'+i]);
                    }

                    if(e.responseJSON['detail_description.'+i]!=undefined){
                        $('table.content-detail-table > tbody > tr:eq('+i+') ').find('textarea[name="detail_description[]"]').addClass('is-invalid');
                        $('table.content-detail-table > tbody > tr:eq('+i+') ').find('textarea[name="detail_description[]"]').siblings().find('strong').html(e.responseJSON['detail_description.'+i]);
                    }
                }

                return false;
            }
        })
    });

    // $('select[name=select_faq_id]').change(function(){
    //     let data = decryptData($('select[name=select_faq_id] option:selected').attr('data'));
    //     $('select[name=select_faq_id]').val(null).trigger('change.select2');

    //     if($('table.faq-table > tbody > tr[key='+data.id+']').length > 0) return false;

    //     let k = $('table.faq-table > tbody > tr').length + 1;
    //     let tr = "<tr key='"+data.id+"'>"+
    //                 "<td><span class='faq-index'>"+k+"</span></td>"+
    //                 "<td>"+data.faq_question+"</td>"+
    //                 "<td><span class='btn remove-faq'><i class='fa fa-times'></i></span><input type='hidden' class='faq_id' name='faq_id[]' value='"+data.id+"'></td>"+
    //              "</tr>";
    //     $('table.faq-table > tbody').append(tr);
    //     $('.faq-err').html(null);
    // });

    // $('select[name=select_quiz_id]').change(function(){
    //     let data = decryptData($('select[name=select_quiz_id] option:selected').attr('data'));
    //     $('select[name=select_quiz_id]').val(null).trigger('change.select2');

    //     if($('table.quiz-table > tbody > tr[key='+data.id+']').length > 0) return false;

    //     let k = $('table.quiz-table > tbody > tr').length + 1;
    //     let tr = "<tr key='"+data.id+"'>"+
    //                 "<td><span class='content-index'>"+k+"</span></td>"+
    //                 "<td>"+data.quiz_question+"</td>"+
    //                 "<td><span class='btn remove-quiz'><i class='fa fa-times'></i></span><input type='hidden' name='quiz_id[]' value='"+data.id+"'></td>"+
    //              "</tr>";
    //     $('table.quiz-table > tbody').append(tr);
    //     $('.quiz-err').html(null);
    // });

    $('.add-detail').click(function(){
        let tr = $('table.content-detail-table > tbody > tr:last').clone();
        $(tr).find('.remove-detail').css('display','');
        $(tr).find('textarea').val(null);
        $(tr).find('input').val(null);
        $(tr).find('input,textarea').removeClass('is-invalid');
        $(tr).find('.invalid-feedback strong').html(null);
        $(tr).find('.preview-row').css('display','none');
        $(tr).find('#upload-preview').attr('src','#');
        $(tr).find('#single-photo').css('display','block');

        let rm_ck = $(tr).find('.detail_description').attr('id');

        let len = $('table.content-detail-table > tbody > tr').length;
        $(tr).find('.tb-index').html(len+1);


        let rd_id = parseInt(Math.random() * 999);

        // $(tr).find('.detail_description').attr('name','detail_description_'+len+'[]');
        $(tr).find('.detail_description').attr('id','detail_description_'+rd_id);

        $('table.content-detail-table > tbody').append(tr);

        CKEDITOR.replace("detail_description_"+rd_id, {
            filebrowserUploadUrl: "{{ route('content.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form'
        });

        $('table.content-detail-table > tbody > tr:last').find('#cke_'+rm_ck).remove();
        $('table.content-detail-table > tbody > tr:last').find('#cke_detail_description_0').remove();

    });

    $(document).on('click','.remove-detail',function(){
        let k = $(this).parent().closest('tr').find('.detail_description').attr('id');
        CKEDITOR.instances[k].destroy(true);
        $(this).parent().closest('tr').remove();

        $('table.content-detail-table > tbody > tr').each(function(i,e){
            let j = i+1;
            $(e).find('.tb-index').html(j);

        });
    });

    // $(document).on('click','.remove-faq',function(){
    //    $(this).parent().closest('tr').remove();
    //    $('table.faq-table > tbody > tr').each(function(i,e){
    //         $(e).find('.faq-index').html(i+1);
    //     });
    // });

    // $(document).on('click','.remove-quiz',function(){
    //    $(this).parent().closest('tr').remove();
    //    $('table.quiz-table > tbody > tr').each(function(i,e){
    //         $(e).find('.content-index').html(i+1);
    //    });
    // });

    $('select[name=content_type]').change(function(){
        checkContentType();
    });


    $('#lte-switch').on('switchChange.bootstrapSwitch', function (event, state) {
       checkIsTrending();
    });

    function checkContentType(){
        if($('select[name=content_type] option:selected').val()==2){
            // $('.faq-quiz').css('display','none');
            $('.author').css('display','block');
        }else if($('select[name=content_type] option:selected').val()==1){
            // $('.faq-quiz').css('display','block');
            $('.author').css('display','none');
        }else if($('select[name=content_type] option:selected').val().length==0){
            // $('.faq-quiz').css('display','none');
            $('.author').css('display','none');
        }
    }

    function checkIsTrending(){
        if($("#lte-switch").is(':checked')){
            $("#lte-switch").val(1);
        }else{
            $("#lte-switch").val(0);
        }
    }

});
</script>
