@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<style>
    a.q-detail{
        color: black!important;
    }
</style>
<div class="card-body">
     @php
         $created_user_name = $content->created_user_name;
         $updated_user_name = $content->updated_user_name;
         $published_user_name = $content->published_user_name;
         $deactivated_user_name = $content->deactivated_user_name;
         
         $created_at = date('d-M-Y H:i:s',strtotime($content->created_at));
         $updated_at = $content->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($content->updated_at));
         $published_at = $content->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($content->published_at));
         $deactivated_at = $content->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($content->deactivated_at));

     @endphp

     <div class="row">
        <div class="col-md-12 text-center">
           <h5>{{$content->content_title}} ( {{config("web_constant.content_types.$content->content_type")}} )</h5>
           <p>{{implode(', ',$content->categories->pluck('name_en')->toArray())}}</p>
        </div>
     </div><br>

     <div class="row">
        <div class="col-md-4 " style="display: flex;">
            @if ($content->is_trending)
                <x-adminlte-input-switch id="lte-switch" name="is_trending" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="1" checked disabled/>
            @else
                <x-adminlte-input-switch id="lte-switch" name="is_trending" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="0" disabled/>
            @endif
            <label for="" class="ml-2 mt-1">Is Trending ?</label>
         </div>
         <div class="col-md-4 text-center">
            <label for="">Reading Time : </label> <b>{{$content->read_time}}</b>
         </div>
         <div class="col-md-4 text-right">
            @if ($content->content_type==2)
              <label for="">Author Name : </label> <b>{{$content->author_name}}</b>
            @endif
         </div>
     </div><br>

     <div class="row">
         <div class="col-md-4">
            <label for="">Preview Photo</label><br>
            <img src="{{$content->content_cover_photo}}" alt="" width="300" height="auto">
         </div>
     </div><br>

     <div class="row">
        <div class="col-md-12">
            <p><b>Keywords : </b>{{implode(', ',$content->keywords->pluck('keyword_name')->toArray())}}</p>
        </div>
     </div>

     <h5>{{config("web_constant.content_types.$content->content_type")}} Detail</h5>

     <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered data-table">
               <thead>
                   <tr>
                      <th style="width : 50px;">No.</th>
                      <th>Photo</th>
                      <th>Description</th>
                   </tr>
               </thead>
               <tbody>
                  @foreach ($content->details as $k => $detail)
                      <tr>
                          <td>{{$k+1}}</td>
                          <td>@if($detail->detail_photo!=null) <img src="{{$detail->detail_photo}}" alt="" width="200" height="auto"> @endif</td>
                          <td>
                            {!! $detail->detail_description !!}
                          </td>
                      </tr>
                  @endforeach
               </tbody>
            </table>
        </div>
     </div><br>
     @if ($content->content_type==1)
         <div class="row">
            <div class="col-md-6">
                <h5>Quiz Lists</h5>
                <table class="table data-table table-bordered">
                    <thead>
                        <tr>
                            <th style="width : 50px;">No.</th>
                            <th>Quiz Question</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($content->quizzes as $k => $quiz)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td><a class="q-detail" href="{{url('quiz/'.$quiz->quiz_id)}}">{{$quiz->quiz_question}}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h5>FAQ Lists</h5>
                <table class="table data-table table-bordered">
                    <thead>
                        <tr>
                            <th style="width : 50px;">No.</th>
                            <th>FAQ Question</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($content->faqs as $k => $faq)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$faq->faq_question}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
         </div><br>
     @endif
     @include('view-detail')
</div>
@stop
