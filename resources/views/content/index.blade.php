@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

@php
    $active_tab = isset($_GET['type']) ? $_GET['type'] : 1;
@endphp
<div class="row">
    @include('search')
    <div class="col-md-3">
        <select class="form-control select2 type-filter-select2 category" name="content_type" style="width: 100%;">
            <option value=""></option>
            @foreach (\App\Category::get() as $k=>$t)
                <option value="{{$t->id}}">{{$t->name_en}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3">
        <select class="form-control select2 faq-filter-select2 type-filter-select2 content_status" name="quiz_type" style="width: 100%;">
            <option value=""></option>
            @foreach (config("web_constant.status") as $k=>$t)
                <option value="{{$k}}">{{$t}}</option>
            @endforeach
        </select>
    </div>
    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">

      <ul class="nav nav-tabs sub-nav-tabs" id="myTab" role="tablist">
        @foreach (config("web_constant.content_types") as $k => $type)
          <li class="nav-item" role="presentation">
            <a class="nav-link content-tab @if($active_tab==$k) active @endif" id="type-{{$k}}-tab" data-toggle="tab" href="#type-{{$k}}" role="tab" aria-controls="type-{{$k}}" aria-selected="true">{{$type}}</a>
          </li>
        @endforeach
      </ul>

    <div class="tab-content sub-tab-content" id="myTabContent">
        <div class="tab-pane fade @if($active_tab==1) show active @endif" id="type-1" role="tabpanel" aria-labelledby="type-1-tab">
            @include('content.partial.type_1')
        </div>

        <div class="tab-pane fade @if($active_tab==2) show active @endif" id="type-2" role="tabpanel" aria-labelledby="type-2-tab">
            @include('content.partial.type_2')
        </div>

    </div>
  </div>
</div>
<script>
     $(document).ready(function () {
          $('.list-search').on( 'keyup', function () {
              if($('#type-1-tab').hasClass('active'))
                table1.search( this.value ).draw();
              else if($('#type-2-tab').hasClass('active'))
                 table2.search( this.value ).draw();
          } );

          $('.list-search').on( 'search', function () {
              if($('#type-1-tab').hasClass('active'))
                table1.search( this.value ).draw();
              else if($('#type-2-tab').hasClass('active'))
                 table2.search( this.value ).draw();
          } );

          $('.category').select2({ placeholder : "Filter By Category", allowClear : true});

          $(document).on('change','.type-filter-select2',function(){
                searchFun();

          });

          // $(document).on('click','.nav-link',function(){
          //     alert($('a.content-tab.active').attr('id'))
          // });


        //   if($('#type-1-tab').hasClass('active'))
        //           table1.ajax.url("{{ url('content?category_id=')}}"+category_id+'&content_type=1').load();
        //       else if($('#type-2-tab').hasClass('active'))
        //           table2.ajax.url("{{ url('content?category_id=')}}"+category_id+'&content_type=2').load();

        //         //
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                category_id = '';
                content_status = '';
                if($('.type-filter-select2').val().length > 0 || $('.content_status').val().length > 0){
                    $('.type-filter-select2').val(null).trigger('change.select2');
                    $('.content_status').val(null).trigger('change.select2');
                    if(e.relatedTarget.id=="type-1-tab"){ // previous active tab
                       table1.ajax.url("{{ url('content?category_id=')}}"+category_id+'&content_type=1&content_status='+content_status).load();
                    }else{
                      table2.ajax.url("{{ url('content?category_id=')}}"+category_id+'&content_type=2&content_status='+content_status).load();                    }
                }

                if($('.list-search').val().length > 0){
                    $('.list-search').val(null);
                    if(e.relatedTarget.id=="type-1-tab"){ // previous active tab
                      table1.search( '' ).draw();
                    }else{
                      table2.search( '' ).draw();
                    }
                }
          });

         $('.content_status').select2({ placeholder : "Filter By Status", allowClear : true});

         $(document).on('change','.content_status',function(){
              searchFun();
         });

         function searchFun(){
            var content_status = $('.content_status').val();
            var category_id = $('.category').val();
            if ($('#type-1-tab').hasClass('active')){
                // table1.ajax.url("{{ url('content')}}"+"?content_status="+content_status).load();
                table1.ajax.url("{{ url('content?category_id=')}}"+category_id+'&content_type=1&content_status='+content_status).load();
            }
            else
                // table2.ajax.url("{{ url('content')}}"+"?content_status="+content_status).load();
                table2.ajax.url("{{ url('content?category_id=')}}"+category_id+'&content_type=2&content_status='+content_status).load();
        }
     });
   </script>
@stop
