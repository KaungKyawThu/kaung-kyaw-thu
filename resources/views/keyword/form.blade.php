<input type="hidden" name="current_index" value="{{$keyword->current_index}}">
{{-- <input type="hidden" name="status" value=""> --}}
<input type="hidden" name="is_most_search" value="">
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Keyword Name') }} <span class="text-red">*</span></label>
            <input id="keyword_name" type="text" class="form-control @error('keyword_name') is-invalid @enderror" name="keyword_name" value="{{ old('keyword_name', $keyword->keyword_name) }}" autocomplete="name" autofocus placeholder="Name">

            @error('keyword_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    @php

    $is_most_search = old('is_most_search') ?? $keyword->is_most_search;

    @endphp
    <div class="col-md-6">
        <div class="form-group">

              <label for="" class="ml-1 mt-1"> Is Most search ? </label>
              @if ($is_most_search)
                <x-adminlte-input-switch id="lte-switch" name="is_most_search" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="1"  checked/>
              @else
                <x-adminlte-input-switch id="lte-switch" name="is_most_search" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="0" />
              @endif



        </div>

    </div>
</div>


@include('save-btn')
<script>

$(document).ready(function(){

    checkIsTrending();

    $('#lte-switch').on('switchChange.bootstrapSwitch', function (event, state) {
       checkIsTrending();
    });

    function checkIsTrending(){
        if($("#lte-switch").is(':checked')){
            $("#lte-switch").val(1);
        }else{
            $("#lte-switch").val(0);
        }
    }

    $(document).on('click','#select_module',function(){

if($(this).is(':checked')){

    $(this).parent().parent().find('input[type=checkbox]').val(1);

}
else{
    $(this).parent().parent().find('input[type=checkbox]').val(0);

}
});

});
</script>
