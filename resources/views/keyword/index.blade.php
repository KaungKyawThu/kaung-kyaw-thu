@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    @include('search')
    {{-- <div class="col-md-3">
        <select class="form-control select2 role-filter-select2" name="role_id" style="width: 100%;">
            <option value=""></option>
            @foreach (DB::table('roles')->get() as $r)
                <option value="{{$r->id}}">{{$r->name}}</option>
            @endforeach
        </select>
    </div> --}}
    <div class="col-md-6"></div>
    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Keyword Name</th>
                <th>Is Most Search?</th>
                <th>Created By</th>
                <th>Created At</th>
                @if(auth()->user()->can('keyword-edit') || auth()->user()->can('keyword-delete') || auth()->user()->can('keyword-publish') || auth()->user()->can('keyword-active'))
                  <th class="action-col" ><span></span></th>
                @else
                  <th class="hide-col" ><span></span></th>
                @endif
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
   <script>
     $(document).ready(function () {
       var table = $('.data-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    ajax: "{{ url('keyword') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'keyword_name', name: 'keyword_name'},
                        {data: 'is_most_search', name: 'is_most_search' },
                        {data: 'created_user_name', name: 'created_user_name' },
                        {data: 'created_at', name: 'created_at' },
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });

       $('.list-search').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $('.list-search').on( 'search', function () {
            table.search( this.value ).draw();
        } );
     });
   </script>
@stop
