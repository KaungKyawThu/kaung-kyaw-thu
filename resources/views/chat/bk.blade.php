@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<style>
    thead{
        display: none;
    }
</style>
<div class="row">
    @include('search')

    <div class="col-md-9"></div>
  </div><br>

  <div class="row">
    <div class="col-md-12 chat-lists">
        <table class=" chat-lists-table">
            <tbody>

            @foreach ($data as $item)
              <tr><td>
                @php
                    $last_msg = collect($item->user->chats)->last();
                @endphp
                <div class="chat-div user-{{$item->user_id}}">
                    <div class="chat-list-title">
                        <h5 class="chat-name">
                            <img src="{{config('web_constant.user_photo')}}" width="40" class="user-avator img-circle elevation-2 mr-2" alt="Admin">
                            {{$item->user->nick_name ?? $item->user->name}}
                        </h5>
                        <span class="chat-last-time">{{date('d/m/Y',strtotime($last_msg->created_at))}}</span>
                    </div>

                    <div class="chat-message">

                        @if ($last_msg->message_type==1)
                           <p>{{$last_msg->message}}</p>
                        @else
                           <img src="{{$last_msg->message}}" alt="" width="75" height="50">
                        @endif
                        @php
                            $unread_count = $item->user->chats->where('is_read',0)->count();
                        @endphp
                        @if($unread_count > 0)
                            <span class="chat-unread-count">
                                {{$unread_count}}
                            </span>
                        @endif
                    </div>
                    <input type="hidden" name="user_chats" value="{{Crypt::encryptString($item->user)}}">
                </div>
            </td></tr>
            @endforeach

        </tbody>
    </table>

    </div>
      <div class="chat-detail col-md-6" style="display: none">
        <div class="card" style="width: 100%;">
            <div class="card-header" style="display: flex;" >
                <div style="display: flex; width: 100%;"></div>
                <button type="button" class="close close-chat float-right" aria-label="Close">
                    <span class="modal-close-btn" aria-hidden="true">&#x2715</span>
                </button>

            </div>
            <div class="card-body chat-component">
                <div><img src="https://miro.medium.com/max/880/0*H3jZONKqRuAAeHnG.jpg" alt="" width="250" class="loading-img"></div>
            </div>
            <div class="card-footer chatting-footer">
                <div style="width:95%;">
                    <textarea name="message" class="form-control" cols="30" rows="3" placeholder="Type a message here" autofocus></textarea>
                    <input type="file" name="photo_message" style="display: none;">
                </div>
                <div class="text-right">
                    <span class="btn btn-primary btn-sm send-photo"><i class="fa fa-image"></i></span>
                    <span class="btn btn-primary btn-sm send-sms"><i class="fa fa-paper-plane"></i></span>
                </div>
            </div>
        </div>
    </div>
  </div>


   <script>
     $(document).ready(function () {
           var reply_user = null;
           var chat_id = null;

           var table = $('.chat-lists-table').DataTable({
                            'bSort': false,
                            'pageLength' : 30,
                            'aoColumns': [
                                    { sWidth: "45%", bSortable: false },
                            ],
                            "info":           true,
                            "paging":         true
                       });

            $('.list-search').on( 'keyup', function () {
                table.search( this.value ).draw();
            });

            $('.list-search').on( 'search', function () {
                table.search( this.value ).draw();
            });

           $(document).on('click','.chat-div',function(){

                if($(this).hasClass('reading')) return false;
                else{

                    let data = decryptData($(this).find('input[name=user_chats]').val());
                    reply_user = data.id;
                    setTimeout(() => {

                        $('.card-body').empty();
                        data.chats.map(function(e,i){
                            let msg = "<div class='row'><div class='col-md-12'>"+
                                        "<div class='chat_type_"+e.chat_type+" "+e.id+"'>";

                            if(e.chat_type==1){
                                msg += "<img src="{{config('web_constant.user_photo')}}" width='20' class='user-avator img-circle elevation-2 mr-2' alt='Admin'> ";
                            }

                            if(e.message_type==1)
                                msg += "<p>"+e.message+"<span class='chat-datetime'>"+changeDateFormat(e.created_at)+"</span></p>";
                            else msg += "<img width='150' height='100' src='"+e.message+"'><span class='chat-datetime with-img'>"+changeDateFormat(e.created_at)+"</span>";

                            msg += "</div>"+
                                    "</div></div>";


                            $('.card-body').append(msg);
                        });

                        let header = "<img src="{{config('web_constant.user_photo')}}" width='40' class='user-avator img-circle elevation-2 mr-2' alt='Admin'>"+
                                    "<h5>"+data.nick_name+"</h5>";
                        $('.card-header div').html(header);

                    }, 500);

                    $(document).find('.chat-div').removeClass('reading');
                    $(this).addClass('reading');

                    $('.chat-lists').addClass('col-md-6');
                    $('.chat-lists').removeClass('col-md-12');
                    $(".chat-detail").fadeOut(500);
                    $(".chat-detail").fadeIn(1000);
                    $(this).find('.chat-unread-count').fadeOut(1000);

                    $('.chat-component').animate({scrollTop:1000000},800);

                    if($(this).find('.chat-unread-count').length > 0)
                      updateIsReadStataus(data.id);
                }

           });

        $(document).on('dblclick','.chat_type_2',function(){
              $('#confirm-modal .modal-footer .save-btn').removeClass('confirm-delete-button');
              $('#confirm-modal .modal-footer .save-btn').removeClass('disabled');
              $('#confirm-modal .modal-footer .save-btn').html('Sure');
              $('#confirm-modal .modal-footer .save-btn').addClass('chat-delete-button');

              let cur_class = $(this).attr('class');
                  chat_id = cur_class.replace("chat_type_2 ", "");
              $('#confirm-modal').modal('show');
        });

        $(document).on('click','.chat-delete-button',function(){
            $('#confirm-modal').modal('hide');
            $.ajax({
                url : '/chat/'+chat_id,
                method : 'DELETE',
                dataType : 'json',
                data : {
                    '_token' : $('meta[name=csrf-token]').attr('content'),
                    'user_id' : reply_user,
                },
                success : function(data){
                    if(!data.success) return false;

                    $('.chat-component').find('.'+data.chat_id).remove();

                    $('.reading').find('input[name=user_chats]').val(data.chats);
                    let last_message = data.last_message;

                    let msg = '';
                    if(last_message.message_type==1)
                        msg = "<p>"+last_message.message+"</p>";
                    else  msg = "<img src="+last_message.message+" width='75' height='50'>";
                    $('.reading .chat-message').html(msg);
                    $('.reading .chat-last-time').html(changeDateFormat(last_message.created_at));
                },
                error : function(e){
                    console.log(e);
                }
            });
        });

        $('.send-sms').click(function(){
            if($('textarea[name=message]').val().length==0)  return false;

            $.ajax({
                url : '/chat',
                method : 'POST',
                dataType : 'json',
                data : {
                    '_token' : $('meta[name=csrf-token]').attr('content'),
                    'message' : $('textarea[name=message]').val(),
                    'user_id' : reply_user,
                    'message_type' : 1
                },
                success : function(data){
                    if(!data.success) return false;

                    $('.reading').find('input[name=user_chats]').val(data.chats);
                    let new_message = data.new_message;
                    let msg = "<div class='row'><div class='col-md-12'>"+
                                    "<div class='chat_type_"+new_message.chat_type+" "+new_message.id+"'>"+
                                      "<p>"+new_message.message+"<span class='chat-datetime'>"+changeDateFormat(new_message.created_at)+"</span></p>";
                                    "</div>"+
                                "</div></div>";

                    $('.card-body').append(msg);
                    $('textarea[name=message]').val(null);
                    $('.reading .chat-message').html("<p>"+new_message.message+"</p>");
                    $('.reading .chat-last-time').html(changeDateFormat(new_message.created_at));
                    $('.chat-component').animate({scrollTop:1000000},800);
                },
                error : function(e){
                    console.log(e);
                }
            });
        });

        $('.send-photo').click(function(e){
            $('input[name=photo_message]').click();
        });

        $(document).on("change","input[name=photo_message]",function(){
            console.log(this);
            readURL(this)
        });

        $(document).on('click','.page-link',function(){
            closeChat();
        });

        $('.close-chat').click(function(){
           closeChat();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                if((input.files[0].size / 1024) > 2500){
                    $(input).val(null);
                    alert('You upload file is exceed 2.5 MB. ');
                    return false;
                }else{

                    var formData = new FormData();
                        formData.append('message', input.files[0]);
                        formData.append('_token', $('meta[name=csrf-token]').attr('content'));
                        formData.append('user_id', reply_user);
                        formData.append('message_type', 2);

                    $.ajax({
                        url : 'chat',
                        type : 'POST',
                        data : formData,
                        processData: false,
                        contentType: false,
                        dataType : 'json',
                        success : function(data) {
                            if(!data.success) return false;

                            $('.reading').find('input[name=user_chats]').val(data.chats);
                            let new_message = data.new_message;
                            let msg = "<div class='row'><div class='col-md-12'>"+
                                    "<div class='chat_type_"+new_message.chat_type+" "+new_message.id+"'>"+
                                       "<img width='150' height='100' src='"+new_message.message+"'><span class='chat-datetime with-img'>"+changeDateFormat(new_message.created_at)+"</span>"+
                                    "</div>"+
                                "</div></div>";

                               $('.card-body').append(msg);
                               $('.reading .chat-message').html("<img src="+new_message.message+" width='75' height='50'>")
                               $('.reading .chat-last-time').html(changeDateFormat(new_message.created_at));
                        },
                        error : function(e){
                            console.log(e);
                        }
                    });
                }
            }
        }

     });

    function updateIsReadStataus(user_id){
            $.ajax({
                url : '/user/'+user_id+'/read_chat',
                method : 'GET',
                dataType : 'json',
                success : function(data){
                    console.log(data);
                },
                error : function(e){
                    console.log(e);
                }
            });
    }

    function closeChat(){
        $(document).find('.chat-div').removeClass('reading');

        $('.chat-lists').addClass('col-md-12');
        $('.chat-lists').removeClass('col-md-6');
        $('.chat-detail').fadeOut(500);
    }

    function changeDateFormat(date){
        var d=new Date(date);
        var dd=d.getDate();
        var mm=d.getMonth()+1;
        var yy=d.getFullYear();
        return dd+"/"+mm+"/"+yy;
    }
   </script>
@stop
