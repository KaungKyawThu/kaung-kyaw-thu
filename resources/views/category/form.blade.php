<input type="hidden" name="current_index" value="{{$category->current_index}}">
{{-- <input type="hidden" name="status" value=""> --}}
<input type="hidden" name="is_popular" value="">
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('English Name') }} <span class="text-red">*</span></label>
            <input id="name_en" type="text" class="form-control @error('name_en') is-invalid @enderror" name="name_en" value="{{ old('name_en', $category->name_en) }}" autocomplete="name" autofocus placeholder="Name">

            @error('name_en')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Myanmar Name') }} <span class="text-red">*</span></label>
            <input id="name_mm" type="text" class="form-control @error('name_mm') is-invalid @enderror" name="name_mm" value="{{ old('name_mm', $category->name_mm) }}" autocomplete="name" autofocus placeholder="Enter Myanmar Name">

            @error('name_mm')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    @php

    $is_popular = old('is_popular') ?? $category->is_popular;

    @endphp
    <div class="col-md-6">
        <div class="form-group">
            <label>
                Is Popular ?
            </label>
            @if ( $is_popular )
           <x-adminlte-input-switch id="lte-switch" name="is_popular" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="1" checked/>
            @else
            <x-adminlte-input-switch id="lte-switch" name="is_popular" data-on-text="YES" data-off-text="NO" data-on-color="primary" value="0"/>
            @endif

        </div>
    </div>
</div>

@include('save-btn')
<script>
    $('#lte-switch').on('switchChange.bootstrapSwitch', function (event, state) {
       checkIsTrending();
    });
    function checkIsTrending(){
        if($("#lte-switch").is(':checked')){
            $("#lte-switch").val(1);
        }else{
            $("#lte-switch").val(0);
        }
    }
     $(document).on('click','#select_module',function(){

            if($(this).is(':checked')){

                $(this).parent().parent().find('input[type=checkbox]').val(1);

            }
            else{
                $(this).parent().parent().find('input[type=checkbox]').val(0);

            }
        });
</script>
