@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    @include('search')
    <div class="col-md-3">
        <select class="form-control select2 faq-filter-select2 category_status" name="quiz_type" style="width: 100%;">
            <option value=""></option>
            @foreach (config("web_constant.status") as $k=>$t)
                <option value="{{$k}}">{{$t}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3"></div>
    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>English Name</th>
                <th>Myanmar Name</th>
                <th>Is Popular</th>
                <th class="status-col">Status</th>
                @if(auth()->user()->can('category-edit') || auth()->user()->can('category-delete') || auth()->user()->can('category-publish') || auth()->user()->can('category-active'))
                  <th class="action-col" ><span></span></th>
                @else
                  <th class="hide-col" ><span></span></th>
                @endif
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
   <script>
     $(document).ready(function () {
       var table = $('.data-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    ajax: "{{ url('category') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'name_en', name: 'name_en'},
                        {data: 'name_mm', name: 'name_mm'},
                        {data: 'is_popular', name: 'is_popular'},
                        {data: 'status', name: 'status' },
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });

       $('.list-search').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $('.list-search').on( 'search', function () {
            table.search( this.value ).draw();
        } );
        $('.category_status').select2({ placeholder : "Filter By Status", allowClear : true});


       $(document).on('change','.category_status',function(){
                    searchFun();
            });

       function searchFun(){
       var category_status = $('.category_status').val();

       table.ajax.url("{{ url('category')}}"+"?category_status="+category_status).load();
       }
     });
   </script>
@stop
