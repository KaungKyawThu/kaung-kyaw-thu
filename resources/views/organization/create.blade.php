@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('organization') }}">
        @csrf
        @include('organization.form')
    </form>
</div>
@stop
