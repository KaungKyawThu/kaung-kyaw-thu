@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

    <div class="row">
        @include('search')
        {{-- <div class="col-md-3">
        <select class="form-control select2 role-filter-select2" name="role_id" style="width: 100%;">
            <option value=""></option>
            @foreach (DB::table('roles')->get() as $r)
                <option value="{{$r->id}}">{{$r->name}}</option>
            @endforeach
        </select>
    </div> --}}
        <div class="col-md-3">
            <select class="form-control select2 type-filter-select2 organization_status" name="quiz_type" style="width: 100%;">
                <option value=""></option>
                @foreach (config('web_constant.status') as $k => $t)
                    <option value="{{ $k }}">{{ $t }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3"></div>
        @include('new-btn')
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>English Name</th>
                        <th>Myanmar Name</th>
                        <th>Created By</th>
                        <th>Published By</th>
                        <th>Published At</th>
                        <th class="status-col">Status</th>
                        @if (auth()->user()->can('organization-edit') ||
                            auth()->user()->can('organization-delete') ||
                            auth()->user()->can('organization-publish') ||
                            auth()->user()->can('organization-active'))
                            <th class="action-col"><span></span></th>
                        @else
                            <th class="hide-col"><span></span></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: false,
                info: false,
                autoWidth: true,
                responsive: true,
                processing: true,
                serverSide: true,
                //displayStart : "{{ $current_index }}",
                //stateSave: true,
                ajax: "{{ url('organization') }}",
                createdRow: function(row, data, dataIndex) {
                    if (data.action.length == 0)
                        $(row).find('td.action-col').css('display', 'none');
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        class: 'data-index'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'organization_mm_name',
                        name: 'organization_mm_name'
                    },
                    {
                        data: 'created_user_name',
                        name: 'created_user_name'
                    },
                    {
                        data: 'published_user_name',
                        name: 'published_user_name'
                    },
                    {
                        data: 'published_at',
                        name: 'published_at'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'action-col'
                    },
                ]
            });

            $('.list-search').on('keyup', function() {
                table.search(this.value).draw();
            });

            $('.list-search').on('search', function() {
                table.search(this.value).draw();
            });
            $('.type-filter-select2').select2({
                placeholder: "Filter By status",
                allowClear: true
            });

            $(document).on('change', '.type-filter-select2', function() {
                searchFun();
            });

            function searchFun() {
                var organization_status = $('.organization_status').val();

                table.ajax.url("{{ url('organization') }}" + "?organization_status=" + organization_status).load();
            }
        });
    </script>
@stop
