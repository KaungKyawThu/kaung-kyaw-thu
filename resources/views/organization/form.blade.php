<input type="hidden" name="current_index" value="{{$organization->current_index}}">
{{-- <input type="hidden" name="status" value=""> --}}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Organization English Name') }} <span class="text-red">*</span></label>
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $organization->name) }}" autocomplete="name" autofocus placeholder="Organization Name">

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Organization Myanmar Name') }} <span class="text-red">*</span></label>
            <input id="organization_mm_name" type="text" class="form-control @error('organization_mm_name') is-invalid @enderror" name="organization_mm_name" value="{{ old('organization_mm_name', $organization->organization_mm_name) }}" autocomplete="organization_mm_name" autofocus placeholder="Organization Myanmar Name">

            @error('organization_mm_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

@include('save-btn')
<script>
     $(document).on('click','#select_module',function(){

            if($(this).is(':checked')){

                $(this).parent().parent().find('input[type=checkbox]').val(1);

            }
            else{
                $(this).parent().parent().find('input[type=checkbox]').val(0);

            }
        });
</script>
