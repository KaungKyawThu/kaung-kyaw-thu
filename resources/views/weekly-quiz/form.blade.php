<input type="hidden" name="current_index" value="{{$quiz->current_index ?? 1}}">
<input type="hidden" name="quiz_id" value="{{$quiz->id}}">
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Schedule Date  <span class="text-red">*</span></label>
           <input type="date" name="schedule_date" class="form-control" value="{{$quiz->schedule_date}}" min="{{date("Y-m-d",time() + 86400)}}">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <label>{{ __('Hours') }} <span class="text-red">*</span></label><br>


            <select name="hours" id="hour" class="form-control " @if($quiz->status==2) disabled @endif>
                @for($i = 1 ; $i < 24 ; $i++)
                    <option value="{{ $i }}" name=""  @if ($i == $quiz->hours ) selected @endif>{{ $i }}</option>
                @endfor
                <option value="0"  @if ($i == $quiz->hours ) selected @endif>0</option>
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label for="">Weekly Fact <span class="text-red">*</span></label>
        <textarea name="weekly_fact" class="form-control" placeholder="Weekly Fact" rows="3">{{$quiz->weekly_fact}}</textarea>
        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>
    </div>
</div><br>

<label for="">Quiz Type </label>
<div class="row">
   @php
       $quiz_type = $quiz->quiz_type;
   @endphp
   @foreach (config("web_constant.quiz_types") as $type => $type_name)
       @if($type!=3)
            <div class="col-md-2">
                <input type="radio" value="{{$type}}" name="quiz_type" @if( $quiz_type==$type || $loop->first) checked @endif> <span class="qType">{{$type_name}}</span>
            </div>
        @endif
   @endforeach
</div><br>

<div class="row">
    <div class="col-md-6">
        <label for="">Quiz Question <span class="text-red">*</span></label>
        <textarea name="quiz_question" class="form-control" placeholder="Quiz Question" rows="3">{{$quiz->quiz_question}}</textarea>
        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>
    </div>
</div>
<div class="row mt-2">
     <div class="col-md-6 quiz_options">
        <div class="true_false" >
            @if($quiz_type==1 && $quiz->options->count() > 0)
                @foreach ($quiz->options as $opt)
                    <div style="display: flex;" class="mt-2">
                        <input type="radio" name="is_correct_answer[]" value="{{$opt->is_correct}}" @if($opt->is_correct) checked @endif>
                        <input type="text" class="form-control ml-1" name="quiz_option[]" value="{{$opt->quiz_option}}" readonly>
                        <input type="hidden" name="option_id[]" value="{{$opt->id}}">
                    </div>
                @endforeach
            @else
                <div style="display: flex;">
                    <input type="radio" name="is_correct_answer[]" value="1" checked>
                    <input type="text" class="form-control ml-1" name="quiz_option[]" value="TRUE (မှန်)" readonly>
                    <input type="hidden" name="option_id[]" value="">
                </div>
                <div style="display: flex;" class="mt-3">
                    <input type="radio" name="is_correct_answer[]" value="0">
                    <input type="text" class="form-control ml-1" name="quiz_option[]" value="FALSE (မှား)" readonly>
                    <input type="hidden" name="option_id[]" value="">
                </div>
            @endif
           
        </div>

        <div class="single_choice" style="display: none;">
            @if(($quiz_type==2 || $quiz_type==4) && $quiz->options->count() > 0)
                @foreach ($quiz->options as $opt)
                    <div class="options">
                        <div style="display: flex;">
                            <input type="radio" name="is_correct_answer[]" value="{{$opt->is_correct}}" @if($opt->is_correct) checked @endif>
                            <input type="text" class="form-control ml-1" name="quiz_option[]" value="{{$opt->quiz_option}}" placeholder="Quiz Option">
                            @if(!$loop->first)
                                <span class="btn remove-option"><i class="fa fa-trash"></i></span>
                            @endif
                        </div>
                        <span class="invalid-feedback" role="alert">
                            <strong></strong>
                        </span>
                        <input type="hidden" name="option_id[]" value="{{$opt->id}}">
                    </div>
                @endforeach
            @else
                <div class="options">
                    <div style="display: flex;">
                        <input type="radio" name="is_correct_answer[]" value="1">
                        <input type="text" class="form-control ml-1" name="quiz_option[]" value="" placeholder="Quiz Option">
                    </div>
                    <span class="invalid-feedback" role="alert">
                        <strong></strong>
                    </span>
                    <input type="hidden" name="option_id[]" value="">
                </div>
            @endif
        </div>
       
     </div>
</div>

<div class="row">
    <div class="col-md-6">
        <input type="hidden" name="other_validation_err">
        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <span class="btn btn-sm btn-info float-right mt-2 add-option" style="display: none;">
            <i class="fa fa-plus"></i>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label for="">Answer Description<span class="text-red">*</span></label>
        <textarea name="answer_description" class="form-control" placeholder="Answer Description" rows="3">{{$quiz->answer_description}}</textarea>
        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>
    </div>
</div><br>

<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div><br>

<script>
$(document).ready(function(){
    checkQuizType();
    $('select[name=quiz_content_type]').select2({ placeholder : "Select Category", allowClear : true});

    $('.add-option').click(function(){
        if($('input[name=quiz_type]:checked').val()==2 || $('input[name=quiz_type]:checked').val()==4){ //if single choice
            let new_option = '<div class="mt-2 options">'+
                                '<div style="display: flex;">'+
                                    '<input type="radio" value="0" name="is_correct_answer[]">'+
                                    '<input type="text" class="form-control ml-1" name="quiz_option[]" value="" placeholder="Quiz Option">'+
                                    '<span class="btn remove-option"><i class="fa fa-trash"></i></span>'+
                                '</div>'+
                                '<span class="invalid-feedback" role="alert">'+
                                    '<strong></strong>'+
                                '</span>'+
                                '<input type="hidden" name="option_id[]" value="">'+
                             '</div>';

            $('.single_choice').append(new_option);
        }

        $('input[name=other_validation_err]').removeClass('is-invalid');
        $('input[name=other_validation_err]').siblings().find('strong').html(null);
    });

    $('input[name=quiz_type]').click(function(){
        checkQuizType();
    });

    $(document).on('click','.remove-option',function(){
        $(this).parent().parent().remove();
    });

    $(document).on('click','input[name="is_correct_answer[]"]',function(){
        $('input[name="is_correct_answer[]"]').val(0);
        $('input[name=other_validation_err]').removeClass('is-invalid');
        $('input[name=other_validation_err]').siblings().find('strong').html(null);
        $(this).val(1);
    });

    $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("weekly-quiz-form"));
        var quiz_id = $('form#weekly-quiz-form').find('input[name=quiz_id]').val();
        if(quiz_id.length > 0){
            var route = '/weeklyquiz/'+quiz_id;
            formData.append('_method', 'PUT');
        }else if(quiz_id.length == 0){
            var route = '/weeklyquiz';
        }

        let quiz_option = [];
        let option_id = [];
        let is_correct_answer = [];
        let correctAns = 0;
        if($('input[name=quiz_type]:checked').val()==1){
            $('.true_false div').each(function(i,e){
                let opt = $(e).find('input[name="quiz_option[]"]').val();
                let is_ans = $(e).find('input[name="is_correct_answer[]"]').val();
                let opt_id = $(e).find('input[name="option_id[]"]').val();

                quiz_option.push(opt);
                is_correct_answer.push(is_ans);
                option_id.push(opt_id);

                if($(e).find('input[name="is_correct_answer[]"]').is(':checked')) correctAns=1;
            });
        }else if($('input[name=quiz_type]:checked').val()==2 || $('input[name=quiz_type]:checked').val()==4){
            $('.single_choice div.options').each(function(i,e){

                let opt = $(e).find('input[name="quiz_option[]"]').val();
                let is_ans = $(e).find('input[name="is_correct_answer[]"]').val();
                let opt_id = $(e).find('input[name="option_id[]"]').val();

                quiz_option.push(opt);
                is_correct_answer.push(is_ans);
                option_id.push(opt_id);
                if($(e).find('input[name="is_correct_answer[]"]').is(':checked')) correctAns=1;
            });
        }

        if(($('input[name=quiz_type]:checked').val()==2 || $('input[name=quiz_type]:checked').val()==4)  && quiz_option.length < 3 ){
            $('input[name=other_validation_err]').addClass('is-invalid');
            $('input[name=other_validation_err]').siblings().find('strong').html('The quiz option must be at least 3 options.');
            return false;
        }

        if( correctAns==0){
            $('input[name=other_validation_err]').addClass('is-invalid');
            $('input[name=other_validation_err]').siblings().find('strong').html('The correct answer must be at least one option.');
            return false;
        }

        formData.append('quiz_option', quiz_option);
        formData.append('is_correct_answer', is_correct_answer);
        formData.append('option_id', option_id);

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                window.location.href = '/weeklyquiz?index='+data.index;
            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.schedule_date!=undefined){
                    $('form#weekly-quiz-form').find('input[name=schedule_date]').addClass('is-invalid');
                    $('form#weekly-quiz-form').find('input[name=schedule_date]').siblings().find('strong').html(e.responseJSON.schedule_date)
                }

                if(e.responseJSON.quiz_question!=undefined){
                    $('form#weekly-quiz-form').find('textarea[name=quiz_question]').addClass('is-invalid');
                    $('form#weekly-quiz-form').find('textarea[name=quiz_question]').siblings().find('strong').html(e.responseJSON.quiz_question)
                }

                if(e.responseJSON.weekly_fact!=undefined){
                    $('form#weekly-quiz-form').find('textarea[name=weekly_fact]').addClass('is-invalid');
                    $('form#weekly-quiz-form').find('textarea[name=weekly_fact]').siblings().find('strong').html(e.responseJSON.weekly_fact)
                }

                
                if(e.responseJSON.answer_description!=undefined){
                    $('form#weekly-quiz-form').find('textarea[name=answer_description]').addClass('is-invalid');
                    $('form#weekly-quiz-form').find('textarea[name=answer_description]').siblings().find('strong').html(e.responseJSON.answer_description)
                }
             
                if($('input[name=quiz_type]:checked').val()==1){
                    for(let i=0; i < $('.true_false div').length; i++ ){
                        if(e.responseJSON['quiz_option.'+i]!=undefined){
                            $('.true_false div:eq('+i+') ').find('input[name="quiz_option[]"]').addClass('is-invalid');
                            $('.true_false div:eq('+i+') ').find('input[name="quiz_option[]"]').siblings().find('strong').html(e.responseJSON['quiz_option.'+i]);
                        }
                    }
                }else if($('input[name=quiz_type]:checked').val()==2 || $('input[name=quiz_type]:checked').val()==4){
                    for(let i=0; i < $('.single_choice div.options').length; i++ ){
                        if(e.responseJSON['quiz_option.'+i]!=undefined){
                            $('.single_choice div.options:eq('+i+') ').find('input[name="quiz_option[]"]').addClass('is-invalid');
                            $('.single_choice div.options:eq('+i+') ').find('strong').html(e.responseJSON['quiz_option.'+i]);
                        }
                    }
                }

                return false;
            }
        })
    });
});

function checkQuizType(){
    if($('input[name=quiz_type]:checked').val()==1){
        $('.true_false').css('display','');
        $('.add-option').css('display','none');
        $('.single_choice').css('display','none');
        $('.multiple_choice').css('display','none');
    }else if($('input[name=quiz_type]:checked').val()==2 || $('input[name=quiz_type]:checked').val()==4){
        $('.true_false').css('display','none');
        $('.single_choice').css('display','');
        $('.multiple_choice').css('display','none');
        $('.add-option').css('display','');
    }
}
</script>