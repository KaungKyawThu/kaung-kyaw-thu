@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    @include('search')
    <div class="col-md-3">
      <input type="date" class="form-control from_date fdr" placeholder="From Date">
    </div>
    <div class="col-md-3">
      <input type="date" class="form-control to_date fdr" placeholder="To Date">
    </div>

    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th class="sc-date">Schedule Date</th>
                <th>Hour</th>
                <th>Weekly Fact</th>
                <th>Created By</th>
                <th>Status</th>
                @if(auth()->user()->can('weekly-quiz-edit') || auth()->user()->can('weekly-quiz-view') || auth()->user()->can('weekly-quiz-delete'))
                  <th class="action-col" ><span></span></th>
                @else
                  <th class="hide-col" ><span></span></th>
                @endif
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
   <script>
     $(document).ready(function () {
        var table = $('.data-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    ajax: "{{ url('weeklyquiz') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'date', name: 'date'},
                        {data: 'hours', name: 'hours'},
                        {data: 'weekly_fact', name: 'weekly_fact'},
                        {data: 'created_user_name', name: 'created_user_name'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                });

        $('.list-search').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $('.list-search').on( 'search', function () {
            table.search( this.value ).draw();
        } );

        $('.from_date, .to_date').change(function(){
              if($(this).val().length==0){
                  $(this).addClass('fdr');
              }else{
                  $(this).removeClass('fdr');
              }

              searchFun();
        });

        function searchFun(){
            var from_date = $('.from_date').val();
            var to_date = $('.to_date').val();
            table.ajax.url("{{ url('weeklyquiz')}}"+"?from_date="+from_date+'&to_date='+to_date).load();
        }

     });
   </script>
@stop
