@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
     @php
         $created_user_name = $quiz->created_user_name;
         $updated_user_name = $quiz->updated_user_name;
         $published_user_name = $quiz->published_user_name;
         $deactivated_user_name = $quiz->deactivated_user_name;
         
         $created_at = date('d-M-Y H:i:s',strtotime($quiz->created_at));
         $updated_at = $quiz->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->updated_at));
         $published_at = $quiz->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->published_at));
         $deactivated_at = $quiz->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->deactivated_at));

     @endphp

     <div class="row">
         <div class="col-md-6">
            <label for="">Schedule</label><br>
            <span>{{date('d/m/Y',strtotime($quiz->schedule_date))}} {{$quiz->hours}}:00:00</span>
         </div>
     </div><br>
     <div class="row">
        <div class="col-md-12">
            <label for="">Weekly Fact</label><br>
            <span>{{$quiz->weekly_fact}}</span>
        </div>
     </div><br>
     
     <div class="row">
        <div class="col-md-12">
            <label for="">Quiz Question</label><br>
            <span>{{$quiz->quiz_question}}</span>
        </div>
     </div><br>

     <label for="">Options</label>
     @foreach ($quiz->options as $opt)
        <div class="row">
            <div class="col-md-12">
                <input type="radio" @if($opt->is_correct) checked @endif disabled>
                {{$opt->quiz_option}}
            </div>
        </div><br>
     @endforeach
 
     <div class="row">
        <div class="col-md-12">
            <label for="">Answer Description</label><br>
            <span>{{$quiz->answer_description}}</span>
        </div>
     </div><br>

     @include('view-detail')
</div>
@stop
