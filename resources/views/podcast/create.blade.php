@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('podcast') }}" id="content-form" enctype="multipart/form-data">
        @csrf
        @include('podcast.form')
    </form>
</div>
@stop
