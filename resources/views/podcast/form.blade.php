<input type="hidden" name="current_index" value="{{ $podcast->current_index ?? 1}}">
<input type="hidden" name="podcast_id" value="{{$podcast->id}}">

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Podcast Title') }} <span class="text-red">*</span></label>
            <input id="name" type="text" class="form-control @error('podcast_title') is-invalid @enderror" name="podcast_title" value="{{ old('podcast_title', $podcast->podcast_title) }}" autocomplete="podcast_title" autofocus placeholder="Enter Podcast Title">
           <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-6">
        <label>{{ __('Thumbnail') }} <span class="text-red">* </span> <span class="photo_upload_description">( {{$upload_photo_description}} )</span></label>
        <div class="row preview-row" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null ) style="" @else style="display:none" @endif>
            <div class="col-md-4">
                <img id="upload-preview" src="{{$podcast->thumbnail}}" alt="upload photo" style="width:100%"/>
            </div>
            <div class="col-md-1">
                <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
            </div>
        </div>
        <div class="row">
            <input type="file" id="single-photo" class="@error('photo') is-invalid @enderror photo_file form-input form-control" name="thumbnail" placeholder="{{__('Upload Photo')}}" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null) style="display:none" @else style="" @endif>
            <input type="hidden" class=" @error('thumbnail') is-invalid @enderror" >
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>

            <input type="hidden" name="virtual_img" id="virtual-img" value="{{$podcast->thumbnail}}">
        </div>
        <br>
    </div>

    <div class="col-md-6">
        <label>{{ __('Podcast Embedded Code') }} <span class="text-red">*</span></label>
        <input id="podcast_link" type="text" class="form-control @error('podcast_link') is-invalid @enderror" name="podcast_link" value="{{ old('podcast_link', $podcast->podcast_link) }}" autocomplete="podcast_link" autofocus placeholder="Enter Podcast Embedded Code">

        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>
    </div>


    <div class="col-md-6">
            <label>{{ __('Podcast Share Link') }} <span class="text-red">*</span></label>
            <input id="podcast_share_link" type="text" class="form-control @error('podcast_share_link') is-invalid @enderror" name="podcast_share_link" value="{{ old('podcast_share_link', $podcast->podcast_share_link) }}" autocomplete="podcast_share_link" autofocus placeholder="Enter Podcast Share Link">

            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        <br>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Podcast Series Name') }} <span class="text-red">*</span></label><br>
            <select name="podcast_series" id="podcast_series" class="form-control" >
                <option></option>
                @foreach ($series_podcast as $item)
                <option value="{{ $item->id }}" @if (($item->id == $podcast->podcast_series_id) || old('podcast_series') == $item->id) selected @endif>
                    {{ $item->series_name }}</option>
            @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>


    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Podcast Duration') }}<span class="text-red"> (Minutes)</span> <span class="text-red">*</span></label>
            <input id="podcast_duration" type="text" class="form-control @error('podcast_duration') is-invalid @enderror" name="podcast_duration" value="{{ old('podcast_duration', $podcast->podcast_duration) }}" autocomplete="podcast_duration" autofocus placeholder="5:30">

            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>



<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div>

<script>
     $(document).ready(function() {

        $selectElement = $('#podcast_series').select2({
            placeholder: "Please select podcast",
            allowClear: true
        });

        $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("content-form"));
        var id = $('form#content-form').find('input[name=podcast_id]').val();

        if(id.length > 0){
            var route = '/podcast/'+id;

        }else if(id.length == 0){
            var route = '/podcast';
        }

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('podcast')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){

                window.location.href = '/podcast?index='+data.index;

            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.podcast_title!=undefined){
                    $('form#content-form').find('input[name="podcast_title"]').addClass('is-invalid');
                    $('form#content-form').find('input[name="podcast_title"]').siblings().find('strong').html(e.responseJSON.podcast_title)
                }

                if(e.responseJSON.thumbnail!=undefined){
                    $('form#content-form').find('input[name=thumbnail]').addClass('is-invalid');
                    $('form#content-form').find('input[name=thumbnail]').siblings().find('strong').html(e.responseJSON.thumbnail)
                }

                if(e.responseJSON.podcast_link!=undefined){
                    $('form#content-form').find('input[name=podcast_link]').addClass('is-invalid');
                    $('form#content-form').find('input[name=podcast_link]').siblings().find('strong').html(e.responseJSON.podcast_link)
                }

                if(e.responseJSON.podcast_share_link!=undefined){
                    $('form#content-form').find('input[name=podcast_share_link]').addClass('is-invalid');
                    $('form#content-form').find('input[name=podcast_share_link]').siblings().find('strong').html(e.responseJSON.podcast_share_link)
                }

                if(e.responseJSON.podcast_series!=undefined){
                    $('form#content-form').find('select[name=podcast_series]').addClass('is-invalid');
                    $('form#content-form').find('select[name=podcast_series]').siblings().find('strong').html(e.responseJSON.podcast_series)
                }

                if(e.responseJSON.podcast_duration!=undefined){
                    $('form#content-form').find('input[name=podcast_duration]').addClass('is-invalid');
                    $('form#content-form').find('input[name=podcast_duration]').siblings().find('strong').html(e.responseJSON.podcast_duration)
                }

                return false;
            }
        })
    });


        });
</script>
