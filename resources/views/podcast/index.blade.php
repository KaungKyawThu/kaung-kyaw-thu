@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    @include('search')
    <div class="col-md-3">
        <select class="form-control select2 status-filter-select2 type-filter-select2 podcast_status" name="quiz_type" style="width: 100%;">
            <option value=""></option>
            @foreach (config("web_constant.status") as $k=>$t)
                <option value="{{$k}}">{{$t}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12">
            <div class="fliter-input">
                <select name="" class="form-control select2 type-filter-select2 podcast_serie_id" id="podcast_serie_id">
                    <option value="">Select Podcast Series</option>
                    @foreach (DB::table('podcast_series')->where('status', '=' ,2)->get() as $s)
                        <option value="{{$s->id}}">{{$s->series_name}}</option>
                    @endforeach
                </select>
            </div>
            </div>
        </div>

    </div>
    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Podcast Title</th>
                <th class="thumbnail">Thumbnail </th>
                <th>Podcast Series</th>
                <th>Podcast Duration</th>
                <th>Created By</th>
                <th>Published By</th>
                <th>Published At</th>
                <th class="status-col">Status</th>
                @if(auth()->user()->can('podcast-edit') || auth()->user()->can('podcast-delete') || auth()->user()->can('podcast-publish') || auth()->user()->can('podcast-active'))
                  <th class="action-col" ><span></span></th>
                @else
                  <th class="hide-col" ><span></span></th>
                @endif
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
   <script>
     $(document).ready(function () {
       var table = $('.data-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                         //stateSave: true,
                    ajax: "{{ url('podcast') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'podcast_title', name: 'podcast_title'},
                        {data: 'thumbnailz', name: 'thumbnailz'},
                        {data: 'series_name', name: 'series_name'},
                        {data: 'podcast_duration', name: 'podcast_duration'},
                        {data: 'created_user_name', name: 'created_user_name'},
                        {data: 'published_user_name', name: 'published_user_name'},
                        {data: 'published_at', name: 'published_at'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });

        $('.podcast_serie_id').select2({ placeholder : "Filter By Podcast Series", allowClear : true});
        $('.podcast_status').select2({ placeholder : "Filter By Status", allowClear : true});

        $(document).on('change','.type-filter-select2',function(){

                            searchFun();
                    });

            function searchFun(){

            var podcast_id = $('.podcast_serie_id').val();
            var podcast_status = $('.podcast_status').val();

            table.ajax.url("{{ url('podcast')}}"+"?podcast_series_id="+podcast_id+"&podcast_status="+podcast_status).load();
        }

       $('.list-search').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $('.list-search').on( 'search', function () {
            table.search( this.value ).draw();
        } );

        // $(document).on('change','.status-filter-select2',function(){
        //                     searchStatus();
        //             });

        //     function searchStatus(){
        //     var podcast_status = $('.podcast_status').val();

        //     table.ajax.url("{{ url('podcast')}}"+"?podcast_status="+podcast_status).load();
        // }

     });
   </script>
@stop
