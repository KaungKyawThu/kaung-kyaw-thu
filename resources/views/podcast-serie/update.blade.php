@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('podcast-serie/'.$podcast->id) }}" id="content-form" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        @include('podcast-serie.form')
    </form>
</div>
@stop
