@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('podcast-serie') }}" id="content-form" enctype="multipart/form-data">
        @csrf
        @include('podcast-serie.form')
    </form>
</div>
@stop
