@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    @include('search')
    <div class="col-md-3">
        <select class="form-control select2 type-filter-select2 podcast_series_status" name="quiz_type" style="width: 100%;">
            <option value=""></option>
            @foreach (config("web_constant.status") as $k=>$t)
                <option value="{{$k}}">{{$t}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3"></div>
    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Series Name</th>
                <th>Thumbnail</th>
                <th>Created By</th>
                <th>Published By</th>
                <th>Published At</th>
                <th class="status-col">Status</th>
                @if(auth()->user()->can('podcast-serie-edit') || auth()->user()->can('podcast-serie-delete') || auth()->user()->can('podcast-serie-publish') || auth()->user()->can('podcast-serie-active'))
                  <th class="action-col" ><span></span></th>
                @else
                  <th class="hide-col" ><span></span></th>
                @endif
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
   <script>
     $(document).ready(function () {
       var table = $('.data-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                         //stateSave: true,
                    ajax: "{{ url('podcast-serie') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'series_name', name: 'series_name'},
                        {data: 'thumbnailz', name: 'thumbnailz'},
                        {data: 'created_user_name', name: 'created_user_name'},
                        {data: 'published_user_name', name: 'published_user_name'},
                        {data: 'published_at', name: 'published_at'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });

       $('.list-search').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $('.list-search').on( 'search', function () {
            table.search( this.value ).draw();
        } );
        $('.podcast_series_status').select2({ placeholder : "Filter By Status", allowClear : true});
        $(document).on('change','.type-filter-select2',function(){
                            searchFun();
                    });

            function searchFun(){
            var podcast_series_status = $('.podcast_series_status').val();

            table.ajax.url("{{ url('podcast-serie')}}"+"?podcast_series_status="+podcast_series_status).load();
        }
     });

   </script>
@stop
