<input type="hidden" name="current_index" value="{{ $podcast->current_index ?? 1}}">
<input type="hidden" name="podcast_id" value="{{$podcast->id}}">

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Podcast Series') }} <span class="text-red">*</span></label>
            <input id="name" type="text" class="form-control @error('series_name') is-invalid @enderror" name="series_name" value="{{ old('series_name', $podcast->series_name) }}" autocomplete="podcast_title" autofocus placeholder="Enter Podcast Series Name">
           <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-6">
        <label>{{ __('Thumbnail') }} <span class="text-red">* </span> <span class="photo_upload_description">( {{$upload_photo_description}} )</span></label>
        <div class="row preview-row" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null ) style="" @else style="display:none" @endif>
            <div class="col-md-4">
                <img id="upload-preview" src="{{$podcast->thumbnail}}" alt="upload photo" style="width:100%"/>
            </div>
            <div class="col-md-1">
                <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
            </div>
        </div>
        <div class="row">
            <input type="file" id="single-photo" class="@error('photo') is-invalid @enderror photo_file form-input form-control" name="thumbnail" placeholder="{{__('Upload Photo')}}" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null) style="display:none" @else style="" @endif>
            <input type="hidden" class=" @error('thumbnail') is-invalid @enderror" >
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>

            <input type="hidden" name="virtual_img" id="virtual-img" value="{{$podcast->thumbnail}}">
        </div>
        <br>
    </div>
</div>



<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div>

<script>
     $(document).ready(function() {

        $selectElement = $('#podcast_series').select2({
            placeholder: "Please select podcast",
            allowClear: true
        });

        $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("content-form"));
        var id = $('form#content-form').find('input[name=podcast_id]').val();

        if(id.length > 0){
            var route = '/podcast-serie/'+id;

        }else if(id.length == 0){
            var route = '/podcast-serie';
        }

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('podcast')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){

                window.location.href = '/podcast-serie?index='+data.index;

            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.series_name!=undefined){
                    $('form#content-form').find('input[name="series_name"]').addClass('is-invalid');
                    $('form#content-form').find('input[name="series_name"]').siblings().find('strong').html(e.responseJSON.podcast_title)
                }

                if(e.responseJSON.thumbnail!=undefined){
                    $('form#content-form').find('input[name=thumbnail]').addClass('is-invalid');
                    $('form#content-form').find('input[name=thumbnail]').siblings().find('strong').html(e.responseJSON.thumbnail)
                }
                return false;
            }
        })
    });


        });
</script>
