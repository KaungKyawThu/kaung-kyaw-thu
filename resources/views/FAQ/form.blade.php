<input type="hidden" name="current_index" value="{{ $faq->current_index ?? 1 }}">
<input type="hidden" name="faq_id" value="{{ $faq->id }}">
<div class="row">
    <div class="col-md-6">
        <label>{{ __('FAQ Categories') }} <span class="text-red">*</span></label><br>
        @php

            $keyword_ids = $faq->category->pluck('category_id')->toArray();
        @endphp
        <select name="category_id[]" id="category_id" class="form-control " multiple>
            @foreach (\App\Category::where('status', 2)->get() as $item)
                <option value="{{ $item->id }}" @if (in_array($item->id, $keyword_ids)) selected @endif>
                    {{ $item->name_en }}</option>
            @endforeach
        </select>
        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>
    </div>
    
    <div class="col-md-6">
        <label for="">{{ __('Keywords') }}</label><br>
        @php
            $keyword_ids = $faq->keywords->where('explore_type', 5)
                                        ->pluck('keyword_id')
                                        ->toArray();
        @endphp

        <select name="keyword_id[]" id="keyword_id" class="form-control " multiple>
            {{-- <option value="">Select Service</option> --}}
            @foreach (\App\Keyword::get() as $item)
                <option value="{{ $item->id }}" @if (in_array($item->id, $keyword_ids)) selected @endif>
                    {{ $item->keyword_name }}</option>
            @endforeach
        </select>

        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>
    </div>
</div><br>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Topic') }} </label>
            @php
                $topics = \App\ContentCategory::whereIn('category_id', $keyword_ids)
                                            ->where(['content_type' => 1, 'status' => 2])
                                            ->leftJoin('contents', 'contents.id', 'content_categories.content_id')
                                            ->select('contents.id', 'contents.content_title')
                                            ->get();
            @endphp
            <select name="content_id" class="form-control">
                <option value=""></option>
                @foreach ($topics as $item)
                    <option value="{{ $item->id }}">{{ $item->content_title }}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h5>Topic Lists</h5>
        <table class="table data-table table-bordered faq-table">
            <thead>
                <th style="width: 50px;">No.</th>
                <th>Topic Title</th>
                <th style="width: 50px;" class="action-col"><span></span></th>
            </thead>
            <tbody>

                @if ($faq->topics->count() > 0)
                    @foreach ($faq->topics as $k => $topic)
                        <tr key="{{ $topic->content_id }}">
                            <td><span class="faq-index">{{ $k + 1 }}</span></td>
                            <td>{{ $topic->content_title }}</td>
                            <td>
                                <span class="btn remove-faq">
                                    <i class="fa fa-times"></i>
                                </span>
                                <input type="hidden" name="topic_id[]" value="{{ $topic->content_id }}">
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div><br>
<div class="row">
    <div class="col-md-12">
        <table class="table data-table list-table project-milestones-table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>FAQ Question<span class="text-red">*</span></th>
                    <th>FAQ Answer <span class="text-red">*</span> </th>
                    <th class="action-col"><span></span></th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td><span class="faq-index">1</span></td>
                    <td>
                        <input id="faqquestion" type="text" class="form-control" name="faq_question[]"
                            value="{{ old('faq_question[]', $faq->faq_question) }}" placeholder="FAQ Question">
                        <span class="invalid-feedback" role="alert">
                            <strong></strong>
                        </span>
                    </td>
                    <td>

                        <textarea id="faqanswer" class="form-control" name="faq_answer[]" rows="5" placeholder="FAQ Answer">{{ old('faq_answer[]', $faq->faq_answer) }}</textarea>
                        <span class="invalid-feedback" role="alert">
                            <strong></strong>
                        </span>
                    </td>

                    <td>
                        <span class="remove-faq" style="display:none;"><i class="fa fa-times"
                                aria-hidden="true"></i></span>

                        <input type="hidden" name="status[]" value="">
                    </td>

                </tr>

            </tbody>
            <tfoot>
                @if ($faq->id == null)
                    <tr>
                        <td colspan="6" class="text-right">

                            <span class="btn btn-sm btn-info float-right mt-2 add-milestone">
                                <i class="fa fa-plus"></i>
                            </span>
                        </td>

                    </tr>
                @endif
            </tfoot>
        </table>
    </div>
</div><br>

<div class="row">

    <div class=" text-left">
        <a href="{{ url('faq') }}" class="btn btn-danger orange-text cancal-btn float-right ml-3">Cancel</a>
        <span class="btn btn-primary dark-text save-btn save-project-data float-right">Save</span>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('.save-btn').click(function() {
            var that = $(this);
            var formData = new FormData(document.getElementById("faq_create"));
            var faq_id = $('form#faq_create').find('input[name=faq_id]').val();

            if (faq_id.length > 0) {
                var route = '/faq/' + faq_id;
                formData.append("_method", 'PUT');
            } else if (faq_id.length == 0) {
                var route = '/faq';
            }
            $.ajax({
                url: route,
                method: "POST",
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    clearErrorMsg();
                },
                success: function(data) {
                    window.location.href = '/faq?index=' + data.index;
                },
                complete: function(data) {

                },
                error: function(e) {
                    removeProcessing(that);
                    if (e.responseJSON.category_id != undefined) {
                        $('form#faq_create').find('select[name="category_id[]"]').addClass(
                            'is-invalid');
                        $('form#faq_create').find('select[name="category_id[]"] ')
                            .siblings().find('strong').html(e.responseJSON.category_id);
                    }
                    for (let i = 0; i < $(
                            'form#faq_create table.project-milestones-table > tbody > tr')
                        .length; i++) {

                        if (e.responseJSON['faq_question.' + i] != undefined) {
                            $('form#faq_create table.project-milestones-table > tbody > tr:eq(' +
                                i + ') ').find('input[name="faq_question[]"]').addClass(
                                'is-invalid');
                            $('form#faq_create table.project-milestones-table > tbody > tr:eq(' +
                                    i + ') ').find('input[name="faq_question[]"] ')
                                .siblings().find('strong').html(e.responseJSON[
                                    'faq_question.' + i]);
                        }



                        if (e.responseJSON['faq_answer.' + i] != undefined) {
                            $('form#faq_create table.project-milestones-table > tbody > tr:eq(' +
                                    i + ') ').find('textarea[name="faq_answer[]"]')
                                .addClass('is-invalid');
                            $('form#faq_create table.project-milestones-table > tbody > tr:eq(' +
                                    i + ') ').find('textarea[name="faq_answer[]"] ')
                                .siblings().find('strong').html(e.responseJSON[
                                    'faq_answer.' + i]);
                        }
                    }



                    return false;
                }
            })
        });

        $('#category_id').change(function() {
            let category_ids = [];
            $('#category_id option:selected').each(function(i, e) {
                category_ids.push($(e).val());
            });

            $.ajax({
                url: '/category/topic?category_id=' + category_ids,
                method: 'GET',
                dataType: 'json',
                success: function(data) {

                    $('select[name=content_id]').empty();
                    $('select[name=content_id]').append($("<option>").attr('value', null)
                        .text(''));

                    data.topics.map((e) => {
                        // console.log(e);
                        $('select[name=content_id]').append($("<option>").attr(
                            'value', e.id).text(e.content_title));
                    });
                },
                error: function(e) {
                    console.log(e);
                }

            })
        });

        $('.add-milestone').click(function() {
            let index = $('table.project-milestones-table > tbody > tr').length + 1;

            let milestone_due_date = $('table.project-milestones-table > tbody > tr:last').clone();
            $(milestone_due_date).find(".remove-faq").show();
            $(milestone_due_date).find(".faq-index").html(index);
            $(milestone_due_date).find("#faqquestion").val("");
            $(milestone_due_date).find("#faqanswer").val("");

            $('table.project-milestones-table > tbody').append(milestone_due_date);

        });
        $('select[name=content_id]').change(function() {
            let id = $('select[name=content_id] option:selected').val();
            let title = $('select[name=content_id] option:selected').text();
            $('select[name=content_id]').val(null).trigger('change.select2');

            if ($('table.faq-table > tbody > tr[key=' + id + ']').length > 0) return false;

            let k = $('table.faq-table > tbody > tr').length + 1;
            let tr = "<tr key='" + id + "'>" +
                "<td><span class='faq-index'>" + k + "</span></td>" +
                "<td>" + title + "</td>" +
                "<td><span class='btn remove-faq'><i class='fa fa-times'></i></span><input type='hidden' class='topic_id' name='topic_id[]' value='" +
                id + "'></td>" +
                "</tr>";
            $('table.faq-table > tbody').append(tr);
            $('.faq-err').html(null);
        });

        $(document).on('click', '.remove-faq', function() {
            $(this).parent().closest('tr').remove();
            // let index = $('table.project-milestones-table > tbody > tr').length;
            // $('table.project-milestones-table > tbody > tr:last').find(".faq-index").html(index);

            $('table.project-milestones-table > tbody > tr').each(function(i, e) {
                let j = i + 1;
                $(e).find('.faq-index').html(j);

            });

            $('table.faq-table > tbody > tr').each(function(i, e) {
                let j = i + 1;
                $(e).find('.faq-index').html(j);

            });
        });


        $('#category_id').select2({
            placeholder: "Select categories",
            allowClear: true
        });
        $('#keyword_id').select2({
            placeholder: "Select keywords",
            allowClear: true
        });
        $('select[name=content_id]').select2({
            placeholder: 'Select Content',
            allowClear: true
        });
    });
</script>
