@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<style>
    a.q-detail{
        color: black!important;
    }
</style>
<div class="card-body">
     @php
         $created_user_name = $faq->created_user_name;
         $updated_user_name = $faq->updated_user_name;
         $published_user_name = $faq->published_user_name;
         $deactivated_user_name = $faq->deactivated_user_name;

         $created_at = date('d-M-Y H:i:s',strtotime($faq->created_at));
         $updated_at = $faq->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($faq->updated_at));
         $published_at = $faq->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($faq->published_at));
         $deactivated_at = $faq->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($faq->deactivated_at));

     @endphp
      <h5>{{$faq->content_title}} </h5>
<table class="table">


    <thead>
        <tr>
            <th style="width:30.3% !important;"><b>Category</b></th>
            <th style="width:30.3% !important;"><b>Keywords</b></th>
            <th style="width:30.3% !important;"><b>Status</b></th>

        </tr>
    </thead>
    <tbody>

        <tr>

         <td>  {{implode(', ',$faq->category->pluck('name_en')->toArray())}}</td>







         <td>   {{implode(', ',$faq->keywords->pluck('keyword_name')->toArray())}}</td>





            <td>  {{config("web_constant.status.$faq->status")}}</td>
        </tr>
        </tbody>
</table>
<br>




         <div class="row">
             <div class="col-md-12">

                <b>FAQ Question</b><br>
               {{$faq->faq_question}}
             </div>
            </div>
            <br>
            <div class="row">
             <div class="col-md-12">
                <b>FAQ Answer</b><br>
               {{$faq->faq_answer}}
             </div>
         </div><br>
         <div class="row">
            <div class="col-md-12">
                <table class="table data-table table-bordered faq-table">
                    <thead>
                        <th style="width: 50px;">No.</th>
                        <th>Topic Title</th>

                    </thead>
                    <tbody>

                        @if ($faq->topics->count() > 0)
                        @foreach ($faq->topics as $k => $topic)

                        <tr key="{{$topic->content_id}}">
                            <td><span class="faq-index">{{$k+1}}</span></td>
                            <td>{{$topic->content_title}}</td>

                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>


            </div>
        </div><br>

     @include('view-detail')
</div>
@stop
