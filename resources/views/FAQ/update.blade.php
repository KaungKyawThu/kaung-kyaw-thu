@extends('adminlte::page')

@section('title', 'ASRH')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('faq/'.$faq->id) }}" id="faq_create">
        @csrf
        @method('PUT')
        @include('FAQ.form')
    </form>
</div>
@stop
