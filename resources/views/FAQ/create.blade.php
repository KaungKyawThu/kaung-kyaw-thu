@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('faq') }}" id="faq_create">
        @csrf
        @include('FAQ.form')
    </form>
</div>
@stop
