@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

    <div class="row">
        @include('search')
        <div class="col-md-3">
            <select class="form-control select2 type-filter-select2 category" name="category_id" style="width: 100%;">
                <option value=""></option>
                @foreach (\App\Category::get() as $k => $t)
                    <option value="{{ $t->id }}">{{ $t->name_en }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3">
            <select class="form-control select2 faq-filter-select2 faq_status" name="faq_status" style="width: 100%;">
                <option value=""></option>
                @foreach (config("web_constant.status") as $k=>$t)
                    <option value="{{$k}}">{{$t}}</option>
                @endforeach
            </select>
        </div>


        @include('new-btn')
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Category</th>
                        <th>FAQ Question</th>
                        <th>Created By</th>
                        <th>Published By</th>
                        <th>Published At</th>
                        <th class="status-col">Status</th>


                        @if (auth()->user()->can('faq-edit') ||
                            auth()->user()->can('faq-delete'))
                            <th class="action-col"><span></span></th>
                        @else
                            <th class="hide-col"><span></span></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: false,
                info: false,
                autoWidth: true,
                responsive: true,
                processing: true,
                serverSide: true,
                displayStart: "{{ $current_index }}",
                ajax: "{{ url('faq') }}",
                createdRow: function(row, data, dataIndex) {
                    if (data.action.length == 0)
                        $(row).find('td.action-col').css('display', 'none');
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        class: 'data-index'
                    },
                    {
                        data: 'category',
                        name: 'category'
                    },
                    {
                        data: 'faq_question',
                        name: 'faq_question'
                    },
                    // {data: 'faq_answer', name: 'faq_answer'},
                    {
                        data: 'created_user_name',
                        name: 'created_user_name'
                    },
                    {
                        data: 'published_user_name',
                        name: 'published_user_name'
                    },
                    {
                        data: 'published_at',
                        name: 'published_at'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        searchable: false,
                        className: 'action-col'
                    },
                ]
            });

            $('.list-search').on('keyup', function() {
                table.search(this.value).draw();
            });

            $('.list-search').on('search', function() {
                table.search(this.value).draw();
            });




            $('.type-filter-select2').select2({
                placeholder: "Filter By Category",
                allowClear: true
            });
            $(document).on('change', '.category', function() {
                searchFun();
            });
            $('.faq_status').select2({ placeholder : "Filter By Status", allowClear : true});


        $(document).on('change','.faq_status',function(){
                            searchFun();
                    });

            function searchFun(){
            var faq_status = $('.faq_status').val();
            var id=$('.category').val();

            table.ajax.url("{{ url('faq')}}" + "?category_id=" + id+"&faq_status="+faq_status).load();
        }
        });
    </script>
@stop
