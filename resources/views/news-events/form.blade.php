<input type="hidden" name="current_index" value="{{$newEvent->current_index ?? 1}}">
<input type="hidden" name="new_event_id" value="{{$newEvent->id}}">

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Title') }} <span class="text-red">*</span></label>
            <input id="title" type="text" class="form-control "
                name="title" value="{{  $newEvent->title }}" autocomplete="title"
                autofocus placeholder="Title">


                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
        </div>
    </div>
    <div class="col-md-6">

    </div><br>

    <div class="col-md-4">
        <div class="form-group">
            <label>Schedule Date<span class="text-red">*</span></label>
           <input type="date" name="schedule_date" class="form-control" value="{{$newEvent->schedule_date}}" min="{{date("Y-m-d",time())}}" @if($newEvent->status==2) readonly @endif>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>


    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>{{ __('Hours') }} <span class="text-red">*</span></label><br>


            <select name="hours" id="hour" class="form-control " @if($newEvent->status==2) disabled @endif>
                @for($i = 1 ; $i < 24 ; $i++)
                    <option value="{{ $i }}" name=""  @if ($i == $newEvent->hours ) selected @endif>{{ $i }}</option>
                @endfor
                <option value="0"  @if ($i == $newEvent->hours ) selected @endif>0</option>
            </select>
           {{-- <input type="radio" value="{{$type}}" name="quiz_type" @if( $quiz->quiz_type==$type || ($quiz->quiz_type==null && $loop->first)) checked @endif> <span class="qType">{{$type_name}}</span> --}}
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
        </div>
    </div>
    <div class="col-md-6">

    </div><br>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Type') }} <span class="text-red">*</span></label><br>


            <select name="type" id="type" class="form-control ">
                <option value="">Select Type</option>
                @foreach (config("web_constant.news_event_type") as $type => $type_name)
                    <option value="{{$type}}" name="type"  @if ($type == $newEvent->type ) selected @endif>{{$type_name}}</option>

                @endforeach
            </select>

           {{-- <input type="radio" value="{{$type}}" name="quiz_type" @if( $quiz->quiz_type==$type || ($quiz->quiz_type==null && $loop->first)) checked @endif> <span class="qType">{{$type_name}}</span> --}}

                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
        </div>


    </div>
    <div class="col-md-6">

    </div><br>
    <div class="col-md-6">
    <div class="form-group">
        <label>{{ __('Description') }} <span class="text-red">*</span></label>
        <textarea id="description"  class="form-control " name="description" rows="7" placeholder="Description">{{$newEvent->description }}</textarea>

        <span class="invalid-feedback" role="alert">
            <strong></strong>
        </span>

    </div>
</div><br>
</div>

<div class="row">

    <div class=" text-left">
        <a href="{{ url('new-event') }}" class="btn btn-danger orange-text cancal-btn float-right ml-3">Cancel</a>
        <span class="btn btn-primary dark-text save-btn save-project-data float-right">Save</span>
    </div>
</div>
<script>
$(document).ready(function() {

    $('#type').select2({
        placeholder: "Select Type",
        allowClear: true
    });

    // $('#hour').select2({
    //     placeholder: "Select Hour",
    //     allowClear: true
    // });

    $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("new_event_create"));
        var new_event_id = $('form#new_event_create').find('input[name=new_event_id]').val();

        if(new_event_id.length>0){
          var route = '/new-event/'+new_event_id;
          formData.append("_method", 'PUT');
        }else if(new_event_id.length==0){
          var route = '/new-event';
        }
        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){
                window.location.href = '/new-event?index='+data.index;
            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.schedule_date!=undefined){
                    $('form#weekly-quiz-form').find('input[name=schedule_date]').addClass('is-invalid');
                    $('form#weekly-quiz-form').find('input[name=schedule_date]').siblings().find('strong').html(e.responseJSON.schedule_date)
                }
                if(e.responseJSON.hours!=undefined){
                    $('form#weekly-quiz-form').find('input[name=hours]').addClass('is-invalid');
                    $('form#weekly-quiz-form').find('input[name=hours]').siblings().find('strong').html(e.responseJSON.hours)
                }

                if(e.responseJSON.title!=undefined){
                $('form#new_event_create').find('input[name="title"]').addClass('is-invalid');
                $('form#new_event_create').find('input[name="title"] ').siblings().find('strong').html(e.responseJSON.title);
                }
                if(e.responseJSON.description!=undefined){
                $('form#new_event_create').find('textarea[name="description"]').addClass('is-invalid');
                $('form#new_event_create').find('textarea[name="description"] ').siblings().find('strong').html(e.responseJSON.description);
                }
                if(e.responseJSON.type!=undefined){
                $('form#new_event_create').find('select[name="type"]').addClass('is-invalid');
                $('form#new_event_create').find('select[name="type"] ').siblings().find('strong').html(e.responseJSON.type);
                }

                return false;
            }
        })
    });

});


</script>
