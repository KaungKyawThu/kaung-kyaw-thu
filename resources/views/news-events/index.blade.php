@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

    <div class="row">
        @include('search')
        <div class="col-md-3">
            <select class="form-control select2 type-filter-select2" name="new_event_type" style="width: 100%;">
                <option value="" ></option>
                @foreach (config('web_constant.news_event_type') as $k => $t)
                    <option value="{{ $k }}">{{ $t }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3"></div>
        @include('new-btn')
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Schedule Date</th>
                        <th>Hours</th>
                        <th>Created By</th>
                        <th>Created At</th>
                        <th>Status</th>
                        @if (auth()->user()->can('new-event-edit') ||
                            auth()->user()->can('new-event-delete'))
                            <th class="action-col"><span></span></th>
                        @else
                            <th class="hide-col"><span></span></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: false,
                info: false,
                autoWidth: true,
                responsive: true,
                processing: true,
                serverSide: true,
                // displayStart: "{{ $current_index }}",
                //stateSave: true,
                ajax: "{{ url('new-event') }}",
                createdRow: function(row, data, dataIndex) {
                    if (data.action.length == 0)
                        $(row).find('td.action-col').css('display', 'none');
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        class: 'data-index'
                    },
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'type',
                        name: 'type'
                    },

                    {
                        data: 'schedule',
                        name: 'schedule'
                    },
                    {
                        data: 'hours', name: 'hours'
                    },
                    {data: 'created_user_name', name: 'created_user_name'},
                    {data: 'created_at', name: 'created_at'},
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        searchable: false,
                        className: 'action-col'
                    },
                ]
            });

            $('.list-search').on('keyup', function() {
                table.search(this.value).draw();
            });

            $('.list-search').on('search', function() {
                table.search(this.value).draw();
            });

            $('.type-filter-select2').select2({
                placeholder: "Filter By Type",
                allowClear: true
            });
            $(document).on('change', '.type-filter-select2', function() {
                var id = $(this).val();
                table.ajax.url("{{ url('new-event') }}" + "?new_event_type=" + id).load();
            });
        });
    </script>
@stop
