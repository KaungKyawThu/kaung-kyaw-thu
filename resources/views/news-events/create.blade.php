@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">

    <form method="POST" action="{{ url('new-event') }}" enctype="multipart/form-data" id="new_event_create" >
        @csrf
        @include('news-events.form')
    </form>
</div>
@stop
