<input type="hidden" name="current_index" value="{{$quiz->current_index ?? 1}}">
<input type="hidden" name="quiz_id" value="{{$quiz->id}}">
<div class="row">
    <div class="col-md-7">
        <div class="form-group">
            <label>Category  <span class="text-red">*</span></label>
            <select class="form-control select2" name="quiz_content_type" style="width: 100%;">
                <option value=""></option>
                @foreach (config("web_constant.quiz_content_types") as $k => $t)
                    <option value="{{$k}}" @if($quiz->quiz_content_type==$k) selected @endif>{{$t}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="topic" style="display: none;">
    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                <label>{{ __('Topic') }} <span class="text-red">*</span></label><br>
                <select name="select_topic_id" class="form-control">
                    <option value="">Select Topic</option>
                    @foreach (\App\Content::where('status',2)->where('content_type',1)->get() as $c)
                        <option value="{{$c->id}}" data="{{Crypt::encryptString($c)}}">{{$c->content_title}}</option>
                    @endforeach
                </select>
              
            </div>
        </div>

    </div><br>

    <div class="row " >

       <div class="col-md-7">
            <h5>Topic Lists</h5>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
            <table class="table data-table table-bordered faq-table">
                <thead>
                    <th style="width: 50px;">No.</th>
                    <th>Topic</th>
                    <th style="width: 50px;" class="action-col"><span></span></th>
                </thead>
                <tbody>
                    @if ($quiz->contents->count() > 0)
                        @foreach ($quiz->contents as $k => $content)
                        <tr key="{{$content->content_id}}">
                            <td><span class="faq-index">{{$k+1}}</span></td>
                            <td>{{$content->content_title}}</td>
                            <td>
                                <span class="btn remove-faq">
                                    <i class="fa fa-times"></i>
                                </span>
                                <input type="hidden" name="content_id[]" value="{{ $content->content_id }}">
                            </td>
                        </tr>
                        @endforeach
                    @endif

                </tbody>

            </table>

            <div>
                <input type="hidden" name="" id="content_error">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
        </div>


    </div><br>
    <div class="row">
        <div class="col-md-6">
            <strong class="text-red quiz-err"></strong>
        </div>
        <div class="col-md-6">
            <strong class="text-red faq-err"></strong>
        </div>
    </div><br>
    </div>
    <div class="course" @if( $course != null && $course->id!=null) style="display: none;" @endif>
        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label>{{ __('Course') }} <span class="text-red">*</span></label><br>
                    <select name="course_id" class="form-control">
                        <option value="">Select Course</option>
                        @foreach (\App\Course::where('status',2)->get() as $c)
                            <option value="{{$c->id}}" data="{{Crypt::encryptString($c)}}"  @if($course!=null && $course->id == $c->id) selected @endif>{{$c->course_name_en}}</option>
                        @endforeach
                    </select>
                    <span class="invalid-feedback" role="alert">
                        <strong></strong>
                    </span>
                </div>
            </div>

        </div><br>

    </div>

<table class="table quizzes-list-table">
    <tbody>
      <tr>
        <td>
            <label for="">Quiz Type </label>
            <div class="row">
                @foreach (config("web_constant.quiz_types") as $type => $type_name)
                    <div class="col-md-2 q-type-{{$type}}">
                        <input type="radio" id="quiz_type" class="qtype-{{$type}}" value="{{$type}}" name="quiz_type[]" @if( $quiz->quiz_type==$type || ($quiz->quiz_type==null && $loop->first)) checked @endif> <span class="qType">{{$type_name}}</span>
                    </div>
                @endforeach
                <div class="col-md-2 remove-quiz"></div>
            </div><br>

            <div class="row">
                <div class="col-md-7">
                    <label for="">Quiz Question <span class="text-red">*</span></label>
                    <textarea  name="quiz_question[]" class="form-control" placeholder="Quiz Question" rows="3">{{$quiz->quiz_question}}</textarea>
                    <span class="invalid-feedback" role="alert">
                        <strong></strong>
                    </span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-7 quiz_options">
                    <div class="true_false" >
                        @if($quiz->options!=null && $quiz->quiz_type==1)
                            @foreach ($quiz->options as $opt)
                                <div style="display: flex;" class="mt-2">
                                    <input type="radio" id="is_correct_answer" name="is_correct_answer[]" value="{{$opt->is_correct}}" @if($opt->is_correct) checked @endif>
                                    <input type="text" class="form-control ml-1 quiz-options t_f" name="quiz_option[]" value="{{$opt->quiz_option}}" readonly>
                                    <input type="hidden" name="option_id[]" value="{{$opt->id}}">
                                </div>
                            @endforeach
                        @else
                            <div style="display: flex;">
                                <input type="radio" id="is_correct_answer" name="is_correct_answer[]" value="1" checked>
                                <input type="text" class="form-control ml-1  quiz-options t_f" name="quiz_option[]" value="TRUE (မှန်)" readonly>
                                <input type="hidden" name="option_id[]" value="">
                            </div>
                            <div style="display: flex;" class="mt-3">
                                <input type="radio" id="is_correct_answer" name="is_correct_answer[]" value="0">
                                <input type="text" class="form-control ml-1  quiz-options t_f" name="quiz_option[]" value="FALSE (မှား)" readonly>
                                <input type="hidden" name="option_id[]" value="">
                            </div>
                        @endif

                    </div>

                    <div class="single_choice" style="display: none;">
                        @if($quiz->options!=null && ($quiz->quiz_type==2 || $quiz->quiz_type==4))
                            @foreach ($quiz->options as $opt)
                                <div class="options">
                                    <div style="display: flex;">
                                        <input type="radio" id="is_correct_answer" name="is_correct_answer[]" value="{{$opt->is_correct}}" @if($opt->is_correct) checked @endif>
                                        <input type="text" class="form-control ml-1 quiz-options" name="quiz_option[]" value="{{$opt->quiz_option}}" placeholder="Quiz Option">
                                        @if(!$loop->first)
                                            <span class="btn remove-option"><i class="fa fa-times"></i></span>

                                        @endif
                                    </div>
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                                    <input type="hidden" name="option_id[]" value="{{$opt->id}}">
                                </div>
                            @endforeach
                        @else
                            <div class="options">
                                <div style="display: flex;">
                                    <input type="radio" id="is_correct_answer" name="is_correct_answer[]" value="1">
                                    <input type="text" class="form-control ml-1  quiz-options" name="quiz_option[]" value="" placeholder="Quiz Option">
                                </div>
                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                                <input type="hidden" name="option_id[]" value="">
                            </div>
                        @endif
                    </div>
                    <div class="multiple_choice" style="display: none;">
                        @if($quiz->options!=null && $quiz->quiz_type==3)
                            @foreach ($quiz->options as $opt)
                                <div class="options">
                                    <div style="display: flex;">
                                        <input type="checkbox" id="is_correct_answer" name="is_correct_answer[]" value="{{$opt->is_correct}}" @if($opt->is_correct) checked @endif>
                                        <input type="text" class="form-control ml-1  quiz-options" name="quiz_option[]" value="{{$opt->quiz_option}}" placeholder="Quiz Option">
                                        @if(!$loop->first)
                                        <span class="btn remove-option"><i class="fa fa-times"></i></span>
                                        @endif
                                    </div>
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                                    <input type="hidden" name="option_id[]" value="{{$opt->id}}">
                                </div>
                            @endforeach
                        @else
                            <div class="options">
                                <div style="display: flex;">
                                    <input type="checkbox" id="is_correct_answer" name="is_correct_answer[]" value="1" checked>
                                    <input type="text" class="form-control ml-1  quiz-options" name="quiz_option[]" value="" placeholder="Quiz Option">
                                </div>
                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                                <input type="hidden" name="option_id[]" value="">
                            </div>

                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-7">
                    <input type="hidden" name="other_validation_err">
                    <span class="invalid-feedback" role="alert">
                        <strong></strong>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-7">
                    <span class="btn btn-sm btn-info float-right mt-2 add-option" style="display: none;">
                        <i class="fa fa-plus"></i>
                    </span>
                </div>
            </div><br>

            <div class="row">
                <div class="col-md-7">
                    <label for="">Answer Description </label>
                    <textarea name="description[]" class="form-control" placeholder="Answer Description" rows="5">{{$quiz->description}}</textarea>
                    <span class="invalid-feedback" role="alert">
                        <strong></strong>
                    </span>
                </div>
            </div><br>
          </td>
        </tr>
    </tbody>
    <tfoot>
        @if($quiz->id==null)
            <tr>
                <td class="text-right">
                    <span class="btn btn-sm btn-info float-right mt-2 add-quiz">
                        <i class="fa fa-plus"></i>
                    </span>
                </td>
            </tr>
        @endif
    </tfoot>
</table>
<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div><br>

<script>
$(document).ready(function(){

    checkContentType();
    $('select[name=quiz_content_type]').change(function(){
        checkContentType();
    });
    $('select[name="select_topic_id"]').select2({
        placeholder : 'Select Topic',
        allowClear : true
    });
    $('select[name="course_id"]').select2({
        placeholder : 'Select Course',
        allowClear : true
    });

    $('.add-quiz').click(function(){
        let tr = $('table.quizzes-list-table > tbody > tr:last').clone();
        let rd_id = parseInt(Math.random() * 999);
        $(tr).find('#quiz_type').attr('name','quiz_type_'+rd_id);
        $(tr).find('#is_correct_answer').attr('name','is_correct_answer_'+rd_id+"[]");
        $(tr).find('.quiz-options').attr('name','quiz_option_'+rd_id+'[]');
        $(tr).find('input:not(".t_f,#quiz_type"),textarea').val(null);
        $(tr).find('#is_correct_answer').val(0);
        $(tr).find('input[type=radio],input[type=checkbox]').prop('checked',false);
        $(tr).find('#quiz_type:first').prop('checked',true);
        $(tr).find('input,textarea').removeClass('is-invalid');
        $(tr).find('.invalid-feedback strong').html(null);
        $(tr).find('.remove-quiz').html('<span class="btn btn-sm btn-link float-right mt-2 rmq"> <i class="fa fa-times"></i></span>')

        $('table.quizzes-list-table > tbody').append(tr);

        checkQuizType();

    });

    $(document).on('click','.rmq',function(){
        $(this).closest('tr').remove();
    });

    function checkContentType(){
        if($('select[name=quiz_content_type] option:selected').val()==2){
            $('.topic').css('display','none');
            $('.course').css('display','block');

        }else if($('select[name=quiz_content_type] option:selected').val()==1){
            $('.topic').css('display','block');
            $('.course').css('display','none');

        }else if($('select[name=quiz_content_type] option:selected').val()==3){
            $('.topic').css('display','none');
            $('.course').css('display','none');

        }
    }
    $('select[name=select_topic_id]').change(function(){
        let data = decryptData($('select[name=select_topic_id] option:selected').attr('data'));
        $('select[name=select_topic_id]').val(null).trigger('change.select2');

        if($('table.faq-table > tbody > tr[key='+data.id+']').length > 0) return false;

        let k = $('table.faq-table > tbody > tr').length + 1;

        let tr = "<tr class='tr' key='"+data.id+"'>"+
                    "<td><span class='faq-index'>"+k+"</span></td>"+
                    "<td>"+data.content_title+"</td>"+
                    "<td><span class='btn remove-faq'><i class='fa fa-times'></i></span><input type='hidden' class='content_id' name='content_id[]' value='"+data.id+"'></td>"+
                 "</tr>";
        $('table.faq-table > tbody').append(tr);
        $('.faq-err').html(null);
    });


    $(document).on('click','.remove-faq',function(){
       $(this).parent().closest('tr').remove();
       $('table.faq-table > tbody > tr').each(function(i,e){
            $(e).find('.faq-index').html(i+1);
        });
    });

    checkQuizType();
    checkGame();
    $('select[name=quiz_content_type]').select2({ placeholder : "Select Category", allowClear : true});

    $(document).on('click','.add-option',function(){
        let rd_id = parseInt(Math.random() * 999);
        let tr = $(this).closest('tr');
        let rename =  $(tr).find('#is_correct_answer:last').attr('name');
        // let rename = 'is_correct_answer_'+rd_id+"[]";
        // $(tr).find('#is_correct_answer').attr('name',rename);
        if($(tr).find('#quiz_type:checked').val()==2 || $(tr).find('#quiz_type:checked').val()==4){ //if single choice
            let new_option = '<div class="mt-2 options">'+
                                '<div style="display: flex;">'+
                                    '<input type="radio" id="is_correct_answer" value="0" name="'+rename+'">'+
                                    '<input type="text" class="form-control ml-1 quiz-options" name="quiz_option[]" value="" placeholder="Quiz Option">'+
                                    '<span class="btn remove-option"><i class="fa fa-times"></i></span>'+
                                '</div>'+
                                '<span class="invalid-feedback" role="alert">'+
                                    '<strong></strong>'+
                                '</span>'+
                                '<input type="hidden" name="option_id[]" value="">'+
                             '</div>';

            $(tr).find('.single_choice').append(new_option);
        }else if($(tr).find('#quiz_type:checked').val()==3){ //if multiple choice
            let new_option = '<div class="mt-2 options">'+
                                '<div  style="display: flex;">'+
                                    '<input type="checkbox" id="is_correct_answer" value="0" name="'+rename+'">'+
                                    '<input type="text" class="form-control ml-1 quiz-options" name="quiz_option[]" value="" placeholder="Quiz Option">'+
                                    '<span class="btn remove-option"><i class="fa fa-times"></i></span>'+
                                '</div>'+
                                '<span class="invalid-feedback" role="alert">'+
                                    '<strong></strong>'+
                                '</span>'+
                                '<input type="hidden" name="option_id[]" value="">'+
                            '</div>';

            $(tr).find('.multiple_choice').append(new_option);
        }

        $(tr).find('input[name=other_validation_err]').removeClass('is-invalid');
        $(tr).find('input[name=other_validation_err]').siblings().find('strong').html(null);
    });

    $(document).on('click','#quiz_type',function(){
        checkQuizType();
    });

    $('select[name=quiz_content_type]').change(function(){
        $(".tr").remove();
        checkGame();
    });

    $(document).on('click','.remove-option',function(){
        $(this).parent().parent().remove();
    });

    $(document).on('click','#is_correct_answer',function(){
        let name = $(this).attr('name');
        if($(this).attr('type')=="radio"){
            $('input[name="'+name+'"]').val(0);
            $(this).val(1);
            $('input[name=other_validation_err]').removeClass('is-invalid');
            $('input[name=other_validation_err]').siblings().find('strong').html(null);
        }else{
            if($(this).is(':checked')){
                $(this).val(1);
                $('input[name=other_validation_err]').removeClass('is-invalid');
                $('input[name=other_validation_err]').siblings().find('strong').html(null);
            }else $(this).val(0);
        }
    });

    $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("quiz-form"));
        var quiz_id = $('form#quiz-form').find('input[name=quiz_id]').val();
        if(quiz_id.length > 0){
            var route = '/quiz/'+quiz_id;
            formData.append('_method', 'PUT');
        }else if(quiz_id.length == 0){
            var route = '/quiz';
        }

        let quiz_option = [];
        let option_id = [];
        let is_correct_answer = [];
        let quiz_options = [];
        let option_ids = [];
        let is_correct_answers = [];
        let quiz_types = [];
        let valid = true;
        $('table.quizzes-list-table > tbody > tr').each(function(i,el){
            let correctAns = 0;
            quiz_types.push($(el).find('#quiz_type:checked').val());
            if($(el).find('#quiz_type:checked').val()==1){
                $(el).find('.true_false div').each(function(i,e){
                    let opt = $(e).find('.quiz-options').val();
                    let is_ans = $(e).find('#is_correct_answer').val();
                    let opt_id = $(e).find('input[name="option_id[]"]').val();

                    quiz_option.push(opt);
                    is_correct_answer.push(is_ans);
                    option_id.push(opt_id);

                    if($(e).find('#is_correct_answer').is(':checked')) correctAns=1;
                });
            }else if($(el).find('#quiz_type:checked').val()==2 || $(el).find('#quiz_type:checked').val()==4){
                $(el).find('.single_choice div.options').each(function(i,e){

                    let opt = $(e).find('.quiz-options').val();
                    let is_ans = $(e).find('#is_correct_answer').val();
                    let opt_id = $(e).find('input[name="option_id[]"]').val();

                    quiz_option.push(opt);
                    is_correct_answer.push(is_ans);
                    option_id.push(opt_id);
                    if($(e).find('#is_correct_answer').is(':checked')) correctAns=1;
                });
            }else if($(el).find('#quiz_type:checked').val()==3){
                $(el).find('.multiple_choice div.options').each(function(i,e){
                    let opt = $(e).find('.quiz-options').val();
                    let is_ans = $(e).find('#is_correct_answer').val();
                    let opt_id = $(e).find('input[name="option_id[]"]').val();

                    quiz_option.push(opt);
                    is_correct_answer.push(is_ans);
                    option_id.push(opt_id);

                    if($(e).find('#is_correct_answer').is(':checked')) correctAns=1;
                });
            }

            quiz_options[i] = quiz_option;
            option_ids[i] = option_id;
            is_correct_answers[i] = is_correct_answer;
            if(($(el).find('#quiz_type:checked').val()==2 || $(el).find('#quiz_type:checked').val()==3 || $(el).find('#quiz_type:checked').val()==4) && quiz_option.length < 3 ){
                $(el).find('input[name=other_validation_err]').addClass('is-invalid');
                $(el).find('input[name=other_validation_err]').siblings().find('strong').html('The quiz option must be at least 3 options.');
                valid = false;
            }
            
            if( correctAns==0){
                $(el).find('input[name=other_validation_err]').addClass('is-invalid');
                $(el).find('input[name=other_validation_err]').siblings().find('strong').html('The correct answer must be at least one option.');
                valid = false;
            }

            quiz_option = ['&&&'];
            option_id = ['&&&'];
            is_correct_answer = ['&&&'];

        });

        if(!valid) return false;

        formData.append('quiz_option', quiz_options);
        formData.append('is_correct_answer', is_correct_answers);
        formData.append('option_id', option_ids); 
        formData.append('quiz_type', quiz_types);

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){
                window.location.href = '/quiz?index='+data.index+'&type='+data.type;
            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.quiz_content_type!=undefined){
                    $('form#quiz-form').find('select[name=quiz_content_type]').addClass('is-invalid');
                    $('form#quiz-form').find('select[name=quiz_content_type]').siblings().find('strong').html(e.responseJSON.quiz_content_type)
                }

                if(e.responseJSON.content_id!=undefined){
                    $('form#quiz-form').find('#content_error').addClass('is-invalid');
                    $('form#quiz-form').find('#content_error').siblings().find('strong').html(e.responseJSON.content_id)
                }

                if(e.responseJSON.course_id!=undefined){
                    $('form#quiz-form').find('select[name="course_id"]').addClass('is-invalid');
                    $('form#quiz-form').find('select[name="course_id"]').siblings().find('strong').html(e.responseJSON.course_id)
                }

                $('table.quizzes-list-table > tbody > tr').each(function(k,el){
                    console.log(e.responseJSON['quiz_question.'+0]);
                    if(e.responseJSON['quiz_question.'+k]!=undefined){
                        $(el).find('textarea[name="quiz_question[]"]').addClass('is-invalid');
                        $(el).find('textarea[name="quiz_question[]"]').siblings().find('strong').html(e.responseJSON['quiz_question.'+k])
                    }

                    if(e.responseJSON['description.'+k]!=undefined){
                        $(el).find('textarea[name=description]').addClass('is-invalid');
                        $(el).find('textarea[name=description]').siblings().find('strong').html(e.responseJSON['description.'+k])
                    }
                   
                    if($(el).find('#quiz_type:checked').val()==1){
                        for(let i=0; i < $(el).find('.true_false div').length; i++ ){
                            if(e.responseJSON['quiz_option.'+k+'.'+i]!=undefined){
                                $(el).find('.true_false div:eq('+i+') ').find('.quiz_options').addClass('is-invalid');
                                $(el).find('.true_false div:eq('+i+') ').find('.quiz_options').siblings().find('strong').html(e.responseJSON['quiz_option.'+k+'.'+i]);
                            }
                        }
                    }else if($(el).find('#quiz_type:checked').val()==2 || $(el).find('#quiz_type:checked').val()==4){
                        for(let i=0; i < $(el).find('.single_choice div.options').length; i++ ){
                            if(e.responseJSON['quiz_option.'+k+'.'+i]!=undefined){
                                $(el).find('.single_choice div.options:eq('+i+') ').find('.quiz_options').addClass('is-invalid');
                                $(el).find('.single_choice div.options:eq('+i+') ').find('strong').html(e.responseJSON['quiz_option.'+k+'.'+i]);
                            }
                        }
                    }else if($(el).find('#quiz_type:checked').val()==3){
                        for(let i=0; i < $(el).find('.multiple_choice div.options').length; i++ ){
                            if(e.responseJSON['quiz_option.'+k+'.'+i]!=undefined){
                                $(el).find('.multiple_choice div.options:eq('+i+') ').find('.quiz_options').addClass('is-invalid');
                                $(el).find('.multiple_choice div.options:eq('+i+') ').find('strong').html(e.responseJSON['quiz_option.'+k+'.'+i]);
                            }
                        }
                    }
                }) 

                return false;
            }
        })
    });
});

function checkQuizType(){
    $('table.quizzes-list-table > tbody > tr').each(function(i,e){
        if($(e).find('#quiz_type:checked').val()==1){
            $(e).find('.true_false').css('display','');
            $(e).find('.add-option').css('display','none');
            $(e).find('.single_choice').css('display','none');
            $(e).find('.multiple_choice').css('display','none');
        }else if($(e).find('#quiz_type:checked').val()==2 || $(e).find('#quiz_type:checked').val()==4){
            $(e).find('.true_false').css('display','none');
            $(e).find('.single_choice').css('display','');
            $(e).find('.multiple_choice').css('display','none');
            $(e).find('.add-option').css('display','');
        }else{
            $(e).find('.true_false').css('display','none');
            $(e).find('.single_choice').css('display','none');
            $(e).find('.multiple_choice').css('display','');
            $(e).find('.add-option').css('display','');
        }
    });
}
function checkGame(){
    if($('select[name=quiz_content_type]').val()==3){
        $('.q-type-3').css('display','none');
        if($('#quiz_type:checked').val()==3){
            $("input[name='quiz_type'][value=1]").prop('checked',true);
            $('.q-type-3').css('display','none');
            checkQuizType();
        }

    }else {
        $('.q-type-3').css('display','block');
    }

}



</script>
