@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
     @php
         $created_user_name = $quiz->created_user_name;
         $updated_user_name = $quiz->updated_user_name;
         $published_user_name = $quiz->published_user_name;
         $deactivated_user_name = $quiz->deactivated_user_name;

         $created_at = date('d-M-Y H:i:s',strtotime($quiz->created_at));
         $updated_at = $quiz->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->updated_at));
         $published_at = $quiz->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->published_at));
         $deactivated_at = $quiz->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->deactivated_at));

     @endphp

     <div class="row">
        <div class="col-md-3">
            <label for="">Category</label><br>
            <span>{{config("web_constant.quiz_content_types.$quiz->quiz_content_type")}}</span>
        </div>
        <div class="col-md-3">
            <label for="">Quiz Type</label><br>
            <span>{{config("web_constant.quiz_types.$quiz->quiz_type")}}</span>
        </div>
        <div class="col-md-3">
            <label for="">Status</label><br>
            <span class="status_{{config("web_constant.status.$quiz->status")}}">{{config("web_constant.status.$quiz->status")}}</span>
        </div>
     </div><br>

     <div class="row">
        <div class="col-md-12">
            <label for="">Description</label><br>
            <span>{{$quiz->description}}</span>
        </div>
     </div><br>

     <div class="row">
        <div class="col-md-12">
            <label for="">Quiz Question</label><br>
            <span>{{$quiz->quiz_question}}</span>
        </div>
     </div><br>
     <label for="">Options</label>
     @foreach ($quiz->options as $opt)
         <div class="row">
             <div class="col-md-12">
                @if($quiz->quiz_type==1 || $quiz->quiz_type==2)
                  <input type="radio" @if($opt->is_correct) checked @endif disabled>
                @else
                  <input type="checkbox" @if($opt->is_correct) checked @endif disabled>
                @endif
                 {{$opt->quiz_option}}
             </div>
         </div><br>
     @endforeach

     <div class="row " >

        <div class="col-md-12">
             <h5>Topic Lists</h5>
             <span class="invalid-feedback" role="alert">
                 <strong></strong>
             </span>
             <table class="table data-table table-bordered faq-table">
                 <thead>
                     <th style="width: 50px;">No.</th>
                     <th>Topic</th>
                 </thead>
                 <tbody>
                     @if ($quiz->contents->count() > 0)
                         @foreach ($quiz->contents as $k => $content)
                         <tr key="{{$content->content_id}}">
                             <td><span class="faq-index">{{$k+1}}</span></td>
                             <td>{{$content->content_title}}</td>
                         </tr>
                         @endforeach
                     @endif

                 </tbody>

             </table>
         </div>


     </div><br>

     @include('view-detail')
</div>
@stop
