@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">

    <form method="POST" action="{{ url('video') }}" enctype="multipart/form-data" id="video_create" >
        @csrf
        @include('video.form')
    </form>
</div>
@stop
