@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    @include('search')
    <div class="col-md-3">
    <select class="form-control select2 type-filter-select2 video_status" name="quiz_type" style="width: 100%;">
        <option value=""></option>
        @foreach (config("web_constant.status") as $k=>$t)
            <option value="{{$k}}">{{$t}}</option>
        @endforeach
    </select>
</div>
    <div class="col-md-3"></div>
    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Video Title</th>
                <th>Thumbnail</th>
                <th>Video Duration</th>
                <th>Display Order</th>
                <th>Created By</th>
                <th>Published By</th>
                <th>Published At</th>
                <th class="status-col">Status</th>
                @if(auth()->user()->can('video-edit') || auth()->user()->can('video-delete'))
                  <th class="action-col" ><span></span></th>
                @else
                  <th class="hide-col" ><span></span></th>
                @endif
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
   <script>
     $(document).ready(function () {
       var table = $('.data-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    aaSorting: [],
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    columnDefs: [
                   { orderable: true,  targets: 4 },
                   { orderable: false, targets: '_all' }
                   ],
                    ajax: "{{ url('video') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'video_title', name: 'video_title'},
                        {data: 'thumbnail', name: 'thumbnail'},
                        {data: 'video_duration', name: 'video_duration'},
                        {data: 'order', name: 'order',ordering: true},
                        {data: 'created_user_name', name: 'created_user_name'},
                        {data: 'published_user_name', name: 'published_user_name'},
                        {data: 'published_at', name: 'published_at'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', searchable: false, className :'action-col'},
                    ]
                  });

       $('.list-search').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $('.list-search').on( 'search', function () {
            table.search( this.value ).draw();
        } );

        $('.role-filter-select2').select2({ placeholder : "Filter By Role", allowClear : true, });

        $(document).on('change','.role-filter-select2',function(){
            var role_id = $(this).val();
            table.ajax.url("{{ url('video')}}"+"?role_id="+role_id).load();
        });
        $('.type-filter-select2').select2({ placeholder : "Filter By Status", allowClear : true});

        $(document).on('change','.type-filter-select2',function(){
                            searchFun();
                    });

            function searchFun(){
            var video_status = $('.video_status').val();

            table.ajax.url("{{ url('video')}}"+"?video_status="+video_status).load();
        }
     });
   </script>
@stop
