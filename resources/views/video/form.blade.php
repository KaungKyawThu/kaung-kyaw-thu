<input type="hidden" name="current_index" value="{{$video->current_index ?? 1}}">
<input type="hidden" name="video_id" value="{{$video->id}}">

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Video Title') }} <span class="text-red">*</span></label>
            <input id="video_title" type="text" class="form-control "
                name="video_title" value="{{ old('video_title', $video->video_title) }}" autocomplete="video_title"
                autofocus placeholder="Video Title">


                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Video Link') }} <span class="text-red">*</span></label>
            <input id="video_link" type="text" class="form-control "
                name="video_link" value="{{ old('video_link', $video->video_link) }}" autocomplete="video_link"
                autofocus placeholder="Video Link">


                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>

        </div>
    </div><br>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Video Duration') }}<span class="text-red"> (Minutes) *</span> </label>
            <input id="video_duration" type="text" class="form-control"
                name="video_duration" value="{{ old('video_duration', $video->video_duration) }}"
                autocomplete="video_duration" autofocus placeholder="5:30">


                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>

        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Keyword') }} </label><br>
            @php
                $keyword_ids = $video->keywords
                    ->where('explore_type', 3)
                    ->pluck('keyword_id')
                    ->toArray();
            @endphp

            <select name="keyword_id[]" id="keyword_id" class="form-control " multiple>
                {{-- <option value="">Select Service</option> --}}
                @foreach (\App\Keyword::get() as $item)
                    <option value="{{ $item->id }}" @if (in_array($item->id, $keyword_ids)) selected @endif>
                        {{ $item->keyword_name }}</option>
                @endforeach
            </select>

                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
        </div>


    </div>

</div>


<div class="row">
        <div class="col-md-6" style="display: block!important;" >
            <label for="" style="display: block;">{{__('Preview Photo')}} <span class="text-red">* </span> <span class="photo_upload_description">( {{$upload_photo_description}} )</span></label>

            <div class="row preview-row" @if(isset($is_update)) style="" @else style="display:none" @endif>
                <div class="col-md-10">
                    <img id="upload-preview" src="{{$video->thumbnail}}" alt="upload photo" style="width:100%"/>
                </div>
                <div class="col-md-1">
                    <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
                </div>
            </div>
            <div class="row">
                <input type="file" id="single-photo" class="@error('photo') is-invalid @enderror photo_file form-input form-control" name="thumbnail" placeholder="{{__('Upload Photo')}}" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null) style="display:none" @else style="" @endif>
                <input type="hidden" class=" @error('content_cover_photo') is-invalid @enderror" >
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>

                <input type="hidden" name="virtual_img" id="virtual-img" value="{{$video->thumbnail}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
            <label for="">{{_('Order')}}<span class="text-red">*</span></label>
            <input type="number" value="{{ old('order', $video->order) }}" name="order" placeholder="Video Order" class="form-control">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
        </div>

</div><br>


<div class="row">

    <div class="text-left">
        <a href="{{ url('video') }}" class="btn btn-danger orange-text cancal-btn float-right ml-3">Cancel</a>
        <span class="btn btn-primary dark-text save-btn save-project-data float-right">Save</span>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.save-btn').click(function(){
        var that = $(this);
    var formData = new FormData(document.getElementById("video_create"));
    var video_id = $('form#video_create').find('input[name=video_id]').val();

    if(video_id.length>0){
    var route = '/video/'+video_id;
      formData.append("_method", 'PUT');
    }else if(video_id.length==0){
      var route = '/video';
    }
        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){
                window.location.href = '/video?index='+data.index;
            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.video_title!=undefined){
                $('form#video_create').find('input[name="video_title"]').addClass('is-invalid');
                $('form#video_create').find('input[name="video_title"] ').siblings().find('strong').html(e.responseJSON.video_title);
                }
                if(e.responseJSON.video_link!=undefined){
                $('form#video_create').find('input[name="video_link"]').addClass('is-invalid');
                $('form#video_create').find('input[name="video_link"] ').siblings().find('strong').html(e.responseJSON.video_link);
                }
                if(e.responseJSON.video_duration!=undefined){
                $('form#video_create').find('input[name="video_duration"]').addClass('is-invalid');
                $('form#video_create').find('input[name="video_duration"] ').siblings().find('strong').html(e.responseJSON.video_duration);
                }
                if(e.responseJSON.keyword_id!=undefined){
                $('form#video_create').find('select[name="keyword_id[]"]').addClass('is-invalid');
                $('form#video_create').find('select[name="keyword_id[]"] ').siblings().find('strong').html(e.responseJSON.keyword_id);
                }
                if(e.responseJSON.thumbnail!=undefined){
                $('form#video_create').find('input[name="thumbnail"]').addClass('is-invalid');
                $('form#video_create').find('input[name="thumbnail"] ').siblings().find('strong').html(e.responseJSON.thumbnail);
                }
                if(e.responseJSON.order!=undefined){
                $('form#video_create').find('input[name="order"]').addClass('is-invalid');
                $('form#video_create').find('input[name="order"] ').siblings().find('strong').html(e.responseJSON.order);
                }

                return false;
            }
        })
    });
        $selectElement = $('#keyword_id').select2({
            placeholder: "Please select keyword",
            allowClear: true
        });
    });


</script>
