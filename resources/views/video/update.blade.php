@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('video/'.$video->id) }}" id="video_create" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        @include('video.form')
    </form>
</div>
@stop
