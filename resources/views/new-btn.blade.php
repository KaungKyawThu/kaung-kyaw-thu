<div class="col-md-3 create-new">
    @if(auth()->user()->can("$create_permission"))
      <a href="{{url($route.'/create')}}" class="btn btn-primary float-right create-new-one"><i class="fa fa-plus" ></i> Create New {{ucwords($keyword)}}</a>
    @endif
</div>
