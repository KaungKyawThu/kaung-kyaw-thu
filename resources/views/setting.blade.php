@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('setting') }}" id="setting-form">
        @csrf
        <input type="hidden" name="about_id" value="{{$about->id}}">
        <div class="row">
            <div class="col-md-3">
                <label for="">Male Phone No. <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="male_phone_no" placeholder="Phone No." value="{{$about->male_phone_no}}">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
            <div class="col-md-3">
                <label for="">Male Phone No. Myanmar <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="male_mm_phone_no" placeholder="Phone No." value="{{$about->male_mm_phone_no}}">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
            <div class="col-md-3">
                <label for="">Female Phone No. <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="female_phone_no" placeholder="Phone No." value="{{$about->female_phone_no}}">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
            <div class="col-md-3">
                <label for="">Female Phone No. Myanmar <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="female_mm_phone_no" placeholder="Phone No." value="{{$about->female_mm_phone_no}}">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
            
        </div><br>

        <div class="row">
            <div class="col-md-3">
                <label for="">Facebook Link <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="facebook_link" placeholder="Facebook Link" value="{{$about->facebook_link}}">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
            <div class="col-md-3">
                <label for="">Facebook Page id <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="facebook_page_id" placeholder="Facebook Link" value="{{$about->facebook_page_id}}">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
            <div class="col-md-3">
                <label for="">Messenger Link <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="messenger_link" placeholder="Messenger Link" value="{{$about->messenger_link}}">
                <span class="invalid-feedback" role="alert">
                    <strong></strong>
                </span>
            </div>
        </div><br>

        <h5>Htarwara Mobile App Policies</h5>
        <div class="row">
            <div class="col-md-12">
                <table class="table data-table policies-table">
                    <thead>
                        <tr>
                            <th style="width: 30px!important;">No.</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th style="width: 50px!important;" class="action-col"><span></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($policies->count() > 0)
                           @foreach ($policies as $k => $p)
                                <tr class="update-content-detail">
                                    <td><span class="policy-index">{{$k+1}}</span></td>
                                    <td>
                                        <input type="text" name="title[]" class="form-control title" value="{{$p->title}}" id="" placeholder="Policy Title">
                                        <span class="invalid-feedback" role="alert">
                                            <strong></strong>
                                        </span>
                                    </td>
                                    <td>
                                        <textarea class="form-control detail_description" rows="10" cols="30" id="detail_description_{{$k}}" name="description[]" placeholder="Policy Description">{{$p->description}}</textarea>
                                        <span class="invalid-feedback" role="alert">
                                            <strong></strong>
                                        </span>
                                    </td>
                                    <td>
                                        <span @if($loop->first) style="display: none;" @endif class="float-right text-red btn remove-policy" ><i class="fa fa-times"></i></span>
                                    </td>
                                </tr>
                           @endforeach
                        @else

                            <tr class="update-content-detail">
                                <td><span class="policy-index">1</span></td>
                                <td>
                                    <input type="text" name="title[]" class="form-control" placeholder="Policy Title" value="">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                                </td>
                                <td>
                                    <textarea class="form-control detail_description" rows="10" cols="30" id="detail_description_0" name="description[]" placeholder="Policy Description"></textarea>
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                                </td>
                                <td>
                                    <span style="display: none;" class="float-right text-red btn remove-policy" ><i class="fa fa-times"></i></span>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right">
                                <span class="float-right btn btn-info btn-sm add-policy"><i class="fa fa-plus"></i></span>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><br>

        <div class="row">
            <div class="mr-3 ml-2">
                <a class="form-control hpy-btn btn btn-primary save-btn">
                    {{ __('Save') }}
                </a>
            </div>
            <div class="mr-3">
                <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
                    {{ __('Cancel') }}
                </a>
            </div>
        </div><br>
    </form>
</div>

<script>
  $(document).ready(function(){
    var len = $('table.policies-table > tbody > tr').length - 1;
    $('.update-content-detail').each(function(i,e){
        CKEDITOR.replace('detail_description_'+i, {
            filebrowserUploadUrl: "{{ route('content.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form',
            image_previewText: ' '
        });
    });
    $('.add-policy').click(function(){
        let tr = $('table.policies-table > tbody > tr:last').clone();
        let k = $('table.policies-table > tbody > tr').length + 1;
        $(tr).find('.remove-policy').css('display','');
        $(tr).find('input,textarea').val(null);
        $(tr).find('.policy-index').html(k);
        $(tr).find('input,textarea').removeClass('is-invalid');
        $(tr).find('strong').html(null);

        $(tr).find('.tb-index').html(k);
        $(tr).find('.detail_description').attr('id','detail_description_'+k);
        $('table.policies-table > tbody').append(tr);
        CKEDITOR.replace("detail_description_"+k, {
            filebrowserUploadUrl: "{{ route('content.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form'
        });

        k = k - 1;

        $('table.policies-table > tbody > tr:last').find('#cke_detail_description_'+k).remove();
        $('table.policies-table > tbody > tr:last').find('#cke_detail_description_'+len).remove();
    });


    $(document).on('click','.remove-policy',function(){
         $(this).parent().closest('tr').remove();
         $('table.policies-table > tbody > tr').each(function(i,e){
            $(e).find('.policy-index').html(i+1);
        });
    });

    $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("setting-form"));
        var about_id = $('form#setting-form').find('input[name=about_id]').val();

        let detail_desc = "";
        $.each( CKEDITOR.instances, function(i,e) {
            detail_desc += e.getData()+"*****";
        });


        formData.append('detail_desc', detail_desc);

        $.ajax({
            url : '/setting',
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){
                window.location.href = '/setting';
            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.male_phone_no!=undefined){
                    $('form#setting-form').find('input[name=male_phone_no]').addClass('is-invalid');
                    $('form#setting-form').find('input[name=male_phone_no]').siblings().find('strong').html(e.responseJSON.male_phone_no)
                }

                if(e.responseJSON.male_mm_phone_no!=undefined){
                    $('form#setting-form').find('input[name=male_mm_phone_no]').addClass('is-invalid');
                    $('form#setting-form').find('input[name=male_mm_phone_no]').siblings().find('strong').html(e.responseJSON.male_mm_phone_no)
                }

                if(e.responseJSON.female_phone_no!=undefined){
                    $('form#setting-form').find('input[name=female_phone_no]').addClass('is-invalid');
                    $('form#setting-form').find('input[name=female_phone_no]').siblings().find('strong').html(e.responseJSON.female_phone_no)
                }

                if(e.responseJSON.female_mm_phone_no!=undefined){
                    $('form#setting-form').find('input[name=female_mm_phone_no]').addClass('is-invalid');
                    $('form#setting-form').find('input[name=female_mm_phone_no]').siblings().find('strong').html(e.responseJSON.female_mm_phone_no)
                }

                if(e.responseJSON.facebook_link!=undefined){
                    $('form#setting-form').find('input[name=facebook_link]').addClass('is-invalid');
                    $('form#setting-form').find('input[name=facebook_link]').siblings().find('strong').html(e.responseJSON.facebook_link)
                }

                if(e.responseJSON.facebook_page_id!=undefined){
                    $('form#setting-form').find('input[name=facebook_page_id]').addClass('is-invalid');
                    $('form#setting-form').find('input[name=facebook_page_id]').siblings().find('strong').html(e.responseJSON.facebook_page_id)
                }

                if(e.responseJSON.messenger_link!=undefined){
                    $('form#setting-form').find('input[name=messenger_link]').addClass('is-invalid');
                    $('form#setting-form').find('input[name=messenger_link]').siblings().find('strong').html(e.responseJSON.messenger_link)
                }

                for(let i=0; i < $('table.policies-table > tbody > tr').length; i++ ){
                    if(e.responseJSON['title.'+i]!=undefined){
                        $('table.policies-table > tbody > tr:eq('+i+') ').find('input[name="title[]"]').addClass('is-invalid');
                        $('table.policies-table > tbody > tr:eq('+i+') ').find('input[name="title[]"]').siblings().find('strong').html(e.responseJSON['title.'+i]);
                    }

                    if(e.responseJSON['description.'+i]!=undefined){
                        $('table.policies-table > tbody > tr:eq('+i+') ').find('textarea[name="description[]"]').addClass('is-invalid');
                        $('table.policies-table > tbody > tr:eq('+i+') ').find('textarea[name="description[]"]').siblings().find('strong').html(e.responseJSON['description.'+i]);
                    }
                }

                return false;
            }
        })
    });
});
</script>
@stop
