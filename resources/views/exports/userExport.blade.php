<table>
    <tr>
        <td colspan="9" class="text-center d-block"><h3>User Report</h3></td>
    </tr>
    <tr><td colspan="9" class="text-center d-block"> {{ $header }} </td>
    </tr>
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Region</th>
        <th>Township</th>
        <th>Gender</th>
        <th>Birth Year</th>
        <th>Register Date</th>
    </tr>
    <?php $no = 0; ?>
    @foreach ($datas as $data)
    <tr>
        <td> {{ $no += 1 }} </td>
        <td>{{ $data['name'] }}</td>
        <td>{{ $data['email'] }}</td>
        <td>{{ $data['phone_no'] }}</td>
        <td>{{ $data['region_name'] }}</td>
        <td>{{ $data['tsp_name'] }}</td>
        <td>{{ $data['gender'] }}</td>
        <td>{{ $data['birth_year'] }}</td>
        <td>{{ $data['DATE_FORMAT(users.created_at,"%d/%m/%Y")'] }}</td>
    </tr>
@endforeach
</table>
