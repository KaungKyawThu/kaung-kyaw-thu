@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<style>
    .card-body{
        font-weight: 400;
    }
</style>
<div class="card-body">
<div class="row">
    <div class="col-md-12 text-center">
        <img src="{{config('web_constant.user_photo')}}" class="user-image img-circle elevation-2" width="80" alt="{{$user->nick_name}}"><br>
        <span><b>{{$user->nick_name}}</span><br>
        <span><i class="fa fa-star" style="color: gold"></i> {{number_format($user->user_point)}}</span>

    </div>
</div><br>

<div class="row">
    <div class="col-md-3">
       <label for=""> Gender : </label>{{config("web_constant.gender.$user->gender_id")}}
    </div>
    <div class="col-md-3">
        <label for=""> Birth Year : </label> {{$user->birth_year}}
     </div>
     <div class="col-md-3">
        <label for=""> Region : </label> {{$user->region_name}}
     </div>
     <div class="col-md-3">
        <label for=""> Township : </label> {{$user->tsp_name}}
     </div>
</div><br>

<hr>
<h5 class="text-center">Course & Certificates</h5>
<div class="row">
    <div class="col-md-4"><label for="">Ongoing Course :</label>  {{config("web_constant.courses.$user->course_level_id")}}</div>
    <div class="col-md-4"><label for="">Certificate Name :</label>  {{$user->certificate_name}}</div>
    <div class="col-md-4"><label for="">Certificate Phone No. :</label>  {{$user->certificate_phone_no}}</div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table data-table table-bordered">
            <thead>
                <tr><td colspan="4" class="text-center">Certificate Lists</td></tr>
                <tr>
                    <th>No.</th>
                    <th>Course</th>
                    <th style="width:170px;">Is Generated ?</th>
                    <th class="action-col"><span></span></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user->certificates as $k => $c)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td>{{config("web_constant.courses.$c->course_level_id")}}</td>
                        <td>{{$c->is_generated ? 'Yes' : 'No'}}</td>
                        @php
                            $certificate_link = $c->is_generated ? $c->certificate_link : '/certificate?level='.$c->course_level_id."&name=".$user->certificate_name ?? $user->nick_name;
                        @endphp
                        <td><a href="{{$certificate_link}}" target="_blank" class="btn btn-sm btn-link"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div><br>
<h5 class="text-center">Security Questions & Answers</h5>
<div class="row">
     <div class="col-md-12">
         <table class="table data-table table-bordered">
             <thead>
                <tr>
                    <th>No.</th>
                    <th>Question</th>
                    <th>Answer</th>
                </tr>
             </thead>
             <tbody>
                @foreach ($user->security_answers as $j => $q)
                    <tr>
                        <td>{{$j+1}}</td>
                        <td>{{$q->question}}</td>
                        <td>{{$q->security_ans}}</td>
                    </tr>
                @endforeach
             </tbody>
         </table>
     </div>
</div><br>

<h5 class="text-center">Answered Quizzes</h5>
<div class="row">
    <div class="col-md-12">
        <table class="table data-table quizzes-table table-bordered">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Quiz</th>
                    <th style="width: 250px;">Type</th>
                </tr>
            </thead>
            <body>
                @foreach ($user->answer_quizzes as $i => $quiz)

                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$quiz->quiz_question}}</td>
                        <td>{{config("web_constant.quiz_content_types.$quiz->quiz_content_type")}}</td>
                    </tr>
                @endforeach
            </body>
        </table>
    </div>
</div>
</div>
<script>
    $('.quizzes-table').dataTable({
        "bSort":false,
        "searching" : true
    });
</script>
@stop
