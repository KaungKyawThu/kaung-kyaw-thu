@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" class="data-form" action="{{ url('user/'.$user->id) }}">
        @csrf  
        @method('PUT')
        <input type="hidden" name="is_profile" value="{{$is_profile}}">
        @include('user.form')
    </form>
</div>
@stop
