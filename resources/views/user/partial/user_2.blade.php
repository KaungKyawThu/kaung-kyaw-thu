<table class="table table-bordered data-table type-2-table">
    <thead>
        <tr>
            <th width="50">No</th>
            <th>Name</th>
            <th>Phone No</th>
            <th>Created At</th>
            <th class="status-col">Status</th>
            @if(auth()->user()->can('user-edit') || auth()->user()->can('user-block') || auth()->user()->can('user-delete'))
              <th class="action-col" ><span></span></th>
            @else
              <th class="hide-col" ><span></span></th>
            @endif
        </tr>
    </thead>
    <tbody>
    </tbody>
  </table>

<script>

   var table2 = $('.type-2-table').DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: false,
                info: false,
                autoWidth: true,
                responsive: true,
                processing: true,
                serverSide: true,
                // displayStart : "{{$current_index}}",
                //stateSave: true,
                ajax: "{{ url('user?role_id=2') }}",
                createdRow: function( row, data, dataIndex ) {
                      if(data.action.length==0)
                        $(row).find('td.action-col').css('display', 'none');
                  },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                    {data: 'name', name: 'name'},
                    {data: 'phone_no', name: 'phone_no'},
                    {data: 'created_date', name: 'created_date'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                ]
              });



</script>
