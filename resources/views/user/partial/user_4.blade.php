<div class="text-right">
    <a class="btn btn-primary text-right text-right export" > <i class='fa fa-download' style='color: white'></i>
        Export User Data</a>
</div>
<table class="table table-bordered data-table type-4-table">
    <thead>
        <tr>
            <th width="50">No</th>
            <th>Name</th>
            <th>Phone No</th>
            <th>Gender</th>
            <th>Birth Year</th>
            <th>Region</th>
            <th>Township</th>
            <th>Created At</th>
            <th class="status-col">Status</th>
            @if(auth()->user()->can('user-edit') || auth()->user()->can('user-block') || auth()->user()->can('user-delete'))
              <th class="action-col" ><span></span></th>
            @else
              <th class="hide-col" ><span></span></th>
            @endif
        </tr>
    </thead>
    <tbody>
    </tbody>
  </table>

<script>

   var table4 = $('.type-4-table').DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: false,
                info: false,
                autoWidth: true,
                responsive: true,
                processing: true,
                serverSide: true,
                // displayStart : "{{$current_index}}",
                //stateSave: true,
                ajax: "{{ url('user?role_id=4') }}",
                createdRow: function( row, data, dataIndex ) {
                      if(data.action.length==0)
                        $(row).find('td.action-col').css('display', 'none');
                  },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                    {data: 'name', name: 'name'},
                    {data: 'phone_no', name: 'phone_no'},
                    {data: 'gender', name: 'gender'},
                    {data: 'birth_year', name: 'birth_year'},
                    {data: 'region_name', name: 'region_name'},
                    {data: 'township_name', name: 'township_name'},
                    {data: 'created_date', name: 'created_date'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                ]
              });



</script>
