@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" class="data-form" action="{{ url('user') }}">
        @csrf  
        @include('user.form')
    </form>
</div>
@stop
