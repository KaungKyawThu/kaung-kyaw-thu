<input type="hidden" name="current_index" value="{{$user->current_index ?? 1}}">
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label>User Role <span class="text-red">*</span></label>
            <select class="form-control select2 user-role-select2 @error('role_id') is-invalid @enderror" @if($is_profile) id="profile-role-id" @endif name="role_id" style="width: 100%;">
                <option value=""></option>
                @foreach (config("web_constant.user_roles") as $k => $r)
                    @if($k!=4)
                      <option value="{{$k}}" @if(old('role_id', $user->role_id)==$k) selected @endif>{{$r}}</option>
                    @endif
                @endforeach
            </select>
            @if($is_profile)
               <input type="hidden" name="role_id" value="{{$user->role_id}}">
            @endif
            @error('role_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Name') }} <span class="text-red">*</span></label>
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" autocomplete="name" autofocus placeholder="Name">

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

<div class="row psw">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('E-Mail Address') }} <span class="text-red">*</span></label>
            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" autocomplete="email" placeholder="E-Mail Address">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Phone No') }} <span class="text-red">*</span></label>
            <input id="phone_no" type="text" class="form-control @error('phone_no') is-invalid @enderror" name="phone_no" value="{{ old('phone_no', $user->phone_no) }}" autocomplete="phone_no" placeholder="Phone No">

            @error('phone_no')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

<div class="row psw">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Password') }} <span class="text-red">*</span></label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Confirm Password') }} <span class="text-red">*</span></label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password">
        </div>
    </div>
</div><br>

@include('save-btn')

<script>
$(document).ready(function(){
    checkUserRole();

    $('.user-role-select2').select2({
        placeholder : 'Select User Role',
        allowClear : true
    });

    $('.user-role-select2').change(function(){
        checkUserRole();
    });

    $('#profile-role-id').prop('disabled','disabled');

})

function checkUserRole(){
    if( $('.user-role-select2 option:selected').val()==5){
        $('.psw').css("display","none");
    }else{
        $('.psw').css("display","");
    }
}
</script>
