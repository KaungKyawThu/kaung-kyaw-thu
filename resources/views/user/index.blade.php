@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
    @php
        $active_tab = isset($_GET['type']) ? $_GET['type'] : 1;

    @endphp
    <div class="row">
        @include('search')
        <div class="col-md-2">
            <select class="form-control select2 type-filter-select2 user_status" name="quiz_type" style="width: 100%;">
                <option value=""></option>
                @foreach (config('web_constant.user_status') as $k => $t)
                    <option value="{{ $k }}">{{ $t }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-md-2 not-user" style="display: none;"></div>
        <div class="col-md-2 not-user" style="display: none;"></div>
        <div class="col-md-2 for-user" style="display: none;">
            <select name="" class="form-control select2 type-filter-select2 region" id="region_id">
                <option value=""></option>
                @foreach (DB::table('regions')->get() as $s)
                    <option value="{{ $s->id }}">{{ $s->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-md-2 for-user" style="display: none;">
            <select name="" class="form-control select2 type-filter-select2 township" id="township_id">
                <option value=""></option>
                @foreach (DB::table('townships')->get() as $s)
                    <option value="{{ $s->id }}">{{ $s->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-1"></div>
        @include('new-btn')
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs sub-nav-tabs" id="myTab" role="tablist">
                @foreach (config('web_constant.user_roles') as $k => $type)
                    <li class="nav-item" role="presentation">
                        <a class="nav-link @if ($active_tab == $k) active @endif"
                            id="user-{{ $k }}-tab" data-toggle="tab" href="#user-{{ $k }}"
                            role="tab" aria-controls="user-{{ $k }}"
                            aria-selected="true">{{ $type }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content sub-tab-content" id="myTabContent">
                <div class="tab-pane fade @if ($active_tab == 1) show active @endif" id="user-1"
                    role="tabpanel" aria-labelledby="user-1-tab">
                    @include('user.partial.user_1')
                </div>

                <div class="tab-pane fade @if ($active_tab == 2) show active @endif" id="user-2"
                    role="tabpanel" aria-labelledby="user-2-tab">
                    @include('user.partial.user_2')
                </div>

                <div class="tab-pane fade @if ($active_tab == 3) show active @endif" id="user-3"
                    role="tabpanel" aria-labelledby="user-3-tab">
                    @include('user.partial.user_3')
                </div>
                <div class="tab-pane fade @if ($active_tab == 4) show active @endif" id="user-4"
                    role="tabpanel" aria-labelledby="user-4-tab">
                    @include('user.partial.user_4')
                </div>

                <div class="tab-pane fade @if ($active_tab == 5) show active @endif" id="user-5"
                    role="tabpanel" aria-labelledby="user-5-tab">
                    @include('user.partial.user_5')
                </div>

            </div>

        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.not-user').css('display','block');
            $('.search-input').removeClass('col-md-3');
            $('.search-input').addClass('col-md-2');


        $('.list-search').on( 'keyup', function () {
              if($('#user-1-tab').hasClass('active'))
                table1.search( this.value ).draw();
              else if($('#user-2-tab').hasClass('active'))
                 table2.search( this.value ).draw();
                 else if($('#user-3-tab').hasClass('active'))
                 table3.search( this.value ).draw();
                 else if($('#user-4-tab').hasClass('active'))
                 table4.search( this.value ).draw();
              else table5.search( this.value ).draw();
          } );

          $('.list-search').on( 'search', function () {
              if($('#user-1-tab').hasClass('active'))
                table1.search( this.value ).draw();
              else if($('#user-2-tab').hasClass('active'))
                 table2.search( this.value ).draw();
                 else if($('#user-3-tab').hasClass('active'))
                 table3.search( this.value ).draw();
                 else if($('#user-4-tab').hasClass('active'))
                 table4.search( this.value ).draw();
              else table5.search( this.value ).draw();
          } );

          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                if(e.delegateTarget.id=="user-4-tab")
                {
                  $('.for-user').css('display','');
                  $('.not-user').css('display','none');
                }
                else {
                   $('.for-user').css('display','none');
                   $('.not-user').css('display','block');
                     }
                if($('.list-search').val().length > 0){
                    $('.list-search').val(null);
                    if(e.relatedTarget.id=="user-1-tab"){ // previous active tab
                        table1.search( '' ).draw();
                    }else if(e.relatedTarget.id=="user-2-tab"){ // previous active tab
                        table2.search( '' ).draw();
                    }else if(e.relatedTarget.id=="user-3-tab"){ // previous active tab
                        table3.search( '' ).draw();
                    }else if(e.relatedTarget.id=="user-4-tab"){ // previous active tab
                        table4.search( '' ).draw();

                    }else{
                        table5.search( '' ).draw();
                    }
                }

                $('#region_id').change(function(){
                let id = 0;
                if($(this).val().length > 0) id = $(this).val();
                $.ajax({
                    url : '/region/'+id+'/township',
                    method : 'GET',
                    dataType : 'json',
                    success : function(data){
                        $('#township_id').empty();
                        $('#township_id').append($("<option>").attr('value',null).text(''));
                        data.townships.map((e)=>{
                            $('#township_id').append($("<option>").attr('value',e.id).text(e.name));
                        });
                    },
                    error : function(e){
                        console.log(e);
                    }

                })
            });
                let user_status = '';
                let township = '';
                let region = '';
                if($('.user_status').val().length > 0 || $('.township').val().length > 0 || $('.region').val().length > 0){
                    $('.user_status').val(null).trigger('change.select2');
                    $('.township').val(null).trigger('change.select2');
                    $('.region').val(null).trigger('change.select2');
                    if(e.relatedTarget.id=="user-1-tab"){ // previous active tab
                        table1.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=1"+"&township="+ township+"&region="+region).load();
                    }else if(e.relatedTarget.id=="user-2-tab"){ // previous active tab
                        table2.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=2"+"&township="+ township+"&region="+region).load();
                    }else if(e.relatedTarget.id=="user-3-tab"){ // previous active tab
                        table3.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=3"+"&township="+ township+"&region="+region).load();
                    }else if(e.relatedTarget.id=="user-4-tab"){ // previous active tab
                        table4.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=4"+"&township="+ township+"&region="+region).load();
                    }else{
                        table5.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=5"+"&township="+ township+"&region="+region).load();
                    }
                }
          });


          $('.type-filter-select2').select2({
                placeholder: "Filter By Status",
                allowClear: true
            });
            $('.township').select2({
                placeholder: "Filter By Township",
                allowClear: true
            });
            $('.region').select2({
                placeholder: "Filter By Region",
                allowClear: true
            });

            $(document).on('change', '.type-filter-select2', function() {

                var user_status = $('.user_status').val();
                var township = $('.township').val();
                var region = $('.region').val();
                if ($('#user-1-tab').hasClass('active'))
                    table1.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=1"+"&township="+ township+"&region="+region).load();
                else if ($('#user-2-tab').hasClass('active'))
                    table2.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=2"+"&township="+ township+"&region="+region).load();
                else if ($('#user-3-tab').hasClass('active'))
                    table3.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=3"+"&township="+ township+"&region="+region).load();
                else if ($('#user-4-tab').hasClass('active'))
                    table4.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=4"+"&township="+ township+"&region="+region).load();
                else table5.ajax.url("{{ url('user?user_status=') }}" + user_status + "&role_id=5"+"&township="+ township+"&region="+region).load();
            });

            $('.export').on('click',function(){
                var user_status = $('.user_status').val();
                var township = $('.township').val();
                var region = $('.region').val();
                window.location = "export?status="+user_status+"&region_id="+region+"&township_id="+township;
        })

     });
   </script>



@stop
