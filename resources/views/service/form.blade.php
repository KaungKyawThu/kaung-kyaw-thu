<input type="hidden" name="current_index" value="{{$clinic->current_index}}">
{{-- <input type="hidden" name="status" value=""> --}}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Service English Name') }} <span class="text-red">*</span></label>
            <input id="service_name" type="text" class="form-control @error('service_name') is-invalid @enderror" name="service_name" value="{{ old('service_name', $clinic->service_name) }}" autocomplete="name" autofocus placeholder="Name">

            @error('service_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Service Myanmar Name') }} <span class="text-red">*</span></label>
            <input id="service_mm_name" type="text" class="form-control @error('service_mm_name') is-invalid @enderror" name="service_mm_name" value="{{ old('service_mm_name', $clinic->service_mm_name) }}" autocomplete="name" autofocus placeholder="Name">

            @error('service_mm_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Service English Description') }} <span class="text-red">*</span></label>

            <textarea id="service_description" name="service_description" rows="4" cols="50" class="form-control @error('service_description') is-invalid @enderror"
            name="service_description"  autocomplete="service_description"
            autofocus placeholder="Enter Service Description Here">{{ old('service_description',$clinic->service_description) }}</textarea>
            @error('service_description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Service Myanmar Description') }} <span class="text-red">*</span></label>

            <textarea id="service_mm_description" name="service_mm_description" rows="4" cols="50" class="form-control @error('service_mm_description') is-invalid @enderror"
            name="service_mm_description"  autocomplete="service_mm_description"
            autofocus placeholder="Enter Service Myanmar Description Here">{{ old('service_mm_description',$clinic->service_mm_description) }}</textarea>
            @error('service_mm_description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

@include('save-btn')
<script>
     $(document).on('click','#select_module',function(){

            if($(this).is(':checked')){

                $(this).parent().parent().find('input[type=checkbox]').val(1);

            }
            else{
                $(this).parent().parent().find('input[type=checkbox]').val(0);

            }
        });
</script>
