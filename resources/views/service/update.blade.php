@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('clinic/'.$clinic->id) }}">
        @csrf
        @method('PUT')
        @include('service.form')
    </form>
</div>
@stop
