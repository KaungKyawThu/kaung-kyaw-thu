@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
    <style>
        .cln-filter {
            margin-right: 10px;
        }
    </style>
    <div class="row">

        @include('search')

        <div class="col-md-2">
            <div class="fliter-input">
                <select name="" class="form-control select2 type-filter-select2 clinic_service_id"
                    id="clinic_service_id">
                    <option value="">Select Service</option>
                    @foreach (DB::table('clinic_services')->where('status', '=', 2)->get() as $s)
                        <option value="{{ $s->id }}">{{ $s->service_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="fliter-input">
                <select name="" class="form-control select2 type-filter-select2 organization_id"
                    id="organization_id">
                    <option value="">Select Organization</option>
                    @foreach (DB::table('clinic_organizations')->where('status', '=', 2)->get() as $s)
                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="fliter-input">
                <select name="" class="form-control select2 type-filter-select2 region_id" id="region_id">
                    <option value="">Select Region</option>
                    @foreach (DB::table('regions')->get() as $s)
                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="fliter-input">
                <select name="" class="form-control select2 type-filter-select2 township_id" id="township_id">
                    <option value="">Select Township</option>
                    @foreach (DB::table('townships')->get() as $s)
                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <select class="form-control select2 type-filter-select2 clinic_status" name="quiz_type" style="width: 100%;">
                <option value=""></option>
                @foreach (config('web_constant.status') as $k => $t)
                    <option value="{{ $k }}">{{ $t }}</option>
                @endforeach
            </select>
        </div>


        {{-- @include('new-btn') --}}
    </div><br>
    <div class="row">
        <div class="create-new col-md-12 text-right">
            @if(auth()->user()->can("$create_permission"))
              <a href="{{url($route.'/create')}}" class="btn btn-primary create-new-one"><i class="fa fa-plus" ></i> Create New {{ucwords($keyword)}}</a>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Clinic Name</th>
                        {{-- <th>Township</th> --}}
                        <th>Clinic Service Name</th>
                        <th>Clinic Organization Name</th>
                        <th>Township</th>
                        <th>Created By</th>
                        <th class="status-col">Status</th>
                        @if (auth()->user()->can('clinic-info-edit') ||
                            auth()->user()->can('clinic-info-delete') ||
                            auth()->user()->can('clinic-info-publish') ||
                            auth()->user()->can('clinic-info-active'))
                            <th class="action-col"><span></span></th>
                        @else
                            <th class="hide-col"><span></span></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.search-input').removeClass('col-md-3');
            $('.search-input').addClass('col-md-2');
            // $('.create-new').removeClass('col-md-3');
            // $('.create-new').addClass('col-md-2');
            var table = $('.data-table').DataTable({
                dom: 'lrtip',
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: false,
                info: false,
                autoWidth: true,
                responsive: true,
                processing: true,
                serverSide: true,
                displayStart : "{{ $current_index }}",
                // //stateSave: true,
                ajax: "{{ url('clinic-info') }}",
                createdRow: function(row, data, dataIndex) {
                    if (data.action.length == 0)
                        $(row).find('td.action-col').css('display', 'none');
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        class: 'data-index'
                    },
                    {
                        data: 'clinic_name',
                        name: 'clinic_name'
                    },
                    {
                        data: 'service_name',
                        name: 'service_name'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'tsp_name',
                        name: 'tsp_name'
                    },
                    {
                        data: 'created_user_name',
                        name: 'created_user_name'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'action-col'
                    },
                ]
            });


            $('.clinic_service_id').select2({
                placeholder: "Filter By Service",
                allowClear: true
            });
            $('.organization_id').select2({
                placeholder: "Filter By Organization",
                allowClear: true
            });
            $('.region_id').select2({
                placeholder: "Filter By Region",
                allowClear: true
            });
            $('.township_id').select2({
                placeholder: "Filter By Township",
                allowClear: true
            });
            $('.clinic_status').select2({
                placeholder: "Filter By Status",
                allowClear: true
            });

            $('.list-search').on('keyup', function() {
                table.search(this.value).draw();
            });

            $('.list-search').on('search', function() {
                table.search(this.value).draw();
            });

            $(document).on('change', '.clinic_service_id, .organization_id,.region_id, .township_id, .clinic_status', function() {
                searchFun();
            });

            $('#region_id').change(function(){
            let id = 0;
            if($(this).val().length > 0) id = $(this).val();
            $.ajax({
                url : '/region/'+id+'/township',
                method : 'GET',
                dataType : 'json',
                success : function(data){
                    $('#township_id').empty();
                    $('#township_id').append($("<option>").attr('value',null).text(''));
                    data.townships.map((e)=>{
                        $('#township_id').append($("<option>").attr('value',e.id).text(e.name));
                    });
                },
                error : function(e){
                    console.log(e);
                }

            })
        });

            function searchFun() {
                var service_id = $('.clinic_service_id').val();
                var client_id = $('.organization_id').val();
                var region_id = $('.region_id').val();
                var tsp_id = $('.township_id').val();
                var clinic_status = $('.clinic_status').val();

                table.ajax.url("{{ url('clinic-info') }}" + "?clinic_service_id=" + service_id +
                    '&clinic_organization_id=' + client_id + '&region_id=' + region_id + '&township_id=' + tsp_id+ "&clinic_status=" + clinic_status).load();
            }

            // $('.role-filter-select2').select2({ placeholder : "Filter By Services", allowClear : true, theme: 'bootstrap4'});

            // $(document).on('change','.role-filter-select2',function(){
            //     var id = $(this).val();
            //     table.ajax.url("{{ url('clinic-info') }}"+"?id="+id).load();
            // });

            // $('.project_client_id, .project_service_id').change(function(){
            //         searchFun();
            // });

            // function searchFun(){
            //     var service_id = $('.project_service_id').val();
            //     var client_id = $('.project_client_id').val();
            //
            // }
        });
    </script>
@stop
