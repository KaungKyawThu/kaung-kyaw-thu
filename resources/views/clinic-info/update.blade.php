@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('clinic-info/'.$clinic_info->id) }}">
        @csrf
        @method('PUT')
        @include('clinic-info.form')
    </form>
</div>
@stop
