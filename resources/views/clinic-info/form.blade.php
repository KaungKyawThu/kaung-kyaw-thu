<input type="hidden" name="current_index" value="{{ $clinic_info->current_index }}">
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Clinic English Name') }} <span class="text-red">*</span></label>
            <input id="clinic_name" type="text" class="form-control @error('clinic_name') is-invalid @enderror"
                name="clinic_name" value="{{ old('clinic_name', $clinic_info->clinic_name) }}" autocomplete="clinic_name"
                autofocus placeholder="Clinic English Name">

            @error('clinic_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Clinic Myanmar Name') }} <span class="text-red">*</span></label>
            <input id="clinic_mm_name" type="text" class="form-control @error('clinic_mm_name') is-invalid @enderror"
                name="clinic_mm_name" value="{{ old('clinic_mm_name', $clinic_info->clinic_mm_name) }}" autocomplete="clinic_mm_name"
                autofocus placeholder="Clinic Myanmar Name">

            @error('clinic_mm_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Clinic Service Name') }} <span class="text-red">*</span></label><br>
            {{-- @if (in_array($item->id, 1))  @endif> --}}
            <select id="service_name" class="form-control" @if(!$is_update) name="service_name[]" multiple @else name="service_name" @endif>
                <option></option>
                @php
                    if($is_update){
                       $old_serv = old('service_name') ?? $clinic_info->clinic_service_id ;
                    }else  $services = old('service_name') ?? [];
                   
                @endphp
                @foreach ($clinic_service as $item)
                   <option value="{{ $item->id }}" @if ($is_update && $old_serv == $item->id) selected @elseif(!$is_update && in_array($item->id,$services)) selected @endif>
                    {{ $item->service_name }} ( {{ $item->service_mm_name}} )</option>
                @endforeach
            </select>
            @error('service_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>


    </div><br>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Organization Name') }} <span class="text-red">*</span></label><br>
            {{-- @if (in_array($item->id, 1))  @endif> --}}
            <select name="clinic_organization_id" id="clinic_organization_id" class="form-control" >
                <option></option>
                @foreach ($clinic_organization as $item)
                <option value="{{ $item->id }}" @if (($item->id == $clinic_info->clinic_organization_id || old('clinic_organization_id') == $item->id)) selected @endif>
                    {{ $item->name }} ( {{$item->organization_mm_name}} )</option>
            @endforeach
            </select>
            @error('clinic_orgnization_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Phone Number (English)') }} </label>
            <input id="clinic_phone_no" type="text" class="form-control @error('clinic_phone_no') is-invalid @enderror"
                name="clinic_phone_no" value="{{ old('clinic_phone_no', $clinic_info->clinic_phone_no) }}" autocomplete="clinic_phone_no"
                autofocus placeholder="Clinic Phone Number">

            @error('clinic_phone_no')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Phone Number (Myanmar)') }} </label>
            <input id="clinic_mm_phone_no" type="text" class="form-control @error('clinic_mm_phone_no') is-invalid @enderror"
                name="clinic_mm_phone_no" value="{{ old('clinic_mm_phone_no', $clinic_info->clinic_mm_phone_no) }}" autocomplete="clinic_mm_phone_no"
                autofocus placeholder="Clinic Myanmar Phone Number">

            @error('clinic_mm_phone_no')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Region Name') }}</label>
            <select name="region_id" class="form-control" id="region_id">
                <option value="">Select Region</option>
                @foreach (\App\Region::all() as $r)
                    <option value="{{$r->id}}" @if(($clinic_info->region_id == $r->id) || (old('region_id')==$r->id) ) selected @endif >{{$r->name}} ( {{$r->mm_unicode}} )</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Township Name') }} <span class="text-red">*</span></label><br>
            {{-- @if (in_array($item->id, 1))  @endif> --}}

            @php
                if($is_update){
                  $old_tsp = old('township_id') ?? $clinic_info->township_id ;
                }else  $townships = old('township_id') ?? [];
            
            @endphp
       
            <select id="township_id" class="form-control" @if(!$is_update) name="township_id[]" multiple @else name="township_id" @endif>
                <option></option>
                @foreach ($township as $item )
                <option value="{{ $item->id }}" @if ($is_update && $old_tsp == $item->id) selected @elseif(!$is_update && in_array($item->id,$townships)) selected @endif>
                    {{ $item->name }} ( {{$item->mm_unicode}} )</option>
            @endforeach
            </select>
            @error('township_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Clinic Facebook Link') }} </label>
            <input id="clinic_fb_link" type="text" class="form-control @error('clinic_fb_link') is-invalid @enderror"
                name="clinic_fb_link" value="{{ old('clinic_fb_link', $clinic_info->clinic_fb_link) }}" autocomplete="clinic_fb_link"
                autofocus placeholder="Clinic Facebook Link">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Facebook Page Id') }} </label>
            <input id="facebook_page_id" type="text" class="form-control @error('facebook_page_id') is-invalid @enderror"
                name="facebook_page_id" value="{{ old('facebook_page_id', $clinic_info->facebook_page_id ) }}" autocomplete="facebook_page_id"
                autofocus placeholder="Facebook Page Id">
        </div>
        @error('facebook_page_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Clinic Website Link') }}</label>
            <input id="clinic_website" type="text" class="form-control @error('clinic_website') is-invalid @enderror"
                name="clinic_website" value="{{ old('clinic_website', $clinic_info->clinic_website) }}" autocomplete="clinic_website"
                autofocus placeholder="Clinic Website Link">
        </div>
    </div>

    <div class="col-md-6"></div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('English Address') }} </label>
            <textarea id="clinic_address" name="clinic_address" rows="4" cols="50" class="form-control @error('clinic_address') is-invalid @enderror"
            name="clinic_address"  autocomplete="clinic_address"
            autofocus placeholder="Enter Clinic Address Here">{{ old('clinic_address',$clinic_info->clinic_address) }}</textarea>
            @error('clinic_address')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Myanmar Address') }} </label>
            <textarea id="clinic_mm_address" name="clinic_mm_address" rows="4" cols="50" class="form-control @error('clinic_mm_address') is-invalid @enderror"
            name="clinic_mm_address"  autocomplete="clinic_mm_address"
            autofocus placeholder="Enter Clinic Address Here">{{ old('clinic_mm_address',$clinic_info->clinic_mm_address) }}</textarea>
            @error('clinic_mm_address')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('English Description') }} <span class="text-red">*</span></label>
            <textarea id="clinic_description_en" name="clinic_description_en" rows="4" cols="50" class="form-control @error('clinic_description_en') is-invalid @enderror"
            name="clinic_description_en"  autocomplete="clinic_description_en"
            autofocus placeholder="Enter Clinic English Description Here">{{ old('clinic_description_en',$clinic_info->clinic_description_en) }}</textarea>
            @error('clinic_description_en')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Myanmar Description') }} <span class="text-red">*</span></label>
            <textarea id="clinic_description_mm" name="clinic_description_mm" rows="4" cols="50" class="form-control @error('clinic_description_mm') is-invalid @enderror"
            name="clinic_description_mm"  autocomplete="clinic_description_mm"
            autofocus placeholder="Enter Clinic Myanmar Description Here">{{ old('clinic_description_mm',$clinic_info->clinic_description_mm ) }}</textarea>
            @error('clinic_description_mm')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

@include('save-btn')
<script>
    $(document).ready(function() {

        $selectElement = $('#service_name').select2({
            placeholder: "Please select service",
            allowClear: true
        });

        $selectElement1 = $('#clinic_organization_id').select2({
            placeholder: "Please select organization",
            allowClear: true
        });
        $selectElement2 = $('#township_id').select2({
            placeholder: "Please select township",
            allowClear: true
        });

        $('#region_id').select2({
            placeholder: "Please select region",
            allowClear: true
        });

        $('#region_id').change(function(){
            let id = 0;
            if($(this).val().length > 0) id = $(this).val(); ;
            $.ajax({
                url : '/region/'+id+'/township',
                method : 'GET',
                dataType : 'json',
                success : function(data){
                    $('#township_id').empty();
                    $('#township_id').append($("<option>").attr('value',null).text(''));
                    data.townships.map((e)=>{
                        $('#township_id').append($("<option>").attr('value',e.id).text(e.name+" ( "+e.mm_unicode+" )"));
                    });
                },
                error : function(e){
                    console.log(e);
                }

            })
        });


    });

</script>
