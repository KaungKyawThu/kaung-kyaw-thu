@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
     @php
         $created_user_name = $clinic_info->created_user_name;
         $updated_user_name = $clinic_info->updated_user_name;
         $published_user_name = $clinic_info->published_user_name;
         $deactivated_user_name = $clinic_info->deactivated_user_name;

         $created_at = date('d-M-Y H:i:s',strtotime($clinic_info->created_at));
         $updated_at = $clinic_info->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($clinic_info->updated_at));
         $published_at = $clinic_info->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($clinic_info->published_at));
         $deactivated_at = $clinic_info->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($clinic_info->deactivated_at));

     @endphp

     <div class="row">
        <div class="col-md-3">
            <label for="">Clinic Name</label><br>
            <span>{{ $clinic_info->clinic_name }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Clinic Myanmar Name</label><br>
            <span>{{ $clinic_info->clinic_mm_name }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Service Name</label><br>
            <span>{{ $clinic_info->service_name }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Service Myanmar Name</label><br>
            <span>{{ $clinic_info->service_mm_name }}</span>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-3">
            <label for="">Clinic Address English</label><br>
            <span>{{ $clinic_info->clinic_address }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Clinic Address Myanmar</label><br>
            <span>{{ $clinic_info->clinic_mm_address }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Clinic Phone Number</label><br>
            <span>{{ $clinic_info->clinic_phone_no }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Clinic Phone Number Myanmar</label><br>
            <span>{{ $clinic_info->clinic_mm_phone_no }}</span>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-3">
            <label for="">Clinic Organization Name</label><br>
            <span>{{ $clinic_info->organization_name }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Clinic Organization Myanmar Name</label><br>
            <span>{{ $clinic_info->organization_mm_name }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Township Name</label><br>
            <span>{{ $clinic_info->name }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Township Myanmar Name</label><br>
            <span>{{ $clinic_info->mm_unicode }}</span>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-3">
            <label for="">Website Link</label><br>
            <span>{{ $clinic_info->clinic_website }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Facebook Link</label><br>
            <span>{{ $clinic_info->clinic_fb_link }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Facebook Page Id</label><br>
            <span>{{ $clinic_info->facebook_page_id }}</span>
        </div>
        <div class="col-md-3">
            <label for="">Status</label><br>
            <span class="status_{{config("web_constant.status.$clinic_info->status")}}" style="display: inline;">{{config("web_constant.status.$clinic_info->status")}}</span>
        </div>
    </div>
<br>
    <div class="row">
        <div class="col-md-6">
            <label for="">Clinic Myanmar Description</label><br>
            <span>{{ $clinic_info->clinic_description_mm }}</span>
        </div>
        <div class="col-md-6">
            <label for="">Clinic English Description</label><br>
            <span>{{ $clinic_info->clinic_description_en }}</span>
        </div>
    </div>
    <br><br>

     @include('view-detail')
</div>
@stop
