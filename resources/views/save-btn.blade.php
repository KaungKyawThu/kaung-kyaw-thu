<div class="row">
    <div class="mr-3 ml-2">
        <button type="submit" class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </button>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div><br>