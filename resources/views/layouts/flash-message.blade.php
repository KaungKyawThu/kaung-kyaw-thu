@if ($message = Session::has('success'))
<div class="alert alert-success alert-block" id="alertMessage" style="display: none">
    <strong>{{Session::get('success') }}</strong>
</div>

@elseif ($message = Session::has('error'))
<div class="alert alert-danger alert-block" id="alertMessage" style="display: none">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ Session::get('error') }}</strong>
</div>

@endif

<script>
    $(document).ready(function(){
        setTimeout(() => {
            $('.alert').css('display','block');

            $("#alertMessage").fadeTo(2000, 500).slideUp(0, function(){
                <?php 
                    session()->forget('success');
                    session()->forget('error');
                ?>
            });
        }, 1000);
    })
</script>