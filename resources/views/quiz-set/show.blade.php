@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<style>
    a.q-detail{
        color: black!important;
    }
</style>
<div class="card-body">
     @php
         $created_user_name = $quiz->created_user_name;
         $updated_user_name = $quiz->updated_user_name;
         $published_user_name = $quiz->published_user_name;
         $deactivated_user_name = $quiz->deactivated_user_name;
         
         $created_at = date('d-M-Y H:i:s',strtotime($quiz->created_at));
         $updated_at = $quiz->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->updated_at));
         $published_at = $quiz->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->published_at));
         $deactivated_at = $quiz->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($quiz->deactivated_at));

     @endphp

    <div class="row">
        <div class="col-md-4">
            <label for="">Course Level : </label>
            @php
                $l = $quiz->course->course_level;
            @endphp
            {{config("web_constant.courses.$l")}}
        </div>
        <div class="col-md-4">
            <label for="">Set Name : </label>
            {{$quiz->set_name}}
        </div>
        <div class="col-md-4">
            <label for="">Status : </label>
            {{config("web_constant.status.$quiz->status")}}
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <h5>Quizzes</h5>
            <table class="table data-table table-bordered">
                <thead>
                    <tr>
                        <th style="width : 50px;">No.</th>
                        <th>Quiz Question</th>
                        <th style="width : 200px;">Quiz Type</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($quiz->details as $k => $d)
                        <tr>
                            <td>{{$k+1}}</td>
                            <td><a target="_blank" href="{{url('quiz/'.$d->quiz_id)}}">{{$d->quiz_question}}</a></td>
                            <td>{{config("web_constant.quiz_types.$d->quiz_type")}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
       
    </div><br>
     @include('view-detail')
</div>
@stop
