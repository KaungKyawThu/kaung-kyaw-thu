<table class="table table-bordered data-table type-4-table" style="width : 100%;">
    <thead>
      <tr>
        <th width="50">No</th>
        <th>Chapter Name</th>
        <th>Quiz Types</th>
        <th>Created By</th>
        <th>Published By</th>
        <th>Published At</th>
        <th class="status-col">Status</th>
        @if(auth()->user()->can('quiz-set-edit') || auth()->user()->can('quiz-set-view') || auth()->user()->can('quiz-set-publish') || auth()->user()->can('quiz-set-reply') || auth()->user()->can('quiz-set-active') || auth()->user()->can('quiz-set-delete'))
          <th class="action-col"><span></span></th>
        @else
          <th class="hide-col" ><span></span></th>
        @endif
      </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<script>
     var table4 = $('.type-4-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    ajax: "{{ url('quiz-set?level=4') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'set_name', name: 'set_name'},
                        {data: 'quiz_types', name: 'quiz_types'},
                        {data: 'created_user_name', name: 'created_user_name'},
                        {data: 'published_user_name', name: 'published_user_name'},
                        {data: 'published_at', name: 'published_at'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });
</script>
