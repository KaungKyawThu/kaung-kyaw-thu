@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" id="quiz-set-form" enctype="multipart/form-data">
        @csrf
        @include('quiz-set.form')
    </form>
</div>
@stop
