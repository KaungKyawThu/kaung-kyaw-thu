<style>
    a.q-detail{
        color: black!important;
    }
</style>
<input type="hidden" name="current_index" value="{{$quiz->current_index ?? 1}}">
<input type="hidden" name="quizset_id" value="{{$quiz->id}}">

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Course') }} <span class="text-red">*</span></label>
            <select name="course_id" class="form-control select-2">
                <option value="">Select Course</option>
                @foreach (\App\Course::where('status',2)->get() as $c)
                    <option value="{{$c->id}}" @if($c->id==$quiz->course_id) selected @endif>{{config("web_constant.courses.$c->course_level")}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Quiz Set Name') }} <span class="text-red">*</span></label>
            <input type="text" class="form-control select-2" name="set_name" value="{{ $quiz->set_name }}" placeholder="Quiz Set Name">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="quiz-topic">
<div class="row">
    <div class="col-md-6">
        <label for="">Quiz Type </label>
        <div class="row">
        <select name="quiz_type" id="quiz_type" class="form-control select-2">
            <option value="">Select Quiz Type</option>
                @foreach (config("web_constant.quiz_types") as $type => $type_name)
                    <option value="{{ $type }}" name="quiz_type"   @if( $quiz->quiz_type==$type || ($quiz->quiz_type==null && $loop->first)) checked @endif>{{ $type_name }}</option>
                @endforeach
            </select>
        </div><br>
    </div>
    
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Quiz') }} <span class="text-red">*</span></label>
            <select name="select_quiz_id" class="form-control">
                <option value="">Select Quiz</option>
                @if ( $quiz->course_id != "" ){
                    @foreach (\App\Quiz::where('quiz_content_type',2)->where('course_id',$quiz->course_id)->where('status',2)->get() as $c)
                    @php
                        $c->quiz_type = config("web_constant.quiz_types.$c->quiz_type");
                    @endphp
                    <option value="{{$c->id}}" @if($c->id==$quiz->quiz_id) selected @endif data-city="{{$c->quiz_type}}">{{$c->quiz_question}}</option>
                    @endforeach
                    }
                @endif

            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row " >
   <div class="col-md-12 ">
        <h5>Quizzes </h5>
        <table class="table data-table table-bordered quizzes-table">
            <thead>
                <th style="width: 50px;">No.</th>
                <th>Quiz Question</th>
                <th style="width:200px;">Quiz Type</th>
                <th style="width: 50px;" class="action-col"><span></span></th>
            </thead>
            <tbody>
                @if ($quiz->details->count() > 0)
                    @foreach ($quiz->details as $k => $detail)
                        <tr class="tr" key="{{$detail->quiz_id}}">
                            <td><span class="quiz-index">{{$k+1}}</span></td>
                            <td><a href="{{url('quiz/'.$detail->quiz_id)}}" class="btn btn-link" target="_blank"> {{$detail->quiz_question}}</a></td>
                            <td>{{config("web_constant.quiz_types.$detail->quiz_type")}}</td>
                            <td>
                                <span class="btn remove-topic">
                                    <i class="fa fa-times"></i>
                                </span>
                                <input type="hidden" name="quiz_id[]" value="{{$detail->quiz_id}}">
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div><br>
<div class="row">
    <div class="col-md-6">
        <strong class="text-red quiz-error"></strong>
    </div>
</div><br>
</div>
<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div><br>

<script>
$(document).ready(function(){

    $('select[name="select_quiz_id"]').select2({
        placeholder : 'Select Quiz',
        allowClear : true
    });

    $('select[name="quiz_type"]').select2({
        placeholder : 'Select Quiz Type',
        allowClear : true
    });

    $('select[name="course_id"]').select2({
        placeholder : 'Select Course',
        allowClear : true
    });

    $(document).on('change', '.select-2', function() {
    // $('select[name=quiz_type]','select[name="course_id"]').change(function(){
        var quiz_type = $('select[name=quiz_type] option:selected').val();
        var course = $('select[name=course_id] option:selected').val();

                if(quiz_type.length>0 && course.length>0)
               {
                  jQuery.ajax({
                     url : '/course/'+ course +'/quiz_type/'+ quiz_type +'/quiz',
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        jQuery('select[name="select_quiz_id"]').empty();
                        $('select[name="select_quiz_id"]').append('<option value=""></option>');
                        jQuery.each(data, function(key,value){
                            var ans = value.quiz_type;
                            if (ans == 1){ var type ="True/False" }
                            else if( ans == 2 ){ var type = "Single Choice" }
                            else if( ans == 3 ){ var type = "Multiple Choice" }
                            else if( ans == 4) { var type = "Fill Type"}
                            else ans ;
                           $('select[name="select_quiz_id"]').append('<option value="'+ value.id +'" data-city="'+type+'">'+ value.quiz_question +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="select_quiz_id"]').empty();
               }
    });
    $('select[name=course_id]').change(function(){
        $(".tr").remove();
     });

        $('select[name=select_quiz_id]').change(function(){
        // let data = decryptData($('select[name=select_quiz_id] option:selected').attr('data'));
        let name = $('select[name=select_quiz_id] option:selected').html();
        let data = $('select[name=select_quiz_id] option:selected').val();
        let types = $('select[name=select_quiz_id] option:selected').data('city');

        $('select[name=select_quiz_id]').val(null).trigger('change.select2');

        if($('table.quizzes-table > tbody > tr[key='+data+']').length > 0) return false;

        let k = $('table.quizzes-table > tbody > tr').length + 1;
        let tr = "<tr class='tr' key="+ data +">"+
                    "<td><span class='quiz-index'>"+k+"</span></td>"+
                    "<td><a href='/quiz/"+data+"' class='btn btn-link' target='_blank'>"+name+"</a></td>"+
                    "<td>"+ types +"</td>"+
                    "<td><span class='btn remove-topic'><i class='fa fa-times'></i></span><input type='hidden' class='quiz_id' name='quiz_id[]' value='"+data+"'></td>"+
                 "</tr>";
        $('table.quizzes-table > tbody').append(tr);
        $('.quiz-error').html(null);
    });

    $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("quiz-set-form"));
        var quizset_id = $('form#quiz-set-form').find('input[name=quizset_id]').val();
        if(quizset_id.length > 0){
            var route = '/quiz-set/'+quizset_id;
            formData.append('_method', 'PUT');
        }else if(quizset_id.length == 0){
            var route = '/quiz-set';
        }

        if($('table.quizzes-table > tbody > tr').length != 10){
            $('.quiz-error').html('The total quizzes must be 10.');
            return false;
        }

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){
                window.location.href = '/quiz-set?index='+data.index+'&level='+data.level;
            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.set_name!=undefined){
                    $('form#quiz-set-form').find('input[name=set_name]').addClass('is-invalid');
                    $('form#quiz-set-form').find('input[name=set_name]').siblings().find('strong').html(e.responseJSON.set_name)
                }

                if(e.responseJSON.course_id!=undefined){
                    $('form#quiz-set-form').find('select[name=course_id]').addClass('is-invalid');
                    $('form#quiz-set-form').find('select[name=course_id]').siblings().find('strong').html(e.responseJSON.course_id)
                }

                return false;
            }
        })
    });

    $(document).on('click','.remove-video',function(){
       $(this).parent().closest('tr').remove();
       $('table.quizs-videos-table > tbody > tr').each(function(i,e){
            $(e).find('.tb-index').html(i+1);
       });
    });

    $(document).on('click','.remove-topic',function(){
       $(this).parent().closest('tr').remove();
       $('table.quizzes-table > tbody > tr').each(function(i,e){
            $(e).find('.quiz-index').html(i+1);
        });
    });

});
</script>
