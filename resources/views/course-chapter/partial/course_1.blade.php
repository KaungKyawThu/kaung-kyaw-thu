<table class="table table-bordered data-table type-1-table" style="width : 100%;">
    <thead>
        <tr>
          <th width="50">No</th>
          <th>Chapter Name</th>
          <th>Order</th>
          <th>Created By</th>
          <th>Published By</th>
          <th>Published At</th>
          <th class="status-col">Status</th>
          @if(auth()->user()->can('course-chapter-edit') || auth()->user()->can('course-chapter-view') || auth()->user()->can('course-chapter-publish') || auth()->user()->can('course-chapter-reply') || auth()->user()->can('course-chapter-active') || auth()->user()->can('course-chapter-delete'))
            <th class="action-col"><span></span></th>
          @else
            <th class="hide-col" ><span></span></th>
          @endif
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<script>
     var table1 = $('.type-1-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: false,
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    ajax: "{{ url('course-chapter?level=1') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'chapter_name', name: 'chapter_name'},
                        {data: 'order', name: 'order'},
                        {data: 'created_user_name', name: 'created_user_name'},
                        {data: 'published_user_name', name: 'published_user_name'},
                        {data: 'published_at', name: 'published_at'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });
</script>
