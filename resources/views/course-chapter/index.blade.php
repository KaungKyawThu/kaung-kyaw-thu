@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

@php
    $active_tab = isset($_GET['level']) ? $_GET['level'] : 1;
@endphp
<div class="row">
    @include('search')
    <div class="col-md-3">
        <select class="form-control select2 course-filter-select2 course_chapter_status" name="quiz_type" style="width: 100%;">
            <option value=""></option>
            @foreach (config("web_constant.status") as $k=>$t)
                <option value="{{$k}}">{{$t}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3"></div>
    @include('new-btn')
  </div><br>

  <div class="row">
    <div class="col-md-12">

      <ul class="nav nav-tabs sub-nav-tabs" id="myTab" role="tablist">
        @foreach (config("web_constant.courses") as $k => $type)
          <li class="nav-item" role="presentation">
            <a class="nav-link @if($active_tab==$k) active @endif" id="type-{{$k}}-tab" data-toggle="tab" href="#type-{{$k}}" role="tab" aria-controls="type-{{$k}}" aria-selected="true">{{$type}}</a>
          </li>
        @endforeach
      </ul>

    <div class="tab-content sub-tab-content" id="myTabContent">
        <div class="tab-pane fade @if($active_tab==1) show active @endif" id="type-1" role="tabpanel" aria-labelledby="type-1-tab">
            @include('course-chapter.partial.course_1')
        </div>

        <div class="tab-pane fade @if($active_tab==2) show active @endif" id="type-2" role="tabpanel" aria-labelledby="type-2-tab">
            @include('course-chapter.partial.course_2')
        </div>

        <div class="tab-pane fade @if($active_tab==3) show active @endif" id="type-3" role="tabpanel" aria-labelledby="type-3-tab">
            @include('course-chapter.partial.course_3')
        </div>

        <div class="tab-pane fade @if($active_tab==4) show active @endif" id="type-4" role="tabpanel" aria-labelledby="type-4-tab">
            @include('course-chapter.partial.course_4')
        </div>

    </div>
  </div>
</div>
<script>
     $(document).ready(function () {
          $('.list-search').on( 'keyup', function () {
              if($('#type-1-tab').hasClass('active'))
                table1.search( this.value ).draw();
              else if($('#type-2-tab').hasClass('active'))
                 table2.search( this.value ).draw();
              else if($('#type-3-tab').hasClass('active'))
                 table3.search( this.value ).draw();
              else if($('#type-4-tab').hasClass('active'))
                 table4.search( this.value ).draw();
          } );

          $('.list-search').on( 'search', function () {
              if($('#type-1-tab').hasClass('active'))
                table1.search( this.value ).draw();
              else if($('#type-2-tab').hasClass('active'))
                 table2.search( this.value ).draw();
              else if($('#type-3-tab').hasClass('active'))
                 table3.search( this.value ).draw();
              else if($('#type-4-tab').hasClass('active'))
                 table4.search( this.value ).draw();
          } );

          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

                if($('.list-search').val().length > 0){
                    $('.list-search').val(null);
                    if(e.relatedTarget.id=="type-1-tab"){ // previous active tab
                      table1.search( '' ).draw();
                    }else if(e.relatedTarget.id=="type-2-tab"){ // previous active tab
                      table2.search( '' ).draw();
                    }else if(e.relatedTarget.id=="type-3-tab"){ // previous active tab
                      table3.search( '' ).draw();
                    }else{
                      table4.search( '' ).draw();
                    }
                }

                let course_chapter_status = '';
                if($('.course-filter-select2').val().length > 0 ){
                    $('.course-filter-select2').val(null).trigger('change.select2');
                    $('.content_status').val(null).trigger('change.select2');
                    if(e.relatedTarget.id=="type-1-tab"){ // previous active tab
                       table1.ajax.url("{{ url('course-chapter?level=1')}}"+"&course_chapter_status="+course_chapter_status).load();
                    }else  if(e.relatedTarget.id=="type-2-tab"){ // previous active tab
                       table2.ajax.url("{{ url('course-chapter?level=2')}}"+"&course_chapter_status="+course_chapter_status).load();
                    }else  if(e.relatedTarget.id=="type-3-tab"){ // previous active tab
                       table3.ajax.url("{{ url('course-chapter?level=3')}}"+"&course_chapter_status="+course_chapter_status).load();
                    }else{
                      table4.ajax.url("{{ url('course-chapter?level=4')}}"+"&course_chapter_status="+course_chapter_status).load();
                }
            }
          });

          $('.course-filter-select2').select2({ placeholder : "Filter By Status", allowClear : true});

          $(document).on('change','.course-filter-select2',function(){
            searchFun();
          });

          function searchFun(){
            var course_chapter_status = $('.course_chapter_status').val();
            if ($('#type-1-tab').hasClass('active'))
            table1.ajax.url("{{ url('course-chapter?level=1')}}"+"&course_chapter_status="+course_chapter_status).load();
            else if ($('#type-2-tab').hasClass('active'))
            table2.ajax.url("{{ url('course-chapter?level=2')}}"+"&course_chapter_status="+course_chapter_status).load();
            else if ($('#type-3-tab').hasClass('active'))
            table3.ajax.url("{{ url('course-chapter?level=3')}}"+"&course_chapter_status="+course_chapter_status).load();
            else
            table4.ajax.url("{{ url('course-chapter?level=4')}}"+"&course_chapter_status="+course_chapter_status).load();
          }
     });

   </script>
@stop
