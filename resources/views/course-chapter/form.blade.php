<style>
    a.q-detail{
        color: black!important;
    }
</style>
<input type="hidden" name="current_index" value="{{$course->current_index ?? 1}}">
<input type="hidden" name="coursechapter_id" value="{{$course->id}}">

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Course') }} <span class="text-red">*</span></label>
            <select name="course_id" class="form-control">
                <option value="">Select Course</option>
                @foreach (\App\Course::where('status',2)->get() as $c)
                    <option value="{{$c->id}}" @if($c->id==$course->course_id) selected @endif>{{config("web_constant.courses.$c->course_level")}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Chapter English Name') }} <span class="text-red">*</span></label>
            <input type="text" class="form-control" name="chapter_name" value="{{ $course->chapter_name }}" placeholder="Chapter English Name">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Chapter Myanmar Name') }} <span class="text-red">*</span></label>
            <input type="text" class="form-control" name="chapter_name_mm" value="{{ $course->chapter_name_mm }}" placeholder="Chapter Myanmar Name">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>{{ __('Order') }} <span class="text-red">*</span></label>
            <input type="text" class="form-control" name="order" value="{{ $course->order }}" placeholder="Order">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <h5>Videos</h5>
    </div>
    <div class="col-md-6 text-right">
        <span class="photo_upload_description"> ***{{$upload_photo_description}} </span>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table data-table table-bordered courses-videos-table">
            <thead>
                <tr>
                    <th style="width: 50px;">No.</th>
                    <th>Video Title <span class="text-red">*</span></th>
                    <th>Video Link <span class="text-red">*</span></th>
                    <th style="width: 200px;">Duration<span class="text-red"> (Minutes)</span> <span class="text-red">*</span></th>
                    <th style="width: 200px;">Thambnail <span class="text-red">*</span></th>
                    <th style="width: 50px;" class="action-col"><span></span></th>
                </tr>
            </thead>
            <tbody>
                @if($course->videos->count() == 0)
                    <tr>
                        <td><span class="tb-index">1</span></td>
                        <td>
                            <input type="text" name="video_title[]" class="form-control" placeholder="Video Title" >
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </td>
                        <td>
                            <input type="text" name="course_video_link[]" class="form-control" placeholder="Video Link" >
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </td>
                        <td>
                            <input type="text" name="video_duration[]" class="form-control" id="" placeholder="Video Duration">
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </td>
                        <td>
                            <div class="row preview-row" style="display:none">
                                <div class="col-md-10">
                                    <img id="upload-preview" src="#" alt="upload photo" style="width:100%"/>
                                </div>
                                <div class="col-md-1">
                                    <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
                                </div>
                            </div>
                            <div class="row">
                                <input type="file" id="single-photo" class=" photo_file form-input form-control" name="video_thumbnail[]" placeholder="{{__('Upload Photo')}}">
                                <input type="hidden" class="" >
                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>

                                <input type="hidden" name="virtual_img[]" id="virtual-img" value="">
                            </div>
                        </td>

                        <td>
                            <span class="btn remove-video" style="display: none;"><i class="fa fa-times"></i></span>
                        </td>
                    </tr>
                @else
                    @foreach ($course->videos as $k => $v)
                        <tr>
                            <td><span class="tb-index">1</span></td>
                            <td>
                                <input type="text" name="video_title[]" class="form-control" placeholder="Video Title" value="{{$v->video_title}}">
                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                            </td>
                            <td>
                                <input type="text" name="course_video_link[]" class="form-control" placeholder="Video Link" value="{{$v->course_video_link}}">
                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                            </td>
                            <td>
                                <input type="text" name="video_duration[]" class="form-control" id="" placeholder="Video Duration" value="{{$v->video_duration}}">
                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                            </td>
                            <td>
                                <div class="row preview-row" @if(isset($is_update) && $v->video_thumbnail!=null) style="" @else style="display:none" @endif>
                                    <div class="col-md-10">
                                        <img id="upload-preview" src="{{$v->video_thumbnail}}" alt="upload photo" style="width:100%"/>
                                    </div>
                                    <div class="col-md-1">
                                        <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <input type="file" id="single-photo" class=" photo_file form-input form-control" name="video_thumbnail[]" placeholder="{{__('Upload Photo')}}" @if(isset($is_update) && $v->video_thumbnail!=null) style="display:none" @else style="" @endif>
                                    <input type="hidden" class="" >
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>

                                    <input type="hidden" name="virtual_img[]" id="virtual-img" value="{{$v->video_thumbnail}}">
                                </div>
                            </td>

                            <td>
                                <span class="btn remove-video" style="display: none;"><i class="fa fa-times"></i></span>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6" class="text-right"><span class="btn btn-info btn-sm add-detail"><i class="fa fa-plus"></i></span></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div><br>

<div class="course-topic">
<div class="row">
    <div class="col-md-6 ">
        <div class="form-group">
            {{-- <label>{{ __('Topics') }} <span class="text-red">*</span></label><br> --}}
            <label>{{ __('Topics') }} </label>
            <select name="select_topic_id" class="form-control">
                <option value="">Select Topic</option>
                @foreach (\App\Content::where('status',2)->where('content_type',1)->get() as $c)
                    <option value="{{$c->id}}" data="{{Crypt::encryptString($c)}}">{{$c->content_title}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row " >
   <div class="col-md-12 ">
        <h5>Topics </h5>
        <table class="table data-table table-bordered topics-table">
            <thead>
                <th style="width: 50px;">No.</th>
                <th>Topic Title</th>
                <th style="width: 50px;" class="action-col"><span></span></th>
            </thead>
            <tbody>
                @if ($course->topics->count() > 0)
                    @foreach ($course->topics as $k => $content)
                    <tr key="{{$content->content_id}}">
                        <td><span class="topic-index">{{$k+1}}</span></td>
                        <td><a href="{{url('content/'.$content->content_id)}}" class="btn btn-link" target="_blank"> {{$content->content_title}}</a></td>
                        <td>
                            <span class="btn remove-topic">
                                <i class="fa fa-times"></i>
                            </span>
                            <input type="hidden" name="content_id[]" value="{{$content->content_id}}">
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div><br>
<div class="row">
    <div class="col-md-6">
        <strong class="text-red topic-err"></strong>
    </div>
</div><br>
</div>
<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div><br>

<script>
$(document).ready(function(){
    $('select[name=course_id]').select2({
        placeholder : 'Select Course',
        allowClear : true
    });

    $('select[name="select_topic_id"]').select2({
        placeholder : 'Select Topic',
        allowClear : true
    });

    $('select[name="select_video_id"]').select2({
        placeholder : 'Select Video',
        allowClear : true
    });

    $('select[name=content_type]').select2({
        placeholder : 'Select Content',
        allowClear : true
    });

    $('select[name=video_id]').select2({
        placeholder : 'Select Video',
        allowClear : true
    });

    $('select[name=author_id]').select2({
        placeholder : 'Select Author',
        allowClear : true
    });

    $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("chapter-form"));
        var coursechapter_id = $('form#chapter-form').find('input[name=coursechapter_id]').val();
        if(coursechapter_id.length > 0){
            var route = '/course-chapter/'+coursechapter_id;
            formData.append('_method', 'PUT');
        }else if(coursechapter_id.length == 0){
            var route = '/course-chapter';
        }

        // if($('table.topics-table > tbody > tr').length==0){
        //     $('.topic-err').html('The topic must be at least one.');
        //     return false;
        // }

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function(){
                clearErrorMsg();
            },
            success : function(data){
                window.location.href = '/course-chapter?index='+data.index+'&level='+data.level;
            },
            complete : function (data) {

            },
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.course_id!=undefined){
                    $('form#chapter-form').find('select[name="course_id"]').addClass('is-invalid');
                    $('form#chapter-form').find('select[name="course_id"]').siblings().find('strong').html(e.responseJSON.course_id)
                }

                if(e.responseJSON.chapter_name!=undefined){
                    $('form#chapter-form').find('input[name=chapter_name]').addClass('is-invalid');
                    $('form#chapter-form').find('input[name=chapter_name]').siblings().find('strong').html(e.responseJSON.chapter_name)
                }

                if(e.responseJSON.chapter_name_mm!=undefined){
                    $('form#chapter-form').find('input[name=chapter_name_mm]').addClass('is-invalid');
                    $('form#chapter-form').find('input[name=chapter_name_mm]').siblings().find('strong').html(e.responseJSON.chapter_name_mm)
                }

                if(e.responseJSON.order!=undefined){
                    $('form#chapter-form').find('input[name=order]').addClass('is-invalid');
                    $('form#chapter-form').find('input[name=order]').siblings().find('strong').html(e.responseJSON.order)
                }


                for(let i=0; i < $('table.courses-videos-table > tbody > tr').length; i++ ){
                    if(e.responseJSON['video_thumbnail.'+i]!=undefined){
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="video_thumbnail[]"]').addClass('is-invalid');
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="video_thumbnail[]"]').siblings().find('strong').html(e.responseJSON['video_thumbnail.'+i]);
                    }

                    if(e.responseJSON['course_video_link.'+i]!=undefined){
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="course_video_link[]"]').addClass('is-invalid');
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="course_video_link[]"]').siblings().find('strong').html(e.responseJSON['course_video_link.'+i]);
                    }

                    if(e.responseJSON['video_title.'+i]!=undefined){
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="video_title[]"]').addClass('is-invalid');
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="video_title[]"]').siblings().find('strong').html(e.responseJSON['video_title.'+i]);
                    }

                    if(e.responseJSON['video_duration.'+i]!=undefined){
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="video_duration[]"]').addClass('is-invalid');
                        $('table.courses-videos-table > tbody > tr:eq('+i+') ').find('input[name="video_duration[]"]').siblings().find('strong').html(e.responseJSON['video_duration.'+i]);
                    }
                }

                return false;
            }
        })
    });

    $('select[name=select_topic_id]').change(function(){
        let data = decryptData($('select[name=select_topic_id] option:selected').attr('data'));
        $('select[name=select_topic_id]').val(null).trigger('change.select2');

        if($('table.topics-table > tbody > tr[key='+data.id+']').length > 0) return false;

        let k = $('table.topics-table > tbody > tr').length + 1;
        let tr = "<tr key='"+data.id+"'>"+
                    "<td><span class='topic-index'>"+k+"</span></td>"+
                    "<td><a href='/content/"+data.id+"' class='btn btn-link' target='_blank'>"+data.content_title+"</a></td>"+
                    "<td><span class='btn remove-topic'><i class='fa fa-times'></i></span><input type='hidden' class='content_id' name='content_id[]' value='"+data.id+"'></td>"+
                 "</tr>";
        $('table.topics-table > tbody').append(tr);
        $('.topic-err').html(null);
    });

    $('.add-detail').click(function(){
        let tr = $('table.courses-videos-table > tbody > tr:last').clone();
        $(tr).find('.remove-video').css('display','');
        $(tr).find('input').val(null);
        $(tr).find('input').removeClass('is-invalid');
        $(tr).find('input').siblings().find('strong').html(null);
        $(tr).find('.preview-row').css('display','none');
        $(tr).find('#upload-preview').attr('src','#');
        $(tr).find('#single-photo').css('display','block');
        $(tr).find('.tb-index').html($('table.courses-videos-table > tbody > tr').length + 1);

        $('table.courses-videos-table > tbody').append(tr);
    });

    $(document).on('click','.remove-video',function(){
       $(this).parent().closest('tr').remove();
       $('table.courses-videos-table > tbody > tr').each(function(i,e){
            $(e).find('.tb-index').html(i+1);
       });
    });

    $(document).on('click','.remove-topic',function(){
       $(this).parent().closest('tr').remove();
       $('table.topics-table > tbody > tr').each(function(i,e){
            $(e).find('.topic-index').html(i+1);
        });
    });

});
</script>
