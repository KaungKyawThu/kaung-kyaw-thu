@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<style>
    a.q-detail{
        color: black!important;
    }
</style>
<div class="card-body">
     @php
         $created_user_name = $course->created_user_name;
         $updated_user_name = $course->updated_user_name;
         $published_user_name = $course->published_user_name;
         $deactivated_user_name = $course->deactivated_user_name;
         
         $created_at = date('d-M-Y H:i:s',strtotime($course->created_at));
         $updated_at = $course->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($course->updated_at));
         $published_at = $course->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($course->published_at));
         $deactivated_at = $course->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($course->deactivated_at));

     @endphp

    <div class="row">
        <div class="col-md-3">
            <label for="">Course Level : </label><br>
            @php
                $l = $course->course->course_level;
            @endphp
            {{config("web_constant.courses.$l")}}
        </div>
        <div class="col-md-3">
            <label for="">Chapter English Name : </label><br>
            {{$course->chapter_name}}
        </div>
        <div class="col-md-3">
            <label for="">Chapter Myanmar Name : </label><br>
            {{$course->chapter_name_mm}}
        </div>
        <div class="col-md-3">
            <label for="">Status : </label><br>
            {{config("web_constant.status.$course->status")}}
        </div>
    </div><br>
    <h5>Videos</h5>
    <div class="row">
        @foreach ($course->videos as $k => $v)
            <div class="col-md-4 mb-3 mt-2">
                <p><b>{{$k+1}}. </b>{{$v->video_title}}</p>
                <p>
                    <a href="{{$v->course_video_link}}" target="_blank">
                        @if($v->video_thumbnail!=null) 
                          <img src="{{$v->video_thumbnail}}" alt="" width="300" height="auto">
                        @endif
                    </a>
                </p>
                <span>Duration : <b>{{$v->video_duration}}</b></span>
            </div>
        @endforeach
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <h5>Topics</h5>
            <table class="table data-table table-bordered">
                <thead>
                    <tr>
                        <th style="width : 50px;">No.</th>
                        <th>Topic Tile</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($course->topics as $k => $topic)
                        <tr>
                            <td>{{$k+1}}</td>
                            <td><a target="_blank" href="{{url('content/'.$topic->content_id)}}">{{$topic->content_title}}</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
       
    </div><br>
     @include('view-detail')
</div>
@stop
