@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" id="chapter-form" enctype="multipart/form-data">
        @csrf
        @include('course-chapter.form')
    </form>
</div>
@stop
