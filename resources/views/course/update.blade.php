@extends('adminlte::page')

@section('title', 'ASRH')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    <form method="POST" action="{{ url('course/'.$course->id) }}" id="course-form" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        @include('course.form')
    </form>
</div>
@stop
