@extends('adminlte::page')

@section('title', 'Htarwara')

@section('course_header')
    @include('content_header')
@stop

@section('content')
<style>
    a.q-detail{
        color: black!important;
    }
</style>
<div class="card-body">
     @php
         $created_user_name = $course->created_user_name;
         $updated_user_name = $course->updated_user_name;
         $published_user_name = $course->published_user_name;
         $deactivated_user_name = $course->deactivated_user_name;

         $created_at = date('d-M-Y H:i:s',strtotime($course->created_at));
         $updated_at = $course->updated_by==null ? '' : date('d-M-Y H:i:s',strtotime($course->updated_at));
         $published_at = $course->published_at==null ? '' : date('d-M-Y H:i:s',strtotime($course->published_at));
         $deactivated_at = $course->deactivated_at==null ? '' : date('d-M-Y H:i:s',strtotime($course->deactivated_at));

     @endphp
<ul class="nav nav-tabs sub-nav-tabs" id="myTab" role="tablist">
      <li class="nav-item" role="presentation">
        <a class="nav-link content-tab active" id="type-1-tab" data-toggle="tab" href="#type-1" role="tab" aria-controls="type-1" aria-selected="true">Course Detail</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link content-tab" id="type-2-tab" data-toggle="tab" href="#type-2" role="tab" aria-controls="type-2" aria-selected="true">Course Chapters</a>
      </li>
</ul>

<div class="tab-content sub-tab-content" id="myTabContent">
    <div class="tab-pane fade show active mt-3" id="type-1" role="tabpanel" aria-labelledby="type-1-tab">

        <div class="row">
            <div class="col-md-4">
                <label for="">Course Level</label>
                <p>{{config("web_constant.courses.$course->course_level")}}</p>
            </div>
            <div class="col-md-4">
                <label for="">Course English Name</label><br>
                <p>{{$course->course_name_en}}</p>
            </div>

            <div class="col-md-4">
                <label for="">Course Myanmar Name</label><br>
                <p>{{$course->course_name_mm}}</p>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-6">
                <label for="">Status : </label> <b>{{config("web_constant.status.$course->status")}}</b>
            </div>
            <div class="col-md-6 text-right">
                <label for="">Order : </label> <b>{{$course->order}}</b>
            </div>
        </div><br>

        <div class="row">
            <div class="col-md-4">
                <label for="">House Photo</label><br>
                <img src="{{$course->house_link}}" alt="" width="500" height="600">
            </div>
        </div><br>

        <div class="row">
            <div class="col-md-12">
                <label for="">Preview Description</label><br>
                <p>{!!$course->preview_text!!} </p>
            </div>
        </div><br>

        <div class="row">
            <div class="col-md-12">
                <label for="">Course English Description</label><br>
                <p>{!!$course->course_description!!}</p>
            </div>
        </div><br>

        <div class="row">
            <div class="col-md-12">
                <label for="">Course Myanmar Description</label><br>
                <p>{!!$course->course_description_mm!!}</p>
            </div>
        </div><br>

        {{-- @include('view-detail') --}}
    </div>
    @php
        $level = 'course_'.$course->course_level;
    @endphp
    <div class="tab-pane fade pt-3" id="type-2" role="tabpanel" aria-labelledby="type-2-tab">
        @include("course-chapter.partial.$level")
    </div>
   </div>
</div>
@stop
