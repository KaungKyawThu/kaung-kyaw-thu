@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    @include('search')
    <div class="col-md-3">
        {{-- <select class="form-control select2 course-filter-select2 course_status" name="quiz_type" style="width: 100%;">
            <option value=""></option>
            @foreach (config("web_constant.status") as $k=>$t)
                <option value="{{$k}}">{{$t}}</option>
            @endforeach
        </select> --}}
    </div>

    @include('new-btn')
    <div class="col-md-3">
      @can('course-overview')
        <a href="{{url('course/overview')}}" class="btn btn-primary float-right"><i class="fa fa-eye" aria-hidden="true"></i> Overview</a>
      @endcan
    </div>
  </div><br>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Course Name</th>
                <th>House Photo</th>
                <th>Course Order</th>
                {{-- <th>Created By</th>
                <th>Published By</th>
                <th>Published At</th> --}}
                <th class="status-col">Status</th>
                @if(auth()->user()->can('course-edit') || auth()->user()->can('course-view') || auth()->user()->can('course-delete') || auth()->user()->can('course-publish') || auth()->user()->can('course-active'))
                  <th class="action-col" ><span></span></th>
                @else
                  <th class="hide-col" ><span></span></th>
                @endif
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
   <script>
     $(document).ready(function () {
       var table = $('.data-table').DataTable({
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    aaSorting: [],
                    info: false,
                    autoWidth: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    displayStart : "{{$current_index}}",
                    //stateSave: true,
                    columnDefs: [
                   { orderable: true,  targets: 3 },
                   { orderable: false, targets: '_all' }
                    ],
                    ajax: "{{ url('course') }}",
                    createdRow: function( row, data, dataIndex ) {
                          if(data.action.length==0)
                            $(row).find('td.action-col').css('display', 'none');
                      },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', class : 'data-index'},
                        {data: 'course_name', name: 'course_name'},
                        {data: 'house', name: 'house'},
                        {data: 'order', name: 'order'},
                        // {data: 'created_user_name', name: 'created_user_name'},
                        // {data: 'published_user_name', name: 'published_user_name'},
                        // {data: 'published_at', name: 'published_at'},
                        {data: 'status', name: 'status' },
                        {data: 'action', name: 'action', orderable: false, searchable: false, className :'action-col'},
                    ]
                  });

       $('.list-search').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $('.list-search').on( 'search', function () {
            table.search( this.value ).draw();
        } );
        // $('.course-filter-select2').select2({ placeholder : "Filter By Status", allowClear : true});

        // $(document).on('change','.course-filter-select2',function(){
        //             searchFun();
        //     });

        // function searchFun(){
        // var course_status = $('.course_status').val();

        // table.ajax.url("{{ url('course')}}"+"?course_status="+course_status).load();
        // }

     });
   </script>
@stop
