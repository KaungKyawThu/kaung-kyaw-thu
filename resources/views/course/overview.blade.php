
@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')
<div class="card-body">
    @php
        $pending = \App\User::whereNotNull('course_level_id')
                        ->groupBy('course_level_id')
                        ->orderBy('course_level_id')
                        ->select(DB::raw("COUNT(id) as users"),'course_level_id')
                        ->pluck(
                            'users',
                            'course_level_id',
                        )
                        ->toArray();

        $passed = \App\UserCertificate::whereNotNull('course_level_id')
                                        ->groupBy('course_level_id')
                                        ->orderBy('course_level_id')
                                        ->select(DB::raw("COUNT(id) as users"),'course_level_id')
                                        ->pluck(
                                             'users',
                                             'course_level_id'

                                        )
                                        ->toArray();
       
        $courses = [];
        foreach (config("web_constant.courses") as $i => $c) {
            $course['course_name'] = $c;
            $course['pending'] = isset($pending[$i]) ? $pending[$i] : 0;
            $course['passed'] = isset($passed[$i]) ? $passed[$i] : 0;
            $course['total'] = $course['pending'] + $course['passed'];

            array_push($courses,$course);
        }

    @endphp
    <div id="columnchart_material" style="width: 100%; height: 500px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['bar']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Course', 'Total', 'Passed', 'Ongoing'],
  
       <?php
            foreach($courses as $row)
            {
                echo "['".$row["course_name"]."', ".$row['total'].",".$row['passed'].",".$row['pending']."],";
            }
        ?>
    ]);

    var options = {
      chart: {
        // title: 'Company Performance',
        // subtitle: 'Sales, Expenses, and Profit: 2014-2017',
      },
      colors: [
        "#007bff",
        "#28a745",
        "#eb4b33",
      ]
    };

    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
  }
</script>
@stop
