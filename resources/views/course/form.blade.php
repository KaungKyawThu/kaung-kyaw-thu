<input type="hidden" name="current_index" value="{{$course->current_index ?? 1}}">
<input type="hidden" name="course_id" value="{{$course->id}}">

<div class="row">
    <div class="col-md-6" style="display: block!important;" >
        <label for="" style="display: block;">{{__('Upload House Photo')}} <span class="text-red">* </span><span class="photo_upload_description">( {{$upload_photo_description}} )</span></label>

        <div class="row preview-row" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null ) style="" @else style="display:none" @endif>
            <div class="col-md-10">
                <img id="upload-preview" src="{{$course->house_link}}" alt="upload photo" style="width:100%"/>
            </div>
            <div class="col-md-1">
                {{-- <span class="remove-photo btn btn-link"><i class="fas fa-times "></i></span> --}}
            </div>
        </div>
        <div class="row">
            <input type="file" id="single-photo" class="form-control" name="house_link" placeholder="{{__('Upload Photo')}}" @if((isset($is_update) && count(old())==0) || old('virtual_img')!=null) style="display:none" @else style="" @endif>
            <input type="hidden" class=" @error('house_link') is-invalid @enderror" >
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>

            <input type="hidden" name="virtual_img" id="virtual-img" value="{{$course->house_link}}">
        </div>
    </div>
</div><br>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Course Level') }} <span class="text-red">*</span></label>
            <select name="course_level" class="form-control" >
                <option value="">Select Course Level</option>
                @foreach (config("web_constant.courses") as $i => $c)
                    <option value="{{$i}}" @if($course->course_level==$i) selected @endif>{{$c}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Order') }} <span class="text-red">*</span></label>
            <input type="text" class="form-control" name="order" value="{{$course->order}}" placeholder="Order" readonly>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Course English Name') }} <span class="text-red"> *</span></label>
            <input type="text" class="form-control" name="course_name_en" value="{{$course->course_name_en}}" placeholder="Course English Name">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Course Myanmar Name') }} <span class="text-red"> *</span></label>
            <input type="text" class="form-control" name="course_name_mm" value="{{$course->course_name_mm}}" placeholder="Course Myanmar Name">
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>{{ __('Preview Description') }} <span class="text-red">*</span></label>
            <textarea name="preview_text" class="form-control" rows="3" placeholder="Preview Description">{{$course->preview_text}}</textarea>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Course English Description') }} <span class="text-red">*</span></label>
            <textarea name="course_description" class="form-control" rows="7" placeholder="Course English Description">{{$course->course_description}}</textarea>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('Course Myanmar Description') }} <span class="text-red">*</span></label>
            <textarea name="course_description_mm" class="form-control" rows="7" placeholder="Course Myanmar Description">{{$course->course_description_mm}}</textarea>
            <span class="invalid-feedback" role="alert">
                <strong></strong>
            </span>
        </div>
    </div>
</div><br>

<div class="row">
    <div class="mr-3 ml-2">
        <a class="form-control hpy-btn btn btn-primary save-btn">
            {{ __('Save') }}
        </a>
    </div>
    <div class="mr-3">
        <a class="form-control btn btn-danger hpy-btn " href="{{url($route)}}">
            {{ __('Cancel') }}
        </a>
    </div>
</div><br>
<script>
$(document).ready(function(){
  $('select[name=course_level]').prop('disabled',true);
  $('select[name=course_level]').select2({
      placeholder : 'Select Course Level',
      allowClear : true
  });

  $('.save-btn').click(function(){
        var that = $(this);
        var formData = new FormData(document.getElementById("course-form"));
        var course_id = $('form#course-form').find('input[name=course_id]').val();
        if(course_id.length > 0){
            var route = '/course/'+course_id;
            formData.append('_method', 'PUT');
        }else if(course_id.length == 0){
            var route = '/course';
        }

        $.ajax({
            url : route,
            method : "POST",
            data : formData,
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
        
            success : function(data){
                window.location.href = '/course';
            },
           
            error: function(e){
                removeProcessing(that);
                if(e.responseJSON.course_level!=undefined){
                    $('form#course-form').find('select[name=course_level]').addClass('is-invalid');
                    $('form#course-form').find('select[name=course_level]').siblings().find('strong').html(e.responseJSON.course_level)
                }

                if(e.responseJSON.course_description!=undefined){
                    $('form#course-form').find('textarea[name=course_description]').addClass('is-invalid');
                    $('form#course-form').find('textarea[name=course_description]').siblings().find('strong').html(e.responseJSON.course_description)
                }

                if(e.responseJSON.course_description_mm!=undefined){
                    $('form#course-form').find('textarea[name=course_description_mm]').addClass('is-invalid');
                    $('form#course-form').find('textarea[name=course_description_mm]').siblings().find('strong').html(e.responseJSON.course_description_mm)
                }

                if(e.responseJSON.preview_text!=undefined){
                    $('form#course-form').find('textarea[name=preview_text]').addClass('is-invalid');
                    $('form#course-form').find('textarea[name=preview_text]').siblings().find('strong').html(e.responseJSON.preview_text)
                }
                
                if(e.responseJSON.house_link!=undefined){
                    $('form#course-form').find('input[name=house_link]').addClass('is-invalid');
                    $('form#course-form').find('input[name=house_link]').siblings().find('strong').html(e.responseJSON.house_link)
                }

                if(e.responseJSON.course_name_en!=undefined){
                    $('form#course-form').find('input[name=course_name_en]').addClass('is-invalid');
                    $('form#course-form').find('input[name=course_name_en]').siblings().find('strong').html(e.responseJSON.course_name_en)
                }

                if(e.responseJSON.course_name_mm!=undefined){
                    $('form#course-form').find('input[name=course_name_mm]').addClass('is-invalid');
                    $('form#course-form').find('input[name=course_name_mm]').siblings().find('strong').html(e.responseJSON.course_name_mm)
                }

                if(e.responseJSON.order!=undefined){
                    $('form#course-form').find('input[name=order]').addClass('is-invalid');
                    $('form#course-form').find('input[name=order]').siblings().find('strong').html(e.responseJSON.order)
                }

                return false;
            }
        })
    });
});
</script>
