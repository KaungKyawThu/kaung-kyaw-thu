<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        body { 
            font-family: 'padaukbook', sans-serif; 
            font-size:12px!important; 
            /* background-image : url("/img/watermark.png") */
        }
        
         @page{
            margin-bottom: 0px;            
         }

         body{
            background-image: url('/images/certificate/bg.png')

         }
         
    </style>
</head>
<body>
    @php
        $level_name = config("web_constant.courses.$level");
    @endphp
    <img src="{{public_path('images/certificate/'.$level_name.'/Header.png')}}" style="width : 800px; margin-left : -50px;margin-top : -45px;">
    
    <p align="center" @if($level==3) style="margin-top : 52px;font-size:22px;" @else style="margin-top : 55px;font-size:24px;" @endif ><b>{{$name}}</b></p>
    <img src="{{public_path('images/certificate/'.$level_name.'/Body.png')}}" style="width : 800px; margin-left : -50px; margin-top : -18px; position : relative;z-index : 1">

    <p align="center" @if($level) style="margin-top: 30px;font-size : 18px;position : relative;z-index : 1" @else style="margin-top: 33px;font-size : 18px;position : relative;z-index : 1" @endif><b>{{date('d-m-Y')}}</b></p>
    <img src="{{public_path('images/certificate/bg.png')}}" style="width : 400px; margin-left : -50px;margin-top : -250px;position : absolute;">

    <img src="{{public_path('images/certificate/'.$level_name.'/Footer.png')}}" style="width : 800px; margin-left : -50px; margin-top : -18px; position : relative;z-index : 1">

</body>
</html>