<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Created By</th>
                    <td>{{$created_user_name}}</td>
                    <th>Created At</th>
                    <td>{{$created_at}}</td>
                </tr>
                <tr>
                    <th>Update By</th>
                    <td>{{$updated_user_name}}</td>
                    <th>Update At</th>
                    <td>{{$updated_at}}</td>
                </tr>
                <tr>
                    <th>Published By</th>
                    <td>{{$published_user_name}}</td>
                    <th>Published At</th>
                    <td>{{$published_at}}</td>
                </tr>
                <tr>
                    <th>Activated By</th>
                    <td>{{$deactivated_user_name}}</td>
                    <th>Activated At</th>
                    <td>{{$deactivated_at}}</td>
                </tr>
            </tbody>
        </table>
    </div>
 </div><br>

 @include('back-btn')
