@extends('adminlte::page')

@section('title', 'Htarwara')

@section('content_header')
    @include('content_header')
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
     <nav>
        <div class="nav nav-tabs role-permission-tab" id="nav-tab" role="tablist">
            @foreach($roles as $role)
              @if($role->id==1 || $role->id==2 || $role->id==3)
                 <a class="nav-link @if($role->id==$role_id) active @endif" id="nav-{{$role->id}}-tab" data-toggle="tab" href="#nav-{{$role->id}}" role="tab" aria-controls="nav-{{$role->id}}" aria-selected="true">{{$role->name}}</a>
              @endif
            @endforeach
        </div>
      </nav>
      <div class="tab-content" id="nav-tabContent">
        @foreach($roles as $role)
            @php
                $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$role->id)
                                        ->leftJoin('permissions','permissions.id','role_has_permissions.permission_id')
                                        ->pluck('permissions.name')
                                        ->all();
            @endphp
            <div class="tab-pane fade @if($role->id==$role_id) show active @endif" id="nav-{{$role->id}}" role="tabpanel" aria-labelledby="nav-{{$role->id}}-tab">
                <div class="row" style="margin: 1% 0;">
                    <div class="col-md-2 RS_all">
                        <input type="checkbox" name="selectAll" id="selectAll">&nbsp; Select All
                    </div>
                </div>
                {!! Form::model($role, ['method' => 'POST','route' => ['permission.store']]) !!}
                    @csrf
                    @method('POST')
                    <input type="hidden" name="role_id" value="{{$role->id}}">
                    <div class="row" style="margin: 0;">
                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Role Permission</h6>
                            <span>{{ Form::checkbox('permission[]', 'permission-list', in_array('permission-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                  {{ "role-permission" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'permission-create', in_array('permission-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                  {{ "role-permission-update" }}
                            </span>
                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">General Setting</h6>
                            <span>{{ Form::checkbox('permission[]', 'setting-list', in_array('setting-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                  {{ "view-setting" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'setting-create', in_array('setting-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                  {{ "update-setting" }}
                            </span>
                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">User Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'user-list', in_array('user-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "user-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'user-create', in_array('user-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "user-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'user-edit', in_array('user-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "user-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'user-block', in_array('user-block', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "user-block/unblock" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'user-view', in_array('user-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "user-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'user-delete', in_array('user-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "user-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Video Management </h6>
                            <span>{{ Form::checkbox('permission[]', 'video-list', in_array('video-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "video-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'video-create', in_array('video-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "video-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'video-edit', in_array('video-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "video-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'video-publish', in_array('video-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "video-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'video-active', in_array('video-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "video-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'video-delete', in_array('video-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "video-delete" }}
                            </span>

                        </div>


                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Quiz Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'quiz-list', in_array('quiz-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-create', in_array('quiz-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-edit', in_array('quiz-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-publish', in_array('quiz-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-active', in_array('quiz-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-view', in_array('quiz-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-delete', in_array('quiz-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Weekly Quiz Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'weekly-quiz-list', in_array('weekly-quiz-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "weekly-quiz-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'weekly-quiz-create', in_array('weekly-quiz-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "weekly-quiz-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'weekly-quiz-edit', in_array('weekly-quiz-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "weekly-quiz-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'weekly-quiz-view', in_array('weekly-quiz-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "weekly-quiz-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'weekly-quiz-delete', in_array('weekly-quiz-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "weekly-quiz-delete" }}
                            </span>
                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Category Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'category-list', in_array('category-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "category-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'category-create', in_array('category-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "category-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'category-edit', in_array('category-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "category-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'category-publish', in_array('category-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "category-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'category-active', in_array('category-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "category-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'category-delete', in_array('category-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "category-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Content Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'content-list', in_array('content-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "content-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'content-create', in_array('content-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "content-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'content-edit', in_array('content-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "content-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'content-publish', in_array('content-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "content-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'content-active', in_array('content-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "content-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'content-view', in_array('content-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "content-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'content-reply', in_array('content-reply', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "reply-comment" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'comment-delete', in_array('comment-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "comment/reply-delete" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'content-delete', in_array('content-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "content-delete" }}
                            </span>
                        </div>

                         <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Keyword </h6>
                            <span>{{ Form::checkbox('permission[]', 'keyword-list', in_array('keyword-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "keyword-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'keyword-create', in_array('keyword-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "keyword-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'keyword-edit', in_array('keyword-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "keyword-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'keyword-publish', in_array('keyword-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "keyword-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'keyword-active', in_array('keyword-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "keyword-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'keyword-delete', in_array('keyword-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "keyword-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Clinic Service </h6>
                            <span>{{ Form::checkbox('permission[]', 'clinic-list', in_array('clinic-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-create', in_array('clinic-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-edit', in_array('clinic-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-publish', in_array('clinic-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-active', in_array('clinic-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-delete', in_array('clinic-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Clinic Organization </h6>
                            <span>{{ Form::checkbox('permission[]', 'organization-list', in_array('organization-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "organization-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'organization-create', in_array('organization-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "organization-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'organization-edit', in_array('organization-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "organization-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'organization-publish', in_array('organization-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "organization-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'organization-active', in_array('organization-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "organization-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'organization-delete', in_array('organization-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "organization-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">FAQ lists </h6>
                            <span>{{ Form::checkbox('permission[]', 'faq-list', in_array('faq-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "faq-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'faq-create', in_array('faq-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "faq-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'faq-edit', in_array('faq-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "faq-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'faq-publish', in_array('quiz-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "faq-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'faq-active', in_array('quiz-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "faq-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'faq-view', in_array('faq-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "faq-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'faq-delete', in_array('faq-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "faq-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Clinic Information </h6>
                            <span>{{ Form::checkbox('permission[]', 'clinic-info-list', in_array('clinic-info-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-info-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-info-create', in_array('clinic-info-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-info-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-info-edit', in_array('clinic-info-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-info-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-info-publish', in_array('clinic-info-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-info-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-info-active', in_array('clinic-info-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-info-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-info-view', in_array('clinic-info-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-info-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'clinic-info-delete', in_array('clinic-info-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "clinic-info-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Course Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'course-overview', in_array('course-overview', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-overview" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-list', in_array('course-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-list" }}
                            </span>
                            {{-- <span>{{ Form::checkbox('permission[]', 'course-create', in_array('course-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-create" }}
                            </span> --}}
                            <span>{{ Form::checkbox('permission[]', 'course-edit', in_array('course-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-edit" }}
                            </span>
                            {{-- <span>{{ Form::checkbox('permission[]', 'course-publish', in_array('course-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-active', in_array('course-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-active/deactive" }}
                            </span> --}}
                            <span>{{ Form::checkbox('permission[]', 'course-view', in_array('course-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-view" }}
                            </span>
                            {{-- <span>{{ Form::checkbox('permission[]', 'course-delete', in_array('course-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-delete" }}
                            </span> --}}
                        </div>
                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Podcast </h6>
                            <span>{{ Form::checkbox('permission[]', 'podcast-list', in_array('podcast-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-create', in_array('podcast-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-edit', in_array('podcast-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-publish', in_array('podcast-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-active', in_array('podcast-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-delete', in_array('podcast-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Course Chapter Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'course-chapter-list', in_array('course-chapter-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-chapter-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-chapter-create', in_array('course-chapter-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-chapter-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-chapter-edit', in_array('course-chapter-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-chapter-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-chapter-publish', in_array('course-chapter-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-chapter-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-chapter-active', in_array('course-chapter-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-chapter-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-chapter-view', in_array('course-chapter-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-chapter-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'course-chapter-delete', in_array('course-chapter-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "course-chapter-delete" }}
                            </span>

                        </div>
                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Course Chapter Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'new-event-list', in_array('new-event-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "new-event-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'new-event-create', in_array('new-event-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "new-event-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'new-event-edit', in_array('new-event-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "new-event-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'new-event-publish', in_array('new-event-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "new-event-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'new-event-active', in_array('new-event-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "new-event-active/deactive" }}
                            </span>

                            <span>{{ Form::checkbox('permission[]', 'new-event-delete', in_array('new-event-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "new-event-delete" }}
                            </span>

                        </div>

                        <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Quiz Set Management</h6>
                            <span>{{ Form::checkbox('permission[]', 'quiz-set-list', in_array('quiz-set-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-set-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-set-create', in_array('quiz-set-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-set-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-set-edit', in_array('quiz-set-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-set-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-set-publish', in_array('quiz-set-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-set-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-set-active', in_array('quiz-set-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-set-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-set-view', in_array('quiz-set-view', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-set-view" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'quiz-set-delete', in_array('quiz-set-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "quiz-set-delete" }}
                            </span>
                        </div>
                         <div class="col-md-2 RS_role">
                            <h6 > <input type="checkbox" id="select_module"  name="select_module">Podcast Serie</h6>
                            <span>{{ Form::checkbox('permission[]', 'podcast-serie-list', in_array('podcast-serie-list', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-serie-list" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-serie-create', in_array('podcast-serie-create', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-serie-create" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-serie-edit', in_array('podcast-serie-edit', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-serie-edit" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-serie-publish', in_array('podcast-serie-publish', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-serie-publish" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-serie-active', in_array('podcast-serie-active', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-serie-active/deactive" }}
                            </span>
                            <span>{{ Form::checkbox('permission[]', 'podcast-serie-delete', in_array('podcast-serie-delete', $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ "podcast-serie-delete" }}
                            </span>

                        </div>

                    </div><br>

                    @include('save-btn')
                </form>
          </div>
        @endforeach
        </div>
      </div>
   </div>

<script>
    $(document).ready(function(){
            load();

            checkPermission();

            $('input[name=selectAll]').click(function(){
                if($(this).is(":checked"))
                    $(this).parent().parent().parent().find('input[type=checkbox]').prop('checked',true);
                else  $(this).parent().parent().parent().find('input[type=checkbox]').prop('checked',false);
            });

            $(document).on('click','.cancal-btn',function(){
                location.href = '/';
            });

            $('input[type=checkbox]').click(function(){
                    if(!$(this).is(":checked")){
                        $('input[name=selectAll]').prop('checked',false);
                        $(this).parent().parent().find('h6').find('input[type=checkbox]').prop('checked',false);
                    }else{
                           if($(this).parent().parent().find('input:checkbox:not(:checked):not(#select_module)').length ==0)
                              $(this).parent().parent().find('h6').find('input[type=checkbox]').prop('checked',true);
                    }
            });

            $(document).on('click','#select_module',function(){
                if($(this).is(':checked')){
                    $(this).parent().parent().find('input[type=checkbox]').prop('checked',true);
                }else{
                    $(this).parent().parent().find('input[type=checkbox]').prop('checked',false);
                }

                checkPermission();
            });
        });

        function load(){
              $.each($('input[name=select_module]'),function(i,e){
                  if($(e).parent().parent().find('input:checkbox:not(:checked):not(#select_module)').length ==0){
                        $(e).prop('checked',true);
                  }
              });

              checkPermission();
        }

        function checkPermission(){
            if($("#nav-1").find('input[name=select_module]:not(:checked)').length==0)
               $("#nav-1").find('#selectAll').prop('checked',true);

            if($("#nav-2").find('input[name=select_module]:not(:checked)').length==0)
               $("#nav-2").find('#selectAll').prop('checked',true);

            if($("#nav-3").find('input[name=select_module]:not(:checked)').length==0)
               $("#nav-3").find('#selectAll').prop('checked',true);

            if($("#nav-4").find('input[name=select_module]:not(:checked)').length==0)
               $("#nav-4").find('#selectAll').prop('checked',true);

            if($("#nav-5").find('input[name=select_module]:not(:checked)').length==0)
               $("#nav-5").find('#selectAll').prop('checked',true);

            if($("#nav-6").find('input[name=select_module]:not(:checked)').length==0)
               $("#nav-6").find('#selectAll').prop('checked',true);

            if($("#nav-7").find('input[name=select_module]:not(:checked)').length==0)
               $("#nav-7").find('#selectAll').prop('checked',true);

        }
</script>
@stop
