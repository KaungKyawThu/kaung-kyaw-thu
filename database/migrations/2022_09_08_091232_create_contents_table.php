<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->string('content_title')->nullable();
            $table->string('content_cover_photo')->nullable();
            $table->integer('video_id')->nullable();
            $table->boolean('is_trending')->default(0);
            $table->boolean('is_new_comment')->default(0);
            $table->integer('author_id')->nullable();
            $table->string('read_time')->nullable();
            $table->integer('content_type')->nullable();
            $table->integer('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('published_by')->nullable();
            $table->integer('deactivated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->datetime('published_at')->nullable();
            $table->datetime('deactivated_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
