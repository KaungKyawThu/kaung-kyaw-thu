<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_infos', function (Blueprint $table) {
            $table->id();
            $table->string('clinic_name')->nullable();
            $table->integer('clinic_service_id')->nullable();
            $table->text('clinic_address');
            $table->integer('clinic_phone_no')->nullable();
            $table->string('clinic_website')->nullable();
            $table->integer('clinic_organization_id')->nullable();
            $table->string('clinic_fb_link')->nullable();
            $table->integer('township_id')->nullable();
            $table->integer('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('published_by')->nullable();
            $table->integer('deactivated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->datetime('published_at')->nullable();
            $table->datetime('deactivated_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_infos');
    }
}
