<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsInClinicInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_infos', function (Blueprint $table) {
            $table->string('facebook_page_id')->nullable()->after('id');
            $table->text('clinic_description_mm')->nullable()->after('facebook_page_id');
            $table->text('clinic_description_en')->nullable()->after('clinic_description_mm');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_infos', function (Blueprint $table) {
            $table->dropColumn('facebook_page_id');
            $table->dropColumn('clinic_description_mm');
            $table->dropColumn('clinic_description_en');
        });
    }
}
