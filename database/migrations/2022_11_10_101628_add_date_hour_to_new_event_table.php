<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateHourToNewEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_events', function (Blueprint $table) {
            $table->date('schedule_date')->nullable()->after('description');
            $table->integer('hours')->nullable()->after('schedule_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_events', function (Blueprint $table) {
            $table->dropColumn('schedule_date');
            $table->dropColumn('hours');
        });
    }
}
