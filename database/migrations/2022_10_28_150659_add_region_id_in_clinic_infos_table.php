<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRegionIdInClinicInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_infos', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
            $table->dropColumn('city_name');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_infos', function (Blueprint $table) {
            $table->dropColumn('region_id');
            $table->integer('city_name')->nullable();
        });
    }
}
