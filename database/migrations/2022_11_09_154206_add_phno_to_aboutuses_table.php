<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhnoToAboutusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aboutuses', function (Blueprint $table) {
            $table->string('facebook_page_id')->nullable()->after('facebook_link');
            $table->string('female_phone_no')->nullable()->after('facebook_page_id');
            $table->string('female_mm_phone_no')->nullable()->after('female_phone_no');
            $table->string('male_mm_phone_no')->nullable()->after('phone_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aboutuses', function (Blueprint $table) {
            $table->dropColumn('facebook_page_id');
            $table->dropColumn('female_phone_no');
            $table->dropColumn('female_mm_phone_no');
            $table->dropColumn('male_mm_phone_no');
        });
    }
}
