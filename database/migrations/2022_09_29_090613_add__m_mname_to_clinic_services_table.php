<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMMnameToClinicServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_services', function (Blueprint $table) {
            $table->string('service_mm_name')->nullable()->after('service_name');
            $table->text('service_mm_description')->nullable()->after('service_description');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_services', function (Blueprint $table) {
            $table->dropColumn('service_mm_name');
            $table->dropColumn('service_mm_description');
        });
    }
}
