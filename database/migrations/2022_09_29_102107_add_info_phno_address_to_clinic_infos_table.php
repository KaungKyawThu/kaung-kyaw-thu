<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfoPhnoAddressToClinicInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_infos', function (Blueprint $table) {
            $table->string('clinic_mm_name')->nullable()->after('clinic_name');
            $table->text('clinic_mm_address')->after('clinic_address');
            $table->string('clinic_mm_phone_no')->nullable()->after('clinic_phone_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_infos', function (Blueprint $table) {
            $table->dropColumn('clinic_mm_name');
            $table->dropColumn('clinic_mm_address');
            $table->dropColumn('clinic_mm_phone_no');
        });
    }
}
