<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('role_id')->nullable();
            $table->string('phone_no',100)->nullable();
            $table->string('gender',100)->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('township_id')->nullable();
            $table->integer('birth_year')->nullable();
            $table->string('avator_name',100)->nullable();
            $table->string('user_no',100)->nullable();
            $table->string('avator_img_link',100)->nullable();
            $table->integer('user_point')->nullable();
            $table->integer('unread_noti_count')->nullable();
            $table->string('fcm_token')->nullable();
            $table->boolean('is_new_msg')->nullable();
            $table->integer('course_level')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role_id');
            $table->dropColumn('phone_no');
        });
    }
}
