<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('gender', 'gender_id');
            $table->renameColumn('avator_name', 'nick_name');
            $table->renameColumn('avator_img_link', 'avator_img_id');
            $table->renameColumn('course_level', 'course_level_id');
            $table->string('certificate_name')->nullable()->after('phone_no');
            $table->string('certificate_phone_no')->nullable()->after('certificate_name');

            $table->integer('gender')->change();
            $table->integer('course_level')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('gender_id', 'gender');
            $table->renameColumn('nick_name', 'avator_name');
            $table->renameColumn('avator_img_id', 'avator_img_link');
            $table->renameColumn('course_level_id', 'course_level');
            $table->dropColumn('certificate_name');
            $table->dropColumn('certificate_phone_no');

        });
    }
}
