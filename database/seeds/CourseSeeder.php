<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            [
                'course_level' => 1,
                'order' => 1,
                'course_name_en' => 'Basic course',
                'course_name_mm' => " “လင်းလက်လူငယ်၊ အနာဂါတ်ကြယ်” အခြေခံသင်တန်း",
                'preview_text' =>"ဆယ်ကျော်သက်နဲ့ လူငယ်တွေအတွက် အခြေခံကျကျ သိထားရမယ့် လိင်မှုရေးရာ (sexuality education) တွေနဲ့ ဘဝသင်ခန်းစာတွေကို စိတ်ဝင်စားဖွယ်တင်ပြပေးမယ့် သင်တန်းဖြစ်ပြီး Certificate of Completion လည်း ရနိုင်ပါတယ်၊",
                'course_description' => "You will learn basic sexual and reproductive health information, basic gender concept and life skill information after spending 10 hours consecutively or intermittently.",
                'course_description_mm' => "တစ်ဆက်တည်းဖြစ်စေ၊ အားလပ်ချိန်ပေါ်မူတည်၍ဖြစ်စေ စုစုပေါင်း ၁၀ နာရီခန့် အသုံးပြုပြီး အခြေခံလိင်မှုရေးရာအသိပညာများ၊ \nမျိုးဆက်ပွါးကျန်းမာရေးဗဟုသုတများ နှင့် ကျားမလူမှုကွဲပြားမှုဆိုင်ရာ အကြောင်းအရာများကို သိရှိသွားပါမည်။",
                'house_link' => 'https://d3ah0vm3g66mbj.cloudfront.net/images/J25LmDLg7BQL8OJzEv6l56cAeFNIXRSAkYJYV2vZ.jpg',
                'status' => 2
            ],
            [
                'course_level' => 2,
                'order' => 2,
                'course_name_en' => 'Intermediate Course',
                'course_name_mm' => " “လင်းလက်လူငယ်၊ အနာဂါတ်ကြယ်” ပေါင်းကူးသင်တန်း",
                'preview_text' =>"ဘဝမှာလိုအပ်တဲ့ လူမှုရေးရာ (Civic education) ကို သင်ယူနားလည်ပြီး ဘဝအောင်မြင်ရေးအတွက် အသုံးချနိုင်တဲ့ အတွေးအမြင်တွေရဖို့ ပေါင်းကူးသင်တန်းဖြစ်ပါတယ်၊ Certificate of Completion နောက်တစ်ခု ယူနိုင်ပါသေးတယ်၊",
                'course_description' => "You will learn additional sexual and reproductive health information, recent gender concepts and other information for your happy and healthy adulthood after spending 3 hours consecutively or intermittently.",
                'course_description_mm' => "တစ်ဆက်တည်းဖြစ်စေ၊ အားလပ်ချိန်ပေါ်မူတည်၍ဖြစ်စေ စုစုပေါင်း ၃ နာရီခန့် အသုံးပြုပြီး လိင်မှုရေးရာပြဿနာများနှင့် ပတ်သက်သောအသိပညာများအပြင် ကျန်းမာပျော်ရွှင်သော လူကြီးဘဝတည်ဆောက်ရန် မျိုးဆက်ပွါးကျန်းမာရေးဗဟုသုတများ နှင့် ခေတ်မီကျားမလူမှုကွဲပြားမှုဆိုင်ရာ အကြောင်းအရာများကို သိရှိသွားပါမည်။",
                'house_link' => 'https://d3ah0vm3g66mbj.cloudfront.net/images/J25LmDLg7BQL8OJzEv6l56cAeFNIXRSAkYJYV2vZ.jpg',
                'status' => 2
            ],

            [
                'course_level' => 3,
                'order' => 3,
                'course_name_en' => 'Advanced Course',
                'course_name_mm' => " “လင်းလက်လူငယ်၊ အနာဂါတ်ကြယ်” အဆင့်မြင့်သင်တန်း",
                'preview_text' =>" လူ့ဘဝမှာ ဉာဏ်ရည်ဖွံ့ဖြိုးရုံသာမက အခြားသော ကိုယ်ရည်ကိုယ်သွေးတွေ မြင့်မားဖို့ အထောက်အကူ ပြုပေးနိုင်မယ့် အဆင့်မြင့်သင်တန်းဖြစ်ပါတယ်၊ တတိယမြောက် Certificate of Completion ပေါ့၊",
                'course_description' => "You will learn additional sexual and reproductive health information, recent gender concepts and other information for your happy and healthy adulthood after spending 3 hours consecutively or intermittently.",
                'course_description_mm' => "တစ်ဆက်တည်းဖြစ်စေ၊ အားလပ်ချိန်ပေါ်မူတည်၍ဖြစ်စေ စုစုပေါင်း ၂ နာရီခန့် အသုံးပြုပြီး လိင်မှုရေးရာသိမှတ်စရာများ၊ ကျင့်ဝတ်ပိုင်းဆိုင်ရာ ဗဟုသုတများနှင့် ဘဝဖွံ့ဖြိုးတိုးတက်ရေးဆိုင်ရာ အသိပညာများကို သိရှိသွားပါမည်။ ",
                'house_link' => 'https://d3ah0vm3g66mbj.cloudfront.net/images/J25LmDLg7BQL8OJzEv6l56cAeFNIXRSAkYJYV2vZ.jpg',
                'status' => 2
            ],

            [
                'course_level' => 4,
                'order' => 4,
                'course_name_en' => 'Trainer Course',
                'course_name_mm' => " “လင်းလက်လူငယ်၊ အနာဂါတ်ကြယ်” နည်းပြသင်တန်း ",
                'preview_text' =>"ကိုယ်တိုင်သိထားတာတွေကို ပတ်ဝန်းကျင်မှာ မျှဝေချင်တဲ့ လူငယ်တွေအတွက် အကျိုးပြုလုပ်ငန်း အနေနဲ့ လုပ်ကိုင်နိုင်ဖို့ လိုအပ်တဲ့ အရည်အသွေးတွေ ပြောပြပေးမယ့်သင်တန်းဖြစ်ပါတယ်၊ Certificate of Completion ရပြီဆိုရင် သိမ်းထားလိုက်ပါ၊",
                'course_description' => "You will learn basic communication concepts, skills for educator or trainer and how to build confidence after spending 6 hours consecutively or intermittently.",
                'course_description_mm' => "တစ်ဆက်တည်းဖြစ်စေ၊ အားလပ်ချိန်ပေါ်မူတည်၍ဖြစ်စေ စုစုပေါင်း ၆ နာရီခန့် အသုံးပြုပြီး ပညာပေးသူ သို့မဟုတ် နည်းပြတစ်ယောက်ဖြစ်ရန် လိုအပ်သော ဆက်သွယ်ပညာပေးခြင်းဆိုင်ရာ အခြေခံသဘောတရားများနှင့် ကျွမ်းကျင်မှုအသိပညာများကို သိရှိသွားပါမည်။",
                'house_link' => 'https://d3ah0vm3g66mbj.cloudfront.net/images/J25LmDLg7BQL8OJzEv6l56cAeFNIXRSAkYJYV2vZ.jpg',
                'status' => 2
            ],

        ]);
    }
}
