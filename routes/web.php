<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::group(['middleware'=>['auth','XSS_texteditor']], function() {

    Route::resource('content','ContentController');

    Route::resource('setting','SettingController');
});

Route::group(['middleware' => ['auth','XSS']], function() {
    Route::get('/home', function() {
        return view('home');
    })->name('home');

    Route::get('/', function() {
        return view('home');
    });

    Route::get('region/{id}/township','ClinicInfoController@getTownships');

    Route::get('export', 'UserController@export')->name('export');

    Route::get('profile','UserController@profile');
    Route::put('user/{id}/block','UserController@block');
    Route::resource('user','UserController');
    Route::resource('permission','PermissionController');

    Route::put('quiz/{id}/active','QuizController@active');
    Route::put('quiz/{id}/publish','QuizController@publish');
    Route::resource('quiz','QuizController');
    Route::resource('weeklyquiz','WeeklyquizController');

    Route::resource('video','VideoController');
    Route::put('video/{id}/publish','VideoController@publish');
    Route::put('video/{id}/active','VideoController@active');

    Route::get('category/topic','CategoryController@topics');

    Route::resource('category','CategoryController');
    Route::put('category/{id}/publish','CategoryController@publish');
    Route::put('category/{id}/active','CategoryController@active');

    Route::post('content/upload','ContentController@upload')->name('content.upload');
    Route::get('to-reply','ContentController@toReply');
    Route::delete('delete-comment','ContentController@deleteComment');
    Route::get('content/{id}/comment','ContentController@comment');
    Route::post('comment/{id}/reply','ContentController@reply');
    Route::put('content/{id}/active','ContentController@active');
    Route::put('content/{id}/publish','ContentController@publish');

    Route::resource('keyword','KeywordController');
    Route::put('keyword/{id}/publish','KeywordController@publish');
    Route::put('keyword/{id}/active','KeywordController@active');

    Route::resource('clinic','ClinicServiceController');
    Route::put('clinic/{id}/publish','ClinicServiceController@publish');
    Route::put('clinic/{id}/active','ClinicServiceController@active');

    Route::resource('podcast','PodcastController');
    Route::put('podcast/{id}/publish','PodcastController@publish');
    Route::put('podcast/{id}/active','PodcastController@active');

    Route::resource('podcast-serie','PodcastSerieController');
    Route::put('podcast-serie/{id}/publish','PodcastSerieController@publish');
    Route::put('podcast-serie/{id}/active','PodcastSerieController@active');

    Route::resource('organization','ClinicOrganizationController');
    Route::put('organization/{id}/publish','ClinicOrganizationController@publish');
    Route::put('organization/{id}/active','ClinicOrganizationController@active');



    Route::resource('clinic-info','ClinicInfoController');
    Route::put('clinic-info/{id}/publish','ClinicInfoController@publish');
    Route::put('clinic-info/{id}/active','ClinicInfoController@active');



    Route::resource('faq','FaqController');
    Route::put('faq/{id}/publish','FaqController@publish');
    Route::put('faq/{id}/active','FaqController@active');

    Route::get('course/overview','CourseController@overview');
    Route::put('course/{id}/active','CourseController@active');
    Route::put('course/{id}/publish','CourseController@publish');
    Route::resource('course','CourseController');

    Route::put('course-chapter/{id}/active','CourseChapterController@active');
    Route::put('course-chapter/{id}/publish','CourseChapterController@publish');
    Route::resource('course-chapter','CourseChapterController');

    Route::put('quiz-set/{id}/active','QuizSetController@active');
    Route::put('quiz-set/{id}/publish','QuizSetController@publish');
    Route::resource('quiz-set','QuizSetController');
    Route::get('course/{course}/quiz_type/{quiz_type}/quiz','QuizSetController@getQuiz');

    Route::put('faq/{id}/publish','FaqController@publish');
    Route::put('faq/{id}/active','FaqController@active');

    Route::resource('chat','ChatController');

    Route::get('user/{id}/read_chat','UserController@read_chat');
    Route::get('new-chat-count','ChatController@getNewCount');
    Route::resource('new-event','NewEventController');

    Route::get('certificate','CourseController@certificate');

});




