<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Auth
Route::post('/auth/register', 'Api\AuthController@register'); 
Route::post('/auth/login', 'Api\AuthController@login');
Route::get('/auth/security-ques', 'Api\AuthController@getSecurityQuestions');
Route::get('auth/facts', 'Api\AuthController@getFacts');
Route::post('/auth/check-ph-no', 'Api\AuthController@checkPhoneNoExist');
Route::post('/auth/forget', 'Api\AuthController@forgetPassword');

//policy
Route::get('/policy', 'Api\ExploreController@getPolicy');

Route::middleware('auth:api')->group(function () {
    //User
    Route::post('/auth/users/{user_id}/bind-account', 'Api\AuthController@binding_account');
    Route::get('/users/{user_id}', 'Api\UserController@getProfile');
    Route::put('/users/{user_id}/nick-name', 'Api\UserController@updateNickName');
    Route::put('/users/{user_id}/avator', 'Api\UserController@updateAvator');
    Route::get('/users/{user_id}/certificates', 'Api\UserController@getUserCerificate');
    Route::delete('/users/{user_id}', 'Api\AuthController@deleteUserAccount');

    //NewsEvent
    Route::get('/newsevent', 'Api\NewsEventsController@getNewsEventsList');
    Route::get('/newsevent/{id}', 'Api\NewsEventsController@getNewsEventDetail');

    //Notification
    Route::get('/users/{user_id}/notifications', 'Api\NotificationController@getNotification');
    Route::get('/users/{user_id}/unread-noti-count', 'Api\NotificationController@getNotiUnreadCount');

    //Course
    Route::get('/users/{user_id}/courses', 'Api\CourseController@getCourseList');
    Route::get('/users/{user_id}/courses/{course_id}', 'Api\CourseController@getCourseDetail');
    Route::get('/users/{user_id}/courses/{course_id}/quizzes', 'Api\CourseController@getCourseQuiz');
    Route::post('/users/{user_id}/courses/{course_id}/complete', 'Api\CourseController@completeCourse');
    Route::get('/users/{user_id}/certificate-data', 'Api\CourseController@getCertificateUserInfo');
    Route::post('/users/{user_id}/courses/{course_id}/certificate', 'Api\CourseController@generateCertificate');
    Route::get('/users/{user_id}/courses/{course_id}/house-link', 'Api\CourseController@getCourseHouseLink');
     
    //Explore
    Route::get('/categories', 'Api\ExploreController@getCategories'); // trending categories
    Route::get('/all-categories', 'Api\ExploreController@allCategories'); // all categories
    Route::get('/latest-videos&podcasts', 'Api\ExploreController@getLatestVideosAndPodcasts');
    Route::get('/popular-topics', 'Api\ExploreController@getPopularTopics');
    Route::get('/videos', 'Api\ExploreController@getVideos');
    Route::get('/podcasts', 'Api\ExploreController@getPodcasts');
    Route::get('/podcasts-series-detail/{series_id}', 'Api\ExploreController@getPodcastSeriesDetail');
    Route::get('/trending-contents', 'Api\ExploreController@getTrendingContents');
    Route::get('/all-contents', 'Api\ExploreController@getAllContents');
    Route::get('/contents/{category_id}', 'Api\ExploreController@getContentsByCategory');
    Route::get('/topic-detail/{topic_id}', 'Api\ExploreController@getTopicDetail');
    Route::get('/topic/{topic_id}/faqs', 'Api\ExploreController@getFaqsByTopic');
    Route::get('/article-detail/{article_id}', 'Api\ExploreController@getArticleDetail');
    Route::get('/keywords', 'Api\ExploreController@getKeywords');
    Route::get('/search', 'Api\ExploreController@search');
    Route::get('/searchByKeywords/{keyword_id}', 'Api\ExploreController@searchByKeywords');
    Route::get('/searchByKeywords/{keyword_id}/explore-type/{explore_type_id}', 'Api\ExploreController@searchByExploreType');
    Route::post('/users/{user_id}/bookmark', 'Api\ExploreController@bookmark');
    Route::post('/users/{user_id}/comment-article/{article_id}', 'Api\ExploreController@commentArticle');
    Route::get('/users/{user_id}/getComments/{article_id}', 'Api\ExploreController@getComments');
    Route::get('/users/{user_id}/article/{article_id}/getReplies/{comment_id}', 'Api\ExploreController@getRepliesByComment');
    Route::post('/users/{user_id}/replyComment/{article_id}', 'Api\ExploreController@replyCommentArticle');
    Route::get('/bookmark-lists', 'Api\ExploreController@getBookmarkLists');

    //quizz
    Route::get('/users/{user_id}/topic-quizz/{topic_id}', 'Api\QuizController@getQuizByTopic');
    Route::get('/check-point', 'Api\QuizController@checkPoint');
    Route::get('/use-point', 'Api\QuizController@usePoint');
    Route::post('/users/{user_id}/quiz-answer', 'Api\QuizController@updateQuizAnswer');

    //weekly quizz
    Route::get('/users/{user_id}/weekly-quizz', 'Api\QuizController@getWeeklyQuiz');

    //service
    Route::get('/faqs', 'Api\ServiceController@getFaqs');
    Route::get('/services', 'Api\ServiceController@getServices');
    Route::get('/service-types', 'Api\ServiceController@getServiceTypes');
    Route::get('/service-townships', 'Api\ServiceController@getServiceTownships');
    Route::get('/contact-us', 'Api\ServiceController@contactUs');
    Route::post('/users/{user_id}/send-message', 'Api\ServiceController@sendMessage');
    Route::get('/users/{user_id}/all-messages', 'Api\ServiceController@getAllMessages');
    //chat noti
    Route::get('/users/{user_id}/chat-noti', 'Api\ServiceController@getChatNoti');

    //Game
    Route::get('/users/{user_id}/game-quiz', 'Api\QuizController@game');
    Route::post('/users/{user_id}/game-result', 'Api\QuizController@updateGameResult');

    //Version
    Route::get('/users/{user_id}/version', 'Api\AppversionController@getCurrentVersion');

    //Leader Board
    Route::get('/leaderBoard', 'Api\QuizController@getLeaderBoardList');

    //offline API
    Route::get('/faq-offline', 'Api\OfflineController@getOfflineFaq');
    Route::get('/topic-offline', 'Api\OfflineController@getOfflineTopic');
    Route::get('/service-type-offline', 'Api\OfflineController@getOfflineServiceType');
    Route::get('/service-townships-offline', 'Api\OfflineController@getOfflineServiceTownships');
    Route::get('/services-offline', 'Api\OfflineController@getOfflineServices');

    Route::get('/regions', 'Api\ServiceController@getRegions');
    Route::get('/townships-by-region', 'Api\ServiceController@townshipsByRegions');

});
